package com.segment.pazwash;

import java.util.ArrayList;
import java.util.List;

import com.segment.pazwash.BL.GlobalApp;
import com.segment.pazwash.BL.NetworkChk;
import com.segment.pazwash.BL.PazWashBaseAdapter;
import com.segment.pazwash.DM.CancelResult;
import com.segment.pazwash.DM.DataParameter;
import com.segment.pazwash.DM.DeleteItem;
import com.segment.pazwash.Enums.ClientExceptionCodeResult;
import com.segment.pazwash.WS.WS;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Typeface;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import android.widget.ListView;

public class RejectActivity extends BaseActivity {

	List<DeleteItem> BidsList = new ArrayList<DeleteItem>();
	PazWashBaseAdapter adapter;
	ListView lstView;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		ActivityType = 1;

		requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.reject);

		loadProgressDialog();

		showProgress();

		userName = getUserName();

		if (NetworkChk.checkInternet(RejectActivity.this, GlobalApp.getIsVPN())) {
			new getLast10Washes(userName).execute(PAZWASH_WS_URL
					+ "/getLast10Washes");
		} else {

			hideProgress();
			showAlertDialog(R.string.Connection, R.string.NoConnection, null);
		}

	}

	private OnClickListener rejectBtnListener = new OnClickListener() {
		public void onClick(View v) {

			Dialog dialog;
			AlertDialog.Builder builder;
			builder = new AlertDialog.Builder(RejectActivity.this);
			builder.setMessage(R.string.DeleteDialog)
					.setCancelable(false)
					.setNegativeButton(R.string.No, null)
					.setPositiveButton(R.string.YesC,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {

									try {

										int i = lstView
												.getCheckedItemPosition();
										Object o = lstView.getItemAtPosition(i);
										DeleteItem fullObject = (DeleteItem) o;

										if (NetworkChk.checkInternet(
												RejectActivity.this,
												GlobalApp.getIsVPN())) {
											new cancelDeal(fullObject.WashId(),
													fullObject.CarNumber())
													.execute(PAZWASH_WS_URL
															+ "/cancelWash");
										} else {

											hideProgress();
											showAlertDialog(
													R.string.Connection,
													R.string.NoConnection, null);
										}

									} catch (Exception e) {
										GlobalApp.WriteLogException(e);
										// WriteLogException(e);
										return;
									}

								}
							});
			dialog = builder.create();
			dialog.show();

		}

	};

	public class getLast10Washes extends
			AsyncTask<String, Void, ArrayList<DeleteItem>> {

		String userName;

		getLast10Washes(String user) {
			this.userName = user;
		}

		protected ArrayList<DeleteItem> doInBackground(String... urls) {

			try {
				WS ws = new WS();

				List<DataParameter> parameters = BuildParameters();
				return ws.getListOfDeleteItems(parameters, urls[0]);

			}

			catch (Exception e) {
				GlobalApp.WriteLogException(e);
				// WriteLogException(e);
				return null;
			}
			// return ws.getPazTicket(urls[0], image_to_string,
			// gps_data[1],gps_data[0], userName,
			// ticketStatus.toString());

		}

		private List<DataParameter> BuildParameters() {
			List<DataParameter> parameters = new ArrayList<DataParameter>();
			parameters.add(new DataParameter("userName", this.userName));
			parameters.add(new DataParameter("deviceId", GlobalApp
					.getDeviceId()));
			parameters.add(new DataParameter("mdCode", GlobalApp.getMDCode()));
			parameters.add(new DataParameter("stationId", GlobalApp
					.getStationID()));
			return parameters;
		}

		protected void onPostExecute(ArrayList<DeleteItem> result) {

			/*
			 * for (int temp = 0; temp < result.size(); temp++) {
			 * 
			 * DeleteItem di = result.get(temp);
			 * di.setDealTitle(getResources().getString(R.string.DealNumer));
			 * di.setCarTitle(getResources().getString(R.string.CarNumber));
			 * di.setDateTitle(getResources().getString(R.string.Data));
			 * BidsList.add(di); }
			 */

			BidsList = result;
			Typeface hebBold = Typeface.createFromAsset(getAssets(), "fonts/droidsanshebrew-bold.ttf");
			adapter = new PazWashBaseAdapter(RejectActivity.this, BidsList,hebBold);
			// adapter = new ArrayAdapter<DeleteItem>(RejectActivity.this,
			// R.drawable.reject_item, BidsList);
			lstView = (ListView) findViewById(R.id.listDelete);
			lstView.setBackgroundResource(R.drawable.listview);
			lstView.setChoiceMode(1);
			lstView.setAdapter(adapter);
			lstView.setCacheColorHint(0);
			lstView.setItemsCanFocus(false);

			lstView.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> a, View v, int position,
						long id) {

					final int p = position;
					Dialog dialog;
					AlertDialog.Builder builder;
					builder = new AlertDialog.Builder(RejectActivity.this);
					builder.setMessage(R.string.DeleteDialog)
							.setCancelable(false)
							.setNegativeButton(R.string.No, null)
							.setPositiveButton(R.string.YesC,
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog, int id) {

											try {

												Object o = lstView
														.getItemAtPosition(p);
												DeleteItem fullObject = (DeleteItem) o;

												showProgress();
												if (NetworkChk.checkInternet(
														RejectActivity.this,
														GlobalApp.getIsVPN())) {
													new cancelDeal(
															fullObject.WashId(),
															fullObject
																	.CarNumber())
															.execute(PAZWASH_WS_URL
																	+ "/cancelWash");
												} else {

													hideProgress();
													showAlertDialog(
															R.string.Connection,
															R.string.NoConnection,
															null);
												}

											} catch (Exception e) {
												GlobalApp.WriteLogException(e);
												// WriteLogException(e);
												return;
											}

										}
									});
					dialog = builder.create();
					dialog.show();

					// Toast.makeText(RejectActivity.this, "You have chosen: " +
					// " " + fullObject.CarNumber(), Toast.LENGTH_LONG).show();
				}
			});

			hideProgress();

		}
	}

	public class cancelDeal extends AsyncTask<String, Void, CancelResult> {
		String washId;
		String carNumber;

		cancelDeal(String id, String number) {
			this.washId = id;
			this.carNumber = number;
		}

		protected CancelResult doInBackground(String... urls) {

			try {
				WS ws = new WS();

				List<DataParameter> parameters = BuildParameters();

				return ws.cancelDealHTTP(parameters, urls[0]);
			}

			catch (Exception e) {
				GlobalApp.WriteLogException(e);
				// WriteLogException(e);
				CancelResult cr = new CancelResult();
				cr.setIsCanceled(false);
				return null;
			}
			// return ws.getPazTicket(urls[0], image_to_string,
			// gps_data[1],gps_data[0], userName,
			// ticketStatus.toString());

		}

		private List<DataParameter> BuildParameters() {
			List<DataParameter> parameters = new ArrayList<DataParameter>();
			parameters.add(new DataParameter("washId", this.washId));
			parameters.add(new DataParameter("carNumber", this.carNumber));
			parameters.add(new DataParameter("userName", GlobalApp
					.getUserName()));
			parameters.add(new DataParameter("deviceId", GlobalApp
					.getDeviceId()));
			parameters.add(new DataParameter("mdCode", GlobalApp.getMDCode()));
			parameters.add(new DataParameter("stationNumber", GlobalApp
					.getStationNumber()));
			parameters.add(new DataParameter("stationId", GlobalApp
					.getStationID()));
			parameters.add(new DataParameter("sysVer", GlobalApp
					.getSysVersion()));
			parameters.add(new DataParameter("mdType", GlobalApp.getMDType()));
			parameters.add(new DataParameter("pazWashAppVer", GlobalApp
					.getVersionApp()));
			return parameters;
		}

		protected void onPostExecute(CancelResult result) {

			if (result.getClientClientExcCode().equals(
					ClientExceptionCodeResult.RESULT_OK.toString())) {

				if (result.IsCanceled() == true) {
					int i = lstView.getCheckedItemPosition();

					BidsList.remove(i);
					adapter.notifyDataSetChanged();

				} else {
					if (result.getBBResult().getBBResultCode() == -100) {

						showAlertDialog(R.string.MenuCancelDealsTitle,
								R.string.Exception, null);
					}
					else
					{

						showAlertDialog(R.string.MenuCancelDealsTitle, result
								.getBBResult().getBBResultDesc(), null);
					}
				}
				hideProgress();
			}

			else {
				if (result.getClientClientExcCode().equals(
						ClientExceptionCodeResult.RESULT_ERROR.toString())) {
					hideProgress();
					showAlertDialog(R.string.Exception,
							R.string.ServerException, null);

				}
				if (result.getClientClientExcCode().equals(
						ClientExceptionCodeResult.RESULT_TIMEOUT.toString())) {
					hideProgress();
					showAlertDialog(R.string.Exception,
							R.string.TimeOutException, null);
				}
			}
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		System.gc();
	}

}
