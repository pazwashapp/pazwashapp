package com.segment.pazwash;


import com.segment.pazwash.BL.GlobalApp;
import com.segment.pazwash.BL.Logger;
import com.segment.pazwash.BL.NetworkChk;

import java.util.Timer;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.util.Log;

import java.util.TimerTask;

public class SendLogsService extends Service {
	
	public static void writeToLogcat(String msg){
		if (GlobalApp.writeToLogcat)
			Log.i("SendLogsService", msg);
	}

	
	static final int UPDATE_INTERVAL = 10;
	private Timer timer = new Timer();

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		SendOfflineRepeatedly();
		return START_STICKY;
	}

	private void SendOfflineRepeatedly() {
		timer.scheduleAtFixedRate(new TimerTask() {
													public void run() {
														writeToLogcat("SendLogsTimer");
														if (NetworkChk.checkInternet(SendLogsService.this,GlobalApp.getIsVPN())) {
															try {
																writeToLogcat("SendLogsTimerIsVPN");
																SendLogs();
															} catch (Exception ex) {
																GlobalApp.WriteLogException(ex);
																stopSelf();
															}
														}
													}
		}, 0, (UPDATE_INTERVAL * 60 * 1000));
	}

	private void SendLogs() {
		writeToLogcat("SendLogs");
		Logger logger = new Logger(this);
		try {
			logger.open();
			logger.close();
			SharedPreferences sharedPreferences = getSharedPreferences("PazWashPreferences", MODE_PRIVATE);
			int number_of_log_rows = sharedPreferences.getInt("number_of_log_rows", 30);
			logger.sendLogs(number_of_log_rows);
		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (timer != null) {
			timer.cancel();
		}
		System.gc();

	}



}
