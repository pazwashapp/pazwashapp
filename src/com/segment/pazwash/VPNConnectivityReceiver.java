package com.segment.pazwash;

import com.segment.pazwash.BL.GlobalApp;
import com.segment.pazwash.DM.VPNState;
import com.segment.pazwash.Enums.VPNStatus;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class VPNConnectivityReceiver extends BroadcastReceiver {
		
	public void onReceive(Context c, Intent intent) {
		
		checkVPN(intent);
	}

	private void checkVPN(Intent intent) {
		VPNState vpnState = new VPNState();
		
		
		String state = intent.getSerializableExtra("connection_state")
				.toString();
		if (state.equals("CONNECTING")) {
			vpnState.setVPNStatus(VPNStatus.CONNECTING);
			GlobalApp.setVPNState(vpnState);
			// Do what needs to be done
		} else if (state.equals("CONNECTED")) {
			vpnState.setVPNStatus(VPNStatus.CONNECTED);
			GlobalApp.setVPNState(vpnState);
			// Do what needs to be done
		} else if (state.equals("IDLE")) {
			int errorCode = intent.getIntExtra("err", 0);
			if (errorCode != 0) {
				// Do what needs to be done to report a failure
				vpnState.setVPNStatus(VPNStatus.ERROR);
				vpnState.setCodeError(errorCode);
				GlobalApp.setVPNState(vpnState);
			} else {
				vpnState.setVPNStatus(VPNStatus.DISCONNECTED);
				GlobalApp.setVPNState(vpnState);
				// Normal disconnect
			}
		} else if (state.equals("DISCONNECTING")) {
			vpnState.setVPNStatus(VPNStatus.DISCONNECTING);
			GlobalApp.setVPNState(vpnState);
			// Usually not very interesting
		}
	}

}
