package com.segment.pazwash;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.segment.pazwash.BL.CameraHelper;
import com.segment.pazwash.BL.GlobalApp;
import com.segment.pazwash.BL.JSONSharedPreferences;
import com.segment.pazwash.BL.NetworkChk;
import com.segment.pazwash.DM.AddItems;
import com.segment.pazwash.DM.DataParameter;
import com.segment.pazwash.DM.LPResult;
import com.segment.pazwash.Enums.BBResult;
import com.segment.pazwash.Enums.BlackBoxStep;
import com.segment.pazwash.Enums.ClientExceptionCodeResult;
import com.segment.pazwash.Enums.RequestStatuz;
import com.segment.pazwash.WS.WS;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class LpManualActivity extends BaseActivity {

	LPResult lpResult;
	EditText lpManual;
	Button btnSave;
	ImageView _image;
	Button btnShortProccess;
	Button btnDeny;
	TextView tvManualTitle;
	boolean isWsLogicalError = false;
	boolean isBtnSkipPressed = false;

	// boolean flag_withImage;
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ActivityType = 1;
		byte[] data = null;
		int mOrientation = 0;
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			mOrientation = extras.getInt("MOrientation");
			data = extras.getByteArray("Data");
		}
		GlobalApp.setImagePath(setImagePath());
		lpResult = GlobalApp.getLPResult();
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.lpmanual);
		Typeface hebBold = Typeface.createFromAsset(getAssets(), "fonts/droidsanshebrew-bold.ttf");
		TextView forgotTv = (TextView) findViewById(R.id.forgotTv);
		forgotTv.setTypeface(hebBold);
		TextView lpManualTv = (TextView) findViewById(R.id.lpManualTv);
		lpManualTv.setTypeface(hebBold);	
		SharedPreferences sharedPreferences = getSharedPreferences("PazWashPreferences", MODE_PRIVATE);
		int image_quality = sharedPreferences.getInt("image_quality", 100);
		CameraHelper.SaveImage(data, GlobalApp.getImagePath(), mOrientation, image_quality);
		_image = (ImageView) findViewById(R.id.lpimage);
		lpManual = (EditText) findViewById(R.id.lpManualEt);
		btnSave = (Button) findViewById(R.id.btnSave);
		btnDeny = (Button) findViewById(R.id.denyBtn);
		tvManualTitle = (TextView) findViewById(R.id.forgotTv);
		
		
		File file = new File(GlobalApp.getImagePath());
		_image.setImageURI(Uri.fromFile(file));
		btnSave.setOnClickListener(btnSaveListener);
		btnSave.setTypeface(hebBold);
		
		//Yaron
		GlobalApp.setIsDWASH(false);
		GlobalApp.setisDWASHLast15MinDeal(false);
		btnDeny.setOnClickListener(btnDenyListener);

		btnShortProccess = (Button) findViewById(R.id.btnSkip);
		if (GlobalApp.short_process==1)
			btnShortProccess.setVisibility(View.VISIBLE);
		
		btnShortProccess.setOnClickListener(btnShortProccessListener);

		lpManual.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (hasFocus) {
					getWindow()
							.setSoftInputMode(
									WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
				}
			}
		});

		lpManual.requestFocus();
		
		
//Yaron
//		if (!GlobalApp.IsOffline()) {
//			loadProgressDialog();
//			showProgress();
//			getCarPlate();
//
//		}

	}
	private OnClickListener btnShortProccessListener = new OnClickListener() {
		public void onClick(View v) {
			isBtnSkipPressed = true;
			OkButtonsClick();
		}
	};

	private OnClickListener btnSaveListener = new OnClickListener() {
		public void onClick(View v) {	
			isBtnSkipPressed = false;
			OkButtonsClick();
		}

	};
	
	private void OkButtonsClick(){
		//Yaron
		if(isWsLogicalError){
			isWsLogicalError = false;
			lpManual.setText("");
			btnDeny.setVisibility(View.GONE);
			btnSave.setText(R.string.btnSave);	
			tvManualTitle.setText(R.string.LpManualTitle);
			tvManualTitle.setTextColor(getResources().getColor(R.color.TitleColor));
			tvManualTitle.setBackgroundColor(Color.TRANSPARENT);
			if (GlobalApp.short_process==1)
				btnShortProccess.setVisibility(View.VISIBLE);
			return;
		}
		
		
		

		String carNumber = lpManual.getText().toString();
		carNumber = carNumber.replace(" ", "");
		if (carNumber.isEmpty()) {
			showAlertDialog(R.string.CarNumberEmptyTitle, R.string.CarNumberEmpty, null);
			return;
		}
		if(carNumber.length() > 8)
		{
			showAlertDialog(R.string.CarNumberEmptyTitle, R.string.CarNumberExc, null);
			return;
		}
		
		loadProgressDialog();
		showProgress();
		getCarPlate();		
	}
	
	//Yaron
	private OnClickListener btnDenyListener = new OnClickListener() {
		public void onClick(View v) {
			
			openMain();

		}

	};
	

	private void getCarPlate() {

		try {

			if (NetworkChk.checkInternet(LpManualActivity.this,
					GlobalApp.getIsVPN())) {

				String image_to_string = "";

				image_to_string = CameraHelper.getLastImage(GlobalApp
						.getImagePath());
				// throw new Exception("test");

				new getPazTicket(image_to_string, GlobalApp.getUserName(), lpManual.getText().toString()).execute(PAZWASH_WS_URL + "/getLPRv2"); //Yaron

			} else {

				openOffline();

			}
		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
		}
	}

	public class getPazTicket extends AsyncTask<String, Void, LPResult> {

		String userName;
		String image;
		String lpManual;

		getPazTicket(String img, String user, String lpManual) {
			this.userName = user;
			this.image = img;
			this.lpManual = lpManual;//Yaron
		}

		protected LPResult doInBackground(String... urls) {

			try {

				WS ws = new WS();
				List<DataParameter> parameters = BuildParameters();
				return ws.getLPR(parameters, urls[0]);

			}

			catch (Exception e) {
				GlobalApp.WriteLogException(e);
				LPResult lpr = new LPResult();
				lpr.setCarNumber("-1");
				lpr.setConfidence("0");
				return lpr;
			}
		}

		private List<DataParameter> BuildParameters() {
			List<DataParameter> parameters = new ArrayList<DataParameter>();
			parameters.add(new DataParameter("img", this.image));
			parameters.add(new DataParameter("userName", this.userName));
			parameters.add(new DataParameter("deviceId", GlobalApp
					.getDeviceId()));
			parameters.add(new DataParameter("mdCode", GlobalApp.getMDCode()));
			parameters.add(new DataParameter("isLpr", "0"));
			parameters.add(new DataParameter("washId", GlobalApp.getWashId())); //Yaron 
			parameters.add(new DataParameter("MdLP", this.lpManual)); //Yaron 
			parameters.add(new DataParameter("stationId", GlobalApp.getStationNumber())); //Yaron 
			return parameters;
		}

		protected void onPostExecute(LPResult result) {
			String tempStr = "START"; 	
		try {
			if (result.getRequestStatus().RequestStatusCode().equals(RequestStatuz.WSGW_OK.toString())) {
				if (result.getClientClientExcCode().equals(ClientExceptionCodeResult.RESULT_OK.toString())) {
					tempStr = "OK-ClientExceptionCodeResult";
					hideProgress();
					lpResult = result;
					if (result.getBBResult().getBBResultCode() == -99) {
						tempStr = "OK-getBBResultCode99";
						if (!isFinishing()) {
							showAlertDialog(R.string.Exception, R.string.TimeOutException, null);
						}
					}
					if (result.getBBResult() != null) {
						if (result.getBBResult().getBBResultCode() == -100) {
							tempStr = "OK-getBBResultCode100";
							if (!isFinishing()) {
								showAlertDialog(R.string.Exception, R.string.Exception, null);
							}
						}
					}
					//Yaron
					switch (result.getBBResult().getMdFlowType()) {
					case 1: //success
						result.setCarNumber(this.lpManual);
						
						GlobalApp.setLPResult(lpResult);
						//Digital wash
						if(result.getBBResult().getBBResultCode() == BBResult.DWASH_OK.ToInt()
								|| result.getBBResult().getBBResultCode() == BBResult.DWASH_IN_PROCESS.ToInt()
								|| result.getBBResult().getBBResultCode() == BBResult.DEAL_LAST_15_MIN_DWASH.ToInt()){

							GlobalApp.setIsDWASH(true);
							
							if (result.getBBResult().getBBResultCode() == BBResult.DEAL_LAST_15_MIN_DWASH
									.ToInt()) {
								GlobalApp.setisDWASHLast15MinDeal(true);
							}
							
							Intent dWashApprove = new Intent(LpManualActivity.this,	DwashApproveActivity.class);
							
							dWashApprove.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							startActivity(dWashApprove);
						}else{
							if(isBtnSkipPressed){
								try {									
										ArrayList<AddItems> generalItems = new ArrayList<AddItems>();
										JSONArray jItems = JSONSharedPreferences.loadJSONArray(getBaseContext(), "generalProducts", "");
										for (int i = 0; i < jItems.length(); i++) {
											JSONObject jO = jItems.getJSONObject(i);
											AddItems ad = new AddItems();
											ad.setItemCode(jO.getInt("Code"));
											ad.setItemDesc(jO.getString("Desc"));
											ad.setItemSort(jO.getInt("Sort"));
											ad.setItemIsMain(jO.getInt("Main"));
											generalItems.add(ad);
											GlobalApp.setAdditem(-1);
											GlobalApp.setAddItemDesc("");
										}
										GlobalApp.setWashCode(generalItems.get(0).ItemCode());
										GlobalApp.setGeneralItemDesc(generalItems.get(0).ItemDesc());
										Intent addItems = new Intent(LpManualActivity.this, ChooseSummaryActivity.class);
										startActivity(addItems);
									
								} catch (Exception e) {
									GlobalApp.WriteLogException(e);
								}
							}else{
								try {
									Intent serviceSelect = new Intent(LpManualActivity.this, ChooseWashActivity.class);
									
									WriteStep(GlobalApp.getWashId(), BlackBoxStep.LP_TYPING_APPROVE_BY_MD, "CarPlate " + this.lpManual + " approved");
									startActivity(serviceSelect);
									
								} catch (Exception e) {
									GlobalApp.WriteLogException(e);
									hideProgress();
								}
							}						
						}
						break;
						
					case 2://Error - try again
						isWsLogicalError = true;
						btnShortProccess.setVisibility(View.GONE);
						btnDeny.setVisibility(View.VISIBLE);
						btnSave.setText(R.string.AnotherManual);	
						tvManualTitle.setText(result.getBBResult().getBBResultDesc());
						tvManualTitle.setTextColor(Color.RED);
						tvManualTitle.setBackgroundColor(Color.WHITE);
						
						break;
						
					case 3:	//Error - can't continue						
					default:
						if (!isFinishing()) {
							showAlertDialog(R.string.CarNumberEmptyTitle, result.getBBResult().getBBResultDesc(), backToMain);
						}
						break;
					}
				} else {
					tempStr = "NOTOK-ClientExceptionCodeResult";
					if (result.getClientClientExcCode().equals(ClientExceptionCodeResult.RESULT_ERROR.toString())) {
						hideProgress();
						if (!isFinishing()) {
							showAlertDialog(R.string.Exception, R.string.ServerException, null);
						}
					}
					if (result.getClientClientExcCode().equals(
							ClientExceptionCodeResult.RESULT_TIMEOUT.toString())) {
						hideProgress();
						if (!isFinishing()) {
							showAlertDialog(R.string.Exception, R.string.TimeOutException, null);
						}
					}
				}
			} else {
				hideProgress();
				showAlertDialog(R.string.Exception, result.getRequestStatus().RequestStatusCode(), null);
			}
		} catch (Exception e) {
			GlobalApp.WriteLogException(e, tempStr);
			hideProgress();
		}

		}
	}
	private DialogInterface.OnClickListener backToMain = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			try {
				openMain();
			} catch (Exception e) {
				GlobalApp.WriteLogException(e);
				hideProgress();
			}

		}
	};
	@Override
	protected void onDestroy() {
		super.onDestroy();
		System.gc();
	}
}