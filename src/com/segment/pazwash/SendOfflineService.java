package com.segment.pazwash;

import com.segment.pazwash.BL.DBAdapter;
import com.segment.pazwash.BL.GlobalApp;
import com.segment.pazwash.BL.NetworkChk;

import java.util.Timer;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import java.util.TimerTask;

public class SendOfflineService extends Service {
	// int counter = 0;
	static final int UPDATE_INTERVAL = 5;
	private Timer timer = new Timer();

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {

		// We want this service to continue running until it is explicitly
		// stopped, so return sticky.
		// Toast.makeText(this, "Service Started", Toast.LENGTH_LONG).show();
		SendOfflineRepeatedly();
		return START_STICKY;
	}

	private void SendOfflineRepeatedly() {
		timer.scheduleAtFixedRate(new TimerTask() {
			public void run() {
				if (NetworkChk.checkInternet(SendOfflineService.this,GlobalApp.getIsVPN())) {

					try {
						SendOffline();

					} catch (Exception ex) {

						GlobalApp.WriteLogException(ex);
						stopSelf();
					}
				}
			}
		}, 0, (UPDATE_INTERVAL * 60 * 1000));
	}

	private void SendOffline() {
		DBAdapter db = new DBAdapter(this);
		try {
			db.open();
           boolean result = db.deleteTrueTickets();
           db.close();
			db.checkOfflineData();
			// db.DeleteSendedOfflineData();
		} catch (Exception ex) {
			GlobalApp.WriteLogException(ex);
			//db.DeleteSendedOfflineData();
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (timer != null) {
			timer.cancel();
		}
		System.gc();
		// Toast.makeText(this, "Service Destroyed", Toast.LENGTH_LONG).show();
	}
	/*
	 * private void initService() {
	 * 
	 * int initialDelay = 15000; // start after 15 seconds
	 * 
	 * int period = 15000; // repeat every 15 seconds for testing Timer timer =
	 * new Timer(); TimerTask task = new TimerTask() { public void run() {
	 * Looper.prepare(); SendOffline(); Looper.loop(); Looper.myLooper().quit();
	 * } }; timer.scheduleAtFixedRate(task, initialDelay, period); }
	 */
}
