package com.segment.pazwash;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.segment.pazwash.DM.AddItems;

import com.segment.pazwash.BL.GlobalApp;
import com.segment.pazwash.BL.JSONSharedPreferences;

import android.content.Intent;
import android.graphics.Typeface;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;

public class ChooseAddItemsActivity extends BaseActivity {
	Button Next;

	ArrayList<AddItems> addItems = new ArrayList<AddItems>();
	int selected_item = -1;
	int uncheckedItem = -2;
	ListView lstView;
	boolean isNextClick = false;

	/** Called when the activity is first created. */

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			requestWindowFeature(Window.FEATURE_NO_TITLE);

			JSONArray jItems = JSONSharedPreferences.loadJSONArray(
					getBaseContext(), "additionalProducts", "");

			for (int i = 0; i < jItems.length(); i++) {
				JSONObject jO = jItems.getJSONObject(i);

				AddItems ad = new AddItems();
				ad.setItemCode(jO.getInt("Code"));
				ad.setItemDesc(jO.getString("Desc"));
				ad.setItemSort(jO.getInt("Sort"));
				ad.setItemIsMain(jO.getInt("Main"));
				addItems.add(ad);

			}

			setContentView(R.layout.add_items);
			loadProgressDialog();
			
			Typeface hebBold = Typeface.createFromAsset(getAssets(), "fonts/droidsanshebrew-bold.ttf");
                
			TextView title =  (TextView) findViewById(R.id.titleTv);
			title.setTypeface(hebBold);
			
			
			lstView = (ListView) findViewById(R.id.listAddItems);
			lstView.setBackgroundResource(R.drawable.listview);
			lstView.setChoiceMode(1);
			

			lstView.setAdapter(new ArrayAdapter<AddItems>(
					ChooseAddItemsActivity.this, R.drawable.checkbox, addItems));

			lstView.setCacheColorHint(0);
			lstView.setItemsCanFocus(false);

			lstView.setOnItemClickListener(new OnItemClickListener() {
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					int i = 0;
					try {
						i = lstView.getCheckedItemPosition();
						if (selected_item == i && selected_item != uncheckedItem) {
							uncheckedItem = i;
							lstView.setItemChecked(i, false);
						}
						else
						{
							uncheckedItem = -2;
							lstView.setItemChecked(i, true);
						}
						selected_item = i;
					} catch (Exception e) {
						i = -1;
						selected_item = -1;
						uncheckedItem = -2;
					}
				}
			});
			Next = (Button) findViewById(R.id.NextBt);
			Next.setOnClickListener(btnSendListener);			
			Next.setTypeface(hebBold);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			GlobalApp.WriteLogException(e);
			finish();
		}
	}

	private String getSelectedItems() {
		try {
			String result = "";
			long[] selectedItems = lstView.getCheckItemIds();

			for (int i = 0; i < selectedItems.length; i++) {

				result += Long.toString(selectedItems[i]) + ";";
			}

			return result;
		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
			return null;
		}
	}

	private OnClickListener btnSendListener = new OnClickListener() {
		public void onClick(View v) {
			// showProgress();
			if (!isNextClick) {
				isNextClick = true;
				Next.setClickable(false);
				Next.setEnabled(false);
				Handler handler = new Handler();
				handler.postDelayed(new Runnable() {
					public void run() {
						isNextClick = false;
						Next.setClickable(true);
						Next.setEnabled(true);
					}
				}, 2000);

				try {
					int i = 0;
					try {
						i = lstView.getCheckedItemPosition();
					} catch (Exception e) {
						i = -1;
					}
					if (i != -1) {
						GlobalApp.setAdditem(addItems.get(i).ItemCode());
						GlobalApp.setAddItemDesc(addItems.get(i).ItemDesc());
					} else {
						GlobalApp.setAdditem(-1);
						GlobalApp.setAddItemDesc("");
					}
					Intent addItems = new Intent(ChooseAddItemsActivity.this, ChooseSummaryActivity.class);
					startActivity(addItems);
				} catch (Exception e) {
					GlobalApp.WriteLogException(e);
				}

				// long[] seldectedItems = new long[1];
				// seldectedItems[0] = Long.parseLong(result);
			}

		}
	};

	@Override
	protected void onDestroy() {
		super.onDestroy();
		System.gc();
	}

}
