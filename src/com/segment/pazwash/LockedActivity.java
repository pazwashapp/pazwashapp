package com.segment.pazwash;

import com.segment.pazwash.BL.GlobalApp;


import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class LockedActivity extends BaseActivity {

	Button LoginOk;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ActivityType = 0;
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.locked);
		loadProgressDialog();
		hideProgress();
		Typeface hebBold = Typeface.createFromAsset(getAssets(), "fonts/droidsanshebrew-bold.ttf");        
		TextView title =  (TextView) findViewById(R.id.titleTv);
		TextView  data =  (TextView) findViewById(R.id.dataTv);
		title.setTypeface(hebBold);
		data.append(" :" + getUserName());
		
		LoginOk = (Button) findViewById(R.id.LoginBt);
		LoginOk.setOnClickListener(btnOkListener);
		LoginOk.setTypeface(hebBold);
	}

	private OnClickListener btnOkListener = new OnClickListener() {
		public void onClick(View v) {
			try {
				Intent intent = new Intent(getApplicationContext(),
						LoginActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				finish();
			} catch (Exception e) {
				GlobalApp.WriteLogException(e);
			}

		}
	};
	
	
	@Override
	public void onBackPressed() {

		android.os.Process.killProcess(android.os.Process.myPid());

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		System.gc();
	}

}
