package com.segment.pazwash;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;

import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

import com.segment.pazwash.BL.DBAdapter;
import com.segment.pazwash.BL.GlobalApp;

import com.segment.pazwash.BL.RejectOfflineAdapter;
import com.segment.pazwash.DM.DeleteItem;

public class RejectOfflineActivity extends BaseActivity {

	List<DeleteItem> WashList = new ArrayList<DeleteItem>();
	RejectOfflineAdapter adapter;
	ListView lstView;
	String password;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		ActivityType = 1;

		requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.reject_offline);

		loadProgressDialog();

		showProgress();

		try {

			SharedPreferences sharedPreferences = getSharedPreferences(
					"PazWashPreferences", MODE_PRIVATE);

			password = sharedPreferences.getString("password_cancel_offline",
					"123456");

			DBAdapter dba = new DBAdapter(this);
			dba.open();
			dba.deleteTrueTickets();
			WashList = dba.getOfflineWashes();
			dba.close();

			Typeface hebBold = Typeface.createFromAsset(getAssets(), "fonts/droidsanshebrew-bold.ttf");
			
			adapter = new RejectOfflineAdapter(RejectOfflineActivity.this,
					WashList,hebBold);

			lstView = (ListView) findViewById(R.id.listDeleteOffline);
			lstView.setBackgroundResource(R.drawable.listview);
			lstView.setChoiceMode(1);
			lstView.setAdapter(adapter);
			lstView.setCacheColorHint(0);
			lstView.setItemsCanFocus(false);

			lstView.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> a, View v, int position,
						long id) {

					showCancelDialog(position);
					/*
					 * final int p = position; Dialog dialog;
					 * AlertDialog.Builder builder; builder = new
					 * AlertDialog.Builder(RejectOfflineActivity.this);
					 * builder.setMessage(R.string.DeleteDialog)
					 * .setCancelable(false)
					 * 
					 * .setNegativeButton(R.string.No, null)
					 * .setPositiveButton(R.string.YesC, new
					 * DialogInterface.OnClickListener() { public void onClick(
					 * DialogInterface dialog, int id) {
					 * 
					 * try {
					 * 
					 * Object o = lstView.getItemAtPosition(p); DeleteItem
					 * fullObject = (DeleteItem)o;
					 * 
					 * 
					 * DBAdapter dba = new
					 * DBAdapter(RejectOfflineActivity.this); dba.open();
					 * boolean result =
					 * dba.deleteRecord(Long.parseLong(fullObject.WashId()));
					 * dba.close();
					 * 
					 * } catch (Exception e) { GlobalApp.WriteLogException(e);
					 * //WriteLogException(e); return; }
					 * 
					 * } }); dialog = builder.create(); dialog.show();
					 * 
					 * // Toast.makeText(RejectActivity.this,
					 * "You have chosen: " + // " " + fullObject.CarNumber(),
					 * Toast.LENGTH_LONG).show();
					 */
				}
			});

		}

		catch (Exception e) {
			GlobalApp.WriteLogException(e);

		}

		hideProgress();

	}

	private void showCancelDialog(final int p) {

		AlertDialog.Builder alert = new AlertDialog.Builder(
				RejectOfflineActivity.this);

		alert.setTitle(R.string.EnterPassword);
		// Set an EditText view to get user input
		final EditText input = new EditText(RejectOfflineActivity.this);
	
		alert.setView(input);
		

		alert.setPositiveButton(R.string.YesC,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						Editable value = input.getText();
						String v = value.toString();

						if (v.toLowerCase().equals(password.toLowerCase())) {
							try {

								Object o = lstView.getItemAtPosition(p);
								DeleteItem fullObject = (DeleteItem) o;

								DBAdapter dba = new DBAdapter(
										RejectOfflineActivity.this);
								dba.open();
								boolean result = dba.deleteRecord(Long
										.parseLong(fullObject.WashId()));
								dba.close();
								if (result == true) {
									WashList.remove(p);
									adapter.notifyDataSetChanged();
								}								
								
								else
								{
									showDialog(R.string.Exception);
								}
                                   

							} catch (Exception e) {
								GlobalApp.WriteLogException(e);

							}

						} else {
							Dialog dialog1;
							AlertDialog.Builder builder;
							builder = new AlertDialog.Builder(
									RejectOfflineActivity.this);
							builder.setMessage(R.string.PasswordOfflineInvalid)
									.setCancelable(false)
									.setNegativeButton(R.string.No, null)
									.setPositiveButton(
											R.string.YesC,
											new DialogInterface.OnClickListener() {
												public void onClick(
														DialogInterface dialog1,
														int id) {

													showCancelDialog(p);
												};
											});

							dialog1 = builder.create();
							dialog1.show();

						}

					}
				});

		alert.setNegativeButton(R.string.btnCancel, null);

		alert.show();

	}
}
