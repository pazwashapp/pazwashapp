package com.segment.pazwash;

import java.util.ArrayList;
import java.util.List;

import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Window;
import android.widget.ListView;
import android.widget.TextView;

import com.segment.pazwash.BL.GlobalApp;

import com.segment.pazwash.BL.SummaryAdapter;
import com.segment.pazwash.DM.DataParameter;
import com.segment.pazwash.DM.ReportSummaryItem;
import com.segment.pazwash.DM.SummaryReportResult;
import com.segment.pazwash.Enums.ClientExceptionCodeResult;
import com.segment.pazwash.Enums.RequestStatuz;

import com.segment.pazwash.WS.ReportWS;

public class ReportSummary extends BaseActivity {

	List<ReportSummaryItem> summary = new ArrayList<ReportSummaryItem>();
	SummaryAdapter adapter;
	ListView lstView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ActivityType = 1;
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.reportsummary);
		loadProgressDialog();
		showProgress();
		
		TextView title = (TextView)findViewById(R.id.titleSummary);
		Typeface hebBold = Typeface.createFromAsset(getAssets(), "fonts/droidsanshebrew-bold.ttf");	
		title.setTypeface(hebBold);

		getSummary cl = new getSummary(GlobalApp.getUserName());
		cl.execute(PAZWASH_WS_URL + "/getReportSummary");

	}

	private class getSummary extends
			AsyncTask<String, Void, SummaryReportResult> {

		String UserName;

		protected SummaryReportResult doInBackground(String... urls) {
			ReportWS ws = new ReportWS();
			List<DataParameter> parameters = BuildParameters();
			return ws.getSummary(parameters, urls[0]);
		}

		getSummary(String userName) {
			this.UserName = userName;
		}

		private List<DataParameter> BuildParameters() {
			List<DataParameter> parameters = new ArrayList<DataParameter>();
			parameters.add(new DataParameter("userName", this.UserName));
			parameters.add(new DataParameter("deviceId", GlobalApp
					.getDeviceId()));
			parameters.add(new DataParameter("mdCode", GlobalApp.getMDCode()));
			parameters.add(new DataParameter("stationId", GlobalApp
					.getStationID()));
			return parameters;
		}

		protected void onPostExecute(SummaryReportResult result) {

			try {
				if (result.getRequestStatus().RequestStatusCode()
						.equals(RequestStatuz.WSGW_OK.toString())) {
					if (result.getClientClientExcCode().equals(
							ClientExceptionCodeResult.RESULT_OK.toString())) {
						if (result != null && result.ReportSummary() != null
								&& result.ReportSummary().size() > 0) {
							summary = result.ReportSummary();

							Typeface hebBold = Typeface.createFromAsset(getAssets(), "fonts/droidsanshebrew-bold.ttf");
							
							
							adapter = new SummaryAdapter(ReportSummary.this,
									summary,hebBold);
							// adapter = new
							// ArrayAdapter<DeleteItem>(RejectActivity.this,
							// R.drawable.reject_item, BidsList);
							lstView = (ListView) findViewById(R.id.reportSummaryList);
							lstView.setBackgroundResource(R.drawable.listview);
							lstView.setChoiceMode(0);
							lstView.setAdapter(adapter);
							lstView.setCacheColorHint(0);
							lstView.setItemsCanFocus(false);

						}
						hideProgress();
					} else {
						if (result.getClientClientExcCode().equals(
								ClientExceptionCodeResult.RESULT_ERROR
										.toString())) {
							hideProgress();
							if (!isFinishing()) {
								showAlertDialog(R.string.Exception,
										R.string.NoData, null);
							}

						}
						if (result.getClientClientExcCode().equals(
								ClientExceptionCodeResult.RESULT_TIMEOUT
										.toString())) {
							hideProgress();
							if (!isFinishing()) {
								showAlertDialog(R.string.Exception,
										R.string.TimeOutException, null);
							}
						}
					}
				} else {
					hideProgress();
					showAlertDialog(R.string.Exception, result
							.getRequestStatus().RequestStatusCode(), null);
				}

			} catch (Exception e) {
				GlobalApp.WriteLogException(e);
				hideProgress();
				showAlertDialog(R.string.Exception,
						R.string.Exception, null);
			}
		}

	}

}
