package com.segment.pazwash;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;


import com.segment.pazwash.BL.GlobalApp;
import com.segment.pazwash.BL.JSONSharedPreferences;
import com.segment.pazwash.DM.AddItems;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;

import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class ChooseWashActivity extends BaseActivity {

	Button Next;
	ListView lstView;
	ArrayList<AddItems> generalItems = new ArrayList<AddItems>();
	boolean isNextClick = false;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			requestWindowFeature(Window.FEATURE_NO_TITLE);
			JSONArray jItems = JSONSharedPreferences.loadJSONArray(getBaseContext(), "generalProducts", "");
			for (int i = 0; i < jItems.length(); i++) {
				JSONObject jO = jItems.getJSONObject(i);
				AddItems ad = new AddItems();
				ad.setItemCode(jO.getInt("Code"));
				ad.setItemDesc(jO.getString("Desc"));
				ad.setItemSort(jO.getInt("Sort"));
				ad.setItemIsMain(jO.getInt("Main"));
				generalItems.add(ad);
			}
			ActivityType = 1;
			setContentView(R.layout.choose_wash);
			loadProgressDialog();
			Typeface hebBold = Typeface.createFromAsset(getAssets(), "fonts/droidsanshebrew-bold.ttf");
              
			TextView title =  (TextView) findViewById(R.id.titleTv);
			title.setTypeface(hebBold);
			
			lstView = (ListView) findViewById(R.id.listService);
			lstView.setBackgroundResource(R.drawable.listview);
			lstView.setChoiceMode(1);

			lstView.setAdapter(new ArrayAdapter<AddItems>(ChooseWashActivity.this, R.drawable.custom, generalItems));

			lstView.setCacheColorHint(0);
			lstView.setItemsCanFocus(false);

			SharedPreferences sharedPreferences = getSharedPreferences("PazWashPreferences", MODE_PRIVATE);
			int selectedItem = sharedPreferences.getInt("selectedWashType", 0);
			int  selectedPosition = 0;

			for (int i = 0; i < lstView.getCount(); i++) {
				if (((AddItems)lstView.getItemAtPosition(i)).ItemCode() == selectedItem)
				{
					selectedPosition = i;
					break;
				}
			}
			lstView.setItemChecked(selectedPosition, true);
			Next = (Button) findViewById(R.id.NextBt);
			Next.setOnClickListener(btnSendListener);
			Next.setTypeface(hebBold);
		
		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
			hideProgress();
			finish();
		}

	}

	private OnClickListener btnSendListener = new OnClickListener() {
		public void onClick(View v) {
			// showProgress();
			try {

				if (!isNextClick) {
					isNextClick = true;
					Next.setClickable(false);
					Next.setEnabled(false);
					Handler handler = new Handler();
					handler.postDelayed(new Runnable() {
						public void run() {
							isNextClick = false;
							Next.setClickable(true);
							Next.setEnabled(true);

						}
					}, 2000);

					int i = lstView.getCheckedItemPosition();
					GlobalApp.setWashCode(generalItems.get(i).ItemCode());
					GlobalApp.setGeneralItemDesc(generalItems.get(i).ItemDesc());
					Intent addItems = new Intent(ChooseWashActivity.this, ChooseAddItemsActivity.class);
					startActivity(addItems);
				}
			} catch (Exception e) {
				GlobalApp.WriteLogException(e);
			}

		}
	};

	@Override
	protected void onDestroy() {
		super.onDestroy();
		System.gc();
	}

}