package com.segment.pazwash.Enums;

public enum MdFlowType {
	SUCCESS(1),
	MANUAL_POSSIBLE(2),
	STOP_FLOW(3)
;
	
	private int value;

    private MdFlowType(int value) { 
            this.value = value; 
    }
    
    
    public int ToInt(){
    	
    	return this.value;    	
    }
    
    @Override
    
    public String toString() {         
   
      return Integer.toString(this.value);   
  };    
}
