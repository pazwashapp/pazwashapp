package com.segment.pazwash.Enums;

public enum ClientExceptionCodeResult {

		RESULT_TIMEOUT(0), RESULT_OK(1), RESULT_ERROR(2);

		private int value;

		private ClientExceptionCodeResult(int value) {
			this.value = value;
		}

		public int ToInt() {
			return this.value;
		}

		@Override
		public String toString() {

			return Integer.toString(this.value);
		};

	}


