package com.segment.pazwash.Enums;

public enum WashResult {
	NOT_DEFINED(0),
    OK(1),
    NOTOK(2),
    EXCEPTION(3);	
	
	private int value;

    private WashResult(int value) { 
            this.value = value; 
    }     
    
    public int ToInt()
    {
    	return this.value;
    }
    
    @Override
    
    public String toString() {         
   
      return Integer.toString(this.value);   
  };        

}
