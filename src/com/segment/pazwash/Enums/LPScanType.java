package com.segment.pazwash.Enums;

public enum LPScanType {
	LICENCE_PLATE_MANUAL(1),
    LICENCE_PLATE_LPR(2),
	BARCODE(3),
	QR_CODE(4),
	SECRET_CODE(5),
	MULTIPLE_WASH_TICKET(6);
	
	
	private int value;

    private LPScanType(int value) { 
            this.value = value; 
    }
    
    
    public int ToInt()
    {
    	return this.value;
    }
    
    @Override
    
    public String toString() {         
   
      return Integer.toString(this.value);   
  };        

}
