package com.segment.pazwash.Enums;

public enum BlackBoxStep {	
	LOGIN_OK(1004),	
	//IMAGE_SAVED_ON_MD(2005), 
	LP_RECOGNIZE_OK(1006),
	WASH_REQUEST_BARCODE(1008),	
	WASH_REQUEST_LPR(1016),
	WASH_REQUEST_TYPING(1017),	
	WASH_APPROVE(1009), 
	WASH_NO_APPROVE_SERVER_ERROR(1010),
	WASH_NO_APPROVE_DENY(1013),		
	LP_TYPING_APPROVE_BY_MD(2021),
	TRY_AGAIN_APPROVE_REQUEST(1011), 
	WASH_SAVED_OFFLINE_ON_MD(2012),	
	OFFLINE_LIMIT_EXCEEDED_ON_MD(2014),
	LP_APPROVE_BY_MD(2007),
	WASH_APPROVE_REQEUST(1015)
	//IMAGE_DELETED_ON_MD(2015)
	;
		
		
		
		
		
		
		
		
		
		 
	
	
	
	

	private int value;

    private BlackBoxStep(int value) { 
            this.value = value; 
    }
    
    
    public int ToInt(){
    	
    	return this.value;    	
    }
    
    @Override
    
    public String toString() {         
   
      return Integer.toString(this.value);   
  };    

}
