package com.segment.pazwash.Enums;

public enum BBResult {
	PAZOMAT_OK(90080),
	ONE_TIME_OK(90081),
	DEAL_LAST_15_MIN(10031),
	DWASH_OK(10082),
	DEAL_LAST_15_MIN_DWASH(10061),
	DWASH_IN_PROCESS(10073),
	DWASH_WAIT_APPROVE(10044)
;
	
	private int value;

    private BBResult(int value) { 
            this.value = value; 
    }
    
    
    public int ToInt(){
    	
    	return this.value;    	
    }
    
    @Override
    
    public String toString() {         
   
      return Integer.toString(this.value);   
  };    
}
