package com.segment.pazwash.Enums;

public enum WSRequestFlags {

	RESULT_TIMEOUT(0), RESULT_OK(1), RESULT_ERROR(2);

	private int value;

	private WSRequestFlags(int value) {
		this.value = value;
	}

	public int ToInt() {
		return this.value;
	}

	@Override
	public String toString() {

		return Integer.toString(this.value);
	};

}
