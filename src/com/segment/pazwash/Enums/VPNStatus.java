package com.segment.pazwash.Enums;

public enum VPNStatus {

	DISCONNECTED(0),CONNECTING(1),CONNECTED(2),ERROR(3),DISCONNECTING(4);

	private int value;

	private VPNStatus(int value) {
		this.value = value;
	}

	public int ToInt() {

		return this.value;
	}

	@Override
	public String toString() {

		return Integer.toString(this.value);
	};

}
