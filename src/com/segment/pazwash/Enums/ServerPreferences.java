package com.segment.pazwash.Enums;

public enum ServerPreferences {
	MIN_CONFIDENCE(1),
	//CAMERA_JPEG_QUALITY(2),
	COMPRESS_IMAGE_QUALITY(3),
	SAVE_OFFLINE_LIMIT(4),	
	//MAX_LOGIN_FAIL_TRY(5), 
	//CAMERA_CROP(6),
	LOGIN_SESSION_MIN(7),
	MAX_NO_LPR(8),	
	WASH_DEFAULT(9),	
	PASSWORD_CANCEL_WASH_OFFLINE(12),
	//GPS_REFRESH_INTERVAL(13),
    WASH_APPROVE_TIMEOUT_SECONDS(14),
    TIMEOUT_SECONDS(15),
    APP_VERSION(16),
	NUMBER_OF_LOG_ROWS(17),
	WRITE_GENERAL_LOG(18),
	mrkt_msg(30),
	mrkt_msg2(32),
	mrkt_msg_show(31),
	short_process(19);

	private int value;

    private ServerPreferences(int value) { 
            this.value = value; 
    }
    public int ToInt(){
    	
    	return this.value;    	
    }
    
    @Override
    
    public String toString() {         
   
      return Integer.toString(this.value);   
  };    
}
