package com.segment.pazwash.Enums;

public enum LoginServerStatus {
	LOGIN_ERROR(0), LOGIN_EXPIRED(1), LOGIN_OK(2),LOGIN_LOCK(3);

	private int value;

	private LoginServerStatus(int value) {
		this.value = value;
	}

	public int ToInt() {
		return this.value;
	}

	@Override
	public String toString() {

		return Integer.toString(this.value);
	};

}


