package com.segment.pazwash.Enums;

public enum RequestStatuz {
	WSGW_OK(0), VALIDATION_ERROR(1), WSGW_ERROR(2),WS_ERROR(3);

	private int value;

	private RequestStatuz(int value) {
		this.value = value;
	}

	public int ToInt() {
		return this.value;
	}

	@Override
	public String toString() {

		return Integer.toString(this.value);
	};

}
