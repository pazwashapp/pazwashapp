package com.segment.pazwash.Enums;

public enum TimeOutTry {
	TIMEOUT_TRY_ZERO(0), TIMEOUT_TRY_FIRST(5), TIMEOUT_TRY_SECOND(10),TIMEOUT_TRY_THIRD(15),TIMEOUT_TRY_OFFLINE(15);

	private int value;

	private TimeOutTry(int value) {
		this.value = value;
	}

	public int ToInt() {
		return this.value;
	}

	@Override
	public String toString() {

		return Integer.toString(this.value);
	};

}
