package com.segment.pazwash.Enums;

public enum WashType {
WASH_EXTERNAL(0), 
WASH_INTERNAL(1), 
WASH_INTERNAL_WAX(2),
WASH_EXTERNAL_INTERNAL(3);
	
	
	private int value;

    private WashType(int value) { 
            this.value = value; 
    }
    
    
    public int ToInt()
    {
    	return this.value;
    }
    
    @Override
    
    public String toString() {         
   
      return Integer.toString(this.value);   
  };        

}
