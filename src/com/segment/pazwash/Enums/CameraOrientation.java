package com.segment.pazwash.Enums;

public enum CameraOrientation {
	
	ORIENTATION_PORTRAIT_NORMAL(0), ORIENTATION_PORTRAIT_INVERTED(1), ORIENTATION_LANDSCAPE_NORMAL(2),ORIENTATION_LANDSCAPE_INVERTED (3);


	private int value;

    private CameraOrientation(int value) { 
            this.value = value; 
    }
    
    
    public int ToInt(){
    	
    	return this.value;    	
    }
    
    @Override
    
    public String toString() {         
   
      return Integer.toString(this.value);   
  };    
  

}
