package com.segment.pazwash;

import java.util.Timer;
import java.util.TimerTask;

import com.segment.pazwash.BL.GlobalApp;


import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;

public class GPService extends Service {
	LocationManager mlocManager;
	LocationListener mlocListener;
	static  int UPDATE_INTERVAL = 15;
	private Timer timer = new Timer();

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		
		SharedPreferences sharedPreferences = getSharedPreferences(
				"PazWashPreferences", MODE_PRIVATE);		
		UPDATE_INTERVAL = sharedPreferences.getInt("gps_refresh_interval", 15);		
		
		GPSRepeatedly();
		return START_STICKY;
	}

	private void GPSRepeatedly() {
		timer.scheduleAtFixedRate(new TimerTask() {
			public void run() {

				try {

					mlocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
					mlocListener = new MyLocationListener();
					mlocManager.requestLocationUpdates(
							LocationManager.NETWORK_PROVIDER, 0, 0,
							mlocListener);

				} catch (Exception ex) {

					GlobalApp.WriteLogException(ex);
					stopSelf();
				}
			}

		}, 0, (UPDATE_INTERVAL * 60 * 1000));
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (timer != null) {
			timer.cancel();
		}
		if (mlocManager != null || mlocListener != null) {
			mlocManager.removeUpdates(mlocListener);
			mlocManager = null;
		}
		System.gc();

	}

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public class MyLocationListener implements LocationListener

	{

		@Override
		public void onLocationChanged(Location loc)
		{			
			if(mlocManager == null || mlocListener == null)
			{
				return;
			}
			
			
			mlocManager.removeUpdates(mlocListener);
			mlocManager = null;	
		}

		@Override
		public void onProviderDisabled(String provider)

		{

		}

		@Override
		public void onProviderEnabled(String provider)

		{

		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras)

		{

		}
	}

}
