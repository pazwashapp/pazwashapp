package com.segment.pazwash;

import java.io.IOException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import com.segment.pazwash.BL.GlobalApp;

import com.segment.pazwash.Enums.CameraOrientation;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;

import android.graphics.ImageFormat;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.ShutterCallback;
import android.hardware.SensorManager;
import android.os.Bundle;

import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.OrientationEventListener;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.Button;

//import android.widget.Toast;

public class CameraActivity extends BaseActivity implements SurfaceHolder.Callback {
	Camera camera;
	WakeLock wakeLock;
	SurfaceView surfaceView;
	SurfaceHolder surfaceHolder;
	boolean previewing = false;
	LayoutInflater controlInflater = null;
	private Timer timer = new Timer();
	//private OrientationEventListener mOrientationEventListener;
	private boolean isFocusModesAuto = false;
	CameraOrientation mOrientation = CameraOrientation.ORIENTATION_PORTRAIT_NORMAL;
	// public static TicketStatus ticketStatus = TicketStatus.NO_STATUS;
	int CallBackCount = 0;
	Button buttonTakePicture;

	static final int UPDATE_INTERVAL = 1;
	private void writeToLogcat(String msg){
		if (GlobalApp.writeToLogcat)
			Log.i("MainActivity", msg);
	}

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
		PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
		wakeLock = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK, "WakeLock Tag");
		ActivityType = 3;
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.camera);
		getWindow().setFormat(ImageFormat.JPEG);
		surfaceView = (SurfaceView) findViewById(R.id.camerapreview);
		surfaceHolder = surfaceView.getHolder();
		surfaceHolder.addCallback(this);
		surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

		controlInflater = LayoutInflater.from(getBaseContext());
		View viewControl = controlInflater.inflate(R.layout.camera_control, null);
		LayoutParams layoutParamsControl = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
		this.addContentView(viewControl, layoutParamsControl);

		buttonTakePicture = (Button) findViewById(R.id.takepicture);

		buttonTakePicture.setOnClickListener(buttonTakePictureListener);
		loadProgressDialog();
		hideProgress();
		timer.schedule(checkCameraTime, UPDATE_INTERVAL * 30 * 1000);
	}

	private TimerTask checkCameraTime = new TimerTask() {
		@Override
		public void run() {
			CameraActivity.this.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					finish();
				}
			});
		}
	};

	private OnClickListener buttonTakePicturelockedListener = new OnClickListener() {
		public void onClick(View v) {
			try {
				showAlertDialog(R.string.CameraPreview, R.string.CameraMessage, null);
			} catch (Exception e) {
				GlobalApp.WriteLogException(e);
				finish();

			}
		}

	};
	private OnClickListener buttonTakePictureListener = new OnClickListener() {
		public void onClick(View v) {
			try {
				showProgress();
				buttonTakePicture.setClickable(false);
				buttonTakePicture.setEnabled(false);
				if (timer != null) {
					timer.cancel();
				}
				if (CallBackCount < 1) {
					getWindow().addFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
					CallBackCount++;
					/*
					if (isFocusModesAuto) {
						camera.autoFocus(myAutoFocusCallback);
					}
					*/
					//2014-09-03 camera.stopPreview(); 
					previewing = false;

					camera.takePicture(null, null, myPictureCallback_JPG);
					camera.setPreviewCallback(null);
				}
			} catch (Exception e) {
				GlobalApp.WriteLogException(e);
				finish();
			}
		}
	};
	ShutterCallback myShutterCallback = new ShutterCallback() {
		@Override
		public void onShutter() {
			// TODO Auto-generated method stub

		}
	};

	AutoFocusCallback myAutoFocusCallback = new AutoFocusCallback() {
		@Override
		public void onAutoFocus(boolean arg0, Camera arg1) {
			try {

				camera.stopPreview();
				previewing = false;

				//Handler handler = new Handler();
				//handler.postDelayed(new Runnable() {
					//public void run() {
						camera.takePicture(null, null,
								myPictureCallback_JPG);
					//}
				//}, 50);

				camera.setPreviewCallback(null);

			} catch (Exception e)

			{
				camera.release();
				hideProgress();
				GlobalApp.WriteLogException(e);
				// WriteLogException(e);
				finish();
			}
		}
	};

	PictureCallback myPictureCallback_RAW = new PictureCallback() {
		@Override
		public void onPictureTaken(byte[] arg0, Camera arg1) {
			// TODO Auto-generated method stub

		}
	};

	PictureCallback myPictureCallback_JPG = new PictureCallback() {
		@Override
		public void onPictureTaken(final byte[] data, Camera camera) {
			try {
				if (!GlobalApp.IsWithOutLpr()) {
					Intent LpApproveActivity = new Intent(CameraActivity.this, LpApproveActivity.class);
					LpApproveActivity.putExtra("MOrientation", mOrientation.ToInt());
					LpApproveActivity.putExtra("Data", data);
					LpApproveActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(LpApproveActivity);
				} else {
					Intent LpManualActivity = new Intent(CameraActivity.this,
							LpManualActivity.class);
					LpManualActivity.putExtra("MOrientation", mOrientation.ToInt());
					LpManualActivity.putExtra("Data", data);
					LpManualActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(LpManualActivity);
				}

				// }
				// }, 50);

				finish();

			} catch (Exception e) {
				GlobalApp.WriteLogException(e);
				// WriteLogException(e);
				finish();
			}
		}
	};

	private Camera.Size getSmallestPictureSize(Camera.Parameters parameters) {
		try {
			Camera.Size result = null;

			for (Camera.Size size : parameters.getSupportedPictureSizes()) {
				if (result == null) {
					result = size;
				} else {
					int resultArea = result.width * result.height;
					int newArea = size.width * size.height;
					if ((newArea < resultArea) && (newArea >= 307200)) {
						result = size;
					}
				}
			}
			return (result);
		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
			finish();
			return null;
		}
	}

	@Override
	protected void onResume() {
		try {
			super.onResume();

			wakeLock.acquire();
           /*
			if (mOrientationEventListener == null) {
				mOrientationEventListener = new OrientationEventListener(this,
						SensorManager.SENSOR_DELAY_NORMAL) {

					@Override
					public void onOrientationChanged(int orientation) {

						// Toast.makeText(CameraActivity.this, orientation,
						// Toast.LENGTH_SHORT).show();

						// int rotate= 0;
						// Camera.Parameters params = camera.getParameters();
						// determine our orientation based on sensor response
						// int lastOrientation =
						// Integer.parseInt(mOrientation.toString());

						if (orientation >= 315 || orientation < 45) {
							if (mOrientation != CameraOrientation.ORIENTATION_PORTRAIT_NORMAL) {
								mOrientation = CameraOrientation.ORIENTATION_PORTRAIT_NORMAL;
								buttonTakePicture
										.setOnClickListener(buttonTakePictureListener);
								// buttonTakePicture.setVisibility(View.VISIBLE);
							}
						} else if (orientation < 315 && orientation >= 225) {
							if (mOrientation != CameraOrientation.ORIENTATION_LANDSCAPE_NORMAL) {
								//mOrientation = CameraOrientation.ORIENTATION_LANDSCAPE_NORMAL;
								buttonTakePicture.setOnClickListener(buttonTakePicturelockedListener);
								// buttonTakePicture.setVisibility(View.INVISIBLE);
							}
						} else if (orientation < 225 && orientation >= 135) {
							if (mOrientation != CameraOrientation.ORIENTATION_PORTRAIT_INVERTED) {
								//mOrientation = CameraOrientation.ORIENTATION_PORTRAIT_INVERTED;
								buttonTakePicture.setOnClickListener(buttonTakePicturelockedListener);
								// buttonTakePicture.setVisibility(View.INVISIBLE);
							}
						} else { // orientation <135 && orientation > 45
							if (mOrientation != CameraOrientation.ORIENTATION_LANDSCAPE_INVERTED) {
								//mOrientation = CameraOrientation.ORIENTATION_LANDSCAPE_INVERTED;
								buttonTakePicture.setOnClickListener(buttonTakePicturelockedListener);
								// buttonTakePicture.setVisibility(View.INVISIBLE);
							}
						}
						/*
						 * switch (mOrientation) { case 0: rotate = 90; break;
						 * case 1: rotate = 270;
						 * 
						 * break; case 2: rotate = 0; break; case 3: rotate =
						 * 180; break; }
						 */
						// params.set("rotation",rotate);
						// camera.setParameters(params);
						// if (lastOrientation != mOrientation) {
						// changeRotation(mOrientation, lastOrientation);
						// }
			
			/*
					}
				};
			}
			if (mOrientationEventListener.canDetectOrientation()) {
				mOrientationEventListener.enable();
			}
			*/
	     
		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
			finish();
		}
	}

	@Override
	protected void onPause() {
		try {
			super.onPause();
			wakeLock.release();
			//mOrientationEventListener.disable();
		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
			finish();
		}
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

		try {
			// TODO Auto-generated method stub
			if (previewing) {
				camera.stopPreview();
				previewing = false;
			}
			Parameters parameters = camera.getParameters();
			Display display = ((WindowManager) getSystemService(WINDOW_SERVICE)).getDefaultDisplay();
			writeToLogcat("height/width:" + Integer.toString(height) + "/" + Integer.toString(width));
			if (display.getRotation() == Surface.ROTATION_0) {
				parameters.setPreviewSize(height, width);
				camera.setDisplayOrientation(90);
			}
//			if (display.getRotation() == Surface.ROTATION_90) {
//				parameters.setPreviewSize(width, height);
//			}

			//if (display.getRotation() == Surface.ROTATION_180) {
			//}

//			if (display.getRotation() == Surface.ROTATION_270) {
//				parameters.setPreviewSize(width, height);
//				camera.setDisplayOrientation(180);
//			}
			//2014-09-03 camera.setParameters(parameters);
			if (camera != null) {
				camera.startPreview();
				// camera.stopSmoothZoom();
				previewing = true;
			}
		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
			finish();
		}
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		// TODO Auto-generated method stub

		try {

			camera = Camera.open();

			configure(camera);
			try {
				camera.setPreviewDisplay(surfaceHolder);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				GlobalApp.WriteLogException(e);
				// WriteLogException(e);
			}
		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
			finish();
		}
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		try {
			camera.stopPreview();
			camera.setPreviewCallback(null);
			camera.release();
			camera = null;
			previewing = false;
			hideProgress();
		} catch (Exception ex) {
			camera.stopPreview();
			camera.setPreviewCallback(null);
			camera.release();
			camera = null;
			previewing = false;
			GlobalApp.WriteLogException(ex);
			// WriteLogException(ex);
			hideProgress();
		}

	}

	private void configure(Camera camera) {
		try {

			Camera.Parameters params = camera.getParameters();
			// List<Integer> formats = params.getSupportedPictureFormats();

			params.setPictureFormat(ImageFormat.JPEG);
			Camera.Size size = getSmallestPictureSize(params);
			params.setPictureSize(size.width, size.height);

			// Flash
			List<String> flashModes = params.getSupportedFlashModes();
			if (flashModes != null) {
				if (flashModes.contains(Camera.Parameters.FLASH_MODE_OFF)) {
					params.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
				}
			}

			// Scene
            /*
			List<String> sceneModes = params.getSupportedSceneModes();
			if (sceneModes != null) {
				if (sceneModes.contains(Camera.Parameters.SCENE_MODE_ACTION)) {
					params.setSceneMode(Camera.Parameters.SCENE_MODE_ACTION);
				} else {
					params.setSceneMode(Camera.Parameters.SCENE_MODE_AUTO);
				}
			}
			*/

			// Focus
			List<String> focusModes = params.getSupportedFocusModes();
			if (focusModes != null) {
				if (focusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO)) {
					params.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
					isFocusModesAuto = true;
				}
			}

			/*
			SharedPreferences sharedPreferences = getSharedPreferences(
					"PazWashPreferences", MODE_PRIVATE);

			int jpeg_quality = sharedPreferences.getInt("jpeg_quality", 100);
			*/

			params.setJpegQuality(100);

			camera.setParameters(params);
		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
			finish();
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		if (timer != null) {
			timer.cancel();
		}

		unbindDrawables(findViewById(R.id.RootView));
		System.gc();
	}

	private void unbindDrawables(View view) {
		if (view.getBackground() != null) {
			view.getBackground().setCallback(null);
		}
		if (view instanceof ViewGroup) {
			for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
				unbindDrawables(((ViewGroup) view).getChildAt(i));
			}
			((ViewGroup) view).removeAllViews();
		}
	}

}
