package com.segment.pazwash;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.segment.pazwash.BL.GlobalApp;

public class FromScanActivity extends BaseActivity {

	boolean isScanValue = false;
	Button yesBtn;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		ActivityType = 1;

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			isScanValue = extras.getBoolean("isScanValue");
		}
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.fromscan);

		loadProgressDialog();
		
		
		Typeface hebBold = Typeface.createFromAsset(getAssets(), "fonts/droidsanshebrew-bold.ttf");
         
		TextView title = (TextView)findViewById(R.id.ScanTv);
		title.setTypeface(hebBold);
		
		 yesBtn = (Button) findViewById(R.id.btnOK);
		 yesBtn.setTypeface(hebBold);

		if (isScanValue) {
			yesBtn.setOnClickListener(btnYesListener);
		}
		else
		{
			yesBtn.setOnClickListener(btnNoScanListener);
			title.setText(R.string.ScanNOTok);
		}
	}

	private OnClickListener btnYesListener = new OnClickListener() {
		public void onClick(View v) {
			try {
				showProgress();
				
				yesBtn.setClickable(false);
				yesBtn.setEnabled(false);

				Handler handler = new Handler();
				handler.postDelayed(new Runnable() {
					public void run() {

						Intent intentCamera = new Intent(FromScanActivity.this,
								CameraActivity.class);
						startActivity(intentCamera);
						hideProgress();
						finish();
					}
				}, 1000);

			} catch (Exception e) {
				GlobalApp.WriteLogException(e);
			}

		}

	};

	private OnClickListener btnNoScanListener = new OnClickListener() {
		public void onClick(View v) {
			try {

				yesBtn.setClickable(false);
				yesBtn.setEnabled(false);
				openScanActivity();
				finish();

			} catch (Exception e) {
				GlobalApp.WriteLogException(e);
			}

		}

	};

}
