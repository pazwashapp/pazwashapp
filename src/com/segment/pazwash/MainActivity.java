package com.segment.pazwash;

import java.util.Timer;
import java.util.TimerTask;

import com.segment.pazwash.BL.DBAdapter;
import com.segment.pazwash.BL.GPS;
import com.segment.pazwash.BL.GlobalApp;
import com.segment.pazwash.BL.Logger;
import com.segment.pazwash.BL.NetworkChk;
import com.segment.pazwash.DM.ConnectionResult;
import com.segment.pazwash.Enums.ClientExceptionCodeResult;
import com.segment.pazwash.Enums.RequestStatuz;
import com.segment.pazwash.Enums.TimeOutTry;
import com.segment.pazwash.WS.WS;
import com.segment.pazwash.SendOfflineService;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.widget.Toast;

public class MainActivity extends BaseActivity {

	// protected String imagePath;
	static final int UPDATE_INTERVAL = 1;

	private Timer timer = new Timer();
	// private Timer gpsOfflineTimer = new Timer();

	protected Button btnCamera;
	String version = "0";
	TextView versionT;
	String imagePath;
	TextView offlineTv;
	String userName;
	String stationNumber;
	// Button rejectBtn;
	LocationManager mlocManager;
	LocationListener mlocListener;
	boolean IsCameraClick = false;
	boolean IsBarcodeClick = false;
	boolean IsReportsClick = false;
	// boolean IsRejectClick = false;
	Button barcodeBtn;
	Button multipleWashTicketBtn;
	Button reports;
	TextView gpsCheckStatus;
	Button refreshGPS;
	Button GPSActivity;
	Button checkConnection;
	boolean isBtnCheckConnectionClicked = false;

	//sigS
	TelephonyManager        Tel;
    MyPhoneStateListener    MyListener;
    
	//boolean writeToLogcat = true;
	private void writeToLogcat(String msg){
		if (GlobalApp.writeToLogcat)
			Log.i("MainActivity", msg);
	}
	//sigS
	private class MyPhoneStateListener extends PhoneStateListener
    {
      /* Get the Signal strength from the provider, each tiome there is an update */
      @Override
      public void onSignalStrengthsChanged(SignalStrength signalStrength)
      {
         super.onSignalStrengthsChanged(signalStrength);
         GlobalApp.gsmSignalStrength = signalStrength.getGsmSignalStrength();
         //Toast.makeText(getApplicationContext(), "GSM Cinr = " + String.valueOf(signalStrength.getGsmSignalStrength()), Toast.LENGTH_SHORT).show();
      }
      
      @Override
    	public void onDataConnectionStateChanged(int state) {
    		super.onDataConnectionStateChanged(state);
    		if (state==0)
    			GlobalApp.DataConnectionState = "DATA_DISCONNECTED";
    		if (state==1)
    			GlobalApp.DataConnectionState = "DATA_CONNECTING";
    		if (state==2)
    			GlobalApp.DataConnectionState = "DATA_CONNECTED";
    		if (state==3)
    			GlobalApp.DataConnectionState = "DATA_SUSPENDED";
			//GlobalApp.WriteLog("GeneralLog","onDataConnectionStateChanged", GlobalApp.DataConnectionState);

            //Toast.makeText(getApplicationContext(), "DataConnectionState:" + GlobalApp.DataConnectionState, Toast.LENGTH_SHORT).show();
    	}

    };/* End of private Class */

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			SharedPreferences sharedPreferences = getSharedPreferences("PazWashPreferences", MODE_PRIVATE);
			GlobalApp.writeGeneralLog = sharedPreferences.getString("write_General_Log", "");
			GlobalApp.mrkt_msg = sharedPreferences.getString("mrkt_msg", "");
			GlobalApp.mrkt_msg2 = sharedPreferences.getString("mrkt_msg2", "");
			//GlobalApp.mrkt_msg_show = sharedPreferences.getInt("mrkt_msg_show", 1);
			GlobalApp.short_process = sharedPreferences.getInt("short_process", 0);
			boolean is_check_gps = sharedPreferences.getBoolean("is_check_gps", true);
			writeToLogcat("onCreate");
			ActivityType = 1;
			userName = getUserName();
			GlobalApp.setUserName(userName);
			stationNumber = getStationNumber();
			requestWindowFeature(Window.FEATURE_NO_TITLE);
			setContentView(R.layout.main);

			loadProgressDialog();
			hideProgress();
			TimeOutTry tot = GlobalApp.getTimeOutTry();

			Typeface hebBold = Typeface.createFromAsset(getAssets(),"fonts/droidsanshebrew-bold.ttf");
			TextView title = (TextView) findViewById(R.id.forgotTv);
			title.setTypeface(hebBold);
			if (sharedPreferences.getString("BaseUrl", "").equals("http://172.30.4.175:8090/PazWashService.asmx"))
				title.setText(R.string.MainTitleQa);
			else
				title.setText(R.string.MainTitle);

			btnCamera = (Button) findViewById(R.id.camera);
			btnCamera.setOnClickListener(btnCameraListener);
			btnCamera.setTypeface(hebBold);

			versionT = (TextView) findViewById(R.id.versionTv);
			barcodeBtn = (Button) findViewById(R.id.barcodeBtn);
			barcodeBtn.setOnClickListener(btnBarcode);
			barcodeBtn.setTypeface(hebBold);

			multipleWashTicketBtn = (Button) findViewById(R.id.multipleWashTicketBtn);
			multipleWashTicketBtn.setOnClickListener(btnMultipleWashTicketListener);
			multipleWashTicketBtn.setTypeface(hebBold);
			
			reports = (Button) findViewById(R.id.reports);
			reports.setOnClickListener(btnReports);
			reports.setTypeface(hebBold);

			gpsCheckStatus = (TextView) findViewById(R.id.chkGPS);
			gpsCheckStatus.setTypeface(hebBold);

			refreshGPS = (Button) findViewById(R.id.btn_refresh_gps);
			refreshGPS.setOnClickListener(btnRefreshGPSClick);

			GPSActivity = (Button) findViewById(R.id.btn_gps_activity);
			GPSActivity.setOnClickListener(btnGPSActivity);

			TextView userName1 = (TextView) findViewById(R.id.userTv);
			userName1.setText(stationNumber);
			if (!isInLoginTime()) {
				openLogin();
			}
			CleanGlobalApp();
			
			offlineTv = (TextView) findViewById(R.id.OfflineTv);
			if (GlobalApp.IsOffline()) {
				offlineTv.setVisibility(View.VISIBLE);
				Button rejectBtn = (Button) findViewById(R.id.rejectBtn);
				rejectBtn.setText(R.string.MenuCancelDealsTitleOffline);
				rejectBtn.setTypeface(hebBold);
				rejectBtn.setVisibility(View.VISIBLE);
				rejectBtn.setOnClickListener(openRejectActivity);

				checkConnection = (Button) findViewById(R.id.btn_check_connection);
				checkConnection.setVisibility(View.VISIBLE);
				checkConnection.setOnClickListener(btnCheckConnectionClick);

				gpsCheckStatus.setVisibility(View.GONE);
				refreshGPS.setVisibility(View.GONE);
				GPSActivity.setVisibility(View.GONE);
				
				 timer.scheduleAtFixedRate(checkIsOnlineRepeatedlyTask, 0, UPDATE_INTERVAL * 30 * 1000);
			} else {
				offlineTv.setVisibility(View.GONE);
				if (timer != null) {
					timer.cancel();
				}

			}
			if (is_check_gps) {
				checkGPS();
			} else {
				gpsCheckStatus.setVisibility(View.GONE);
				refreshGPS.setVisibility(View.GONE);
				GPSActivity.setVisibility(View.GONE);
			}

			setVersion();
			GlobalApp.setCountDhe(0);
			if (is_check_gps) {
				gps = GPS.getGPS(MainActivity.this);
				long gpsTime = sharedPreferences.getLong("GPSTime", 0);
				int TIME_TO_UPDATE = sharedPreferences.getInt("gps_refresh_interval", 15);

				if ((System.currentTimeMillis() - gpsTime) >= (TIME_TO_UPDATE * 60 * 1000) || gpsTime == 0
						|| gps.getLon().equals("0.0") || gps.getLat().equals("0.0")) {
					gpsCheckStatus.setText(R.string.NoGPSStatus);
					checkGPSMain();
				} else {
					gpsCheckStatus.setText(R.string.GPSStatus);
				}
			}
			if (GlobalApp.getTimeOutTry() != TimeOutTry.TIMEOUT_TRY_OFFLINE) {
				InitServices();
			}
			InitLogService();
			//sigS
			MyListener = new MyPhoneStateListener();
			Tel = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
			Tel.listen(MyListener ,PhoneStateListener.LISTEN_SIGNAL_STRENGTHS|PhoneStateListener.LISTEN_DATA_CONNECTION_STATE);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			GlobalApp.WriteLogException(e);
		}
	}

	
	@Override
	protected void onResume() {
		super.onResume();
		writeToLogcat("onResume");
		Tel.listen(MyListener,PhoneStateListener.LISTEN_SIGNAL_STRENGTHS|PhoneStateListener.LISTEN_DATA_CONNECTION_STATE);
		//P InitLogService();
	}
	
	//sigS
	@Override
	protected void onPause() {
		super.onPause();
		Tel.listen(MyListener, PhoneStateListener.LISTEN_NONE);
	}
	
	private void InitLogService(){
		try {
			int logServiceState = GlobalApp.getLogServiceState();

			Logger lg = new Logger(this);
			lg.open();
			int countExc = lg.getCountOffline();
			lg.close();
			if (NetworkChk.checkInternet(MainActivity.this, GlobalApp.getIsVPN())) {
				writeToLogcat("beforeDecideLogService:" + Integer.toString(logServiceState) + ":" + Integer.toString(countExc));
				if (logServiceState == 0) {
					writeToLogcat("afterDecideLogService");
					//p SharedPreferences sharedPreferences = getSharedPreferences("PazWashPreferences", MODE_PRIVATE);
					//p int number_of_log_rows = sharedPreferences.getInt("number_of_log_rows", 30);
					//p if (countExc > 0 && number_of_log_rows>0) {
						writeToLogcat("startLogService");
						startService(new Intent(getBaseContext(), SendLogsService.class));
						GlobalApp.setLogServiceState(1);
					//p}
				} /*//p else {
					if (countExc == 0) {
						writeToLogcat("stopLogService");
						stopService(new Intent(MainActivity.this, SendLogsService.class));
						GlobalApp.setLogServiceState(0);
					}
				}*/
			}
		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
		}
	}
	
	private void InitServices() {
		try {
			DBAdapter db = new DBAdapter(this);
			db.open();
			db.deleteTrueTickets();
			int countOffline = db.getCountOffline();
			db.close();

			int serviceState = GlobalApp.getServiceState();
			int logServiceState = GlobalApp.getLogServiceState();

			Logger lg = new Logger(this);
			lg.open();
			int countExc = lg.getCountOffline();
			lg.close();
			if (NetworkChk.checkInternet(MainActivity.this, GlobalApp.getIsVPN())) {
				//Service send offline data
				if (serviceState == 0) {
					if (countOffline > 0) {
						startService(new Intent(getBaseContext(), SendOfflineService.class));
						GlobalApp.setServiceState(1);
					}
				} else {
					if (countOffline == 0) {
						stopService(new Intent(MainActivity.this, SendOfflineService.class));
						GlobalApp.setServiceState(0);
					}
				}
				//Service send log
				writeToLogcat("beforeDecideLogService");
				if (logServiceState == 0) {
					SharedPreferences sharedPreferences = getSharedPreferences("PazWashPreferences", MODE_PRIVATE);
					int number_of_log_rows = sharedPreferences.getInt("number_of_log_rows", 30);
					if (countExc > 0 && number_of_log_rows>0) {
						writeToLogcat("startLogService");
						startService(new Intent(getBaseContext(), SendLogsService.class));
						GlobalApp.setLogServiceState(1);
					}
				} else {
					if (countExc == 0) {
						writeToLogcat("stopLogService");
						stopService(new Intent(MainActivity.this, SendLogsService.class));
						GlobalApp.setLogServiceState(0);
					}
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			GlobalApp.WriteLogException(e);
		}
	}

	private void setVersion() {
		try {
			version = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
			GlobalApp.setVersionApp(version);
			versionT.setText(version);
			//versionT.setText(String.format("U:%s S:%s V:%s", userName, stationNumber, version));
			
		} catch (Exception ex) {
			GlobalApp.WriteLogException(ex);
			// WriteLogException(ex);
		}
	}

	private DialogInterface.OnClickListener checkConnectionSuccessfull = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			try {
				GlobalApp.setIsOffline(false);
				GlobalApp.setIsWithOutLpr(false);
				GlobalApp.setIsServerOK(true);
				openMain();

			}

			catch (Exception e) {
				GlobalApp.WriteLogException(e);
			}
		}
	};

	private OnClickListener btnCheckConnectionClick = new OnClickListener() {
		public void onClick(View v) {

			checkConnection.setClickable(false);
			checkConnection.setEnabled(false);

			/*
			 * if (timer != null) { timer.cancel(); }
			 */
			showProgress();

			if (NetworkChk.checkInternet(MainActivity.this, false)) {
				new checkConnectionWithServer(false).execute(PAZWASH_WS_URL
						+ "/CheckConnection");
			}

			else {

				hideProgress();
				showAlertDialog(R.string.CheckConnectionDialog,
						R.string.NoTriG, null);

			}

			Handler handler = new Handler();
			handler.postDelayed(new Runnable() {
				public void run() {

					checkConnection.setClickable(true);
					checkConnection.setEnabled(true);

				}
			}, 3000);
		}

	};

	private OnClickListener btnGPSActivity = new OnClickListener() {
		public void onClick(View v) {

			GPSActivity.setClickable(false);
			GPSActivity.setEnabled(false);

			openGPS();

			Handler handler = new Handler();
			handler.postDelayed(new Runnable() {
				public void run() {

					GPSActivity.setClickable(true);
					GPSActivity.setEnabled(true);

				}
			}, 3000);
		}

	};

	private OnClickListener btnRefreshGPSClick = new OnClickListener() {
		public void onClick(View v) {

			refreshGPS.setClickable(false);
			refreshGPS.setEnabled(false);

			checkGPSMain();

			Handler handler = new Handler();
			handler.postDelayed(new Runnable() {
				public void run() {

					refreshGPS.setClickable(true);
					refreshGPS.setEnabled(true);

				}
			}, 3000);
		}

	};

	private OnClickListener btnBarcode = new OnClickListener() {
		public void onClick(View v) {

			GlobalApp.setWithBarcode(true);

			if (!IsBarcodeClick) {
				barcodeBtn.setEnabled(false);
				setClickTrue();
				Handler handler = new Handler();
				handler.postDelayed(new Runnable() {
					public void run() {
						barcodeBtn.setEnabled(true);
						setClickFalse();

					}
				}, 3000);

				openScanBarcode();
			}

		}
	};

	private OnClickListener btnReports = new OnClickListener() {
		public void onClick(View v) {

			if (!IsReportsClick) {
				reports.setEnabled(false);
				setClickTrue();
				Handler handler = new Handler();
				handler.postDelayed(new Runnable() {
					public void run() {
						reports.setEnabled(true);
						setClickFalse();

					}
				}, 1000);

				openReports();
			}
		}
	};

	@Override
	public void onBackPressed() {
		Dialog dialog;
		AlertDialog.Builder builder;
		builder = new AlertDialog.Builder(MainActivity.this);
		builder.setMessage(R.string.ExitDialog).setCancelable(true).setNegativeButton(R.string.No, null).setPositiveButton(R.string.YesC, new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								android.os.Process.killProcess(android.os.Process.myPid());
							}
						});
		dialog = builder.create();
		dialog.show();

	}

	
	private OnClickListener btnCameraListener = new OnClickListener() {
		public void onClick(View v) {
			GlobalApp.setWithBarcode(false);
			if (!IsCameraClick) {
				btnCamera.setEnabled(false);
				setClickTrue();
				Handler handler = new Handler();
				handler.postDelayed(new Runnable() {
														public void run() {
															btnCamera.setEnabled(true);
															setClickFalse();
														}
													},
									3000);
				openCamera();
			}
		}

	};

	private OnClickListener btnMultipleWashTicketListener = new OnClickListener() {
		public void onClick(View v) {
			Intent intentMultipleWashTicket = new Intent(getBaseContext(), MultipleWashTicket.class);
			startActivity(intentMultipleWashTicket);
		}

	};
	
	
	public void setClickFalse() {
		IsBarcodeClick = false;
		IsReportsClick = false;
		// IsRejectClick = false;
		IsCameraClick = false;
		// reports.setEnabled(true);
		btnCamera.setClickable(true);
		// btnCamera.setEnabled(true);
		// rejectBtn.setClickable(true);
		barcodeBtn.setClickable(true);
		reports.setClickable(true);
		// rejectBtn.setEnabled(true);
		// barcodeBtn.setEnabled(true);
	}

	public void setClickTrue() {
		IsBarcodeClick = true;
		IsReportsClick = true;
		// IsRejectClick = true;
		IsCameraClick = true;
		barcodeBtn.setClickable(false);
		// barcodeBtn.setEnabled(false);
		reports.setClickable(false);
		// rejectBtn.setClickable(false);
		btnCamera.setClickable(false);
		// btnCamera.setEnabled(false);
		// rejectBtn.setEnabled(false);
		// reports.setEnabled(false);
	}

	private OnClickListener openRejectActivity = new OnClickListener() {
		public void onClick(View v) {
			/*
			 * if (!IsRejectClick) { rejectBtn.setEnabled(false);
			 * setClickTrue(); Handler handler = new Handler();
			 * handler.postDelayed(new Runnable() { public void run() {
			 * rejectBtn.setEnabled(true); setClickFalse();
			 * 
			 * } }, 1000);
			 * 
			 * if (!GlobalApp.IsOffline()) { Intent rejectActivity = new
			 * Intent(MainActivity.this, RejectActivity.class);
			 * startActivity(rejectActivity); } else {
			 */
			Intent rejectActivity = new Intent(MainActivity.this,
					RejectOfflineActivity.class);
			startActivity(rejectActivity);
			// }
			// }

		}
	};

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (timer != null) {
			timer.cancel();
		}
		/*
		 * if (gpsOfflineTimer != null) { gpsOfflineTimer.cancel(); }
		 */
		if (mlocManager != null || mlocListener != null) {

			mlocManager.removeUpdates(mlocListener);
			mlocManager = null;
			mlocListener = null;
		}

		System.gc();
	}

	private TimerTask checkIsOnlineRepeatedlyTask = new TimerTask() {
		@Override
		public void run() {
			MainActivity.this.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					try {

						// if (GlobalApp.getGPSOffline()) {

						if (NetworkChk.checkInternet(MainActivity.this, false)) {
							new checkConnectionWithServer(true)
									.execute(PAZWASH_WS_URL
											+ "/CheckConnection");
						}

						// }

					} catch (Exception ex) {

						GlobalApp.WriteLogException(ex);

					}
				}
			});
		}
	};

	private void checkGPSMain() {

		// if (NetworkChk.checkInternet(MainActivity.this,
		// GlobalApp.getIsVPN())) {

		try {
			if (mlocManager == null && mlocListener == null) {

				// GlobalApp.setGPSOffline(false);
				// gpsOfflineTimer.scheduleAtFixedRate(checkGPSOfflienTask,
				// 0,UPDATE_INTERVAL * 60 * 1000);

				gpsCheckStatus.setText(R.string.GPSStatus);

				mlocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

				mlocListener = new MyLocationListener();

				mlocManager.requestLocationUpdates(
						LocationManager.GPS_PROVIDER, 0, 0, mlocListener);
			}

		} catch (Exception ex) {

			GlobalApp.WriteLogException(ex);

		}
		// }

	}

	public class MyLocationListener implements LocationListener

	{

		@Override
		public void onLocationChanged(Location loc)

		{
			try {

				if (mlocManager == null || mlocListener == null) {
					return;
				}

				// GlobalApp.setGPSOffline(true);

				SharedPreferences sharedPreferences = getSharedPreferences(
						"PazWashPreferences", MODE_PRIVATE);

				SharedPreferences.Editor prefEditor = sharedPreferences.edit();

				prefEditor.putLong("GPSTime", System.currentTimeMillis());

				prefEditor.commit();

				mlocManager.removeUpdates(mlocListener);
				mlocManager = null;
				mlocListener = null;
				hideProgress();
			} catch (Exception e) {
				hideProgress();
				GlobalApp.WriteLogException(e);
				mlocManager.removeUpdates(mlocListener);
				mlocListener = null;
				mlocManager = null;

			}

		}

		@Override
		public void onProviderDisabled(String provider)

		{

		}

		@Override
		public void onProviderEnabled(String provider)

		{

		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras)

		{

		}
	}

	private class checkConnectionWithServer extends
			AsyncTask<String, Void, ConnectionResult> {
		boolean isTimerTask = false;

		protected ConnectionResult doInBackground(String... urls) {
			WS ws = new WS();

			return ws.CheckConnection(urls[0]);
		}

		checkConnectionWithServer(boolean isTimer) {
			this.isTimerTask = isTimer;
		}

		protected void onPostExecute(ConnectionResult result) {

			if (!this.isTimerTask) {

				try {

					if (result.getRequestStatus().RequestStatusCode()
							.equals(RequestStatuz.WSGW_OK.toString())) {
						if (result.getClientClientExcCode().equals(
								ClientExceptionCodeResult.RESULT_OK.toString())) {
							if (result.IsConnected()) {

								isBtnCheckConnectionClicked = true;
								hideProgress();
								
								showAlertDialog(R.string.CheckConnectionDialog,
										R.string.ConnectionSuccessfull,
										checkConnectionSuccessfull);
								// if (GlobalApp.getIsVPN() == true) {

								// }

							} else {
								if (!isFinishing()) {

									hideProgress();

									showAlertDialog(
											R.string.CheckConnectionDialog,
											R.string.NoConnectionWithServer,
											null);
								}

							}

						} else {
							if (result.getClientClientExcCode().equals(
									ClientExceptionCodeResult.RESULT_ERROR
											.toString())) {

								hideProgress();
								if (!isFinishing()) {

									showAlertDialog(
											R.string.CheckConnectionDialog,
											R.string.NoConnectionWithServer,
											null);
								}

							}
							if (result.getClientClientExcCode().equals(
									ClientExceptionCodeResult.RESULT_TIMEOUT
											.toString())) {

								hideProgress();

								if (!isFinishing()) {
									showAlertDialog(
											R.string.CheckConnectionDialog,
											R.string.NoConnectionWithServer,
											null);
								}
							}
						}
					} else {
						hideProgress();

						showAlertDialog(R.string.Exception, result
								.getRequestStatus().RequestStatusCode(), null);
					}

				} catch (Exception e) {
					GlobalApp.WriteLogException(e);

					hideProgress();

				}
			} else {

				try {

					if (result.getRequestStatus().RequestStatusCode()
							.equals(RequestStatuz.WSGW_OK.toString())) {
						if (result.getClientClientExcCode().equals(
								ClientExceptionCodeResult.RESULT_OK.toString())) {
							if (result.IsConnected()) {

								if (!isBtnCheckConnectionClicked) {

									GlobalApp.setIsOffline(false);
									GlobalApp.setIsWithOutLpr(false);
									GlobalApp.setIsServerOK(true);
									openMain();
								}
							}
						}
					}

				}

				catch (Exception e) {
					GlobalApp.WriteLogException(e);

				}
			}

		}

	}

}