package com.segment.pazwash;

import java.util.GregorianCalendar;

import org.json.JSONArray;
import org.json.JSONObject;

import com.segment.pazwash.BL.BBStepWriter;
import com.segment.pazwash.BL.GlobalApp;
import com.segment.pazwash.BL.JSONSharedPreferences;
import com.segment.pazwash.DM.AddItems;
import com.segment.pazwash.DM.ApproveResult;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class DealApproveActivity extends BaseActivity {
	ApproveResult approveResult;
	TextView numberDealTv;
	TextView lastWashDateTv;
	TextView monthWashCountTv;
	// TextView carType;
	Button approveBtn;
	boolean isApproveClick = false;
	TextView washTypeTv;
	TextView selectedProdTv;
	
	// String serviceCode;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		ActivityType = 1;
		userName = getUserName();

		approveResult = GlobalApp.getApproveResult();
		/*
		 * Bundle extras = getIntent().getExtras(); if (extras != null) {
		 * 
		 * serviceCode = extras.getString("ServiceCode");
		 * approveResult.setDealNumber(extras.getString("DealNumber"));
		 * approveResult.setDbId(extras.getString("DBID"));
		 * approveResult.setMonthWashCount(extras.getString("MonthWashCount"));
		 * approveResult.setLastWashDate(extras.getString("LastWashDate"));
		 * approveResult.setIsApprove(true); }
		 */
		setContentView(R.layout.deal_approve);

		loadProgressDialog();
		hideProgress();
		
		Typeface hebBold = Typeface.createFromAsset(getAssets(), "fonts/droidsanshebrew-bold.ttf");		
		TextView title =  (TextView) findViewById(R.id.forgotTv);
		title.setTypeface(hebBold);
		
		
		TextView carNumberLb =  (TextView) findViewById(R.id.carNumberLTv);
		carNumberLb.setTypeface(hebBold);
		
		TextView carNumber =  (TextView) findViewById(R.id.carNumberTv);
		carNumber.setText(GlobalApp.getLPResult().CarNumber());
		

		approveBtn = (Button) findViewById(R.id.approveBtn);
		approveBtn.setOnClickListener(approveBtnListener);
		approveBtn.setTypeface(hebBold);
		
		TextView ndl = (TextView) findViewById(R.id.numberDealLTv);
		ndl.setTypeface(hebBold);
		
		numberDealTv = (TextView) findViewById(R.id.numberDealTv);
		//numberDealTv.setTypeface(hebBold);
		TextView lvd = (TextView) findViewById(R.id.lastWashDateLTv);
		lvd.setTypeface(hebBold);
		
		
		lastWashDateTv = (TextView) findViewById(R.id.lastWashDateTv);
		//lastWashDateTv.setTypeface(hebBold);
		
		
		
		TextView typingLimitBalance = (TextView) findViewById(R.id.TypingLimitBalanceTv);
		
		
		TextView typingLimitBalanceLTv = (TextView) findViewById(R.id.TypingLimitBalanceLTv);
		typingLimitBalanceLTv.setTypeface(hebBold);
		
		TextView mvc = (TextView) findViewById(R.id.monthWashCountLTv);
				
		mvc.setTypeface(hebBold);
		
		monthWashCountTv = (TextView) findViewById(R.id.monthWashCountTv);
		//monthWashCountTv.setTypeface(hebBold);
		// carType = (TextView) findViewById(R.id.carTypeTv);
		washTypeTv = (TextView) findViewById(R.id.washTypeTv);
		washTypeTv.setTypeface(hebBold);
		
		TextView  wtv = (TextView) findViewById(R.id.washTypeLTv);
		wtv.setTypeface(hebBold);
		

		numberDealTv.setText(approveResult.DealNumber());
		lastWashDateTv.setText(approveResult.LastWashDate());
		
		// carType.setText(approveResult.getCarType());
		
		String tpbStr = Integer.toString(approveResult.getStationTypingLimitBalance());
		typingLimitBalance.setText(tpbStr);
		
		washTypeTv.setText(approveResult.getWashType());
				
		LinearLayout layout9 = (LinearLayout) findViewById(R.id.LinearLayout9);
		LinearLayout layout2 = (LinearLayout) findViewById(R.id.linearLayout2);
		LinearLayout layout6 = (LinearLayout) findViewById(R.id.linearLayout6);
		selectedProdTv = (TextView) findViewById(R.id.selectedProdTv);
		
		
		
		//Get product name by its code
		try {
			JSONArray jItems = JSONSharedPreferences.loadJSONArray(getBaseContext(), "generalProducts", "");
			for (int i = 0; i < jItems.length(); i++) {
				JSONObject jO = jItems.getJSONObject(i);
				
				if(Integer.parseInt(approveResult.getServiceCode()) == jO.getInt("Code")){
					selectedProdTv.setText(jO.getString("Desc"));
					break;
				}
				
				
			}
		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
		}	
		
		if(selectedProdTv.getText().length() > 0){
			layout9.setVisibility(View.VISIBLE);
			//Position layout6 bellow layout9
			android.widget.RelativeLayout.LayoutParams lParams1 = (android.widget.RelativeLayout.LayoutParams) layout6.getLayoutParams();
			lParams1.addRule(RelativeLayout.BELOW,layout9.getId());
			layout6.setLayoutParams(lParams1);
		}else{
			layout9.setVisibility(View.GONE);
		}
		
		
		if(GlobalApp.getIsDWASH()){
					
			layout2.setVisibility(View.GONE);								
					
		}else{			
			
			monthWashCountTv.setText(approveResult.MonthWashCount());
			layout2.setVisibility(View.VISIBLE);		
			android.widget.RelativeLayout.LayoutParams lParams = (android.widget.RelativeLayout.LayoutParams) layout9.getLayoutParams();
			lParams.addRule(RelativeLayout.BELOW,layout2.getId());
			layout9.setLayoutParams(lParams);
		}

	}

	private OnClickListener approveBtnListener = new OnClickListener() {
		public void onClick(View v) {
			try {

				showProgress();
				if (!isApproveClick) {
					isApproveClick = true;
					approveBtn.setClickable(false);
					approveBtn.setEnabled(false);
					Handler handler = new Handler();
					handler.postDelayed(new Runnable() {
						public void run() {
							isApproveClick = false;
							approveBtn.setClickable(true);
							approveBtn.setEnabled(true);

						}
					}, 3000);

					SendBBS();
					//Peter 10/05/2016
					//if (GlobalApp.mrkt_msg_show != 1 || GlobalApp.getIsDWASH())
						openMain();
					//else {
					//	Intent LpMarketingIntent = new Intent(DealApproveActivity.this, MarketingActivity.class);
					//	LpMarketingIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					//	startActivity(LpMarketingIntent);
					//}
					//Peter 10/05/2016
				}

			} catch (Exception e) {
				GlobalApp.WriteLogException(e);

			}

		}
	};

	@Override
	public void onBackPressed() {
		approveBtn.performClick();
	}

	private void SendBBS() {

		if (!GlobalApp.IsOffline()) {
			BBStepWriter bbs = new BBStepWriter(this);
			try {
				bbs.open();
				boolean result = bbs.deleteSendedBB();
				bbs.close();
				bbs.sendBB();

			} catch (Exception e) {
				GlobalApp.WriteLogException(e);
				// logger.DeleteSendedLogs();
			}
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		System.gc();
	}
}