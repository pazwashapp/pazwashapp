package com.segment.pazwash;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.segment.pazwash.BL.GlobalApp;




import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;

import android.net.ParseException;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

public class ReportsMenu extends BaseActivity {
	static final int DATE_DIALOG_ID = 999;

	private int year;
	private int month;
	private int day;
	String selectedDate;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		ActivityType = 1;

		requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.reportsmenu);

	
		
		
		loadProgressDialog();
		
		
		Typeface hebBold = Typeface.createFromAsset(getAssets(),
				"fonts/droidsanshebrew-bold.ttf");
		
	   Button dayReport = (Button) findViewById(R.id.dayreport);
	   dayReport.setTypeface(hebBold);
		
		Button lastWash = (Button) findViewById(R.id.lastWash);
		lastWash.setTypeface(hebBold);
		Button Summary = (Button) findViewById(R.id.summary);
		Summary.setTypeface(hebBold);
	    Button purchases = (Button) findViewById(R.id.purchases);
	    purchases.setTypeface(hebBold);

		TextView reportsTv = (TextView) findViewById(R.id.reportsTv);
		reportsTv.setTypeface(hebBold);

	
		
		
		final Calendar c = Calendar.getInstance();
		year = c.get(Calendar.YEAR);
		month = c.get(Calendar.MONTH);
		day = c.get(Calendar.DAY_OF_MONTH);

		
		
		dayReport.setOnClickListener(dayReportClick);
		lastWash.setOnClickListener(lastWashClick);
		Summary.setOnClickListener(summaryClick);
		purchases.setOnClickListener(purchaseClick);

	}
	
	private OnClickListener purchaseClick = new OnClickListener() {
		public void onClick(View v) {

			Intent purchaseReport = new Intent(
					ReportsMenu.this,
					ReportPurchases.class);
		
			startActivity(purchaseReport);

		}
	};
	
	
	private OnClickListener summaryClick = new OnClickListener() {
		public void onClick(View v) {

			Intent dayReport = new Intent(
					ReportsMenu.this,
					ReportSummary.class);
		
			startActivity(dayReport);

		}
	};
	
	
	
	private OnClickListener lastWashClick = new OnClickListener() {
		public void onClick(View v) {

			Intent dayReport = new Intent(
					ReportsMenu.this,
					ReportLastWash.class);
		
			startActivity(dayReport);

		}
	};
	
	

	private OnClickListener dayReportClick = new OnClickListener() {
		public void onClick(View v) {

			showDialog(DATE_DIALOG_ID);

		}
	};

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_DIALOG_ID:
			// set date picker as current date
			return new DatePickerDialog(ReportsMenu.this, datePickerListener, year, month,
					day);
		}
		return null;
	}

	private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

		// when dialog box is closed, below method will be called.
		public void onDateSet(DatePicker view, int selectedYear,
				int selectedMonth, int selectedDay) {
			year = selectedYear;
			month = selectedMonth;
			day = selectedDay;

			// set selected date into textview
			
			StringBuilder sb = new StringBuilder();
			//sb.append(month + 1).append("-").append(day).append("-").append(year).append(" ");
						sb.append(year).append("-").append(month + 1).append("-").append(day);
			selectedDate = sb.toString();			
			
			   SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			    Date convertedDate = new Date();
			    try {
			        convertedDate = dateFormat.parse(selectedDate);
			        selectedDate  =  dateFormat.format(convertedDate);
			    } catch (ParseException e) {
			        // TODO Auto-generated catch block
			    	GlobalApp.WriteLogException(e);
			    } catch (java.text.ParseException e) {
					// TODO Auto-generated catch block
			    	GlobalApp.WriteLogException(e);
				}
			
			
			Intent dayReport = new Intent(
					ReportsMenu.this,
					ReportSelectedDate.class);
			dayReport.putExtra("selectedDate", selectedDate);
			startActivity(dayReport);
			


		}
	};
	
	


}
