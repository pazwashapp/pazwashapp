package com.segment.pazwash.BL;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.segment.pazwash.R;
import com.segment.pazwash.DM.AddItems;
import com.segment.pazwash.DM.DeleteItem;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.segment.pazwash.BL.JSONSharedPreferences;

public class RejectOfflineAdapter extends BaseAdapter {

	// Context current_context;
	private static List<DeleteItem> deleteList;

	private LayoutInflater mInflater;
	ArrayList<AddItems> generalItems = new ArrayList<AddItems>();
	Typeface hebBold;

	public RejectOfflineAdapter(Context context, List<DeleteItem> results,Typeface b) {

		try {
			
			hebBold = b;
			deleteList = results;
			mInflater = LayoutInflater.from(context);

			JSONArray jItems = JSONSharedPreferences.loadJSONArray(
					mInflater.getContext(), "generalProducts", "");

			for (int i = 0; i < jItems.length(); i++) {
				JSONObject jO = jItems.getJSONObject(i);

				AddItems ad = new AddItems();

				ad.setItemCode(jO.getInt("Code"));
				ad.setItemDesc(jO.getString("Desc"));
				ad.setItemSort(jO.getInt("Sort"));
				ad.setItemIsMain(jO.getInt("Main"));
				generalItems.add(ad);

			}

		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
		}

	}

	public int getCount() {
		return deleteList.size();
	}

	public Object getItem(int position) {
		return deleteList.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		try {
			ViewHolder holder;

			if (convertView == null) {
				convertView = mInflater.inflate(R.drawable.reject_offline_view,
						null);
				holder = new ViewHolder();
				holder.txtWashDesc = (TextView) convertView
						.findViewById(R.id.WashDescTv);
				holder.txtCarNumber = (TextView) convertView
						.findViewById(R.id.carNumberTv);
				holder.txtData = (TextView) convertView
						.findViewById(R.id.dataTv);
				
				holder.WashDescLTv =(TextView) convertView.findViewById(R.id.WashDescLTv);
				holder.Car =(TextView) convertView.findViewById(R.id.Car);
				holder.Data =(TextView) convertView.findViewById(R.id.Data); 
				
				//holder.txtData.setTypeface(hebBold);
				//holder.txtCarNumber.setTypeface(hebBold);
				holder.txtWashDesc.setTypeface(hebBold);
				holder.WashDescLTv.setTypeface(hebBold);
				holder.Car.setTypeface(hebBold);
				holder.Data.setTypeface(hebBold);
				

				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			int service_code = Integer.parseInt(deleteList.get(position)
					.ServiceCode());
			String service_desc = "";

			for (int i = 0; i < this.generalItems.size(); i++) {
				if (service_code == generalItems.get(i).ItemCode()) {
					service_desc = generalItems.get(i).ItemDesc();
					break;
				}
			}

			holder.txtWashDesc.setText(service_desc);
			// holder.txtWashDesc.setText("dfsdafgsd");
			holder.txtCarNumber.setText(deleteList.get(position).CarNumber());
			holder.txtData.setText(deleteList.get(position).WashDate());
		}

		catch (Exception e) {
			GlobalApp.WriteLogException(e);
		}
		return convertView;
	}

	static class ViewHolder {
		
		TextView WashDescLTv;
		TextView Car;
		TextView Data;
		
		TextView txtWashDesc;
		TextView txtCarNumber;
		TextView txtData;
	}

}
