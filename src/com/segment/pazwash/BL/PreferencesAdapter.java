package com.segment.pazwash.BL;

import java.util.List;

import android.content.Context;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.segment.pazwash.R;

import com.segment.pazwash.DM.ReportSummaryItem;

public class PreferencesAdapter extends BaseAdapter {

	Typeface hebBold;
	private static List<ReportSummaryItem> summaryList;

	private LayoutInflater mInflater;

	public PreferencesAdapter(Context context, List<ReportSummaryItem> results,
			Typeface b) {
		summaryList = results;
		mInflater = LayoutInflater.from(context);
		hebBold = b;
	}

	public int getCount() {
		return summaryList.size();
	}

	public Object getItem(int position) {
		return summaryList.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		convertView = null;
		if (convertView == null) {
			convertView = mInflater.inflate(R.drawable.preferences_item, null);
			holder = new ViewHolder();
			holder.txtTitle = (TextView) convertView
					.findViewById(R.id.PreferencesTitle);
			holder.txtValue = (TextView) convertView
					.findViewById(R.id.PreferencesValue);

	
			holder.txtTitle.setTypeface(hebBold);
			holder.txtTitle.setText(summaryList.get(position).getTitle());
			holder.txtValue.setText(summaryList.get(position).getValue());

			if (holder.txtTitle.getText().equals(
					mInflater.getContext().getString(R.string.WashByDefault))) {
				holder.txtValue.setTypeface(hebBold);
				holder.txtValue.setGravity(Gravity.LEFT);

			}

			if (holder.txtTitle.getText().equals(
					mInflater.getContext().getString(R.string.Sviva))) {
				holder.txtValue.setTypeface(hebBold);
				holder.txtValue.setGravity(Gravity.LEFT);

			}

			convertView.setTag(holder);
		}

		return convertView;
	}

	static class ViewHolder {
		TextView txtValue;
		TextView txtTitle;

	}
}
