package com.segment.pazwash.BL;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import com.segment.pazwash.DM.DataParameter;
import com.segment.pazwash.DM.DeleteItem;
import com.segment.pazwash.DM.SQLiteWSResult;
import com.segment.pazwash.Enums.ClientExceptionCodeResult;
import com.segment.pazwash.Enums.RequestStatuz;
import com.segment.pazwash.WS.SqlLiteWS;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

public class DBAdapter {

	protected static boolean isEmpty;
	// Integer counter = 0;
	// Integer Count = 0;
	public static String isServerGet;
	String Wash_Id_Data;
	String Car_Number_Data;
	String Image_Path_Data;
	String Ticket;
	String Insert_Date;
	String Gps_Lat_Data;
	String Gps_Lon_Data;
	String User_Name_Data;
	String Service_Code_Data;
	String Add_Item_Data;
	String Device_Id_Data;
	String MD_Code_Data;
	String Station_Number_Data;
	String Station_Id_Data;
	String QrCode_Value_Data;
	String LP_Scan_Type_Data;
	String Version_App_Data;

	String serverURL = "";

	Integer Id;
	String IMG;
	protected boolean isData;
	static final String KEY_ROWID = "id";
	static final String KEY_WASH_ID = "Wash_Id";
	static final String KEY_CAR_NUMBER = "Car_Number";
	static final String KEY_IMAGE_PATH = "Image_Path";
	static final String KEY_TICKET = "Ticket";
	static final String KEY_INSERT_DATE = "Insert_Date";
	static final String KEY_USER_NAME = "User_Name";
	static final String KEY_GPS_LAT = "GPS_Lat";
	static final String KEY_GPS_LON = "GPS_Lon";
	static final String KEY_SERVICE_CODE = "service_code";
	static final String KEY_ADD_ITEM = "Add_Item";
	static final String KEY_DEVICE_ID = "Device_Id";
	static final String KEY_MD_CODE = "MD_Code";
	static final String KEY_STATION_NUMBER = "Station_Number";
	static final String KEY_STATION_ID = "Station_Id";
	static final String KEY_QR_CODE_VALUE = "QrCode_Value";
	static final String KEY_LP_SCAN_TYPE = "LP_Scan_Type";
	static final String KEY_VERSION_APP = "Version_App";

	static final String TAG = "DBAdapter";
	static final String DATABASE_NAME = "paz_offline.db";
	static final String DATABASE_TABLE = "data_offline";
	static final int DATABASE_VERSION = 1;
	static final String DATABASE_CREATE = "create table data_offline (id integer primary key autoincrement, "
			+ "Wash_Id text not null,Car_Number text not null, Image_Path text not null,Ticket text not null,Insert_Date text not null,GPS_Lon text not null,GPS_Lat text not null,User_Name text not null,service_code text not null,Add_Item text not null,"
			+ "Device_Id text not null, MD_Code text not null, Station_Number text not null, Station_Id text not null, QrCode_Value text not null, LP_Scan_Type text not null, Version_App text not null);";
	final Context context;
	// final Activity activity;
	DatabaseHelper DBHelper;
	SQLiteDatabase db;

	public DBAdapter(Context ctx) {
		this.context = ctx;
		DBHelper = new DatabaseHelper(context);
	}

	/*
	 * public DBAdapter(Context ctx, Activity act) { this.context = ctx;
	 * this.activity = act; DBHelper = new DatabaseHelper(context); }
	 */
	private static class DatabaseHelper extends SQLiteOpenHelper {
		DatabaseHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db)

		{
			try {
				db.execSQL(DATABASE_CREATE);
			} catch (SQLException e) {

				// GlobalApp.WriteLogException(e);
			}
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
					+ newVersion + ", which will destroy all old data");
			db.execSQL("DROP TABLE IF EXISTS data_bb_steps");
			db.execSQL("DROP TABLE IF EXISTS data_offline");
			db.execSQL("DROP TABLE IF EXISTS data_exceptions");
			onCreate(db);
		}
	}

	// ---opens the database---
	public DBAdapter open() throws SQLException {
		db = DBHelper.getWritableDatabase();
		return this;
	}

	// ---closes the database---
	public void close() {
		DBHelper.close();
	}

	public ArrayList<DeleteItem> getOfflineWashes() {
		ArrayList<DeleteItem> rejectOfflineList = new ArrayList<DeleteItem>();

		try {

			this.open();
			Cursor c = this.getNotSendedData();

			if (c.moveToFirst()) {
				do {

					Id = c.getInt(c.getColumnIndex("id"));
					
					
					Wash_Id_Data = c.getString(c
							.getColumnIndex("Wash_Id"));
					
					
					Car_Number_Data = c.getString(c
							.getColumnIndex("Car_Number"));
					Image_Path_Data = c.getString(c
							.getColumnIndex("Image_Path"));
					Ticket = c.getString(c.getColumnIndex("Ticket"));
					Insert_Date = c.getString(c.getColumnIndex("Insert_Date"));
					Gps_Lat_Data = c.getString(c.getColumnIndex("GPS_Lat"));
					Gps_Lon_Data = c.getString(c.getColumnIndex("GPS_Lon"));
					User_Name_Data = c.getString(c.getColumnIndex("User_Name"));
					Service_Code_Data = c.getString(c
							.getColumnIndex("service_code"));
					Add_Item_Data = c.getString(c.getColumnIndex("Add_Item"));

					Device_Id_Data = c.getString(c.getColumnIndex("Device_Id"));
					MD_Code_Data = c.getString(c.getColumnIndex("MD_Code"));

					Station_Number_Data = c.getString(c
							.getColumnIndex("Station_Number"));

					Station_Id_Data = c.getString(c
							.getColumnIndex("Station_Id"));

					QrCode_Value_Data = c.getString(c
							.getColumnIndex("QrCode_Value"));

					LP_Scan_Type_Data = c.getString(c
							.getColumnIndex("LP_Scan_Type"));

					Version_App_Data = c.getString(c
							.getColumnIndex("Version_App"));

					DeleteItem di = new DeleteItem();

					di.setWashId(Integer.toString(Id));
					di.setServiceCode(Service_Code_Data);
					di.setCarNumber(Car_Number_Data);
					di.setWashDate(Insert_Date);
					rejectOfflineList.add(di);

				} while (c.moveToNext());
			}
			this.close();

			return rejectOfflineList;

		} catch (Exception ex) {
			this.close();
			GlobalApp.WriteLogException(ex);
			return null;
		}

	}

	// ---insert a contact into the database---
	public long insertOffline(String Wash_Id, String CarNumber, String Image_Path,
			String Ticket, String Insert_Date, String GPS_Lon, String GPS_Lat,
			String User_Name, String service_code, String Add_Item,
			String Device_Id, String MD_Code, String Station_Number,
			String Station_Id, String QrCode_Value, String LP_Scan_Type,
			String Version_App) {
		ContentValues initialValues = new ContentValues();
		initialValues.put(KEY_WASH_ID, Wash_Id);
		initialValues.put(KEY_CAR_NUMBER, CarNumber);
		initialValues.put(KEY_IMAGE_PATH, Image_Path);
		initialValues.put(KEY_TICKET, Ticket);
		initialValues.put(KEY_INSERT_DATE, Insert_Date);
		initialValues.put(KEY_GPS_LON, GPS_Lon);
		initialValues.put(KEY_GPS_LAT, GPS_Lat);
		initialValues.put(KEY_USER_NAME, User_Name);
		initialValues.put(KEY_SERVICE_CODE, service_code);
		initialValues.put(KEY_ADD_ITEM, Add_Item);
		initialValues.put(KEY_DEVICE_ID, Device_Id);
		initialValues.put(KEY_MD_CODE, MD_Code);
		initialValues.put(KEY_STATION_NUMBER, Station_Number);
		initialValues.put(KEY_STATION_ID, Station_Id);
		initialValues.put(KEY_QR_CODE_VALUE, QrCode_Value);
		initialValues.put(KEY_LP_SCAN_TYPE, LP_Scan_Type);
		initialValues.put(KEY_VERSION_APP, Version_App);

		return db.insert(DATABASE_TABLE, null, initialValues);
	}

	// ---deletes a particular contact---
	public boolean deleteRecord(long rowId) {
		return db.delete(DATABASE_TABLE, KEY_ROWID + "=" + rowId, null) > 0;
	}

	public boolean deleteTrueTickets() {
		// return db.delete(DATABASE_TABLE, "Id=?",new String[]{"true"});
		return db.delete(DATABASE_TABLE, "Ticket=?", new String[] { "true" }) > 0;
	}

	public Cursor selectTrueTickets() {
		// return db.delete(DATABASE_TABLE, "Id=?",new String[]{"true"});
		return db.query(DATABASE_TABLE, new String[] { KEY_IMAGE_PATH },
				"Ticket=?", new String[] { "true" }, null, null, null, null);
	}

	public Cursor getNotSendedData() {
		// return db.delete(DATABASE_TABLE, "Id=?",new String[]{"true"});
		return db.query(DATABASE_TABLE, new String[] { KEY_ROWID,KEY_WASH_ID,
				KEY_CAR_NUMBER, KEY_IMAGE_PATH, KEY_TICKET, KEY_INSERT_DATE,
				KEY_GPS_LON, KEY_GPS_LAT, KEY_USER_NAME, KEY_SERVICE_CODE,
				KEY_ADD_ITEM, KEY_DEVICE_ID, KEY_MD_CODE, KEY_STATION_NUMBER,
				KEY_STATION_ID, KEY_QR_CODE_VALUE, KEY_LP_SCAN_TYPE,
				KEY_VERSION_APP }, "Ticket=?", new String[] { "false" }, null,
				null, null, null);
	}

	// ---retrieves all the contacts---
	public Cursor getAllRecords() {
		return db.query(DATABASE_TABLE, new String[] { KEY_ROWID,KEY_WASH_ID,
				KEY_CAR_NUMBER, KEY_IMAGE_PATH, KEY_TICKET, KEY_INSERT_DATE,
				KEY_GPS_LON, KEY_GPS_LAT, KEY_USER_NAME, KEY_SERVICE_CODE,
				KEY_ADD_ITEM, KEY_DEVICE_ID, KEY_MD_CODE, KEY_STATION_NUMBER,
				KEY_STATION_ID, KEY_QR_CODE_VALUE, KEY_LP_SCAN_TYPE,
				KEY_VERSION_APP }, null, null, null, null, null, null);
	}

	public Cursor getFirstRecord(long rowId) throws SQLException {
		Cursor mCursor = db.query(true, DATABASE_TABLE, new String[] {
				KEY_ROWID,KEY_WASH_ID, KEY_CAR_NUMBER, KEY_IMAGE_PATH, KEY_TICKET,
				KEY_INSERT_DATE, KEY_GPS_LON, KEY_GPS_LAT, KEY_USER_NAME,
				KEY_SERVICE_CODE, KEY_ADD_ITEM, KEY_DEVICE_ID, KEY_MD_CODE,
				KEY_STATION_NUMBER, KEY_STATION_ID, KEY_QR_CODE_VALUE,
				KEY_LP_SCAN_TYPE, KEY_VERSION_APP }, KEY_ROWID + "=" + rowId,
				null, null, null, null, null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;
	}

	// ---updates a contact---
	public boolean updateRecord(long rowId,String Wash_Id, String CarNumber,
			String Image_Path, String Ticket, String Insert_Date,
			String GPS_Lon, String GPS_Lat, String User_Name,
			String service_code, String Add_Item, String Device_Id,
			String MD_Code, String Station_Number, String Station_Id,
			String QrCode_Value, String LP_Scan_Type, String Version_App) {
		ContentValues args = new ContentValues();		
		
		args.put(KEY_WASH_ID, Wash_Id);
		args.put(KEY_CAR_NUMBER, CarNumber);
		args.put(KEY_IMAGE_PATH, Image_Path);
		args.put(KEY_TICKET, Ticket);
		args.put(KEY_INSERT_DATE, Insert_Date);
		args.put(KEY_GPS_LON, GPS_Lon);
		args.put(KEY_GPS_LAT, GPS_Lat);
		args.put(KEY_USER_NAME, User_Name);
		args.put(KEY_SERVICE_CODE, service_code);
		args.put(KEY_ADD_ITEM, Add_Item);
		args.put(KEY_DEVICE_ID, Device_Id);
		args.put(KEY_MD_CODE, MD_Code);
		args.put(KEY_STATION_NUMBER, Station_Number);
		args.put(KEY_STATION_ID, Station_Id);
		args.put(KEY_QR_CODE_VALUE, QrCode_Value);
		args.put(KEY_LP_SCAN_TYPE, LP_Scan_Type);
		args.put(KEY_VERSION_APP, Version_App);

		return db.update(DATABASE_TABLE, args, KEY_ROWID + "=" + rowId, null) > 0;
	}

	public int getCountOffline() {
		int Count = 0;
		Cursor c = this.getAllRecords();
		Count = c.getCount();
		return Count;
	}

	public boolean updateRecordTicket(long rowId, String Ticket) {
		ContentValues args = new ContentValues();
		args.put(KEY_TICKET, Ticket);
		return db.update(DATABASE_TABLE, args, KEY_ROWID + "=" + rowId, null) > 0;
	}

	public void checkOfflineData() {
		try {
			isData = false;
			// ---get all contacts---
			this.open();
			// Cursor c = this.getAllRecords();

			Cursor c = this.getNotSendedData();
			// Count = c.getCount();
			// Cursor c = this.getFirstRecord(17);
			if (c.moveToFirst()) {
				do {

					Id = c.getInt(c.getColumnIndex("id"));
					
					
					Wash_Id_Data = c.getString(c
							.getColumnIndex("Wash_Id"));
					
					
					Car_Number_Data = c.getString(c
							.getColumnIndex("Car_Number"));
					Image_Path_Data = c.getString(c
							.getColumnIndex("Image_Path"));
					Ticket = c.getString(c.getColumnIndex("Ticket"));
					Insert_Date = c.getString(c.getColumnIndex("Insert_Date"));
					Gps_Lat_Data = c.getString(c.getColumnIndex("GPS_Lat"));
					Gps_Lon_Data = c.getString(c.getColumnIndex("GPS_Lon"));
					User_Name_Data = c.getString(c.getColumnIndex("User_Name"));
					Service_Code_Data = c.getString(c
							.getColumnIndex("service_code"));
					Add_Item_Data = c.getString(c.getColumnIndex("Add_Item"));

					Device_Id_Data = c.getString(c.getColumnIndex("Device_Id"));
					MD_Code_Data = c.getString(c.getColumnIndex("MD_Code"));

					Station_Number_Data = c.getString(c
							.getColumnIndex("Station_Number"));

					Station_Id_Data = c.getString(c
							.getColumnIndex("Station_Id"));

					QrCode_Value_Data = c.getString(c
							.getColumnIndex("QrCode_Value"));

					LP_Scan_Type_Data = c.getString(c
							.getColumnIndex("LP_Scan_Type"));

					Version_App_Data = c.getString(c
							.getColumnIndex("Version_App"));

					isData = true;
					if (Car_Number_Data != null && Car_Number_Data != ""
							&& User_Name_Data != null && User_Name_Data != "") {
						isEmpty = false;
						// c.moveToNext();
					} else {
						isEmpty = true;
						// after true wrong id will be delete
						// counter++;
						DBAdapter.this.updateRecordTicket(Id, "true");
					}

					// Toast.makeText(context,"New Offline Raw " +
					// Car_Number_Data, Toast.LENGTH_SHORT).show();

					if (isData == true && isEmpty == false) {

						if (Image_Path_Data != null && Image_Path_Data != "") {
							try {
								IMG = CameraHelper
										.getLastImage(Image_Path_Data);
							} catch (IOException e) {
								GlobalApp.WriteLogException(e);
								Image_Path_Data = "";
							}
						}

						else {
							Image_Path_Data = "";
						}
						// Toast.makeText(context,"Send To Server " +
						// Car_Number_Data,Toast.LENGTH_SHORT).show();

						SharedPreferences sharedPreferences = context
								.getSharedPreferences("PazWashPreferences",
										context.MODE_PRIVATE);

						serverURL = sharedPreferences.getString("BaseUrl",
								"http://172.30.4.75:8090/PazWashService.asmx");

						SendDataOffline sdo = new SendDataOffline(
								Integer.toString(Id),Wash_Id_Data, Car_Number_Data, IMG,
								Ticket, Insert_Date, Gps_Lat_Data,
								Gps_Lon_Data, User_Name_Data,
								Service_Code_Data, Add_Item_Data,
								Device_Id_Data, MD_Code_Data,
								Station_Number_Data, Station_Id_Data,
								QrCode_Value_Data, LP_Scan_Type_Data,
								Version_App_Data);

						try {

							sdo.execute(serverURL + "/GetOfflineData");
							sdo.get(GlobalApp.getTimeoutSeconds() + 5, TimeUnit.SECONDS);
						} catch (TimeoutException e) {
							GlobalApp.WriteLogException(e);
							sdo.cancel(true);
							// counter++;
							// DBAdapter.this.updateRecordTicket(Id, "false");
						} catch (Exception e) {
							GlobalApp.WriteLogException(e);
							sdo.cancel(true);
							// counter++;
							// DBAdapter.this.updateRecordTicket(Id, "false");
						}

					}

					/*
					 * if (isData == true && isEmpty == true) {
					 * 
					 * //this.open(); if (this.deleteRecord(Row_Number)) {
					 * Toast.makeText(context, "Delete empty data raw.",
					 * Toast.LENGTH_SHORT).show(); } else {
					 * Toast.makeText(context, "Delete failed.",
					 * Toast.LENGTH_SHORT).show(); } //this.close();
					 * 
					 * }
					 */

					// DisplayContact(c);
				} while (c.moveToNext());
			}
			this.close();

		} catch (Exception ex) {
			this.close();
			GlobalApp.WriteLogException(ex);
		}

	}

	public void CopyDB(java.io.InputStream inputStream,
			FileOutputStream fileOutputStream) throws IOException {
		// ---copy 1K bytes at a time---
		byte[] buffer = new byte[1024];
		int length;
		while ((length = inputStream.read(buffer)) > 0) {
			fileOutputStream.write(buffer, 0, length);
		}
		inputStream.close();
		fileOutputStream.close();
	}

	public void DeleteSendedOfflineData() {
		try {
			List<String> imagesToDelete = new ArrayList<String>();
			this.open();
			Cursor c = this.selectTrueTickets();

			if (c.moveToFirst()) {
				do {

					String image_path = c.getString(0);
					if (image_path != null && image_path != "") {
						imagesToDelete.add(image_path);
					}

				} while (c.moveToNext());
			}
			this.close();

			for (int i = 0; i < imagesToDelete.size(); i++) {
				File file = new File(imagesToDelete.get(i));
				boolean deleted = file.delete();
				// Toast.makeText(context, Boolean.toString(deleted),
				// Toast.LENGTH_LONG).show();

			}

		} catch (Exception ex) {
			this.close();
			GlobalApp.WriteLogException(ex);
		}

		Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
			public void run() {
				try {
					DBAdapter.this.open();
					if (DBAdapter.this.deleteTrueTickets()) {
						DBAdapter.this.close();
						// Toast.makeText(context, "Delete successful.",
						// Toast.LENGTH_SHORT).show();
					} else {
						// Toast.makeText(context, "Delete failed.",
						// Toast.LENGTH_SHORT).show();
						DBAdapter.this.close();
					}

				} catch (Exception ex) {
					DBAdapter.this.close();
					GlobalApp.WriteLogException(ex);
				}
			}
		}, 3000);
	}

	private class SendDataOffline extends
			AsyncTask<String, Void, SQLiteWSResult> {

		String Wash_ID;
		String Car_Number_Data;
		String Ticket;
		String Insert_Date;
		String Gps_Lat_Data;
		String Gps_Lon_Data;
		String User_Name_Data;
		String Id;
		String IMG;
		String Service_Code;
		String Add_Item_Data;
		String Device_Id;
		String MD_Code;
		String Station_Number_Data;
		String Station_Id_Data;
		String QrCode_Value_Data;
		String LP_Scan_Type_Data;
		String Version_App_Data;

		protected SQLiteWSResult doInBackground(String... urls) {
			SqlLiteWS ws = new SqlLiteWS();
			return ws.SendOfflineHTTP(BuildParameters(), urls[0]);
		}

		public SendDataOffline(String Id,String Wash_Id_Data, String Car_Number_Data, String IMG,
				String Ticket, String Insert_Date, String Gps_Lat_Data,
				String Gps_Lon_Data, String User_Name_Data,
				String Service_Code_Data, String Add_Item_Data,
				String Device_Id, String MD_Code, String Station_Number,
				String StationId, String QrCode_Value, String LPScanType,
				String Version_App) {
			this.Id = Id;
			this.Wash_ID = Wash_Id_Data;
			this.Car_Number_Data = Car_Number_Data;
			this.IMG = IMG;
			this.Ticket = Ticket;
			this.Insert_Date = Insert_Date;
			this.Gps_Lon_Data = Gps_Lon_Data;
			this.Gps_Lat_Data = Gps_Lat_Data;
			this.User_Name_Data = User_Name_Data;
			this.Service_Code = Service_Code_Data;
			this.Add_Item_Data = Add_Item_Data;
			this.Device_Id = Device_Id;
			this.MD_Code = MD_Code;
			this.Station_Number_Data = Station_Number;
			this.Station_Id_Data = StationId;
			this.QrCode_Value_Data = QrCode_Value;
			this.LP_Scan_Type_Data = LPScanType;
			this.Version_App_Data = Version_App;

		}

		private List<DataParameter> BuildParameters() {
			List<DataParameter> parameters = new ArrayList<DataParameter>();
			parameters.add(new DataParameter("id", this.Id));
			parameters.add(new DataParameter("washId", this.Wash_ID));
			parameters.add(new DataParameter("lpReference",
					this.Car_Number_Data));
			parameters.add(new DataParameter("img", this.IMG));

			parameters.add(new DataParameter("insert_date", this.Insert_Date));
			parameters.add(new DataParameter("GPS_Lon", this.Gps_Lon_Data));
			parameters.add(new DataParameter("GPS_Lat", this.Gps_Lat_Data));
			parameters.add(new DataParameter("userName", this.User_Name_Data));
			parameters.add(new DataParameter("serviceCode", this.Service_Code));
			parameters.add(new DataParameter("additionalItems",
					this.Add_Item_Data));
			parameters.add(new DataParameter("deviceId", this.Device_Id));
			parameters.add(new DataParameter("mdCode", this.MD_Code));
			parameters.add(new DataParameter("scan_type_code",
					this.LP_Scan_Type_Data));
			parameters.add(new DataParameter("qrCodeValue",
					this.QrCode_Value_Data));
			parameters.add(new DataParameter("stationNumber",
					this.Station_Number_Data));
			parameters
					.add(new DataParameter("stationId", this.Station_Id_Data));
			parameters.add(new DataParameter("sysVer", GlobalApp
					.getSysVersion()));
			parameters.add(new DataParameter("mdType", GlobalApp.getMDType()));
			parameters.add(new DataParameter("pazWashAppVer",
					this.Version_App_Data));

			return parameters;
		}

		protected void onPostExecute(SQLiteWSResult result) {

			// counter++;

			// if (counter <= Count) {
			if (result.getRequestStatus().RequestStatusCode()
					.equals(RequestStatuz.WSGW_OK.toString())) {
				if (result.getClientClientExcCode().equals(
						ClientExceptionCodeResult.RESULT_OK.toString())) {
					Integer res = -1;

					if (!res.equals(Integer.parseInt(result.getResult()))) {

						DBAdapter.this.open();
						if (DBAdapter.this.updateRecordTicket(
								Long.parseLong(result.getResult()), "true")) {
							DBAdapter.this.close();
							// Toast.makeText(context, Integer.toString(counter)
							// +
							// " in " + Integer.toString(Count),
							// Toast.LENGTH_LONG).show();

						} else {
							// Toast.makeText(context, "Update failed. " +
							// Integer.toString(counter) + " in " +
							// Integer.toString(Count) ,
							// Toast.LENGTH_LONG).show();
							DBAdapter.this.close();
						}

					} else {
						// Toast.makeText(context, "Send data failed.",
						// Toast.LENGTH_SHORT).show();
					}
				}
			}
		}
		// if (counter == Count) {
		//
		// DeleteSendedOfflineData();
		//
		// }
		// }

	}

}

/*
 * 
 * //if (result == "true") { Toast.makeText(context, "CarNumber " +
 * Car_Number_Data + "Sended", Toast.LENGTH_SHORT).show();
 * DBAdapter.this.open(); if
 * (DBAdapter.this.deleteRecord(Integer.parseInt(result)))
 * Toast.makeText(context, "Delete successful.", Toast.LENGTH_SHORT).show();
 * else Toast.makeText(context, "Delete failed.", Toast.LENGTH_SHORT).show();
 * DBAdapter.this.close(); }
 * 
 * else { Toast.makeText(context, "Send data failed.", Toast.LENGTH_SHORT)
 * .show(); }
 * 
 * //checkOfflineData(); // Log.setText("Server Get Offline Data"); //
 * progress.setVisibility(View.GONE); // Toast.makeText(getBaseContext(),
 * result, // Toast.LENGTH_LONG).show(); }
 * 
 * }
 */

// }
