package com.segment.pazwash.BL;

import com.segment.pazwash.Enums.VPNStatus;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkChk {

	public static boolean checkInternet(Context ctx, boolean flag) {

		try {

			if (flag) {
				if (checkInet(ctx)) {
					return checkVPN();
				}
				else
				{
					return false;
				}
			} else {
				return checkInet(ctx);
			}
		}

		catch (Exception e) {
			GlobalApp.WriteLogException(e);
			return false;
		}
	}

	private static boolean checkInet(Context ctx) {
		NetworkInfo info = (NetworkInfo) ((ConnectivityManager) ctx
				.getSystemService(Context.CONNECTIVITY_SERVICE))
				.getActiveNetworkInfo();

		if (info == null || !info.isConnected()) {
			return false;
		}
		if (!info.isAvailable()) {
			return false;
		}

		if (info.isRoaming()) {
			// here is the roaming option you can change it if you want to
			// disable internet while roaming, just return false
			return true;
		}

		return true;
	}

	private static boolean checkVPN() {

		try {
			VPNStatus vps = GlobalApp.getVPNState().getVPNStatus();

			if (vps == VPNStatus.CONNECTED) {
				return true;
			} else {
				return false;
			}

		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
			return false;
		}

	}
}
