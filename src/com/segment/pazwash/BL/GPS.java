package com.segment.pazwash.BL;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;


import com.segment.pazwash.DM.GPSData;

import android.content.Context;

import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;

public class GPS {

	public static boolean CheckGps(Context context) {
		try {

			LocationManager locManager = (LocationManager) context
					.getSystemService(Context.LOCATION_SERVICE);

			if (!locManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
				return false;
			} else {
				return true;
			}
		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
			return false;
		}
	}
	
	private static String getAddress(double lat, double lon,Context context) {
		try {

			if (GlobalApp.IsOffline()) {
				return "";
			}
			
			Geocoder gCoder = new Geocoder(context);
			StringBuilder sb = new StringBuilder();
			List<Address> addresses = gCoder.getFromLocation(lat, lon, 1);
			if (addresses != null && addresses.size() > 0) {

				int maxIndex = addresses.get(0).getMaxAddressLineIndex();
				
				for (int x = 0; x <= maxIndex; x++) {
					sb.append(addresses.get(0).getAddressLine(x));
					sb.append(",");
				}
				sb.append(addresses.get(0).getLocality());
				sb.append("\n");
				
			
			}
			return sb.toString();
		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
			return "";
		}
		
	}
	
	
	public static GPSData getGPSWithTime(Context context) {
		try {

			
			LocationManager lm = (LocationManager) context
					.getSystemService(Context.LOCATION_SERVICE);
			//String address = "";
			//String gpsData = "";
			
			
		
			List<String> providers = lm.getProviders(true);
			/*
			 * Loop over the array backwards, and if you get an accurate
			 * location, then break out the loop
			 */
			Location l = null;

			l = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);

			if (l == null) {            
			
				
				for (int i = providers.size() - 1; i >= 0; i--) {
					l = lm.getLastKnownLocation(providers.get(i));
				
					if (l != null)
						break;
				}
			}
			else
			{
				
				if (l.getLatitude() == 0 || l.getLongitude() == 0) {
					
					for (int i = providers.size() - 1; i >= 0; i--) {
						l = lm.getLastKnownLocation(providers.get(i));
					
						if (l != null)
							break;
					}
				}
			}

			double[] gps = new double[2];
			if (l != null) {
				gps[0] = l.getLatitude();
				gps[1] = l.getLongitude();	
				/*
				Date date= new Date(l.getTime());
				SimpleDateFormat format = new SimpleDateFormat(
						"dd/MM/yyyy HH:mm");				
				gpsData = format.format(date);
				*/
				
			}
			
			
			
			
			return new GPSData(Double.toString(gps[0]),Double.toString(gps[1]),l.getTime());
		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
			return null;
		}
	}
	
	
	public static GPSData getGPSWithAddress(Context context) {
		try {

			
			LocationManager lm = (LocationManager) context
					.getSystemService(Context.LOCATION_SERVICE);
			String address = "";
			String gpsData = "";
			
			
		
			List<String> providers = lm.getProviders(true);
			/*
			 * Loop over the array backwards, and if you get an accurate
			 * location, then break out the loop
			 */
			Location l = null;

			l = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);

			if (l == null) {            
			
				
				for (int i = providers.size() - 1; i >= 0; i--) {
					l = lm.getLastKnownLocation(providers.get(i));
				
					if (l != null)
						break;
				}
			}
			else
			{
				
				if (l.getLatitude() == 0 || l.getLongitude() == 0) {
					
					for (int i = providers.size() - 1; i >= 0; i--) {
						l = lm.getLastKnownLocation(providers.get(i));
					
						if (l != null)
							break;
					}
				}
			}

			double[] gps = new double[2];
			if (l != null) {
				gps[0] = l.getLatitude();
				gps[1] = l.getLongitude();	
				address = getAddress(l.getLatitude(),l.getLongitude(),context);
				Date date= new Date(l.getTime());
				SimpleDateFormat format = new SimpleDateFormat(
						"dd/MM/yyyy HH:mm");				
				gpsData = format.format(date);
				
			}
			
			
			
			
			return new GPSData(Double.toString(gps[0]),Double.toString(gps[1]),address,gpsData,l.getTime());
		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
			return null;
		}
	}

	public static GPSData getGPS(Context context) {
		try {

			LocationManager lm = (LocationManager) context
					.getSystemService(Context.LOCATION_SERVICE);
			/*
			 * Criteria c = new Criteria();
			 * c.setAccuracy(Criteria.ACCURACY_HIGH);
			 * c.setAltitudeRequired(false); c.setBearingRequired(false);
			 * c.setSpeedRequired(false); c.setCostAllowed(true);
			 * c.setPowerRequirement(Criteria.POWER_HIGH);
			 * 
			 * LocationProvider high=
			 * lm.getProvider(lm.getBestProvider(c,true));
			 */
			List<String> providers = lm.getProviders(true);
			/*
			 * Loop over the array backwards, and if you get an accurate
			 * location, then break out the loop
			 */
			Location l = null;

			l = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);

			if (l == null) {            
			
				
				for (int i = providers.size() - 1; i >= 0; i--) {
					l = lm.getLastKnownLocation(providers.get(i));
				
					if (l != null)
						break;
				}
			}
			else
			{
				
				if (l.getLatitude() == 0 || l.getLongitude() == 0) {
					
					for (int i = providers.size() - 1; i >= 0; i--) {
						l = lm.getLastKnownLocation(providers.get(i));
					
						if (l != null)
							break;
					}
				}
			}

			double[] gps = new double[2];
			if (l != null) {
				gps[0] = l.getLatitude();
				gps[1] = l.getLongitude();
			}
			return new GPSData(Double.toString(gps[0]),Double.toString(gps[1]));
		} catch (Exception e) {
			GlobalApp.WriteLogException(e);		
			return new GPSData("0.0","0.0");
			
		}
	}
}