package com.segment.pazwash.BL;

import java.util.List;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;




import com.segment.pazwash.DM.ReportSummaryItem;
import com.segment.pazwash.R;

public class SummaryAdapter extends BaseAdapter {
	
	
	Typeface hebBold;
	private static List<ReportSummaryItem> summaryList;

	private LayoutInflater mInflater;

	public SummaryAdapter(Context context, List<ReportSummaryItem> results,Typeface b) {
		summaryList = results;
		mInflater = LayoutInflater.from(context);
		hebBold = b;
	}

	public int getCount() {
		return summaryList.size();
	}

	public Object getItem(int position) {
		return summaryList.get(position);
	}

	public long getItemId(int position) {
		return position;
	}
	

	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.drawable.report_summary_view, null);
			holder = new ViewHolder();
			holder.txtTitle = (TextView) convertView.findViewById(R.id.summaryTitleTv);
			holder.txtValue = (TextView) convertView.findViewById(R.id.summaryValueTv);
			
			//.holder.txtValue.setTypeface(hebBold);
			holder.txtTitle.setTypeface(hebBold);
			
		

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		
		
		holder.txtTitle.setText(summaryList.get(position).getTitle());
		holder.txtValue.setText(summaryList.get(position).getValue());
		

		return convertView;
	}

	static class ViewHolder {
		TextView txtValue;
		TextView txtTitle;
		
	}

}
