package com.segment.pazwash.BL;

import org.json.JSONObject; 
import org.json.JSONArray; 
import org.json.JSONException; 
 
import android.content.Context; 
import android.content.SharedPreferences; 
 
public class JSONSharedPreferences { 
    private static final String PREFIX = "json"; 
 
    public static void saveJSONObject(Context c, String prefName, String key, JSONObject object) { 
        SharedPreferences settings = c.getSharedPreferences(prefName, 0); 
        SharedPreferences.Editor editor = settings.edit(); 
        editor.putString(JSONSharedPreferences.PREFIX+key, object.toString()); 
        editor.commit(); 
    } 
 
    public static void saveJSONArray(Context c, String prefName, String key, JSONArray array) { 
        SharedPreferences settings = c.getSharedPreferences(prefName, 0); 
        SharedPreferences.Editor editor = settings.edit(); 
        editor.putString(JSONSharedPreferences.PREFIX+key, array.toString()); 
        editor.commit(); 
    } 
 
    public static JSONObject loadJSONObject(Context c, String prefName, String key) throws JSONException { 
        SharedPreferences settings = c.getSharedPreferences(prefName, 0); 
        return new JSONObject(settings.getString(JSONSharedPreferences.PREFIX+key, "{}")); 
    } 
 
    public static JSONArray loadJSONArray(Context c, String prefName, String key) throws JSONException { 
        SharedPreferences settings = c.getSharedPreferences(prefName, 0); 
        return new JSONArray(settings.getString(JSONSharedPreferences.PREFIX+key, "[]")); 
    } 
 
    public static void remove(Context c, String prefName, String key) { 
        SharedPreferences settings = c.getSharedPreferences(prefName, 0); 
        if (settings.contains(JSONSharedPreferences.PREFIX+key)) { 
            SharedPreferences.Editor editor = settings.edit(); 
            editor.remove(JSONSharedPreferences.PREFIX+key); 
            editor.commit(); 
        } 
    } 
    
    public String[] loadArray(String arrayName, Context mContext) {   
        SharedPreferences prefs = mContext.getSharedPreferences("preferencename", 0);   
        int size = prefs.getInt(arrayName + "_size", 0);   
        String array[] = new String[size];   
        for(int i=0;i<size;i++)   
            array[i] = prefs.getString(arrayName + "_" + i, null);   
        return array;   
    }  
    
    
    public boolean saveArray(String[] array, String arrayName, Context mContext) {    
        SharedPreferences prefs = mContext.getSharedPreferences("preferencename", 0);   
        SharedPreferences.Editor editor = prefs.edit();   
        editor.putInt(arrayName +"_size", array.length);   
        for(int i=0;i<array.length;i++)   
            editor.putString(arrayName + "_" + i, array[i]);   
        return editor.commit();   
    }  

    
} 
