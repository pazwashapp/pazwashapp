package com.segment.pazwash.BL;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import com.segment.pazwash.DM.DataParameter;
import com.segment.pazwash.DM.SQLiteWSResult;
import com.segment.pazwash.Enums.ClientExceptionCodeResult;
import com.segment.pazwash.Enums.RequestStatuz;
import com.segment.pazwash.WS.SqlLiteWS;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.os.Handler;

import android.util.Log;

public class Logger {

	protected static boolean isEmpty;
	// Integer counter = 0;
	// Integer Count = 0;
	public static String isServerGet;
	String Class_Data;
	String Method_Data;
	String User_Data;
	String Error_Data;
	String Is_Send_Data;
	String Ip_Address_Data;
	String Error_Time_Data;
	String Device_Id_Data;
	String MD_Code_Data;
	String Station_Number_Data;
	String Station_Id_Data;
	String Version_App_Data;
	Integer Id;

	String serverURL = "";

	protected boolean isData;
	static final String KEY_ROWID = "id";
	static final String KEY_CLASS_NAME = "Class_Data";
	static final String KEY_METHOD_NAME = "Method_Data";
	static final String KEY_USER_NAME = "User_Data";
	static final String KEY_ERROR_MESSAGE = "Error_Data";
	static final String KEY_IS_SEND = "Is_Send_Data";
	static final String KEY_ERROR_DATE = "Error_Time_Data";
	static final String KEY_IP_ADDRESS = "Ip_Address_Data";
	static final String KEY_DEVICE_ID = "Device_Id";
	static final String KEY_MD_CODE = "MD_Code";
	static final String KEY_STATION_NUMBER = "Station_Number";
	static final String KEY_STATION_ID = "Station_Id";
	static final String KEY_VERSION_APP = "Version_App";

	static final String TAG = "Logger";
	static final String DATABASE_NAME = "paz_offline.db";
	static final String DATABASE_TABLE = "data_exceptions";
	static final int DATABASE_VERSION = 1;
	static final String DATABASE_CREATE = "create table data_exceptions (id integer primary key autoincrement, "
			+ "Class_Data text not null, Method_Data text not null,User_Data text not null,Error_Data text not null,Is_Send_Data text not null, Error_Time_Data text not null,Ip_Address_Data text not null,"
			+ "Device_Id text not null,MD_Code text not null, Station_Number text not null,Station_Id text not null , Version_App text not null);";
	final Context context;
	// final Activity activity;
	DatabaseHelper DBHelper;
	SQLiteDatabase db;
	public static void writeToLogcat(String msg){
		if (GlobalApp.writeToLogcat)
			Log.i("Logger", msg);
	}

	
	public Logger(Context ctx) {
		this.context = ctx;
		DBHelper = new DatabaseHelper(context);
	}

	/*
	 * public DBAdapter(Context ctx, Activity act) { this.context = ctx;
	 * this.activity = act; DBHelper = new DatabaseHelper(context); }
	 */
	private static class DatabaseHelper extends SQLiteOpenHelper {
		DatabaseHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db)

		{
			try {
				db.execSQL(DATABASE_CREATE);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			Log.w(TAG, "Upgrading database from version " + oldVersion + " to " + newVersion + ", which will destroy all old data");
			db.execSQL("DROP TABLE IF EXISTS data_bb_steps");
			db.execSQL("DROP TABLE IF EXISTS data_offline");
			db.execSQL("DROP TABLE IF EXISTS data_exceptions");
			onCreate(db);
		}
	}

	// ---opens the database---
	public Logger open() throws SQLException {
		db = DBHelper.getWritableDatabase();
		return this;
	}

	// ---closes the database---
	public void close() {
		DBHelper.close();
	}

	// ---insert a contact into the database---
	public long addLog(String className, String methodName, String userName,
			String error, String isSend, String errorTime, String ipAddress,
			String Device_Id, String MD_Code, String Station_Number,
			String Station_Id, String Version_App) {
		ContentValues initialValues = new ContentValues();
		initialValues.put(KEY_CLASS_NAME, className);
		initialValues.put(KEY_METHOD_NAME, methodName);
		initialValues.put(KEY_USER_NAME, userName);
		initialValues.put(KEY_ERROR_MESSAGE, error);
		initialValues.put(KEY_IS_SEND, isSend);
		initialValues.put(KEY_ERROR_DATE, errorTime);
		initialValues.put(KEY_IP_ADDRESS, ipAddress);
		initialValues.put(KEY_DEVICE_ID, Device_Id);
		initialValues.put(KEY_MD_CODE, MD_Code);
		initialValues.put(KEY_STATION_NUMBER, Station_Number);
		initialValues.put(KEY_STATION_ID, Station_Id);
		initialValues.put(KEY_VERSION_APP, Version_App);

		return db.insert(DATABASE_TABLE, null, initialValues);
	}

	// ---deletes a particular contact---
	public boolean deleteLog(long rowId) {
		return db.delete(DATABASE_TABLE, KEY_ROWID + "=" + rowId, null) > 0;
	}

	public boolean deleteSendedLogs() {
		writeToLogcat("deleteSendedLogs");
		//return db.delete(DATABASE_TABLE, "Is_Send_Data=?", new String[] { "true" }) > 0;
		return db.delete(DATABASE_TABLE, null, null) > 0;
	}

	// ---retrieves all the contacts---
	public Cursor getAllLogs() {
		return db.query(DATABASE_TABLE, new String[] { KEY_ROWID,
				KEY_CLASS_NAME, KEY_METHOD_NAME, KEY_USER_NAME,
				KEY_ERROR_MESSAGE, KEY_IS_SEND, KEY_ERROR_DATE, KEY_IP_ADDRESS,
				KEY_DEVICE_ID, KEY_MD_CODE, KEY_STATION_NUMBER, KEY_STATION_ID,
				KEY_VERSION_APP }, null, null, null, null, null, null);
	}

	public Cursor getFirstLog(long rowId) throws SQLException {
		Cursor mCursor = db.query(true, DATABASE_TABLE, new String[] {
				KEY_ROWID, KEY_CLASS_NAME, KEY_METHOD_NAME, KEY_USER_NAME,
				KEY_ERROR_MESSAGE, KEY_IS_SEND, KEY_ERROR_DATE, KEY_IP_ADDRESS,
				KEY_DEVICE_ID, KEY_MD_CODE, KEY_STATION_NUMBER, KEY_STATION_ID,
				KEY_VERSION_APP }, KEY_ROWID + "=" + rowId, null, null, null,
				null, null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;
	}

	public Cursor getNotSendedData(int numberOfRows) {
		String sql = "SELECT " + KEY_ROWID + ", " + KEY_CLASS_NAME + ", " + KEY_METHOD_NAME + ", " + KEY_USER_NAME + ", " + KEY_ERROR_MESSAGE + ", " +
				KEY_IS_SEND + ", " + KEY_ERROR_DATE + ", " + KEY_IP_ADDRESS + ", " + KEY_DEVICE_ID + ", " + KEY_MD_CODE + ", " + KEY_STATION_NUMBER + ", " +
				KEY_STATION_ID + ", " + KEY_VERSION_APP + ", " +
				"CASE WHEN "+ KEY_CLASS_NAME +" = 'OVER_OFFLINE_LIMIT' THEN 1 ELSE 0 END AS IS_OFFLINE_LOG_ROW " +
				"FROM "+ DATABASE_TABLE + " " +
				"ORDER BY CASE WHEN "+ KEY_CLASS_NAME +" = 'OVER_OFFLINE_LIMIT' THEN 1 ELSE 0 END DESC, "+ KEY_ROWID +" DESC " +
				"LIMIT "+ numberOfRows +" ";
		return db.rawQuery(sql, null);
	}

	// ---updates a contact---
	public boolean updateRecord(long rowId, String className,
			String methodName, String userName, String error, String isSend,
			String errorTime, String ipAddress, String Device_Id,
			String MD_Code, String Station_Number, String Station_Id,
			String Version_App) {
		ContentValues args = new ContentValues();

		args.put(KEY_CLASS_NAME, className);
		args.put(KEY_METHOD_NAME, methodName);
		args.put(KEY_USER_NAME, userName);
		args.put(KEY_ERROR_MESSAGE, error);
		args.put(KEY_IS_SEND, isSend);
		args.put(KEY_ERROR_DATE, errorTime);
		args.put(KEY_IP_ADDRESS, ipAddress);
		args.put(KEY_DEVICE_ID, Device_Id);
		args.put(KEY_MD_CODE, MD_Code);
		args.put(KEY_STATION_NUMBER, Station_Number);
		args.put(KEY_STATION_ID, Station_Id);
		args.put(KEY_VERSION_APP, Version_App);

		return db.update(DATABASE_TABLE, args, KEY_ROWID + "=" + rowId, null) > 0;
	}

	public int getCountOffline() {
		int Count = 0;
		Cursor c = this.getAllLogs();
		Count = c.getCount();
		return Count;
	}

	public boolean updateLogisSend(long rowId, String isSend) {
		ContentValues args = new ContentValues();
		args.put(KEY_IS_SEND, isSend);
		return db.update(DATABASE_TABLE, args, KEY_ROWID + "=" + rowId, null) > 0;
	}

	public void sendLogs(int numberOfRows) {
		try {
			isData = false;
			this.open();
			int entriesCount = getCountOffline();
			writeToLogcat("entriesCount:" + Integer.toString(entriesCount));
			if (entriesCount>0){
				GlobalApp.WriteLog("GeneralLog","getCountLog","entriesCount:" + String.valueOf(entriesCount) + "|numberOfRows:" + String.valueOf(numberOfRows));
				Cursor c = this.getNotSendedData(numberOfRows);
				writeToLogcat("getNotSendedData:" + Integer.toString(c.getCount()));
				if (c.moveToFirst()) {
					do {
						Id = c.getInt(c.getColumnIndex("id"));
						Class_Data = c.getString(c.getColumnIndex("Class_Data"));
						Method_Data = c.getString(c.getColumnIndex("Method_Data"));
						User_Data = c.getString(c.getColumnIndex("User_Data"));
						Error_Data = c.getString(c.getColumnIndex("Error_Data"));
						Is_Send_Data = c.getString(c.getColumnIndex("Is_Send_Data"));
						Error_Time_Data = c.getString(c.getColumnIndex("Error_Time_Data"));
						Ip_Address_Data = c.getString(c.getColumnIndex("Ip_Address_Data"));
						Device_Id_Data = c.getString(c.getColumnIndex("Device_Id"));
						MD_Code_Data = c.getString(c.getColumnIndex("MD_Code"));
						Station_Number_Data = c.getString(c.getColumnIndex("Station_Number"));
						Station_Id_Data = c.getString(c.getColumnIndex("Station_Id"));
						Version_App_Data = c.getString(c.getColumnIndex("Version_App"));
						isData = true;
						SharedPreferences sharedPreferences = context.getSharedPreferences("PazWashPreferences", context.MODE_PRIVATE);
						serverURL = sharedPreferences.getString("BaseUrl", "http://172.30.4.75:8090/PazWashService.asmx");
						SendLogs sdo = new SendLogs(Integer.toString(Id), Class_Data, Method_Data, User_Data, Error_Data,
								Error_Time_Data, Ip_Address_Data, Device_Id_Data, MD_Code_Data, Station_Number_Data, Station_Id_Data,
								Version_App_Data);
						try {
							sdo.execute(serverURL + "/addDataExceptions");
							sdo.get(GlobalApp.getTimeoutSeconds() + 5, TimeUnit.SECONDS);
						} catch (TimeoutException e) {
							GlobalApp.WriteLogException(e);
							sdo.cancel(true);
						} catch (Exception e) {
							GlobalApp.WriteLogException(e);
							sdo.cancel(true);
						}
					} while (c.moveToNext());
				}			
				this.deleteSendedLogs();
				this.close();
			}
		} catch (Exception ex) {
			writeToLogcat("exceptionSendLogs:" + ex.toString());
			this.close();
		}
	}

	public void CopyDB(java.io.InputStream inputStream,
			FileOutputStream fileOutputStream) throws IOException {
		// ---copy 1K bytes at a time---
		byte[] buffer = new byte[1024];
		int length;
		while ((length = inputStream.read(buffer)) > 0) {
			fileOutputStream.write(buffer, 0, length);
		}
		inputStream.close();
		fileOutputStream.close();
	}

	public void DeleteSendedLogs() {
		writeToLogcat("DeleteSendedLogs");
		Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
			public void run() {
				Logger.this.open();
				if (Logger.this.deleteSendedLogs()) {
					Logger.this.close();
				} else {
					Logger.this.close();
				}
			}
		}, 3000);
	}

	private class SendLogs extends AsyncTask<String, Void, SQLiteWSResult> {

		String Class_Data;
		String Method_Data;
		String User_Data;
		String Error_Data;
		String Ip_Address_Data;
		String Error_Time_Data;
		String Id;
		String Device_Id;
		String MD_Code;
		String Station_Number_Data;
		String Station_Id_Data;
		String Version_App_Data;

		protected SQLiteWSResult doInBackground(String... urls) {
			SqlLiteWS ws = new SqlLiteWS();
			List<DataParameter> parameters = BuildParameters();
			return ws.SendLogsHTTP(parameters, urls[0]);
		}

		public SendLogs(String Id, String className, String methodName,
				String userName, String error, String errorTime,
				String ipAddress, String Device_Id_Data, String MD_Code_Data,
				String Station_Number, String Station_Id, String Version_App) {
			this.Id = Id;
			this.Class_Data = className;
			this.Method_Data = methodName;
			this.User_Data = userName;
			this.Error_Data = error;
			this.Ip_Address_Data = ipAddress;
			this.Error_Time_Data = errorTime;
			this.Device_Id = Device_Id_Data;
			this.MD_Code = MD_Code_Data;
			this.Station_Number_Data = Station_Number;
			this.Station_Id_Data = Station_Id;
			this.Version_App_Data = Version_App;

		}

		private List<DataParameter> BuildParameters() {
			List<DataParameter> parameters = new ArrayList<DataParameter>();

			parameters.add(new DataParameter("id", this.Id));
			parameters.add(new DataParameter("className", this.Class_Data));
			parameters.add(new DataParameter("methodName", this.Method_Data));
			parameters.add(new DataParameter("userName", this.User_Data));
			parameters.add(new DataParameter("errorMessage", this.Error_Data));
			parameters
					.add(new DataParameter("errorDate", this.Error_Time_Data));
			parameters.add(new DataParameter("Ip", this.Ip_Address_Data));
			parameters.add(new DataParameter("deviceId", this.Device_Id));
			parameters.add(new DataParameter("mdCode", this.MD_Code));
			parameters.add(new DataParameter("stationNumber",
					this.Station_Number_Data));
			parameters
					.add(new DataParameter("stationId", this.Station_Id_Data));

			parameters.add(new DataParameter("pazWashAppVer",
					this.Version_App_Data));

			return parameters;
		}

		protected void onPostExecute(SQLiteWSResult result) {

			return;

			/*

			if (result.getRequestStatus().RequestStatusCode()
					.equals(RequestStatuz.WSGW_OK.toString())) {
				if (result.getClientClientExcCode().equals(
						ClientExceptionCodeResult.RESULT_OK.toString())) {

					Integer res = -1;
					long rowId = Long.parseLong(result.getResult());

					//if (!res.equals(Integer.parseInt(result.getResult()))) {						
						Logger.this.open();
						//if (Logger.this.updateLogisSend(Long.parseLong(result.getResult()), "true")) {
							//Logger.this.deleteLog(rowId);
							Logger.this.updateLogisSend(rowId, "true");
							Logger.this.close();
						//} 
						//else {
							//Logger.this.close();
						//}

					//} 
									}
			}
			*/

			 //DeleteSendedLogs();

		}

	}

}
