package com.segment.pazwash.BL;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CameraHelper {

	//private static final int IMAGE_QUALITY = 75;

	// public static String image_path;
	SharedPreferences preferences;

	public int cameraCount;

	public CameraHelper() {
	}

	
	public static void SaveImage(byte[] data, String imagePath, int mOrientation, int image_quality) {

		try {

			int rotate = 0;
			Bitmap bitmap;

			// bitmap = loadResizedBitmap(data, 640, 480, true);
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inSampleSize = 1;

			bitmap = BitmapFactory.decodeByteArray(data, 0, data.length,
					options);

			// bitmap = loadResizedBitmap(data,480,360,true);
			// BitmapFactory.Options options = new BitmapFactory.Options();
			// options.inSampleSize = 4;
			// bitmap = BitmapFactory.decodeByteArray(data, 0,
			// data.length,options);
			// Bitmap mutableBitmap = bmp.copy(Bitmap.Config.ARGB_8888, true);
			// Canvas canvas = new Canvas(mutableBitmap); // now it should work
			// ok
			// boolean isTablet = false;

			// if (isTablet == false) {

			switch (mOrientation) {
			case 0:
				rotate = 90;
				break;
			case 1:
				rotate = 270;
				break;
			case 2:
				rotate = 0;
				break;
			case 3:
				rotate = 180;
				break;
			}

			if (rotate != 2) {
				int w = bitmap.getWidth();
				int h = bitmap.getHeight();

				// Setting pre rotate
				Matrix mtx = new Matrix();
				mtx.preRotate(rotate);

				bitmap = Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, false);
			}

			bitmap = CropAndShrinkBitmap(bitmap, bitmap.getWidth(),
					bitmap.getHeight(),2);

			try {				
				
				FileOutputStream out = new FileOutputStream(imagePath);
				bitmap.compress(Bitmap.CompressFormat.JPEG, image_quality, out);

			} catch (Exception e) {
				GlobalApp.WriteLogException(e);
			}

		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
		}
	}

	public static Bitmap loadResizedBitmap(String filename, int width,
			int height, boolean exact) {

		try {

			Bitmap bitmap = null;
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds = true;
			BitmapFactory.decodeFile(filename, options);
			if (options.outHeight > 0 && options.outWidth > 0) {
				options.inJustDecodeBounds = false;
				options.inSampleSize = 2;
				while (options.outWidth / options.inSampleSize > width
						&& options.outHeight / options.inSampleSize > height) {
					options.inSampleSize++;
				}
				options.inSampleSize--;

				bitmap = BitmapFactory.decodeFile(filename, options);
				if (bitmap != null && exact) {
					bitmap = Bitmap.createScaledBitmap(bitmap, width, height,
							false);
				}
			}
			return bitmap;
		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
			return null;
		}
	}

	public static Bitmap loadResizedBitmap(byte[] image, int width, int height,
			boolean exact) {

		try {
			Bitmap bitmap = null;
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds = true;
			BitmapFactory.decodeByteArray(image, 0, image.length, options);
			if (options.outHeight > 0 && options.outWidth > 0) {
				options.inJustDecodeBounds = false;
				options.inSampleSize = 2;
				while (options.outWidth / options.inSampleSize > width
						&& options.outHeight / options.inSampleSize > height) {
					options.inSampleSize++;
				}
				options.inSampleSize--;

				bitmap = BitmapFactory.decodeByteArray(image, 0, image.length,
						options);
				if (bitmap != null && exact) {
					bitmap = Bitmap.createScaledBitmap(bitmap, width, height,
							false);
				}
			}
			return bitmap;
		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
			return null;
		}
	}

	private static Bitmap CropAndShrinkBitmap(Bitmap io_BitmapFromFile,
			int i_NewWidth, int i_NewHeight,int camera_crop) {
		try {
			int cuttingOffsetWith = 0;
			int cuttingOffsetHeight = 0;
			//int cuttingOffset = 0;			
			
			int currentWidth = io_BitmapFromFile.getWidth();
			int currentHeight = io_BitmapFromFile.getHeight();

			if (currentWidth > currentHeight) {
				cuttingOffsetHeight = (currentWidth - currentHeight) *2 ;			
				cuttingOffsetWith = (int) (currentWidth * 0.7);				
				io_BitmapFromFile = Bitmap.createBitmap(io_BitmapFromFile, 0, cuttingOffsetHeight / camera_crop, cuttingOffsetWith, currentHeight - cuttingOffsetHeight);
				//io_BitmapFromFile = Bitmap.createBitmap(io_BitmapFromFile, cuttingOffset / 2, 0, currentWidth - cuttingOffset,  currentHeight);
			} 
			else {
				// cuttingOffset = i_NewHeight - currentWidth;
				//cuttingOffsetWith = (int) (currentWidth * 0.8);
				cuttingOffsetHeight = (int) ((i_NewHeight - currentWidth)* 2.5);
				io_BitmapFromFile = Bitmap.createBitmap(io_BitmapFromFile, 0, cuttingOffsetHeight / camera_crop,currentWidth, currentHeight - cuttingOffsetHeight);
			}

			/*
			 * 
			 * io_BitmapFromFile = Bitmap.createBitmap(io_BitmapFromFile,
			 * cuttingOffset / 2, 0, currentWidth - cuttingOffset,
			 * currentHeight);
			 */
			return io_BitmapFromFile;

		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
			return null;
		}
	}

	public static String GetTimeCheck() {
		try {
			Date now = new Date();
			SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
			return format.format(now);
		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
			return "";
		}
	}

	public static String getLastImage(String ImagePath) throws IOException {
		try {
			//byte[] IMG;

		
			
			//Bitmap bitmap = BitmapFactory.decodeFile(ImagePath);

			//ByteArrayOutputStream stream = new ByteArrayOutputStream();
			// bitmap = Bitmap.createScaledBitmap(bitmap, 320, 240, true);
			//bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
			//byte[] byteArray = stream.toByteArray();
			//IMG = byteArray;
			//String image_str = Base64.encodeBytes(IMG);
			String image_str = Base64.encodeFromFile(ImagePath);
		
			return image_str;
		}

		catch (Exception e) {
			GlobalApp.WriteLogException(e);
			return "";
		}

	}

	
	public static Bitmap getPopImage(String ImagePath) throws IOException {
		try {
			Bitmap bitmap = BitmapFactory.decodeFile(ImagePath);
			return bitmap;
		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
			return null;
		}

	}

}
