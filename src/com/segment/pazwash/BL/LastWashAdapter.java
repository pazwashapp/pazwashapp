package com.segment.pazwash.BL;

import java.util.List;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.segment.pazwash.R;


import com.segment.pazwash.DM.SelectedDayWashes;

public class LastWashAdapter extends BaseAdapter{
	private static List<SelectedDayWashes> List;
	Typeface hebBold;
	private LayoutInflater mInflater;

	public LastWashAdapter(Context context, List<SelectedDayWashes> results,Typeface b) {
		List = results;
		mInflater = LayoutInflater.from(context);
	    hebBold = b;
	}

	public int getCount() {
		return List.size();
	}

	public Object getItem(int position) {
		return List.get(position);
	}

	public long getItemId(int position) {
		return position;
	}
	

	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.drawable.lastwashview, null);
			holder = new ViewHolder();
			holder.txtDealNumber = (TextView) convertView.findViewById(R.id.dealNumberTv);
			holder.txtCarNumber = (TextView) convertView.findViewById(R.id.carNumberTv);
			holder.txtData = (TextView) convertView.findViewById(R.id.dataTv);
			holder.txtWashType = (TextView) convertView.findViewById(R.id.washTypeTv);
			
			holder.DealNumerTv = (TextView) convertView.findViewById(R.id.washTypeTv);	
			holder.Car = (TextView) convertView.findViewById(R.id.Car);	
			holder.Data = (TextView) convertView.findViewById(R.id.Data);		
			holder.washTypeLTv = (TextView) convertView.findViewById(R.id.washTypeLTv);	
			holder.addItemLTv = (TextView) convertView.findViewById(R.id.addItemLTv);
			
			
			holder.txtWashType.setTypeface(hebBold);
			holder.DealNumerTv.setTypeface(hebBold);
			holder.Car.setTypeface(hebBold);
			holder.Data.setTypeface(hebBold);
			holder.washTypeLTv.setTypeface(hebBold);
			holder.addItemLTv.setTypeface(hebBold);
			
			
			
			
			
			
			//holder.txtData.setTypeface(hebBold);
			//holder.txtCarNumber.setTypeface(hebBold);
			//holder.txtDealNumber.setTypeface(hebBold);
			
			
			//holder.txtAddItem = (TextView) convertView.findViewById(R.id.addItemTv);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		
		
		holder.txtDealNumber.setText(List.get(position).WashId());
		holder.txtCarNumber.setText(List.get(position).CarNumber());
		holder.txtData.setText(List.get(position).WashDate());
		holder.txtWashType.setText(List.get(position).ServiceCode());
		//holder.txtAddItem.setText(List.get(position).getAddItem());

		return convertView;
	}

	static class ViewHolder {
		
		TextView DealNumerTv;		
		TextView Car;		
		TextView Data;		
		TextView washTypeLTv;		
		TextView addItemLTv;
		
		TextView txtDealNumber;
		TextView txtCarNumber;
		TextView txtData;
		TextView txtWashType;
		//TextView txtAddItem;
	}
}
