package com.segment.pazwash.BL;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

import com.segment.pazwash.DM.DataParameter;
import com.segment.pazwash.DM.SQLiteWSResult;
import com.segment.pazwash.Enums.ClientExceptionCodeResult;
import com.segment.pazwash.Enums.RequestStatuz;
import com.segment.pazwash.WS.SqlLiteWS;

public class BBStepWriter {

	protected static boolean isEmpty;
	public static String isServerGet;
	String Wash_Id_Db;
	String Step_Code_Db;
	String Step_Date_Db;
	String Add_Data_Db;
	String Is_Send_Data;
	String User_Name_Db;

	String MD_Code_Data;
	String Device_Id_Data;
	String Station_Number_Data;
	String Station_Id_Data;

	String serverURL = "";

	Integer Id;

	protected boolean isData;

	static final String KEY_ROWID = "id";
	static final String KEY_WASH_ID = "Wash_Id";
	static final String KEY_STEP_CODE = "Step_Code";
	static final String KEY_STEP_DATE = "Step_Date";
	static final String KEY_ADD_DATA = "Add_Data";
	static final String KEY_IS_SEND = "Is_Send_Data";
	static final String KEY_USER_NAME = "User_Name";

	static final String KEY_STATION_ID = "Station_Id";

	static final String KEY_MD_CODE = "MD_Code";
	static final String KEY_DEVICE_ID = "Device_Id";
	static final String KEY_STATION_NUMBER = "Station_Number";
	static final String TAG = "BBStepWriter";
	static final String DATABASE_NAME = "paz_offline.db";
	static final String DATABASE_TABLE = "data_bb_steps";
	static final int DATABASE_VERSION = 1;
	static final String DATABASE_CREATE = "create table data_exceptions (id integer primary key autoincrement, "
			+ "Wash_Id text not null, Step_Code text not null,Step_Date text not null,Add_Data text not null,Is_Send_Data text not null, User_Name text not null,"
			+ "MD_Code text not null, Device_Id text not null, Station_Number text not null,Station_Id text not null );";
	final Context context;
	// final Activity activity;
	DatabaseHelper DBHelper;
	SQLiteDatabase db;

	public BBStepWriter(Context ctx) {
		this.context = ctx;
		DBHelper = new DatabaseHelper(context);
	}

	/*
	 * public DBAdapter(Context ctx, Activity act) { this.context = ctx;
	 * this.activity = act; DBHelper = new DatabaseHelper(context); }
	 */
	private static class DatabaseHelper extends SQLiteOpenHelper {
		DatabaseHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db)

		{
			try {
				db.execSQL(DATABASE_CREATE);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
					+ newVersion + ", which will destroy all old data");
			db.execSQL("DROP TABLE IF EXISTS data_bb_steps");
			db.execSQL("DROP TABLE IF EXISTS data_offline");
			db.execSQL("DROP TABLE IF EXISTS data_exceptions");
			onCreate(db);
		}
	}

	// ---opens the database---
	public BBStepWriter open() throws SQLException {
		db = DBHelper.getWritableDatabase();
		return this;
	}

	// ---closes the database---
	public void close() {
		DBHelper.close();
	}

	// ---insert a contact into the database---
	public long addStep(String Wash_Id, String Step_Code, String Step_Date,
			String Add_Data, String Is_Send_Data, String User_Name,
			String MD_Code, String Device_Id, String Station_Number,
			String StationId) {
		ContentValues initialValues = new ContentValues();
		initialValues.put(KEY_WASH_ID, Wash_Id);
		initialValues.put(KEY_STEP_CODE, Step_Code);
		initialValues.put(KEY_STEP_DATE, Step_Date);
		initialValues.put(KEY_ADD_DATA, Add_Data);
		initialValues.put(KEY_IS_SEND, Is_Send_Data);
		initialValues.put(KEY_USER_NAME, User_Name);
		initialValues.put(KEY_STATION_NUMBER, Station_Number);
		initialValues.put(KEY_MD_CODE, MD_Code);
		initialValues.put(KEY_DEVICE_ID, Device_Id);
		initialValues.put(KEY_STATION_ID, StationId);

		return db.insert(DATABASE_TABLE, null, initialValues);
	}

	// ---deletes a particular contact---
	public boolean deleteStep(long rowId) {
		return db.delete(DATABASE_TABLE, KEY_ROWID + "=" + rowId, null) > 0;
	}

	public boolean deleteSendedBB() {
		// return db.delete(DATABASE_TABLE, "Id=?",new String[]{"true"});
		return db.delete(DATABASE_TABLE, "Is_Send_Data=?",
				new String[] { "true" }) > 0;
	}

	// ---retrieves all the contacts---
	public Cursor getBB() {

		return db.query(DATABASE_TABLE, new String[] { KEY_ROWID, KEY_WASH_ID,
				KEY_STEP_CODE, KEY_STEP_DATE, KEY_ADD_DATA, KEY_IS_SEND,
				KEY_USER_NAME, KEY_MD_CODE, KEY_DEVICE_ID, KEY_STATION_NUMBER,
				KEY_STATION_ID }, null, null, null, null, null, null);
	}

	public Cursor getFirstStep(long rowId) throws SQLException {
		Cursor mCursor = db.query(true, DATABASE_TABLE, new String[] {
				KEY_ROWID, KEY_WASH_ID, KEY_STEP_CODE, KEY_STEP_DATE,
				KEY_ADD_DATA, KEY_IS_SEND, KEY_USER_NAME, KEY_MD_CODE,
				KEY_DEVICE_ID, KEY_STATION_NUMBER, KEY_STATION_ID }, KEY_ROWID
				+ "=" + rowId, null, null, null, null, null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;
	}

	public Cursor getNotSendedData() {

		return db.query(DATABASE_TABLE, new String[] { KEY_ROWID, KEY_WASH_ID,
				KEY_STEP_CODE, KEY_STEP_DATE, KEY_ADD_DATA, KEY_IS_SEND,
				KEY_USER_NAME, KEY_MD_CODE, KEY_DEVICE_ID, KEY_STATION_NUMBER,
				KEY_STATION_ID }, "Is_Send_Data=?", new String[] { "false" },
				null, null, null, null);
	}
	public int getCountBB() {
		int Count = 0;
		Cursor c = this.getBB();
		Count = c.getCount();
		return Count;
	}

	// ---updates a contact---
	public boolean updateRecord(long rowId, String Wash_Id, String Step_Code,
			String Step_Date, String Add_Data, String Is_Send_Data,
			String User_Name, String MD_Code, String Device_Id,
			String Station_Number, String Station_Id) {
		ContentValues args = new ContentValues();

		args.put(KEY_WASH_ID, Wash_Id);
		args.put(KEY_STEP_CODE, Step_Code);
		args.put(KEY_STEP_DATE, Step_Date);
		args.put(KEY_ADD_DATA, Add_Data);
		args.put(KEY_IS_SEND, Is_Send_Data);
		args.put(KEY_USER_NAME, User_Name);
		args.put(KEY_MD_CODE, MD_Code);
		args.put(KEY_DEVICE_ID, Device_Id);
		args.put(KEY_STATION_NUMBER, Station_Number);
		args.put(KEY_STATION_ID, Station_Id);

		return db.update(DATABASE_TABLE, args, KEY_ROWID + "=" + rowId, null) > 0;
	}

	public boolean updateBBSend(long rowId, String isSend) {
		ContentValues args = new ContentValues();
		args.put(KEY_IS_SEND, isSend);
		return db.update(DATABASE_TABLE, args, KEY_ROWID + "=" + rowId, null) > 0;
	}

	public void sendBB() {
		try {
			isData = false;

			this.open();

			Cursor c = this.getNotSendedData();

			if (c.moveToFirst()) {
				do {

					Id = c.getInt(c.getColumnIndex("id"));
					Wash_Id_Db = c.getString(c.getColumnIndex("Wash_Id"));
					Step_Code_Db = c.getString(c.getColumnIndex("Step_Code"));
					Step_Date_Db = c.getString(c.getColumnIndex("Step_Date"));
					Add_Data_Db = c.getString(c.getColumnIndex("Add_Data"));
					Is_Send_Data = c
							.getString(c.getColumnIndex("Is_Send_Data"));
					User_Name_Db = c.getString(c.getColumnIndex("User_Name"));

					MD_Code_Data = c.getString(c.getColumnIndex("MD_Code"));
					Device_Id_Data = c.getString(c.getColumnIndex("Device_Id"));

					Station_Number_Data = c.getString(c
							.getColumnIndex("Station_Number"));

					Station_Id_Data = c.getString(c
							.getColumnIndex("Station_Id"));

					isData = true;

					SharedPreferences sharedPreferences = context
							.getSharedPreferences("PazWashPreferences",
									context.MODE_PRIVATE);

					serverURL = sharedPreferences.getString("BaseUrl",
							"http://172.30.4.75:8090/PazWashService.asmx");

					Runnable r = new Runnable() {

						@Override
						public void run() {

							SendBB sdo = new SendBB(Integer.toString(Id),
									Wash_Id_Db, Step_Code_Db, Step_Date_Db,
									Add_Data_Db, User_Name_Db, MD_Code_Data,
									Device_Id_Data, Station_Number_Data,
									Station_Id_Data);
							try {
								sdo.execute(serverURL + "/addDataWashesBBSteps");
								sdo.get(GlobalApp.getTimeoutSeconds() + 5, TimeUnit.SECONDS);
							} catch (TimeoutException e) {
								GlobalApp.WriteLogException(e);
								sdo.cancel(true);
								// counter++;
								// DBAdapter.this.updateRecordTicket(Id,
								// "false");
							} catch (Exception e) {
								GlobalApp.WriteLogException(e);
								sdo.cancel(true);
								// counter++;
								// DBAdapter.this.updateRecordTicket(Id,
								// "false");
							}
						}
					};

					Executors.newSingleThreadExecutor().execute(r);

				} while (c.moveToNext());
			}
			this.close();

		} catch (Exception ex) {
			this.close();
		}

	}

	public void CopyDB(java.io.InputStream inputStream,
			FileOutputStream fileOutputStream) throws IOException {
		// ---copy 1K bytes at a time---
		byte[] buffer = new byte[1024];
		int length;
		while ((length = inputStream.read(buffer)) > 0) {
			fileOutputStream.write(buffer, 0, length);
		}
		inputStream.close();
		fileOutputStream.close();
	}

	public void DeleteSendedBB() {
		Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
			public void run() {

				BBStepWriter.this.open();
				if (BBStepWriter.this.deleteSendedBB()) {
					BBStepWriter.this.close();
					// Toast.makeText(context, "Delete successful.",
					// Toast.LENGTH_SHORT).show();
				} else {
					// Toast.makeText(context, "Delete failed.",
					// Toast.LENGTH_SHORT).show();
					BBStepWriter.this.close();
				}

			}
		}, 3000);
	}

	private class SendBB extends AsyncTask<String, Void, SQLiteWSResult> {

		String Wash_Id;
		String Step_Code;
		String Step_Date;
		String Add_Data;
		String User_Name;
		String Station_ID;
		String MD_Code;
		String Device_Id;
		String Station_Number;
		String Id;

		protected SQLiteWSResult doInBackground(String... urls) {
			SqlLiteWS ws = new SqlLiteWS();

			List<DataParameter> parameters = BuildParameters();

			return ws.SendBBHTTP(parameters, urls[0]);
		}

		public SendBB(String Id, String Wash_Id_Db, String Step_Code_Db,
				String Step_Date_Db, String Add_Data_Db, String User_Name_Db,
				String MD_Code_Data, String Device_Id_Data,
				String Station_Number_Data, String Station_Id_Data) {
			this.Id = Id;
			this.Wash_Id = Wash_Id_Db;
			this.Step_Code = Step_Code_Db;
			this.Step_Date = Step_Date_Db;
			this.Add_Data = Add_Data_Db;
			this.User_Name = User_Name_Db;
			this.MD_Code = MD_Code_Data;
			this.Device_Id = Device_Id_Data;
			this.Station_Number = Station_Number_Data;
			this.Station_ID = Station_Id_Data;

		}

		private List<DataParameter> BuildParameters() {
			List<DataParameter> parameters = new ArrayList<DataParameter>();

			parameters.add(new DataParameter("Id", this.Id));
			parameters.add(new DataParameter("washId", this.Wash_Id));
			parameters.add(new DataParameter("Step_Code_Db", this.Step_Code));
			parameters.add(new DataParameter("Step_Date_Db", this.Step_Date));
			parameters.add(new DataParameter("Add_Data_Db", this.Add_Data));
			parameters.add(new DataParameter("userName", this.User_Name));
			parameters.add(new DataParameter("mdCode", this.MD_Code));
			parameters.add(new DataParameter("deviceId", this.Device_Id));
			parameters.add(new DataParameter("stationNumber",
					this.Station_Number));
			parameters.add(new DataParameter("stationId", this.Station_ID));

			return parameters;
		}

		protected void onPostExecute(SQLiteWSResult result) {

			if (result.getRequestStatus().RequestStatusCode()
					.equals(RequestStatuz.WSGW_OK.toString())) {
				if (result.getClientClientExcCode().equals(
						ClientExceptionCodeResult.RESULT_OK.toString())) {
					Integer res = -1;

					if (!res.equals(Integer.parseInt(result.getResult()))) {

						BBStepWriter.this.open();
						if (BBStepWriter.this.updateBBSend(
								Long.parseLong(result.getResult()), "true")) {
							BBStepWriter.this.close();

						} else {

							BBStepWriter.this.close();
						}

					} else {

					}

				}
			}
		}

	}

}
