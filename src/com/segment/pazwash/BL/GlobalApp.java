package com.segment.pazwash.BL;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

import com.segment.pazwash.BaseActivity;
import com.segment.pazwash.R;
import com.segment.pazwash.DM.ApproveResult;
import com.segment.pazwash.DM.VPNState;

import com.segment.pazwash.DM.LPResult;
import com.segment.pazwash.Enums.TimeOutTry;

import android.app.AlertDialog;
import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class GlobalApp extends Application {

	// add this variable declaration:
	private static int serviceState = 0;
	private static int logserviceState = 0;
	private static int bBServiceState = 0;
	private static int GPServiceState = 0;
	private static Context baseContext;
	private static LPResult lpResult;
	private static ApproveResult approveResult;
	private static String imagePath;
	private static VPNState vpnState;

	private static int washCode;
	private static int addItem;
	private static int countDhe;
	private static String userName;	
	private static String ipAddress;
	private static String className;
	private static String methodName;
	private static String deviceId;
	private static String qr_code = "";
	private static String generalItemDesc;
	private static String additemDesc;
	private static String washId = "0";
	private static String mdCode;
	private static String versionApp = "0";
	private static String lpScanType;
	private static boolean is_withoutLpr = false;
	private static boolean is_offline = false;
	private static String stationNumber;
	private static String station_ID;
	private static String sysVersion = "";
	public static String md_type = "";
	public static String phoneNumber = "";
	public static boolean isVPN = false;
	public static boolean withBarcode = false;
	private static int washApproveTimeoutSeconds = 30;
	private static int timeoutSeconds = 10;
	private static TimeOutTry timeoutTry = TimeOutTry.TIMEOUT_TRY_ZERO;
	private static boolean isServerOK = true;
	private static int countServerError = 0;
	public static int gsmSignalStrength = 0;
	public static String DataConnectionState = "";
	public static boolean writeToLogcat = true;
	public static String writeGeneralLog = "";
	public static String mrkt_msg = "#";
	public static String mrkt_msg2 = "##";
	public static int mrkt_msg_show = 1;
	public static int short_process = 0;
	public static boolean isDWASH = false;
	public static boolean isDWASHLast15MinDeal = false;
	
	private static void writeToLogcat(String msg){
		if (writeToLogcat)
			Log.i("Application", msg);
	}
	
	public static void setCountServerError(int v) {
		countServerError  = v;
	}
	
	public static int countServerError() {
		return countServerError ;
	}
	
	public static boolean IsServerOK  () {
		return isServerOK;
	}
	public static void setIsServerOK (boolean serv) {
		isServerOK = serv;
	}
	
	public static TimeOutTry getTimeOutTry() {
		return timeoutTry;
	}
	public static void setTimeOutTry(TimeOutTry _timeoutTry) {
		timeoutTry  = _timeoutTry;
	}
	
	
	public static int getTimeoutSeconds() {
		return timeoutSeconds ;
	}
	public static void setTimeoutSeconds(int v) {
		timeoutSeconds  = v;
	}
	
	
	public static int getWashApproveTimeoutSeconds() {
		return washApproveTimeoutSeconds;
	}
	public static void setWashApproveTimeoutSeconds(int v) {
		washApproveTimeoutSeconds = v;
	}

	public static boolean withBarcode () {
		return withBarcode;
	}
	public static void setWithBarcode (boolean v) {
		withBarcode = v;
	}
	
	public static boolean getIsVPN () {
		return isVPN;
	}
	public static void setIsVPN (boolean v) {
		isVPN = v;
	}
	

	public static String getPhoneNumber() {
		return phoneNumber;
	}
	public static void setPhoneNumber(String v) {
		phoneNumber = v;
	}
	
	
	public static String getMDType() {
		return md_type;
	}
	public static void setMDType(String v) {
		md_type = v;
	}
	
	
	public static String getSysVersion() {
		return sysVersion;
	}
	public static void setSysVersion(String v) {
		sysVersion = v;
	}
	
	
	public static void setVPNState(VPNState state){
		vpnState = state;
	}
	
 	public static  VPNState getVPNState(){
 		return vpnState;
 	}


	public static String getVersionApp() {
		return versionApp;
	}

	public static void setVersionApp(String v) {
		versionApp = v;
	}

	public static void setStationId(String Id) {
		station_ID = Id;
	}

	public static String getStationID() {
		return station_ID;
	}

	public static String getStationNumber() {
		return stationNumber;
	}

	public static void setStationNumber(String n) {
		stationNumber = n;
	}

	public static boolean IsOffline() {
		return is_offline;
	}

	public static void setIsOffline(boolean off) {
		is_offline = off;
	}

	public static void setIsWithOutLpr(boolean flag) {
		is_withoutLpr = flag;
	}

	public static boolean IsWithOutLpr() {
		return is_withoutLpr;
	}

	public static String getLPScanType() {
		return lpScanType;
	}

	public static void setLPScanType(String lpst) {
		lpScanType = lpst;
	}

	public static String getWashId() {
		return washId;
	}

	public static void setWashId(String wi) {
		washId = wi;
	}

	public static String getMDCode() {
		return mdCode;
	}

	public static void setMDCode(String md) {
		mdCode = md;
	}

	public static void setClassAndMethod(StackTraceElement[] ste) {

		for (int i = 0; i < ste.length; i++) {
			if (ste[i].getClassName().contains("com.segment.pazwash")) {
				className = ste[i].getClassName();
				methodName = ste[i].getMethodName();
			}
		}
	}

	public static int getGPSServiceState() {
		return GPServiceState;
	}

	public static void setGPSServiceState(int st) {
		GPServiceState = st;
	}

	public static int getBBServiceState() {
		return bBServiceState;
	}

	public static void setBBServiceState(int st) {
		bBServiceState = st;
	}

	public static void setGeneralItemDesc(String desc) {
		generalItemDesc = desc;
	}

	public static void setAddItemDesc(String d) {
		additemDesc = d;
	}

	public static void setBaseContext(Context context) {
		baseContext = context;
	}

	public static String GeneralItemDesc() {
		return generalItemDesc;
	}

	public static String AddItemDesc() {
		return additemDesc;
	}

	public static String getQRCode() {
		return qr_code;
	}

	public static void setQRCode(String code) {
		qr_code = code;
	}

	public static String getMethodName() {
		return methodName;
	}

	public static String getClassName() {
		return className;
	}

	public static String getIpAddress() {
		return ipAddress;
	}

	public static void setIpAddress() {
		ipAddress = getLocalIpAddress();
	}

	public static String getDeviceId() {
		return deviceId;
	}

	public static void setDeviceId(String s) {
		deviceId = s;
	}

	public static String getUserName() {
		return userName;
	}

	public static void setUserName(String user) {
		userName = user;
	}

	public static LPResult getLPResult() {
		return lpResult;
	}

	public static String getImagePath() {
		return imagePath;
	}

	public static ApproveResult getApproveResult() {
		return approveResult;
	}

	

	public static int getWashCode() {
		return washCode;
	}

	public static int getAddItem() {
		return addItem;
	}

	public static int getCountDhe() {
		return countDhe;
	}

	public static void setCountDhe(int denyCount) {
		countDhe = denyCount;
	}

	public static void setCountDhe() {
		countDhe++;
	}

	public static void setAdditem(int item) {
		addItem = item;
	}

	public static void setWashCode(int wash) {
		washCode = wash;
	}



	public static void setImagePath(String path) {
		imagePath = path;
	}

	public static void setApproveResult(ApproveResult result) {
		approveResult = result;
	}

	public static void setLPResult(LPResult lp) {
		lpResult = lp;
	}

	public static int getLogServiceState() {
		return logserviceState;
	}

	public static void setLogServiceState(int i) {
		logserviceState = i;
	}

	public static int getServiceState() {
		return serviceState;
	}

	public static void setServiceState(int i) {
		serviceState = i;
	}
	
	public static boolean getIsDWASH () {
		return isDWASH;
	}
	public static void setIsDWASH (boolean v) {
		isDWASH = v;
	}
	
	public static boolean getisDWASHLast15MinDeal () {
		return isDWASHLast15MinDeal;
	}
	public static void setisDWASHLast15MinDeal (boolean v) {
		isDWASHLast15MinDeal = v;
	}

	private static GlobalApp singleton;

	public static GlobalApp getInstance() {
		return singleton;
	}
	public static void WriteLogException(Exception e, String exInfo) {
		try {
			StackTraceElement[] ste = e.getStackTrace();
			GlobalApp.setClassAndMethod(ste);
			Logger logger = new Logger(baseContext);
			String message;
			if (e.getMessage() == null) {
				message = e.toString();
			} else {
				message = e.getMessage();
			}
			if (!exInfo.equals(""))
				message = message + ":" + exInfo;
			message = "(" + String.valueOf(GlobalApp.gsmSignalStrength) + "," + GlobalApp.DataConnectionState + ")" + message;
			logger.open();
			long id = logger.addLog(getClassName(), getMethodName(),getUserName(), message, "false", GetTimeNow(),
					getIpAddress(), getDeviceId(), getMDCode(),getStationNumber(), getStationID(), getVersionApp());
			logger.close();
		} catch (Exception e2) {
			// TODO: handle exception
		}
		
	}
	public static void WriteLogException(Exception e) {
		WriteLogException(e, "");
	}

	
	public static void WriteLog(String className, String methodName, String msg) {
		try {
			List<String> stationList = new ArrayList<String>(Arrays.asList(writeGeneralLog.split(";")));
			if (stationList.contains(getStationNumber()) || stationList.contains("ALL")) {
				Logger logger = new Logger(baseContext);
				logger.open();
				long id = logger.addLog(className, methodName, getUserName(), msg, "false", GetTimeNow(), getIpAddress(),getDeviceId(), getMDCode(), getStationNumber(), getStationID(), getVersionApp());
				writeToLogcat("WriteLog id:" + Long.toString(id));
				logger.close();
			}
		} catch (Exception e) {
			writeToLogcat("WriteLog exception:" + e.getMessage());
		}
	}

	public static void WriteSimpleLog(String className, String methodName, String msg) {
		try {
			Logger logger = new Logger(baseContext);
			logger.open();
			long id = logger.addLog(className, methodName, "",msg, "false", GetTimeNow(), "-1", "-1", "-1", "-1", "",  getVersionApp());
			logger.close();
		} catch (Exception e) {
		}
	}

	private static String getLocalIpAddress() {
		try {
			for (Enumeration<NetworkInterface> en = NetworkInterface
					.getNetworkInterfaces(); en.hasMoreElements();) {
				NetworkInterface intf = en.nextElement();
				for (Enumeration<InetAddress> enumIpAddr = intf
						.getInetAddresses(); enumIpAddr.hasMoreElements();) {
					InetAddress inetAddress = enumIpAddr.nextElement();
					if (!inetAddress.isLoopbackAddress()) {
						return inetAddress.getHostAddress().toString();
					}
				}
			}
		} catch (SocketException e) {
			GlobalApp.WriteLogException(e);
		}
		return null;
	}

	public static String GetTimeNow() {
		try {
			Date now = new Date();
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			return format.format(now);
		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
			return "";
		}
	}

	@Override
	public void onCreate() {
		super.onCreate();
		writeToLogcat("onCreate");
		singleton = this;
	}

}