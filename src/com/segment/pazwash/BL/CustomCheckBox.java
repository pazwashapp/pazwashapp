package com.segment.pazwash.BL;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.CheckBox;

public class CustomCheckBox extends CheckBox {

	public CustomCheckBox(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	public CustomCheckBox(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public CustomCheckBox(Context context) {
		super(context);
		init();
	}

	private void init() {

		Typeface hebBold = Typeface.createFromAsset(getContext().getAssets(),
				"fonts/droidsanshebrew-bold.ttf");

		setTypeface(hebBold);
	}

}
