package com.segment.pazwash.BL;

import java.util.List;

import com.segment.pazwash.R;

import com.segment.pazwash.DM.Purchase;


import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class PurchaseAdapter  extends BaseAdapter {
	

	private static List<Purchase> List;
	Typeface hebBold;

	private LayoutInflater mInflater;

	public PurchaseAdapter(Context context, List<Purchase> results,Typeface b) {
		List = results;
		mInflater = LayoutInflater.from(context);
		hebBold = b;
	}

	public int getCount() {
		return List.size();
	}

	public Object getItem(int position) {
		return List.get(position);
	}

	public long getItemId(int position) {
		return position;
	}
	

	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			//convertView = mInflater.inflate(R.drawable.reportdayview, null);
			
			convertView = mInflater.inflate(R.drawable.purchaselistitem, null);
			
			
			holder = new ViewHolder();
			//holder.txtDealNumber = (TextView) convertView.findViewById(R.id.dealnumber);
			
			holder.washCodeTv = (TextView) convertView.findViewById(R.id.washCodeTv);
			holder.washTypeTv = (TextView) convertView.findViewById(R.id.washTypeTv);
			holder.PurchaseNumberTv  = (TextView) convertView.findViewById(R.id.PurchaseNumberTv);
			holder.PurchaseValueTv = (TextView) convertView.findViewById(R.id.PurchaseValueTv);
			holder.MamTv = (TextView) convertView.findViewById(R.id.MamTv);
			holder.PurchaseValueMamTv  = (TextView) convertView.findViewById(R.id.PurchaseValueMamTv);
			
			holder.washCodeLTv = (TextView) convertView.findViewById(R.id.washCodeLTv);
			holder.washTypeLTv= (TextView) convertView.findViewById(R.id.washTypeLTv);
			holder.PurchaseNumberLTv= (TextView) convertView.findViewById(R.id.PurchaseNumberLTv);
			holder.PurchaseValueLTv= (TextView) convertView.findViewById(R.id.PurchaseValueLTv);
			holder.MamLTv= (TextView) convertView.findViewById(R.id.MamLTv);
			holder.PurchaseValueMamLTv= (TextView) convertView.findViewById(R.id.PurchaseValueMamLTv);
			
			
			holder.washCodeLTv.setTypeface(hebBold);
			holder.washTypeLTv.setTypeface(hebBold);
			holder.PurchaseNumberLTv.setTypeface(hebBold);
			holder.PurchaseValueLTv.setTypeface(hebBold);
			holder.MamLTv.setTypeface(hebBold);
			holder.PurchaseValueMamLTv.setTypeface(hebBold);
			
			
			
			//holder.PurchaseValueMamTv.setTypeface(hebBold);
			//holder.MamTv.setTypeface(hebBold);
			//holder.PurchaseValueTv.setTypeface(hebBold);
			//holder.PurchaseNumberTv.setTypeface(hebBold);
			holder.washTypeTv.setTypeface(hebBold);
			//holder.washCodeTv.setTypeface(hebBold);
			
			
			
			
			
			
			
			
			//holder.txtCarNumber = (TextView) convertView.findViewById(R.id.carnumber);
			//holder.txtData = (TextView) convertView.findViewById(R.id.date);
			//holder.txtWashType = (TextView) convertView.findViewById(R.id.washtype);
			//holder.txtAddItem = (TextView) convertView.findViewById(R.id.additem);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		
		
		holder.washCodeTv.setText(List.get(position).getProductCode());
		holder.washTypeTv.setText(List.get(position).getProductDesc());
		holder.PurchaseNumberTv.setText(List.get(position).getPurchaseNumber());
		holder.PurchaseValueTv.setText(List.get(position).getPurchaseValue());
		holder.MamTv.setText(List.get(position).getMam());
		holder.PurchaseValueMamTv.setText(List.get(position).getPurchaseValueMam());

		return convertView;
	}

	static class ViewHolder {
		
		TextView washCodeLTv;
		TextView washTypeLTv;
		TextView PurchaseNumberLTv;
		TextView PurchaseValueLTv;
		TextView MamLTv;
		TextView PurchaseValueMamLTv;
		
		TextView washCodeTv;
		TextView washTypeTv;
		TextView PurchaseNumberTv;
		TextView PurchaseValueTv;
		TextView MamTv;
		TextView PurchaseValueMamTv;
	}

}
