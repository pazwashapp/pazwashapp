package com.segment.pazwash.BL;


import java.util.List;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.segment.pazwash.R;
import com.segment.pazwash.DM.DeleteItem;

public class PazWashBaseAdapter extends BaseAdapter {
	private static List<DeleteItem> deleteList;

	private LayoutInflater mInflater;
	Typeface hebBold;

	public PazWashBaseAdapter(Context context, List<DeleteItem> results,Typeface b) {
		deleteList = results;
		mInflater = LayoutInflater.from(context);
		hebBold = b;
	}

	public int getCount() {
		return deleteList.size();
	}

	public Object getItem(int position) {
		return deleteList.get(position);
	}

	public long getItemId(int position) {
		return position;
	}
	

	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.drawable.custom_row_view, null);
			holder = new ViewHolder();
			holder.txtDealNumber = (TextView) convertView.findViewById(R.id.dealNumberTv);
			holder.txtCarNumber = (TextView) convertView.findViewById(R.id.carNumberTv);
			holder.txtData = (TextView) convertView.findViewById(R.id.dataTv);
			
			//holder.txtData.setTypeface(hebBold); 
			//holder.txtCarNumber.setTypeface(hebBold);
			//holder.txtDealNumber.setTypeface(hebBold);
			
			holder.DealNumerTv = (TextView) convertView.findViewById(R.id.DealNumerTv);	
			holder.Car =(TextView) convertView.findViewById(R.id.Car);	
			holder.Data = (TextView) convertView.findViewById(R.id.Data);
			
			
			holder.Data.setTypeface(hebBold);
			holder.Car.setTypeface(hebBold);
			holder.DealNumerTv.setTypeface(hebBold);
			

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		
		
		holder.txtDealNumber.setText(deleteList.get(position).WashId());
		holder.txtCarNumber.setText(deleteList.get(position).CarNumber());
		holder.txtData.setText(deleteList.get(position).WashDate());

		return convertView;
	}

	static class ViewHolder {
		TextView DealNumerTv;		
		TextView Car;		
		TextView Data;	
		
		TextView txtDealNumber;
		TextView txtCarNumber;
		TextView txtData;
	}
}