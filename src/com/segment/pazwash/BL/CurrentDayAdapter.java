package com.segment.pazwash.BL;

import java.util.List;

import com.segment.pazwash.R;

import com.segment.pazwash.DM.SelectedDayWashes;

import android.content.Context;
import android.graphics.Typeface;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class CurrentDayAdapter extends BaseAdapter {

	Typeface hebBold;

	private static List<SelectedDayWashes> List;

	private LayoutInflater mInflater;

	public CurrentDayAdapter(Context context, List<SelectedDayWashes> results,
			Typeface b) {
		List = results;
		mInflater = LayoutInflater.from(context);
		hebBold = b;
	}

	public int getCount() {
		return List.size();
	}

	public Object getItem(int position) {
		return List.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		convertView = null;
		if (convertView == null) {
			// convertView = mInflater.inflate(R.drawable.reportdayview, null);

			convertView = mInflater.inflate(R.drawable.custom_row_view, null);

			holder = new ViewHolder();
			// holder.txtDealNumber = (TextView)
			// convertView.findViewById(R.id.dealnumber);

			holder.txtDealNumber = (TextView) convertView
					.findViewById(R.id.dealNumberTv);
			holder.txtCarNumber = (TextView) convertView
					.findViewById(R.id.carNumberTv);
			holder.txtWashType = (TextView) convertView
					.findViewById(R.id.dataTv);
			holder.txtHour = (TextView) convertView.findViewById(R.id.dateTv);
			holder.Hour = (TextView) convertView.findViewById(R.id.Date);

			holder.DealNumerTv = (TextView) convertView
					.findViewById(R.id.DealNumerTv);
			holder.Car = (TextView) convertView.findViewById(R.id.Car);
			holder.Data = (TextView) convertView.findViewById(R.id.Data);

			holder.Data.setTypeface(hebBold);
			holder.Car.setTypeface(hebBold);
			holder.DealNumerTv.setTypeface(hebBold);
			holder.Hour.setTypeface(hebBold);

			// holder.txtDealNumber.setTypeface(hebBold);
			// holder.txtCarNumber.setTypeface(hebBold);
			holder.txtWashType.setTypeface(hebBold);
			//holder.txtWashType.setGravity(Gravity.LEFT);

			holder.txtDealNumber.setText(List.get(position).WashId());
			holder.txtCarNumber.setText(List.get(position).CarNumber());
			holder.txtHour.setText(List.get(position).WashDate());
			holder.txtWashType.setText(List.get(position).ServiceCode());

			// holder.txtCarNumber = (TextView)
			// convertView.findViewById(R.id.carnumber);
			// holder.txtData = (TextView) convertView.findViewById(R.id.date);
			// holder.txtWashType = (TextView)
			// convertView.findViewById(R.id.washtype);
			// holder.txtAddItem = (TextView)
			// convertView.findViewById(R.id.additem);

			convertView.setTag(holder);
		}

		// holder.txtAddItem.setText(List.get(position).getAddItem());

		return convertView;
	}

	static class ViewHolder {

		TextView DealNumerTv;
		TextView Car;
		TextView Data;
		TextView Hour;
		TextView txtDealNumber;
		TextView txtCarNumber;
		TextView txtHour;
		TextView txtWashType;
		// TextView txtAddItem;
	}

}
