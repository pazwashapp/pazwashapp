package com.segment.pazwash;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Window;
import android.widget.ListView;
import android.widget.TextView;

import com.segment.pazwash.BL.GlobalApp;
import com.segment.pazwash.BL.JSONSharedPreferences;
import com.segment.pazwash.BL.PreferencesAdapter;

import com.segment.pazwash.DM.AddItems;
import com.segment.pazwash.DM.ReportSummaryItem;

public class PreferencesActivity extends BaseActivity {
	List<ReportSummaryItem> preferences = new ArrayList<ReportSummaryItem>();
	ArrayList<AddItems> generalItems = new ArrayList<AddItems>();
	PreferencesAdapter adapter;
	ListView lstView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		try {
			ActivityType = 1;
			userName = getUserName();
			GlobalApp.setUserName(userName);
			requestWindowFeature(Window.FEATURE_NO_TITLE);
			setContentView(R.layout.preferences);
			loadProgressDialog();
			hideProgress();
			Typeface hebBold = Typeface.createFromAsset(getAssets(), "fonts/droidsanshebrew-bold.ttf");
			TextView title = (TextView) findViewById(R.id.PreferencesTv);
			title.setTypeface(hebBold);

			JSONArray jItems = JSONSharedPreferences.loadJSONArray(getBaseContext(), "generalProducts", "");

			for (int i = 0; i < jItems.length(); i++) {
				JSONObject jO = jItems.getJSONObject(i);

				AddItems ad = new AddItems();

				ad.setItemCode(jO.getInt("Code"));
				ad.setItemDesc(jO.getString("Desc"));
				ad.setItemSort(jO.getInt("Sort"));
				ad.setItemIsMain(jO.getInt("Main"));
				generalItems.add(ad);

			}

			SharedPreferences sharedPreferences = getSharedPreferences(
					"PazWashPreferences", MODE_PRIVATE);

			ReportSummaryItem userSession = new ReportSummaryItem();
			int lgs = sharedPreferences.getInt("login_session_min", 30);
			userSession.setTitle(this.getString(R.string.LoginSessionServ));
			userSession.setValue(Integer.toString(lgs));
			preferences.add(userSession);

			ReportSummaryItem userLoginTime = new ReportSummaryItem();
			Long ult = sharedPreferences.getLong("LoginTime",
					System.currentTimeMillis());
			userLoginTime.setTitle(this.getString(R.string.LoginTime));
			userLoginTime.setValue(getDate(ult, "dd/MM/yyyy hh:mm"));
			preferences.add(userLoginTime);

			ReportSummaryItem maxNoLpr = new ReportSummaryItem();
			int mno = sharedPreferences.getInt("max_no_lpr", 3);
			maxNoLpr.setTitle(this.getString(R.string.MaxNoLPRTry));
			maxNoLpr.setValue(Integer.toString(mno));
			preferences.add(maxNoLpr);

			ReportSummaryItem defaultWash = new ReportSummaryItem();
			int dw = sharedPreferences.getInt("selectedWashType", 0);
			if (dw != 0) {

				for (int i = 0; i < generalItems.size(); i++) {
					if (generalItems.get(i).ItemCode() == dw) {
						defaultWash.setTitle(this
								.getString(R.string.WashByDefault));
						defaultWash.setValue(generalItems.get(i).ItemDesc());
						preferences.add(defaultWash);
					}
				}

			}

			ReportSummaryItem waTO = new ReportSummaryItem();
			waTO.setTitle(this.getString(R.string.RosemanTimeout));
			waTO.setValue(Integer.toString(GlobalApp
					.getWashApproveTimeoutSeconds()));
			preferences.add(waTO);

			ReportSummaryItem TO = new ReportSummaryItem();
			TO.setTitle(this.getString(R.string.TimeoutSeconds));
			TO.setValue(Integer.toString(GlobalApp.getTimeoutSeconds()));
			preferences.add(TO);

			String serverVersion = sharedPreferences.getString(
					"server_app_version", "0.0");

			ReportSummaryItem serverApp = new ReportSummaryItem();
			serverApp.setTitle(this.getString(R.string.AppVerServ));
			serverApp.setValue(serverVersion);
			preferences.add(serverApp);

			ReportSummaryItem verApp = new ReportSummaryItem();
			verApp.setTitle(this.getString(R.string.AppVerCurrent));
			verApp.setValue(GlobalApp.getVersionApp());
			preferences.add(verApp);

			ReportSummaryItem typingLimitBalance = new ReportSummaryItem();
			int balance = sharedPreferences.getInt("Station_Typing_Limit_Balance",0);
			typingLimitBalance.setTitle(this.getString(R.string.TypingLimitBalance));
			typingLimitBalance.setValue(Integer.toString(balance));
			preferences.add(typingLimitBalance);			
			
			
			String svivaStr = "";

			final String URL = "http://172.30.4.75:8090/PazWashService.asmx";
			final String QA = "http://172.30.4.175:8090/PazWashService.asmx";
			final String Segment = "http://wt1.segment.co.il/PazWashWS/PazWashService.asmx";

			String PAZWASH_WS_URL = sharedPreferences.getString("BaseUrl",
					"http://172.30.4.75:8090/PazWashService.asmx");

			if (PAZWASH_WS_URL.equals(URL)) {
				svivaStr = this.getString(R.string.ServerProductionPaz);
			}
			if (PAZWASH_WS_URL.equals(QA)) {
				svivaStr = this.getString(R.string.ServerQA);
			}
			if (PAZWASH_WS_URL.equals(Segment)) {
				svivaStr = this.getString(R.string.ServerSegment);
			}

			ReportSummaryItem sviva = new ReportSummaryItem();
			sviva.setTitle(this.getString(R.string.Sviva));
			sviva.setValue(svivaStr);
			preferences.add(sviva);

			adapter = new PreferencesAdapter(PreferencesActivity.this,
					preferences, hebBold);

			lstView = (ListView) findViewById(R.id.PreferencesList);
			lstView.setBackgroundResource(R.drawable.listview);
			lstView.setChoiceMode(0);
			lstView.setAdapter(adapter);
			lstView.setCacheColorHint(0);
			lstView.setItemsCanFocus(false);

		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
			hideProgress();
			finish();
		}

	}

}
