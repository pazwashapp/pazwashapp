package com.segment.pazwash;

import java.util.ArrayList;
import java.util.List;

import com.segment.pazwash.BL.GlobalApp;
import com.segment.pazwash.DM.ChangePasswordResult;
import com.segment.pazwash.DM.DataParameter;
import com.segment.pazwash.Enums.ClientExceptionCodeResult;
import com.segment.pazwash.Enums.RequestStatuz;
import com.segment.pazwash.WS.LoginWS;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class LoginChangePasswordActivity extends BaseActivity {
	Button submitBt;
	EditText oldPasswordEt;
	EditText newPasswordEt;
	EditText newPasswordVerifyEt;
	String _userId;
	boolean isSubmitClick = false;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.change_password);

		loadProgressDialog();
		hideProgress();
		
	    Typeface hebBold = Typeface.createFromAsset(getAssets(), "fonts/droidsanshebrew-bold.ttf");		
		TextView title =  (TextView) findViewById(R.id.titleTv);
		title.setTypeface(hebBold);
		
		TextView oldPas =  (TextView) findViewById(R.id.oldPasswordTv);
		oldPas.setTypeface(hebBold);
		
		TextView newPas =  (TextView) findViewById(R.id.newPasswordTv);
		newPas.setTypeface(hebBold);
		
		TextView pasVer =  (TextView) findViewById(R.id.newPasswordVerifyTv);
		pasVer.setTypeface(hebBold);
		
		
		

		submitBt = (Button) findViewById(R.id.submitBt);
		submitBt.setOnClickListener(submitBtListener);
		submitBt.setTypeface(hebBold);

		oldPasswordEt = (EditText) findViewById(R.id.oldPasswordEt);
		newPasswordEt = (EditText) findViewById(R.id.newPasswordEt);
		newPasswordVerifyEt = (EditText) findViewById(R.id.newPasswordVerifyEt);

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			userName = extras.getString("UserName");
			_userId = extras.getString("userId");
		}

		oldPasswordEt
				.setOnFocusChangeListener(new View.OnFocusChangeListener() {
					@Override
					public void onFocusChange(View v, boolean hasFocus) {
						if (hasFocus) {
							getWindow()
									.setSoftInputMode(
											WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
						}
					}
				});

		newPasswordEt
				.setOnFocusChangeListener(new View.OnFocusChangeListener() {
					@Override
					public void onFocusChange(View v, boolean hasFocus) {
						if (hasFocus) {
							getWindow()
									.setSoftInputMode(
											WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
						}
					}
				});

		oldPasswordEt.requestFocus();

	}

	private OnClickListener submitBtListener = new OnClickListener() {
		public void onClick(View v) {

			try {

				if (!isSubmitClick) {
					isSubmitClick = true;
					submitBt.setClickable(false);
					submitBt.setEnabled(false);
					Handler handler = new Handler();
					handler.postDelayed(new Runnable() {
						public void run() {
							isSubmitClick = false;
							submitBt.setClickable(true);
							submitBt.setEnabled(true);

						}
					}, 2000);

					String oldPassword = oldPasswordEt.getText().toString()
							.replace(" ", "");
					String newPassword = newPasswordEt.getText().toString()
							.replace(" ", "");
					String newPasswordVerify = newPasswordVerifyEt.getText()
							.toString().replace(" ", "");

					if (userName == null) {
						return;
					}

					if ((oldPassword.isEmpty()) || (newPassword.isEmpty())
							|| (newPasswordVerify.isEmpty())) {
						showAlertDialog(R.string.ChangePasswordTitle,
								R.string.ChangePasswordMsgEmptyFields, null);
						return;
					}

					if (!newPasswordEt.getText().toString()
							.equals(newPasswordVerifyEt.getText().toString())) {
						showAlertDialog(R.string.ChangePasswordTitle,
								R.string.ChangePasswordMsgNewPasswordVerify,
								null);
						return;
					}

					showProgress();
					ChangePassword cp = new ChangePassword(userName,
							oldPassword, newPassword, _userId);
					cp.execute(PAZWASH_WS_URL + "/setNewPassword");
				}
			} catch (Exception e) {
				GlobalApp.WriteLogException(e);
			}
		}

	};

	@Override
	protected void onDestroy() {
		super.onDestroy();
		System.gc();
	}

	private DialogInterface.OnClickListener LoginOkListener = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {

			try {
				Intent intentMain = new Intent(getApplicationContext(),
						LoginActivity.class);
				intentMain.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intentMain);
				finish();
				dialog.dismiss();
			} catch (Exception e) {
				GlobalApp.WriteLogException(e);
			}
		}
	};

	private class ChangePassword extends
			AsyncTask<String, Void, ChangePasswordResult> {
		private static final int CHANGE_PASSWORD_SERVER_ERROR = -1;
		private static final int CHANGE_PASSWORD_ERROR = 0;
		private static final int CHANGE_PASSWORD_OK = 2;
		private static final int PASSWORD_ALREADY_USED = 1;
		private static final int PASSWORD_NOT_IN_CORRECT_FORMAT = 3;
		private static final int RUNTIME_ERROR = 4;

		String UserName;
		String OldPassword;
		String NewPassword;
		String userID;

		protected ChangePasswordResult doInBackground(String... urls) {
			LoginWS ws = new LoginWS();
			return ws.ChangePasswordShortHTTP(BuildParameters(), urls[0]);
		}

		ChangePassword(String userName, String oldPassword, String newPassword,
				String userId) {
			this.UserName = userName;
			this.OldPassword = oldPassword;
			this.NewPassword = newPassword;
			this.userID = userId;
		}

		private List<DataParameter> BuildParameters() {
			List<DataParameter> parameters = new ArrayList<DataParameter>();
			parameters.add(new DataParameter("userName", this.UserName));
			parameters.add(new DataParameter("userId", this.userID));
			parameters.add(new DataParameter("oldPassword", this.OldPassword));
			parameters.add(new DataParameter("newPassword", this.NewPassword));
			parameters.add(new DataParameter("deviceId", GlobalApp
					.getDeviceId()));
			parameters.add(new DataParameter("mdCode", GlobalApp.getMDCode()));
			return parameters;
		}

		protected void onPostExecute(ChangePasswordResult result) {

			try {

				if (result.getRequestStatus().RequestStatusCode()
						.equals(RequestStatuz.WSGW_OK.toString())) {
					if (result.getClientClientExcCode().equals(
							ClientExceptionCodeResult.RESULT_OK.toString())) {

						hideProgress();

						switch (Integer.parseInt(result.ChangePasswordStatus())) {
						case CHANGE_PASSWORD_SERVER_ERROR:
							showAlertDialog(R.string.ChangePasswordTitle,
									R.string.ChangePasswordMsgError, null);
							break;

						case CHANGE_PASSWORD_ERROR:
							showAlertDialog(R.string.ChangePasswordTitle,
									R.string.ChangePasswordMsgOldPasswordErr,
									null);
							break;

						case CHANGE_PASSWORD_OK:
							showAlertDialog(R.string.ChangePasswordTitle,
									R.string.ChangePasswordMsgOk,
									LoginOkListener);
							break;

						case PASSWORD_ALREADY_USED:
							showAlertDialog(R.string.ChangePasswordTitle,
									R.string.PasswordAlreadyUsed, null);
							break;
						case PASSWORD_NOT_IN_CORRECT_FORMAT:
							showAlertDialog(R.string.ChangePasswordTitle,
									R.string.IncorrectPassword, null);
							break;
						case RUNTIME_ERROR:
							showAlertDialog(R.string.ChangePasswordTitle,
									R.string.Exception, null);
							break;
						}
					} else {
						if (result.getClientClientExcCode().equals(
								ClientExceptionCodeResult.RESULT_ERROR
										.toString())) {
							hideProgress();
							showAlertDialog(R.string.Exception,
									R.string.ServerException, null);

						}
						if (result.getClientClientExcCode().equals(
								ClientExceptionCodeResult.RESULT_TIMEOUT
										.toString())) {
							hideProgress();
							showAlertDialog(R.string.Exception,
									R.string.TimeOutException, null);
						}
					}
					hideProgress();
				} else {
					hideProgress();
					showAlertDialog(R.string.Exception, result
							.getRequestStatus().RequestStatusCode(), null);
				}

			} catch (Exception e) {
				GlobalApp.WriteLogException(e);
				hideProgress();
			}

		}
	}

}
