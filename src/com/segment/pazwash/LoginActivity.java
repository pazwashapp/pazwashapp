package com.segment.pazwash;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import com.segment.pazwash.BL.GlobalApp;
import com.segment.pazwash.BL.NetworkChk;
import com.segment.pazwash.DM.DataParameter;
import com.segment.pazwash.DM.LoginResult;
import com.segment.pazwash.Enums.ClientExceptionCodeResult;
import com.segment.pazwash.Enums.RequestStatuz;
import com.segment.pazwash.WS.LoginWS;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends BaseActivity {

	EditText userNameEt;
	EditText userPasswordEt;
	EditText userNumberEt;
	Button LoginOk;
	boolean isTimeout = false;
	boolean isError = false;
	String version = "0";
	TextView versionT;
	boolean RememberMe = false;
	CheckBox chRemember;
	

	boolean IsLoginClick = false;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.login);
		ActivityType = 0;
		loadProgressDialog();
		Typeface hebBold = Typeface.createFromAsset(getAssets(), "fonts/droidsanshebrew-bold.ttf");
		
		TextView title =  (TextView) findViewById(R.id.titleTv);
		title.setTypeface(hebBold);
		
		TextView unT =  (TextView) findViewById(R.id.userNameTv);
		unT.setTypeface(hebBold);
		
		TextView userZTv =  (TextView) findViewById(R.id.userZTv);
		userZTv.setTypeface(hebBold);
		
		TextView userPasswordTv =  (TextView) findViewById(R.id.userPasswordTv);
		userPasswordTv.setTypeface(hebBold);
		
		chRemember = (CheckBox) findViewById(R.id.chRemember);
		chRemember.setTypeface(hebBold);
		userNameEt = (EditText) findViewById(R.id.userNameEt);
		//userNameEt.setTypeface(hebBold);
		userPasswordEt = (EditText) findViewById(R.id.userPasswordEt);
		//userPasswordEt.setRawInputType(Configuration.SCREENLAYOUT_SIZE_XLARGE);
		userNumberEt = (EditText) findViewById(R.id.userNumberEt);
		LoginOk = (Button) findViewById(R.id.LoginBt);
        
		LoginOk.setOnClickListener(btnOkListener);
		LoginOk.setTypeface(hebBold);
		
		versionT = (TextView) findViewById(R.id.versionTv);
		versionT.setOnLongClickListener(versionTlistener);
		setVersion();

		SharedPreferences sharedPreferences = getSharedPreferences("PazWashPreferences", MODE_PRIVATE);
		RememberMe = sharedPreferences.getBoolean("RememberMe", false);
		if (RememberMe) {
			userNumberEt.setText(sharedPreferences.getString("UserNumber", ""));
			userNameEt.setText(getUserName());
		}
		if (sharedPreferences.getString("BaseUrl", "").equals("http://172.30.4.175:8090/PazWashService.asmx"))
			title.setText(R.string.LoginTitleQA);
		else
			title.setText(R.string.LoginTitle);
		
		setFocuses();

	}
	
	private void setVersion() {
		try {
			version = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
			GlobalApp.setVersionApp(version);
			versionT.setText(version);
		} catch (Exception ex) {
			GlobalApp.WriteLogException(ex);
			// WriteLogException(ex);
		}
	}

	private void setFocuses() {

		try {

			userNameEt
					.setOnFocusChangeListener(new View.OnFocusChangeListener() {
						@Override
						public void onFocusChange(View v, boolean hasFocus) {
							if (hasFocus) {
								getWindow()
										.setSoftInputMode(
												WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
							}
						}
					});

			userPasswordEt
					.setOnFocusChangeListener(new View.OnFocusChangeListener() {
						@Override
						public void onFocusChange(View v, boolean hasFocus) {
							if (hasFocus) {
								getWindow()
										.setSoftInputMode(
												WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
							}
						}
					});
			userNumberEt
					.setOnFocusChangeListener(new View.OnFocusChangeListener() {
						@Override
						public void onFocusChange(View v, boolean hasFocus) {
							if (hasFocus) {
								getWindow()
										.setSoftInputMode(
												WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
							}
						}
					});

			if (userNameEt.getText().toString().isEmpty()) {

				userNameEt.requestFocus();
			} else {
				userPasswordEt.requestFocus();

			}
		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
		}
	}
	
	private OnLongClickListener versionTlistener = new OnLongClickListener(){
		public boolean onLongClick (View v){
			showSettingsDialog();
			return true;
		}
};
	private OnClickListener btnOkListener = new OnClickListener() {
		public void onClick(View v) {
			showProgress();
			if (!IsLoginClick) {
				IsLoginClick = true;
				LoginOk.setClickable(false);
				LoginOk.setEnabled(false);
				Handler handler = new Handler();
				handler.postDelayed(new Runnable() {
					public void run() {
						IsLoginClick = false;
						LoginOk.setClickable(true);
						LoginOk.setEnabled(true);
					}
				}, 3000);

				try {
					if (!NetworkChk.checkInternet(LoginActivity.this, GlobalApp.getIsVPN())) {
						Toast.makeText(LoginActivity.this, "no internet", Toast.LENGTH_SHORT).show();
						hideProgress();
						return;
					}
					final String userName = userNameEt.getText().toString().replace(" ", "");
					final String userId = userNumberEt.getText().toString().replace(" ", "");
					final String userPassword = userPasswordEt.getText().toString().replace(" ", "");
					if ((!userName.isEmpty()) && (!userId.isEmpty()) && (!userPassword.isEmpty())) {
						//peter 15-05-2015 in order to show user name first time
						SharedPreferences sharedPreferences = getSharedPreferences("PazWashPreferences", MODE_PRIVATE);
						SharedPreferences.Editor prefEditor = sharedPreferences.edit();
						prefEditor.putString("UserName", userName);
						//peter 15-05-2015
						Runnable r = new Runnable() {
							@Override
							public void run() {
								CheckLogin cl = new CheckLogin(userName, userId, userPassword);
								try {
									cl.execute(PAZWASH_WS_URL + "/getLogin");
									cl.get(GlobalApp.getTimeoutSeconds() + 5, TimeUnit.SECONDS);
								} catch (InterruptedException e) {
									GlobalApp.WriteLogException(e);
									cl.cancel(true);
									isError = true;
								} catch (ExecutionException e) {
									GlobalApp.WriteLogException(e);
									cl.cancel(true);
									isError = true;
								} catch (TimeoutException e) {
									GlobalApp.WriteLogException(e);
									cl.cancel(true);
									isTimeout = true;
								} catch (CancellationException e) {
									GlobalApp.WriteLogException(e);
									cl.cancel(true);
									isError = true;
								} finally {
								}
							}
						};
						Executors.newSingleThreadExecutor().execute(r);

						Handler handler1 = new Handler();
						handler1.postDelayed(new Runnable() {
							public void run() {
								hideProgress();
								if (isTimeout == true) {
									if (!isFinishing()) {
										showAlertDialog(R.string.Exception, R.string.TimeOutException, null);
									}
								}
								if (isError == true) {
									if (!isFinishing()) {
										showAlertDialog(R.string.Exception, R.string.Exception, null);
									}
								}
							}
						}, 11000);
					} else {
						hideProgress();
						showAlertDialog(R.string.LoginDialog, R.string.LoginEmptyField, null);
					}
				} catch (Exception e) {
					hideProgress();
					GlobalApp.WriteLogException(e);
				}
			}

		}
	};

	private DialogInterface.OnClickListener changePasswordOkListener = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			try {
				showProgress();
				Intent intentChangePassword = new Intent(getApplicationContext(), LoginChangePasswordActivity.class);
				intentChangePassword.putExtra("UserName", userNameEt.getText().toString());
				intentChangePassword.putExtra("userId", userNumberEt.getText().toString());

				startActivity(intentChangePassword);
				dialog.dismiss();
			} catch (Exception e) {
				hideProgress();
				GlobalApp.WriteLogException(e);
				showAlertDialog(R.string.Exception, R.string.Exception, null);
			}
		}
	};

	private class CheckLogin extends AsyncTask<String, Void, LoginResult> {

		// private static final int MAX_FAILED_NUMBER = 3;
		private static final int LOGIN_ERROR = 0;
		private static final int LOGIN_CHANGE_PASSWORD = 1;
		private static final int LOGIN_OK = 2;
		private static final int LOGIN_LOCKED = 3;

		String UserName;
		String UserId;
		String UserPassword;

		protected LoginResult doInBackground(String... urls) {
			LoginWS ws = new LoginWS();
			return ws.CheckLoginShortHTTP(BuildParameters(), urls[0]);
		}

		CheckLogin(String userName, String userId, String userPassword) {
			this.UserName = userName;
			this.UserId = userId;
			this.UserPassword = userPassword;

		}

		private List<DataParameter> BuildParameters() {
			List<DataParameter> parameters = new ArrayList<DataParameter>();
			parameters.add(new DataParameter("userName", this.UserName));
			parameters.add(new DataParameter("userId", this.UserId));
			parameters
					.add(new DataParameter("userPassword", this.UserPassword));

			parameters.add(new DataParameter("deviceId",
			// "359859064376292"));
					GlobalApp.getDeviceId()));

			return parameters;
		}

		protected void onPostExecute(LoginResult result) {
			try {
				if (result.getRequestStatus().RequestStatusCode().equals(RequestStatuz.WSGW_OK.toString())) {
					if (result.getClientClientExcCode().equals(
							ClientExceptionCodeResult.RESULT_OK.toString())) {
						switch (Integer.parseInt(result.LoginStatus())) {
						case LOGIN_ERROR:
							hideProgress();
							showAlertDialog(R.string.LoginMsgTitle, result.getUserStatusDesc(), null);
							break;
						case LOGIN_CHANGE_PASSWORD:
							hideProgress();
							showAlertDialog(R.string.LoginMsgTitle, result.getUserStatusDesc(), changePasswordOkListener);
							break;
						case LOGIN_LOCKED:
							showAlertDialog(R.string.LoginMsgTitle, result.getUserStatusDesc(), userLocked);
							break;
						case LOGIN_OK:
							RememberMe = chRemember.isChecked();
							SharedPreferences sharedPreferences = getSharedPreferences(									"PazWashPreferences", MODE_PRIVATE);
							SharedPreferences.Editor prefEditor = sharedPreferences.edit();
							prefEditor.putString("UserName", result.UserName());
							prefEditor.putString("UserNumber", result.UserId());
							prefEditor.putLong("LoginTime", System.currentTimeMillis());
							prefEditor.putBoolean("RememberMe", RememberMe);
							prefEditor.putString("StationID", result.StationId());
							prefEditor.putString("StationNumber", result.stationNumber());
							prefEditor.putString("mdCode", result.MDCode());
							prefEditor.putInt("offline_limit", Integer.parseInt(result.getOfflineLimit()));
							GlobalApp.setMDCode(result.MDCode());
							GlobalApp.setStationId(result.StationId());
							GlobalApp.setStationNumber(result.stationNumber());

							boolean is_check_gps = true;
							if (result.getIsCheckGPS().equals("0")) {
								is_check_gps = false;
							}
							prefEditor.putBoolean("is_check_gps", is_check_gps);

							prefEditor.commit();
							hideProgress();
							openMain();
							break;

						}

					} else {
						if (result.getClientClientExcCode().equals(
								ClientExceptionCodeResult.RESULT_ERROR
										.toString())) {
							hideProgress();
							showAlertDialog(R.string.Exception,
									R.string.ServerException, null);

						}
						if (result.getClientClientExcCode().equals(
								ClientExceptionCodeResult.RESULT_TIMEOUT
										.toString())) {
							hideProgress();
							showAlertDialog(R.string.Exception,
									R.string.TimeOutException, null);
						}
					}
					hideProgress();
				} else {
					hideProgress();
					showAlertDialog(R.string.Exception, result
							.getRequestStatus().RequestStatusCode(), null);
				}

			} catch (Exception e) {
				hideProgress();
				showAlertDialog(R.string.LoginMsgTitle, R.string.Exception,
						null);
				GlobalApp.WriteLogException(e);
			}
		}

	}

	private DialogInterface.OnClickListener userLocked = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			try {

				Intent intentLocked = new Intent(getApplicationContext(), LockedActivity.class);
				intentLocked.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intentLocked);
				hideProgress();
				finish();
			} catch (Exception e) {
				GlobalApp.WriteLogException(e);
				hideProgress();

			}

		}
	};

	@Override
	public void onBackPressed() {
	
		
		Dialog dialog;
		AlertDialog.Builder builder;
		builder = new AlertDialog.Builder(LoginActivity.this);
		builder.setMessage(R.string.ExitDialog)
				.setCancelable(true)
				.setNegativeButton(R.string.No, null)
				.setPositiveButton(R.string.YesC,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								  android.os.Process.killProcess(android.os.Process.myPid());
							}
						});
		dialog = builder.create();
		dialog.show();
		

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		System.gc();
	}
}