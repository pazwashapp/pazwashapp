package com.segment.pazwash;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.segment.pazwash.BL.BBStepWriter;
import com.segment.pazwash.BL.GlobalApp;

import com.segment.pazwash.BL.NetworkChk;

public class SendBBService extends Service {
	
	
	static final int UPDATE_INTERVAL = 1;
	private Timer timer = new Timer();

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {

		// We want this service to continue running until it is explicitly
		// stopped, so return sticky.
		// Toast.makeText(this, "Service Started", Toast.LENGTH_LONG).show();
		SendOfflineRepeatedly();
		return START_STICKY;
	}

	private void SendOfflineRepeatedly() {
		timer.scheduleAtFixedRate(new TimerTask() {
			public void run() {
				if (NetworkChk.checkInternet(SendBBService.this,GlobalApp.getIsVPN())) {

					try {
						SendBBS();

					} catch (Exception ex) {

						GlobalApp.WriteLogException(ex);
						stopSelf();
					}
				}
			}
		}, 0, (UPDATE_INTERVAL * 60 * 1000));
	}

	private void SendBBS() {
		BBStepWriter bbs = new BBStepWriter(this);
		try {
			bbs.open();
			boolean result = bbs.deleteSendedBB();
			bbs.close();
			bbs.sendBB();

		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
			//logger.DeleteSendedLogs();			
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (timer != null) {
			timer.cancel();
		}
		System.gc();

	}

}
