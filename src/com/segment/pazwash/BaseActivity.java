package com.segment.pazwash;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;

import com.segment.pazwash.BL.BBStepWriter;
import com.segment.pazwash.BL.DBAdapter;
import com.segment.pazwash.BL.GPS;
import com.segment.pazwash.BL.GlobalApp;
import com.segment.pazwash.BL.IntentIntegrator;
import com.segment.pazwash.BL.Logger;
import com.segment.pazwash.BL.NetworkChk;
import com.segment.pazwash.DM.DataParameter;
import com.segment.pazwash.DM.GPSData;
import com.segment.pazwash.DM.IsActiveResult;
import com.segment.pazwash.DM.RequestWash;
import com.segment.pazwash.Enums.BlackBoxStep;
import com.segment.pazwash.Enums.ClientExceptionCodeResult;
import com.segment.pazwash.Enums.LPScanType;
import com.segment.pazwash.Enums.RequestStatuz;
import com.segment.pazwash.Enums.TimeOutTry;
import com.segment.pazwash.WS.WS;

public abstract class BaseActivity extends Activity {
	// private static final int TIME_TO_SAVE_LOGIN = 30;
	protected static String PAZWASH_WS_URL = "http://172.30.4.75:8090/PazWashService.asmx";
	protected static String PAZWASH_WS_LINK = "http://172.30.4.75:8091/PazWashApp.apk";
	protected static String PAZWASH_WS_ZXINGLINK = "http://172.30.4.75:8091/Barcode.apk";

	// protected static String PAZWASH_WS_LINK =
	// "http://172.30.13.60/pazwashws";

	protected int ActivityType = 0;
	protected ProgressDialog progressDialog;
	protected String userName;
	GPSData gps = new GPSData("0.0", "0.0");
	ProgressDialog mProgressDialog;;

	// protected int CountDhe = 0;
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		try {
			if (ActivityType == 1) {
				MenuInflater inflater = getMenuInflater();
				inflater.inflate(R.menu.menu, menu);
				return true;
			}
			if (ActivityType == 0) {
				MenuInflater inflater = getMenuInflater();
				inflater.inflate(R.menu.initmenu, menu);
				return true;
			}

			return false;

		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
			return false;
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		try {

			CleanGlobalApp();

			switch (item.getItemId()) {
			case R.id.icMenuLogin:
				openLogin();
				break;

			case R.id.icMenuCamera:
				GlobalApp.setWithBarcode(false);
				openCamera();
				break;

			case R.id.icMenuManual:
				GlobalApp.setWithBarcode(false);
				openManual();
				break;

			case R.id.icMenuCancelDeals:
				openCancelDeals();
				break;

			case R.id.icMenuBarcode:
				GlobalApp.setWithBarcode(true);
				openScanBarcode();
				break;

			case R.id.icMenuReports:
				openReports();
				break;

			case R.id.icMenuHelp:
				break;

			case R.id.icMenuGps:
				openGPS();
				break;

			case R.id.icMenuCancelDealsOffline:
				openRejectOffline();
				break;

			case R.id.icMenuSettings:
				showSettingsDialog();
				// openSettings();
				break;

			case R.id.icMenuPreferences:
				showPreferencesActivity();
				break;

			/*
			 * case R.id.icMenuDownload: downloadLastVersion(); break;
			 */
			}
			return true;
		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
			return false;
		}
	}

	/*
	 * protected void downloadLastVersion() {
	 * 
	 * Dialog dialog; AlertDialog.Builder builder; builder = new
	 * AlertDialog.Builder(BaseActivity.this);
	 * builder.setMessage(R.string.DownloadDialog) .setCancelable(true)
	 * .setNegativeButton(R.string.No, null) .setPositiveButton(R.string.YesC,
	 * new DialogInterface.OnClickListener() { public void
	 * onClick(DialogInterface dialog, int id) {
	 * 
	 * showProgress();
	 * 
	 * DBAdapter db = new DBAdapter(BaseActivity.this); db.open();
	 * 
	 * if (!isOfflineData(db)) { db.close(); BaseActivity.this
	 * .deleteDatabase("paz_offline.db"); SharedPreferences sharedPreferences =
	 * getSharedPreferences( "PazWashPreferences", MODE_PRIVATE);
	 * SharedPreferences.Editor prefEditor = sharedPreferences .edit();
	 * prefEditor.clear(); prefEditor.commit();
	 * 
	 * DownloadFile downloadFile = new DownloadFile( "PazWashApp.apk");
	 * downloadFile.execute(PAZWASH_WS_LINK + "/apk/PazWashApp.apk");
	 * 
	 * } else {
	 * 
	 * showAlertDialog(R.string.DownloadDialog, R.string.OfflineMode, null); } }
	 * }); dialog = builder.create(); dialog.show();
	 * 
	 * }
	 */

	protected boolean isCallable(Intent intent) {
		List<ResolveInfo> list = getPackageManager().queryIntentActivities(
				intent, PackageManager.MATCH_DEFAULT_ONLY);
		return list.size() > 0;
	}

	protected void openSettings() {

		Intent settings = new Intent(BaseActivity.this, SettingsActivity.class);
		settings.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(settings);
		finish();
	}

	protected void showSettingsDialog() {

		AlertDialog.Builder alert = new AlertDialog.Builder(BaseActivity.this);

		alert.setTitle(R.string.EnterPassword);
		// Set an EditText view to get user input
		final EditText input = new EditText(BaseActivity.this);
		input.setInputType(InputType.TYPE_CLASS_NUMBER);
		alert.setView(input);

		alert.setPositiveButton(R.string.YesC,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						Editable value = input.getText();
						String v = value.toString().replace(" ", "");
						SharedPreferences sharedPreferences = getSharedPreferences("PazWashPreferences", MODE_PRIVATE);
						String deviceId = sharedPreferences.getString("deviceId", "0");
						if (deviceId == "0") {
							TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
							GlobalApp.setDeviceId(telephonyManager.getDeviceId());
							SharedPreferences.Editor prefEditor = sharedPreferences.edit();
							prefEditor.putString("deviceId", telephonyManager.getDeviceId());
							prefEditor.commit();
						}

						if (v.equals(deviceId)) {
							try {

								openSettings();

							} catch (Exception e) {
								GlobalApp.WriteLogException(e);

							}

						} else {
							Dialog dialog1;
							AlertDialog.Builder builder;
							builder = new AlertDialog.Builder(BaseActivity.this);
							builder.setMessage(R.string.PasswordOfflineInvalid)
									.setCancelable(false)
									.setNegativeButton(R.string.No, null)
									.setPositiveButton(
											R.string.YesC,
											new DialogInterface.OnClickListener() {
												public void onClick(
														DialogInterface dialog1,
														int id) {

													showSettingsDialog();
												};
											});

							dialog1 = builder.create();
							dialog1.show();

						}

					}
				});

		alert.setNegativeButton(R.string.btnCancel, null);

		alert.show();

	}	

	protected void loadProgressDialog() {
		try {
			progressDialog = new ProgressDialog(this);
			progressDialog.setCancelable(false);
			progressDialog.setMessage(getResources().getString(
					R.string.ProgressMessage));
		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
		}
	}

	protected void showProgress() {
		try {
			progressDialog.show();
		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
		}
	}

	protected void hideProgress() {
		try {
			if (progressDialog != null) {
				progressDialog.hide();
			}
		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
		}
	}

	protected void showAlertDialog(int title, String msg,
			DialogInterface.OnClickListener onClickListener) {

		try {
			Resources res = getResources();
			AlertDialog.Builder myAlertDialog = new AlertDialog.Builder(
					BaseActivity.this);
			myAlertDialog.setTitle(String.format(res.getString(title)));
			myAlertDialog.setMessage(msg);
			myAlertDialog.setPositiveButton(res.getString(R.string.btnOK),
					onClickListener);
			myAlertDialog.setCancelable(false);
			myAlertDialog.show();
		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
		}
	}

	protected void showAlertDialog(int title, int msg, DialogInterface.OnClickListener onClickListener) {
		try {
			Resources res = getResources();
			AlertDialog.Builder myAlertDialog = new AlertDialog.Builder(BaseActivity.this);
			myAlertDialog.setTitle(String.format(res.getString(title)));
			myAlertDialog.setMessage(String.format(res.getString(msg)));
			myAlertDialog.setPositiveButton(res.getString(R.string.btnOK), onClickListener);
			myAlertDialog.setCancelable(false);
			myAlertDialog.show();
		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
		}
	}

	protected void openLogin() {
		// Open loginActivity
		try {
			Intent intentLogin = new Intent(BaseActivity.this,
					LoginActivity.class);
			intentLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intentLogin);
			finish();
		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
		}
	}

	protected void showPreferencesActivity() {
		try {

			Intent preferences = new Intent(BaseActivity.this,
					PreferencesActivity.class);
			preferences.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(preferences);

		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
		}
	}

	protected void openGPS() {
		try {

			Intent intentGPS = new Intent(BaseActivity.this, GPSActivity.class);
			intentGPS.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intentGPS);

		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
		}
	}

	protected void openRejectOffline() {
		try {

			Intent intentReject = new Intent(BaseActivity.this,
					RejectOfflineActivity.class);
			intentReject.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intentReject);

		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
		}
	}

	protected void openMain() {
		try {
			Intent intentLogin = new Intent(BaseActivity.this, MainActivity.class);
			intentLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intentLogin);
			finish();
		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
		}
	}

	private String setTempWashId() {
		try {
			Date now = new Date();
			SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
			return format.format(now);
		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
			return "";
		}
	}

	protected String setImagePath() {
		try {

			File f = new File(Environment.getExternalStorageDirectory()
					+ "/images");
			if (f.isDirectory()) {

				Date now = new Date();
				SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
				return Environment.getExternalStorageDirectory() + "/images/"
						+ GlobalApp.getUserName() + format.format(now) + ".jpg";
			} else {
				String newFolder = "/images";
				String extStorageDirectory = Environment
						.getExternalStorageDirectory().toString();
				File myNewFolder = new File(extStorageDirectory + newFolder);
				myNewFolder.mkdir();

				Date now = new Date();
				SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
				return Environment.getExternalStorageDirectory() + "/images/"
						+ GlobalApp.getUserName() + format.format(now) + ".jpg";

			}

		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
			return "";
		}
	}

	protected void CleanGlobalApp() {

		if (!GlobalApp.withBarcode) {
			GlobalApp.setQRCode("");
		}

		GlobalApp.setWashId("0");
		GlobalApp.setApproveResult(null);
		GlobalApp.setLPResult(null);
	
	}

	protected void onBPressed() {
		// Your code here
		super.onBackPressed();
	}

	protected void openScanBarcode() {
		try {
			showProgress();
			CleanGlobalApp();
			GlobalApp.setLPScanType(LPScanType.QR_CODE.toString());
			if (NetworkChk.checkInternet(BaseActivity.this,GlobalApp.getIsVPN()) && GlobalApp.getTimeOutTry() != TimeOutTry.TIMEOUT_TRY_OFFLINE && GlobalApp.IsServerOK() == true) {
				GlobalApp.setIsOffline(false);
				SharedPreferences sharedPreferences = getSharedPreferences("PazWashPreferences", MODE_PRIVATE);
				boolean is_check_gps = sharedPreferences.getBoolean("is_check_gps", true);
				if (is_check_gps) {
					checkGPS();
					gps = GPS.getGPS(BaseActivity.this);
				} else {
					gps = new GPSData("0.0", "0.0");
				}
				getWashRequestResult rw = new getWashRequestResult(GlobalApp.getUserName(), GlobalApp.getMDCode(),GlobalApp.getDeviceId(), Integer.toString(BlackBoxStep.WASH_REQUEST_BARCODE.ToInt()));
				rw.execute(PAZWASH_WS_URL + "/addBBWashRequest");
			} else {
				if (!GlobalApp.IsOffline()) {
					openOffline();
				} else {
					DBAdapter db = new DBAdapter(BaseActivity.this);
					db.open();
					int offline_count = db.getCountOffline();
					db.close();
					SharedPreferences sharedPreferences = getSharedPreferences("PazWashPreferences", MODE_PRIVATE);
					int offline_limit = sharedPreferences.getInt("offline_limit", 10);
					if (offline_count < offline_limit) {
						hideProgress();
						IntentIntegrator.initiateScan(BaseActivity.this);
					} else {
						hideProgress();
						WriteOfflineLimitDB(offline_limit);	
						showAlertDialog(R.string.OfflineMsgTitle, R.string.OfflineMsgLimitError, null);
					}
				}
			}
			// IntentIntegrator.initiateScan(this);
		}
		catch (Exception e) {
			GlobalApp.WriteLogException(e);
		}
	}

	protected void openScanActivity() {
		IntentIntegrator.initiateScan(BaseActivity.this);
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case IntentIntegrator.REQUEST_CODE: {
			if (resultCode != RESULT_CANCELED) {
				com.segment.pazwash.BL.IntentResult scanResult = IntentIntegrator
						.parseActivityResult(requestCode, resultCode, data);

				boolean isScanValue = false;

				if (scanResult != null) {

					// showProgress();
					String upc = scanResult.getContents();
					upc = upc.replaceAll("[^\\d.]", "");
					if (upc != null && upc != "" && !(upc.contains("No"))) {
						isScanValue = true;
					}

					GlobalApp.setQRCode(upc);

					// Handler handler = new Handler();
					// handler.postDelayed(new Runnable() {
					// public void run() {
					/*
					 * Intent intentCamera = new Intent(BaseActivity.this,
					 * CameraActivity.class);
					 * 
					 * intentCamera.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					 * startActivity(intentCamera);
					 */

					// }
					// }, 3000);

					// Toast.makeText(getBaseContext(), upc, Toast.LENGTH_LONG)
					// .show();

				}

				Intent fromScanActivity = new Intent(BaseActivity.this,
						FromScanActivity.class);
				fromScanActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				fromScanActivity.putExtra("isScanValue", isScanValue);
				startActivity(fromScanActivity);
			}

			break;
		}
		}
	}

	
	
	
	
	protected void openCamera() {
		try {
			GlobalApp.WriteLog("GeneralLog","openCamera","start");
			showProgress();
			CleanGlobalApp();
			GlobalApp.setLPScanType(LPScanType.LICENCE_PLATE_LPR.toString());
			if (NetworkChk.checkInternet(BaseActivity.this,GlobalApp.getIsVPN())&& GlobalApp.getTimeOutTry() != TimeOutTry.TIMEOUT_TRY_OFFLINE && GlobalApp.IsServerOK() == true) {
				SharedPreferences sharedPreferences = getSharedPreferences("PazWashPreferences", MODE_PRIVATE);
				boolean is_check_gps = sharedPreferences.getBoolean("is_check_gps", true);
				if (is_check_gps) {
					GlobalApp.WriteLog("GeneralLog","openCamera-checkGPS","before");
					checkGPS();
					GlobalApp.WriteLog("GeneralLog","openCamera-checkGPS","after");
					gps = GPS.getGPS(BaseActivity.this);
				} else {
					gps = new GPSData("0.0", "0.0");
				}
				GlobalApp.setIsOffline(false);
				getWashRequestResult rw = new getWashRequestResult(GlobalApp.getUserName(), GlobalApp.getMDCode(), GlobalApp.getDeviceId(), Integer.toString(BlackBoxStep.WASH_REQUEST_LPR.ToInt()));
				rw.execute(PAZWASH_WS_URL + "/addBBWashRequest");
			}
			else {
				if (!GlobalApp.IsOffline()) {
					openOffline();
				} else {
					DBAdapter db = new DBAdapter(BaseActivity.this);
					db.open();
					int offline_count = db.getCountOffline();
					db.close();
					SharedPreferences sharedPreferences = getSharedPreferences("PazWashPreferences", MODE_PRIVATE);
					int offline_limit = sharedPreferences.getInt("offline_limit", 10);
					if (offline_count < offline_limit) {
						Intent intentCamera = new Intent(BaseActivity.this, CameraActivity.class);
						intentCamera.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(intentCamera);
						Handler handler = new Handler();
						handler.postDelayed(new Runnable() {
							public void run() {
								hideProgress();
							}
						}, 1500);

					} else {
						hideProgress();						
						WriteOfflineLimitDB(offline_limit);						
						showAlertDialog(R.string.OfflineMsgTitle, R.string.OfflineMsgLimitError, null);
					}
				}

			}

		}
		catch (Exception e) {
			GlobalApp.WriteLogException(e);
		}
	}

	protected void WriteOfflineLimitDB(int offline_limit) {
		Logger logger = new Logger(BaseActivity.this);
		logger.open();
		long id = logger.addLog("OVER_OFFLINE_LIMIT", "Over Offline Limit", getUserName(),
				R.string.OfflineMsgLimitError + ": " + Integer.toString(offline_limit), "false", GlobalApp.GetTimeNow(), GlobalApp.getIpAddress(), GlobalApp.getDeviceId(),
				GlobalApp.getMDCode(), GlobalApp.getStationNumber(), GlobalApp.getStationID(),
				GlobalApp.getVersionApp());
		logger.close();
	}

	protected void openManual() {
		try {

			showProgress();
			CleanGlobalApp();
			GlobalApp.setLPScanType(LPScanType.LICENCE_PLATE_MANUAL.toString());

			if (NetworkChk.checkInternet(BaseActivity.this,
					GlobalApp.getIsVPN())
					&& GlobalApp.getTimeOutTry() != TimeOutTry.TIMEOUT_TRY_OFFLINE) {

				GlobalApp.setIsOffline(false);

				getWashRequestResult rw = new getWashRequestResult(
						GlobalApp.getUserName(), GlobalApp.getMDCode(),
						GlobalApp.getDeviceId(),
						Integer.toString(BlackBoxStep.WASH_REQUEST_TYPING
								.ToInt()));
				rw.execute(PAZWASH_WS_URL + "/addBBWashRequest");
			} else {

				if (!GlobalApp.IsOffline()) {
					openOffline();
				} else {
					Intent intentCamera = new Intent(BaseActivity.this,
							CameraActivity.class);
					intentCamera.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intentCamera);
				}
			}

			/*
			 * Intent intentManual = new Intent(BaseActivity.this,
			 * LpManualActivity.class);
			 * 
			 * intentManual.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			 * startActivity(intentManual);
			 */
		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
		}

	}

	public void openOfflineTry() {

		showAlertDialog(R.string.NoConnection, R.string.noConnectionTry,
				noConnection);

	}

	public void openOffline() {

		showAlertDialog(R.string.NoConnection, R.string.OfflineMode,
				noConnection);

	}

	private DialogInterface.OnClickListener noConnection = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			hideProgress();

			GlobalApp.setIsOffline(true);
			GlobalApp.setIsWithOutLpr(true);
			GlobalApp.setWashId("0");
			GlobalApp.setLPScanType(LPScanType.LICENCE_PLATE_MANUAL.toString());
			Intent offlineMain = new Intent(BaseActivity.this, MainActivity.class);
			offlineMain.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(offlineMain);
			finish();
		}
	};

	protected void openReports() {
		try {
			Intent reports = new Intent(BaseActivity.this, ReportsMenu.class);
			reports.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(reports);
		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
		}
	}

	protected void openCancelDeals() {
		try {
			Intent intentCancelDeals = new Intent(BaseActivity.this,
					RejectActivity.class);
			intentCancelDeals.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intentCancelDeals);
		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
		}
	}

	protected String getDate(long milliSeconds, String dateFormat) {
		// Create a DateFormatter object for displaying date in specified
		// format.
		DateFormat formatter = new SimpleDateFormat(dateFormat);

		// Create a calendar object that will convert the date and time value in
		// milliseconds to date.
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(milliSeconds);
		return formatter.format(calendar.getTime());
	}

	protected boolean isInLoginTime() {

		try {

			boolean res = false;

			SharedPreferences sharedPreferences = getSharedPreferences(
					"PazWashPreferences", MODE_PRIVATE);
			String userName = sharedPreferences.getString("UserName", "");
			int TIME_TO_SAVE_LOGIN = sharedPreferences.getInt("login_session_min", 480);
			if (!userName.isEmpty()) {
				Long loginTime = sharedPreferences.getLong("LoginTime", 0);
				if ((System.currentTimeMillis() - loginTime) <= (TIME_TO_SAVE_LOGIN * 60 * 1000)) {
					res = true;
				}
			}
			return res;
		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
			return false;
		}
	}

	protected void checkGPS() {

		try {

			if (GPS.CheckGps(this)) {

			} else {
				showAlertDialog(R.string.CheckGPS, R.string.PleaseSetGpsOn,
						openGPSSettingsListener);
			}
		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
		}

	}

	protected void setGPS() {

		try {
			// Toast.makeText(this, "����� GPS",
			// Toast.LENGTH_SHORT).show();

			if (GPS.CheckGps(this)) {

				gps = GPS.getGPS(BaseActivity.this);

			} else {
				showAlertDialog(R.string.CheckGPS, R.string.PleaseSetGpsOn,
						openGPSSettingsListener);
			}
		} catch (Exception e) {

			GlobalApp.WriteLogException(e);

		}

	}

	private DialogInterface.OnClickListener openGPSSettingsListener = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			startActivity(new Intent(
					android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
		}
	};

	protected void WriteStep(String WashId, BlackBoxStep bbs,
			String AdditionalData) {
		try

		{

			BBStepWriter bbwriter = new BBStepWriter(BaseActivity.this);

			bbwriter.open();
			bbwriter.addStep(WashId, bbs.toString(), GlobalApp.GetTimeNow(),
					AdditionalData, "false", GlobalApp.getUserName(),
					GlobalApp.getMDCode(), GlobalApp.getDeviceId(),
					GlobalApp.getStationNumber(), GlobalApp.getStationID());
			bbwriter.close();

		}

		catch (Exception e) {
			GlobalApp.WriteLogException(e);
		}

	}

	protected String getUserName() {
		try {
			SharedPreferences sharedPreferences = getSharedPreferences("PazWashPreferences", MODE_PRIVATE);
			return sharedPreferences.getString("UserName", "");
		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
			return "";
		}
	}
	
	
	protected String getStationNumber() {

		try {
			SharedPreferences sharedPreferences = getSharedPreferences("PazWashPreferences", MODE_PRIVATE);
			return sharedPreferences.getString("StationNumber", "");
		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
			return "";
		}
	}

	
	protected void ServerExceptionDialog()
	{
		   GlobalApp.setCountServerError(0);
		   GlobalApp.setIsServerOK(false);
		   openOffline();
	}
	private DialogInterface.OnClickListener TimeOutListener = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			openOffline();
		}
	};
	public String getNetworkState() {
	    try {
	        ConnectivityManager nInfo = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	        return nInfo.getActiveNetworkInfo().getDetailedState().toString() + ":" + nInfo.getActiveNetworkInfo().getTypeName() + ":" + nInfo.getActiveNetworkInfo().getReason();
	    } catch (Exception e) {
	        return "exception";
	    }
	}

	
	
	
	public class getWashRequestResult extends AsyncTask<String, Void, RequestWash> {
		String UserName;
		String MdCode;
		String DeviceId;
		String RequestWashType;
		getWashRequestResult(String userName, String mdCode, String deviceId, String requestWashType) {
			this.UserName = userName;
			this.MdCode = mdCode;
			this.DeviceId = deviceId;
			this.RequestWashType = requestWashType;
		}
		
		
		protected RequestWash doInBackground(String... urls) {
			GlobalApp.WriteLog("GeneralLog","getWashRequestResult-doInBackground", "(" + String.valueOf(GlobalApp.gsmSignalStrength) + "," + GlobalApp.DataConnectionState +  "):" + getNetworkState());
			WS ws = new WS();
			List<DataParameter> parameters = BuildParameters();
			return ws.getRequestWashResult(parameters, urls[0]);
		}

		private List<DataParameter> BuildParameters() {			
			List<DataParameter> parameters = new ArrayList<DataParameter>();
			parameters.add(new DataParameter("userName", this.UserName));
			parameters.add(new DataParameter("mdCode", this.MdCode));
			parameters.add(new DataParameter("deviceId", this.DeviceId));
			if(GlobalApp.getQRCode() != "")
			{
				parameters.add(new DataParameter("scan_type_code", LPScanType.QR_CODE.toString()));
			}
			else
			{
				parameters.add(new DataParameter("scan_type_code", GlobalApp.getLPScanType()));
			}
			parameters.add(new DataParameter("requestWashType", this.RequestWashType));
			parameters.add(new DataParameter("GPS_Lon", gps.getLon()));
			parameters.add(new DataParameter("GPS_Lat", gps.getLat()));
			parameters.add(new DataParameter("stationNumber", GlobalApp.getStationNumber()));
			parameters.add(new DataParameter("stationId", GlobalApp.getStationID()));
			parameters.add(new DataParameter("sysVer", GlobalApp.getSysVersion()));
			parameters.add(new DataParameter("mdType", GlobalApp.getMDType()));
			parameters.add(new DataParameter("pazWashAppVer", GlobalApp.getVersionApp()));
			return parameters;
		}

		protected void onPostExecute(RequestWash result) {
			try {
				GlobalApp.WriteLog("GeneralLog","getWashRequestResult-onPostExecute","");
				if (result.getRequestStatus().RequestStatusCode().equals(RequestStatuz.WSGW_OK.toString())) {
					if (result.getClientClientExcCode().equals(ClientExceptionCodeResult.RESULT_OK.toString())) {
						if (result.getRequestWashResult() != -1) {
							SharedPreferences sharedPreferences = getSharedPreferences("PazWashPreferences", MODE_PRIVATE);
							SharedPreferences.Editor prefEditor = sharedPreferences.edit();
							prefEditor.putInt("Station_Typing_Limit_Balance",result.getStationTypingLimitBalance());
							prefEditor.commit();
							GlobalApp.setWashId(Integer.toString(result.getRequestWashResult()));
							String type = "";
							type = GlobalApp.getLPScanType();
							switch (Integer.parseInt(type)) {
							case 2:
								GlobalApp.setCountServerError(0);
								GlobalApp.setIsWithOutLpr(false);
								Intent intentCamera = new Intent(BaseActivity.this, CameraActivity.class);
								intentCamera.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
								startActivity(intentCamera);
								hideProgress();
								break;

							case 1:
								GlobalApp.setCountServerError(0);
								GlobalApp.setIsWithOutLpr(true);
								Intent intentCamera1 = new Intent(BaseActivity.this, CameraActivity.class);
								intentCamera1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
								startActivity(intentCamera1);
								hideProgress();
								break;

							case 4:
								GlobalApp.setCountServerError(0);
								GlobalApp.setIsWithOutLpr(false);
								openScanActivity();
								hideProgress();
								break;
							}
						}

						else if (result.getBBResult() != null && result.getBBResult().getBBResultCode() == -99) {
							GlobalApp.setCountServerError(0);
							GlobalApp.setWashId("-1");
							if (!isFinishing()) {
								showAlertDialog(R.string.WashRequestDeny,  R.string.Exception, null);
							}

						}

						else {
							GlobalApp.setWashId("-1");
							GlobalApp.setCountServerError(0);
							if (result.getBBResult() != null
									&& result.getBBResult().getBBResultCode() == -100) {
								if (!isFinishing()) {
									showAlertDialog(R.string.WashRequestDeny, R.string.Exception, null);
								}
							} else {
								if (!isFinishing()) {
									showAlertDialog(R.string.WashRequestDeny,
											result.getBBResult()
													.getBBResultDesc(), null);
								}
							}

						}

					} else {
						if (result.getClientClientExcCode().equals(
								ClientExceptionCodeResult.RESULT_ERROR
										.toString())) {
							hideProgress();
							if (!isFinishing()) {						
								//ServerExceptionDialog();
								int countServErr  = GlobalApp.countServerError();
								if(countServErr == 2)
								{
									ServerExceptionDialog();
									
								}
								else
								{
									GlobalApp.setCountServerError(countServErr + 1);
								//production version:
								showAlertDialog(R.string.Exception,
										R.string.OnceAgain, null);
								}
							}

						}
						if (result.getClientClientExcCode().equals(
								ClientExceptionCodeResult.RESULT_TIMEOUT
										.toString())) {
							GlobalApp.setCountServerError(0);
							hideProgress();
							if (!isFinishing()) {

								if (GlobalApp.getTimeOutTry() == TimeOutTry.TIMEOUT_TRY_OFFLINE) {
									showAlertDialog(R.string.Exception,
											R.string.TimeOutException,
											TimeOutListener);
								} else {
									showAlertDialog(R.string.Exception,
											R.string.TimeOutException, null);
								}
							}
						}
					}
					hideProgress();
				} else {
					hideProgress();
					showAlertDialog(R.string.Exception, result
							.getRequestStatus().RequestStatusCode(), null);
				}

			} catch (Exception e) {
				GlobalApp.WriteLogException(e);
				hideProgress();
			}

		}
	}

	public class getIsActiveResult extends
			AsyncTask<String, Void, IsActiveResult> {

		String MdCode;

		getIsActiveResult(String mdCode) {

			this.MdCode = mdCode;

		}

		protected IsActiveResult doInBackground(String... urls) {

			WS ws = new WS();
			List<DataParameter> parameters = BuildParameters();
			return ws.getIsActiveResult(parameters, urls[0]);

		}

		private List<DataParameter> BuildParameters() {

			List<DataParameter> parameters = new ArrayList<DataParameter>();
			parameters.add(new DataParameter("mdCode", this.MdCode));
			return parameters;

		}

		protected void onPostExecute(IsActiveResult result) {
			try {
				if (result.getIsActive() == true) {
					Intent intentLogin = new Intent(BaseActivity.this, MainActivity.class);
					intentLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intentLogin);
					finish();
				}
				else {
					if (result.getClientClientExcCode().equals(
							ClientExceptionCodeResult.RESULT_ERROR.toString())) {
						hideProgress();
						if (!isFinishing()) {
							showAlertDialog(R.string.Exception,
									R.string.ServerException, null);
						}
					}
					if (result.getClientClientExcCode()
							.equals(ClientExceptionCodeResult.RESULT_TIMEOUT.toString())) {
						hideProgress();
						if (!isFinishing()) {
							showAlertDialog(R.string.Exception,
									R.string.TimeOutException, null);
						}
					}
				}
				hideProgress();
			} catch (Exception e) {
				GlobalApp.WriteLogException(e);
				hideProgress();
			}
		}
	}

	// public class DownloadFile extends AsyncTask<String, Integer, String> {
	//
	// String appName;
	//
	// DownloadFile(String _appName) {
	//
	// this.appName = _appName;
	//
	// }
	//
	// @Override
	// protected String doInBackground(String... sUrl) {
	// try {
	// hideProgress();
	// URL url = new URL(sUrl[0]);
	// URLConnection connection = url.openConnection();
	// HttpURLConnection httpConn = (HttpURLConnection) connection;
	// httpConn.setAllowUserInteraction(false);
	// httpConn.setInstanceFollowRedirects(true);
	// httpConn.setDoOutput(true);
	// httpConn.connect();
	// // this will be useful so that you can show a typical 0-100%
	// // progress bar
	// int fileLength = connection.getContentLength();
	//
	// // download the file
	// InputStream input = new BufferedInputStream(url.openStream());
	// OutputStream output = new FileOutputStream(
	// // "/sdcard/PazWashApp.apk");
	// "/sdcard/" + this.appName);
	// byte data[] = new byte[1024];
	// long total = 0;
	// int count;
	// while ((count = input.read(data)) != -1) {
	// total += count;
	// // publishing the progress....
	// publishProgress((int) (total * 100 / fileLength));
	// output.write(data, 0, count);
	// }
	//
	// output.flush();
	// output.close();
	// input.close();
	//
	// // progress.setVisibility(View.GONE);
	//
	// Intent intent = new Intent(Intent.ACTION_VIEW);
	// intent.setDataAndType(
	// // Uri.fromFile(new File("/sdcard/PazWashApp.apk")),
	// Uri.fromFile(new File("/sdcard/" + this.appName)),
	// "application/vnd.android.package-archive");
	// startActivity(intent);
	//
	// } catch (Exception e) {
	// hideProgress();
	// GlobalApp.WriteLogException(e);
	// // WriteLogException(e);
	// return "-1";
	// }
	// return null;
	// }
	//
	// @Override
	// protected void onPreExecute() {
	// try {
	// super.onPreExecute();
	// Resources res = getResources();
	// mProgressDialog = new ProgressDialog(BaseActivity.this);
	// mProgressDialog.setMessage(res.getString(R.string.DownloadLatestVersion));
	// mProgressDialog.setIndeterminate(false);
	// mProgressDialog.setMax(100);
	// mProgressDialog
	// .setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
	// mProgressDialog.show();
	//
	// } catch (Exception e) {
	// hideProgress();
	// GlobalApp.WriteLogException(e);
	// }
	// }
	//
	// @Override
	// protected void onProgressUpdate(Integer... progress) {
	// super.onProgressUpdate(progress);
	// mProgressDialog.setProgress(progress[0]);
	// }
	//
	// }

}
