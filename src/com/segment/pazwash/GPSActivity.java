package com.segment.pazwash;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.segment.pazwash.BL.GPS;
import com.segment.pazwash.BL.GlobalApp;

import com.segment.pazwash.DM.DataParameter;
import com.segment.pazwash.DM.GPSResult;

import com.segment.pazwash.WS.WS;

import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;

import android.os.AsyncTask;
import android.os.Bundle;

import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;

import android.widget.TextView;

public class GPSActivity extends BaseActivity {
	// LocationManager mlocManager;
	// LocationListener mlocListener;
	TextView longtitude;
	TextView latitude;
	// TextView address;
	TextView gpsDate;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		ActivityType = 1;
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.gps);
		loadProgressDialog();
		showProgress();
		// gps = GPS.getGPSWithAddress(this);

		Typeface hebBold = Typeface.createFromAsset(getAssets(), "fonts/droidsanshebrew-bold.ttf");
		
		TextView gpsTitle = (TextView) findViewById(R.id.gpsTv);
		gpsTitle.setTypeface(hebBold);
		
		TextView gpsLatTitle = (TextView) findViewById(R.id.gpsLatLTv);
		gpsLatTitle.setTypeface(hebBold);
		
		TextView gpsLonTitle = (TextView) findViewById(R.id.gpsLonLTv);
		gpsLonTitle.setTypeface(hebBold);
		
		
		TextView gpsDateTitle = (TextView) findViewById(R.id.gpsDateLTv);
		gpsDateTitle.setTypeface(hebBold);
		
		
		
		
		gps = GPS.getGPS(this);

		longtitude = (TextView) findViewById(R.id.gpsLonTv);
		latitude = (TextView) findViewById(R.id.gpsLatTv);
		// address = (TextView) findViewById(R.id.addressTv);
		gpsDate = (TextView) findViewById(R.id.gpsDateTv);
		
		

		if (gps != null) {
			// address.setText(gps.getAddress());
			longtitude.setText(gps.getLon());
			latitude.setText(gps.getLat());

			SharedPreferences sharedPreferences = getSharedPreferences(
					"PazWashPreferences", MODE_PRIVATE);

			long gpsTime = sharedPreferences.getLong("GPSTime", 0);
			if (gpsTime > 0) {
				Date date = new Date(gpsTime);
				SimpleDateFormat format = new SimpleDateFormat(
						"dd/MM/yyyy HH:mm");
				gpsDate.setText(format.format(date));
			}
		}

		// Button checkGPS = (Button) findViewById(R.id.checkGPSBtn);
		// checkGPS.setVisibility(View.GONE);
		// checkGPS.setOnClickListener(checkGPSlistener);

		try {

			hideProgress();

		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
			hideProgress();
		}

	}

	private void getAddress(double lat, double lon) {
		try {

			Geocoder gCoder = new Geocoder(GPSActivity.this);
			List<Address> addresses = gCoder.getFromLocation(lat, lon, 1);
			if (addresses != null && addresses.size() > 0) {

				int maxIndex = addresses.get(0).getMaxAddressLineIndex();
				StringBuilder sb = new StringBuilder();
				for (int x = 0; x <= maxIndex; x++) {
					sb.append(addresses.get(0).getAddressLine(x));
					sb.append(",");
				}
				sb.append(addresses.get(0).getLocality());
				sb.append("\n");
				// address.setText(sb.toString());
				// Toast.makeToast(GPSActivity.this, "country: " +
				// adresses.get(0).getCountryName(), Toast.LENGTH_LONG).show();
			}
		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
		}
	}

	private OnClickListener checkGPSlistener = new OnClickListener() {
		public void onClick(View v) {

			showProgress();

			// mlocManager = (LocationManager)
			// getSystemService(Context.LOCATION_SERVICE);

			// mlocListener = new MyLocationListener();

			// mlocManager.requestLocationUpdates(
			// LocationManager.NETWORK_PROVIDER, 0, 0, mlocListener);

		}
	};

	/*
	 * public class MyLocationListener implements LocationListener
	 * 
	 * {
	 * 
	 * @Override public void onLocationChanged(Location loc)
	 * 
	 * { try {
	 * 
	 * if (mlocManager == null || mlocListener == null) { return; }
	 * 
	 * Date date= new Date(loc.getTime()); SimpleDateFormat format = new
	 * SimpleDateFormat( "dd/MM/yyyy HH:mm");
	 * 
	 * gpsDate.setText(format.format(date));
	 * 
	 * longtitude.setText(Double.toString(loc.getLongitude()));
	 * latitude.setText(Double.toString(loc.getLatitude()));
	 * getAddress(loc.getLatitude(), loc.getLongitude());
	 * 
	 * // if (NetworkChk.checkInternet(GPSActivity.this)) { // new
	 * checkGPS(Double.toString(loc.getLongitude()), //
	 * Double.toString(loc.getLatitude())) // .execute(PAZWASH_WS_URL +
	 * "/getRadius"); // } else { // // hideProgress(); //
	 * showAlertDialog(R.string.Connection, R.string.NoConnection, // null); //
	 * }
	 * 
	 * mlocManager.removeUpdates(mlocListener); mlocManager = null;
	 * hideProgress(); } catch (Exception e) { hideProgress();
	 * GlobalApp.WriteLogException(e); mlocManager.removeUpdates(mlocListener);
	 * mlocManager = null;
	 * 
	 * }
	 * 
	 * }
	 * 
	 * @Override public void onProviderDisabled(String provider)
	 * 
	 * { hideProgress(); }
	 * 
	 * @Override public void onProviderEnabled(String provider)
	 * 
	 * {
	 * 
	 * }
	 * 
	 * @Override public void onStatusChanged(String provider, int status, Bundle
	 * extras)
	 * 
	 * {
	 * 
	 * } }
	 */

	public class checkGPS extends AsyncTask<String, Void, GPSResult> {
		String lon;
		String lat;

		checkGPS(String lon, String lat) {
			this.lon = lon;
			this.lat = lat;
		}

		protected GPSResult doInBackground(String... urls) {

			WS ws = new WS();
			List<DataParameter> parameters = BuildParameters();
			return ws.checkGPS(parameters, urls[0]);
		}

		private List<DataParameter> BuildParameters() {
			List<DataParameter> parameters = new ArrayList<DataParameter>();
			parameters.add(new DataParameter("Station_GPS_Lat", this.lat));
			parameters.add(new DataParameter("Station_GPS_Lon", this.lon));
			parameters.add(new DataParameter("station", GlobalApp
					.getStationID()));

			return parameters;
		}

		protected void onPostExecute(GPSResult result) {
			if (result.InRadius()) {

				showAlertDialog(R.string.CheckGPS, R.string.InRadius, null);

			} else {
				if (result.getBBResult().getBBResultCode() == -100) {

					showAlertDialog(R.string.CheckGPS, R.string.Exception, null);
				}
				{

					showAlertDialog(R.string.CheckGPS, result.getBBResult()
							.getBBResultDesc(), null);
				}
			}

			hideProgress();
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		/*
		 * if (mlocManager != null || mlocListener != null) {
		 * mlocManager.removeUpdates(mlocListener); mlocManager = null; }
		 */
		System.gc();
	}

}
