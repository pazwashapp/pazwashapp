package com.segment.pazwash;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import com.segment.pazwash.BL.DBAdapter;
import com.segment.pazwash.BL.GPS;
import com.segment.pazwash.BL.GlobalApp;
import com.segment.pazwash.BL.NetworkChk;
import com.segment.pazwash.DM.ApproveResult;
import com.segment.pazwash.DM.DataParameter;
import com.segment.pazwash.DM.GPSData;
import com.segment.pazwash.DM.LPResult;
import com.segment.pazwash.Enums.BlackBoxStep;
import com.segment.pazwash.Enums.ClientExceptionCodeResult;
import com.segment.pazwash.Enums.LPScanType;
import com.segment.pazwash.Enums.RequestStatuz;
import com.segment.pazwash.Enums.TimeOutTry;
import com.segment.pazwash.WS.WS;

public class ChooseSummaryActivity extends BaseActivity {
	Button Send;
	ArrayAdapter<String> adapter;

	String[] WashTypes;
	ListView lstView;
	LPResult lpResult = new LPResult();
	TextView selectedWashType;
	boolean isSendClick = false;
	boolean withWashId = false;

	// TextView selectedAddItem;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);

		lpResult = GlobalApp.getLPResult();

		setContentView(R.layout.selecteditems);

		loadProgressDialog();

		Typeface hebBold = Typeface.createFromAsset(getAssets(),
				"fonts/droidsanshebrew-bold.ttf");

		TextView title = (TextView) findViewById(R.id.titleTv);
		title.setTypeface(hebBold);

		selectedWashType = (TextView) findViewById(R.id.ErrorTv);
		selectedWashType.setTypeface(hebBold);
		// selectedAddItem = (TextView) findViewById(R.id.addItemTv);

		SetSelectedItems();

		Send = (Button) findViewById(R.id.SendBt);
		Send.setOnClickListener(btnSendListener);
		Send.setTypeface(hebBold);
	}

	private void SetSelectedItems() {

		try {

			StringBuilder sb = new StringBuilder();

			sb.append(GlobalApp.GeneralItemDesc());
			sb.append("\n");
			sb.append(GlobalApp.AddItemDesc());
			selectedWashType.setText(sb.toString());

			// selectedWashType.setText(GlobalApp.GeneralItemDesc());
			// selectedAddItem.setText(GlobalApp.AddItemDesc());
		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
		}

	}

	private OnClickListener btnSendListener = new OnClickListener() {
		public void onClick(View v) {

			showProgress();

			if (!isSendClick) {
				isSendClick = true;
				Send.setClickable(false);
				Send.setEnabled(false);
				Handler handler = new Handler();
				handler.postDelayed(new Runnable() {
					public void run() {
						isSendClick = false;
						Send.setClickable(true);
						Send.setEnabled(true);

					}
				}, 3000);
				try {
					if (NetworkChk.checkInternet(ChooseSummaryActivity.this, GlobalApp.getIsVPN()) && !GlobalApp.IsOffline()) {
						SharedPreferences sharedPreferences = getSharedPreferences("PazWashPreferences", MODE_PRIVATE);
						boolean is_check_gps = sharedPreferences.getBoolean("is_check_gps", true);
						if (is_check_gps) {
							checkGPS();
							gps = GPS.getGPS(ChooseSummaryActivity.this);
						} else {
							gps = new GPSData("0.0", "0.0");
						}
						new getApprove(lpResult.CarNumber(), lpResult.DbId(),
								Integer.toString(GlobalApp.getWashCode()),
								Integer.toString(GlobalApp.getAddItem()))
								.execute(PAZWASH_WS_URL + "/setBBWashApproveV2");
					} else {

						if (GlobalApp.IsOffline()) {
							withWashId = true;
							insertOffline();
						} else {
							openOffline();
						}
					}
				} catch (Exception e) {
					GlobalApp.WriteLogException(e);
				}
			}
		}
	};

	public boolean insertOffline() {
		try {
			setGPS();
			DBAdapter db = new DBAdapter(ChooseSummaryActivity.this);
			db.open();
			int offline_count = db.getCountOffline();
			db.close();

			SharedPreferences sharedPreferences = getSharedPreferences(
					"PazWashPreferences", MODE_PRIVATE);

			int offline_limit = sharedPreferences.getInt("offline_limit", 10);
			String washId = "0";
			if (withWashId) {
				washId = GlobalApp.getWashId();
			}

			if (offline_count < offline_limit) {

				db.open();

				Date now = new Date();
				SimpleDateFormat format = new SimpleDateFormat(
						"yyyy-MM-dd HH:mm:ss");

				long id = db.insertOffline(washId, lpResult.CarNumber(),
						GlobalApp.getImagePath(), "false", format.format(now),
						gps.getLon(), gps.getLat(), GlobalApp.getUserName(),
						Integer.toString(GlobalApp.getWashCode()),
						Integer.toString(GlobalApp.getAddItem()),
						GlobalApp.getDeviceId(), GlobalApp.getMDCode(),
						GlobalApp.getStationNumber(), GlobalApp.getStationID(),
						GlobalApp.getQRCode(), GlobalApp.getLPScanType(),
						GlobalApp.getVersionApp());

				db.close();

				if (id != -1) {

					WriteStep(
							GlobalApp.getWashId(),
							BlackBoxStep.WASH_SAVED_OFFLINE_ON_MD,
							"Saved offline " + " CarPlate "
									+ lpResult.CarNumber() + " WashCode "
									+ GlobalApp.getWashCode() + " AddItemCode "
									+ GlobalApp.getAddItem());
					// showAlertDialog(R.string.OfflineMsgTitle,
					// R.string.OfflineMsgText, openMainListener);

					// Toast.makeText(ChooseSummaryActivity.this,
					// R.string.OfflineMsgText, Toast.LENGTH_SHORT).show();
					GlobalApp.setCountDhe();
					showAlertDialog(R.string.OfflineMsgTitle,
							R.string.OfflineMsgText, SaveOkListener);

				} else {
					showAlertDialog(R.string.OfflineMsgTitle,
							R.string.SaveOfflineError, null);
					hideProgress();
				}

			} else {

				WriteStep(GlobalApp.getWashId(),
						BlackBoxStep.OFFLINE_LIMIT_EXCEEDED_ON_MD,
						"count offline: " + "" + " limit: " + offline_limit
								+ " CarPlate " + lpResult.CarNumber()
								+ " WashCode " + GlobalApp.getWashCode()
								+ " AddItemCode " + GlobalApp.getAddItem());
				
				WriteOfflineLimitDB(offline_limit);	
				

				showAlertDialog(R.string.OfflineMsgTitle,
						R.string.OfflineMsgLimitError, sendOfflineListener);
			}

			return true;
		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
			// WriteLogException(e);
			return false;
		}
	}

	private DialogInterface.OnClickListener sendOfflineListener = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			// Toast.makeText(ChooseSummaryActivity.this, "Checking Data...",
			// Toast.LENGTH_SHORT).show();
			if (NetworkChk.checkInternet(ChooseSummaryActivity.this,
					GlobalApp.getIsVPN())) {
				DBAdapter db = new DBAdapter(ChooseSummaryActivity.this);
				db.checkOfflineData();
			}
		}
	};

	private DialogInterface.OnClickListener SaveOkListener = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			openMain();
		}
	};
	
	private DialogInterface.OnClickListener TimeOutListenerApprove = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			openOfflineTry();
		}
	};

	private class getApprove extends AsyncTask<String, Void, ApproveResult> {

		String carNumber;
		String DbId;
		String service_code;
		String addServiceCode;
		String user;

		protected ApproveResult doInBackground(String... urls) {

			WS ws = new WS();
			List<DataParameter> parameters = BuildParameters();
			return ws.getApproveAddHTTP(parameters, urls[0]);
		}

		getApprove(String carNumber, String DbId, String servicecode,
				String AddServiceCode) {
			this.carNumber = carNumber;
			this.DbId = DbId;
			this.service_code = servicecode;
			this.addServiceCode = AddServiceCode;
			this.user = GlobalApp.getUserName();

		}

		private List<DataParameter> BuildParameters() {
			List<DataParameter> parameters = new ArrayList<DataParameter>();

		

			Date now = new Date();
			SimpleDateFormat format = new SimpleDateFormat(
					"yyyy-MM-dd HH:mm:ss");

			parameters.add(new DataParameter("washId", GlobalApp.getWashId()));
			parameters.add(new DataParameter("userName", this.user));
			parameters.add(new DataParameter("lpReference", this.carNumber));
			parameters.add(new DataParameter("lpImageId", this.DbId));
			parameters.add(new DataParameter("serviceCode", this.service_code));
			parameters.add(new DataParameter("washdate", format.format(now)));
			parameters.add(new DataParameter("additionalItems",
					this.addServiceCode));
			parameters.add(new DataParameter("GPS_Lon", gps.getLon()));
			parameters.add(new DataParameter("GPS_Lat", gps.getLat()));
			parameters.add(new DataParameter("deviceId", GlobalApp
					.getDeviceId()));
			parameters.add(new DataParameter("mdCode", GlobalApp.getMDCode()));

			
			
			if(GlobalApp.getQRCode() != "")
			{							
				parameters.add(new DataParameter("scan_type_code", LPScanType.QR_CODE.toString()));				
			}
			else
			{			
				parameters.add(new DataParameter("scan_type_code", GlobalApp
					.getLPScanType()));
			}			
			
			/*parameters.add(new DataParameter("scan_type_code", GlobalApp
					.getLPScanType()));
					*/
			
			parameters.add(new DataParameter("qrCodeValue", GlobalApp
					.getQRCode()));
			
			// parameters.add(new DataParameter("tender_scan_type_code",
			// GlobalApp.getTenderScantType()));
			parameters.add(new DataParameter("is_offline", "0"));
			parameters.add(new DataParameter("stationNumber", GlobalApp
					.getStationNumber()));
			parameters.add(new DataParameter("stationId", GlobalApp
					.getStationID()));
			parameters.add(new DataParameter("sysVer", GlobalApp
					.getSysVersion()));
			parameters.add(new DataParameter("mdType", GlobalApp.getMDType()));
			parameters.add(new DataParameter("pazWashAppVer", GlobalApp
					.getVersionApp()));
			parameters.add(new DataParameter("Type", "PZM"));
			return parameters;
		}

		protected void onPostExecute(ApproveResult result) {
			try {
				hideProgress();

				if (result.getRequestStatus().RequestStatusCode()
						.equals(RequestStatuz.WSGW_OK.toString())) {
					if (result.getClientClientExcCode().equals(
							ClientExceptionCodeResult.RESULT_OK.toString())) {
						GlobalApp.setApproveResult(result);

						if (result.IsApprove()) {
							// Approve
							// Delete image File
							/*
							 * WriteStep(GlobalApp.getWashId(),
							 * BlackBoxStep.DEAL_APPROVE, " Deal " +
							 * result.DealNumber() + " Approved " + " CarPlate "
							 * + lpResult.CarNumber());
							 */

							if (GlobalApp.getImagePath() != null) {
								File file = new File(GlobalApp.getImagePath());
								boolean deleted = file.delete();
								/*
								 * if (deleted) {
								 * WriteStep(GlobalApp.getWashId(),
								 * BlackBoxStep.IMAGE_DELETED_ON_MD, "Image " +
								 * GlobalApp.getImagePath() + " deleted"); }
								 */
							}

							// Open DealApproveActivity
							Intent dealApprove = new Intent(
									getApplicationContext(),
									DealApproveActivity.class);
							dealApprove
									.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							startActivity(dealApprove);
							finish();
						} else {

							/*
							 * WriteStep(GlobalApp.getWashId(),
							 * BlackBoxStep.WASH_NO_APPROVE_DENY, " Deal " +
							 * " NOT Approved " + " CarPlate " +
							 * lpResult.CarNumber());
							 */

							Intent dealnoApprove = new Intent(
									getApplicationContext(),
									DealNoApproveActivity.class);
							dealnoApprove
									.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							startActivity(dealnoApprove);
							finish();
						}

					}

					else {
						if (result.getClientClientExcCode().equals(
								ClientExceptionCodeResult.RESULT_ERROR
										.toString())) {
							hideProgress();
							showAlertDialog(R.string.Exception,
									R.string.ServerException, null);

						}
						if (result.getClientClientExcCode().equals(
								ClientExceptionCodeResult.RESULT_TIMEOUT
										.toString())) {

							hideProgress();

							if (GlobalApp.getTimeOutTry() == TimeOutTry.TIMEOUT_TRY_OFFLINE) {
								showAlertDialog(R.string.Exception,
										R.string.TimeOutException,
										TimeOutListenerApprove);
							} else {
								showAlertDialog(R.string.Exception,
										R.string.TimeOutException, null);
							}
						}
					}
				}

				else {
					hideProgress();
					showAlertDialog(R.string.Exception, result
							.getRequestStatus().RequestStatusCode(), null);
				}

			}

			catch (Exception e) {
				GlobalApp.WriteLogException(e);
				hideProgress();
			}

		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		System.gc();
	}

}
