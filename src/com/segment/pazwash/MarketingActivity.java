package com.segment.pazwash;

import com.segment.pazwash.BL.GlobalApp;
import com.segment.pazwash.DM.ApproveResult;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MarketingActivity extends BaseActivity {
	ApproveResult approveResult;
	Button approveBtn;
	private OnClickListener approveBtnListener = new OnClickListener() {
		public void onClick(View v) {
			try {
				showProgress();
				openMain();
			} catch (Exception e) {
				GlobalApp.WriteLogException(e);

			}

		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_marketing);
		approveResult = GlobalApp.getApproveResult();
		Typeface hebBold = Typeface.createFromAsset(getAssets(), "fonts/droidsanshebrew-bold.ttf");		
		approveBtn = (Button) findViewById(R.id.approveMrkBtn);
		approveBtn.setOnClickListener(approveBtnListener);
		approveBtn.setTypeface(hebBold);
		TextView washesLeftTxt;
		TextView mrktTextTxt;
		washesLeftTxt = (TextView) findViewById(R.id.RemainingText);
		washesLeftTxt.setText(getResources().getString(R.string.WashMonthCount) + " " + approveResult.MonthWashCount());
		mrktTextTxt = (TextView) findViewById(R.id.MrktText);
		if (approveResult.MonthWashCount().equals("0"))
			mrktTextTxt.setText(GlobalApp.mrkt_msg2);
		else
			mrktTextTxt.setText(GlobalApp.mrkt_msg);
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		//super.onBackPressed();
	}
}
