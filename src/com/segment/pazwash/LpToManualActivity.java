package com.segment.pazwash;

import com.segment.pazwash.BL.GlobalApp;
import com.segment.pazwash.DM.LPResult;
import com.segment.pazwash.DM.ReportSummaryItem;

import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class LpToManualActivity extends BaseActivity {

	LPResult lpResult;
	Button yesBtn;
	Button noBtn;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		ActivityType = 1;

		lpResult = GlobalApp.getLPResult();

		/*
		 * lpResult = new LPResult();
		 * 
		 * Bundle extras = getIntent().getExtras(); if (extras != null) {
		 * lpResult.setDbId(extras.getString("Id"));
		 * lpResult.setTicket(extras.getString("Ticket"));
		 * lpResult.setConfidence(extras.getString("Confidence"));
		 * 
		 * }
		 */

		requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.lptomanual);

		loadProgressDialog();

		Typeface hebBold = Typeface.createFromAsset(getAssets(),
				"fonts/droidsanshebrew-bold.ttf");

		TextView tomanualTv = (TextView) findViewById(R.id.tomanualTv);
	
		tomanualTv.setTypeface(hebBold);
		yesBtn = (Button) findViewById(R.id.yesBtn);
		yesBtn.setTypeface(hebBold);
		noBtn = (Button) findViewById(R.id.noBtn);
		noBtn.setTypeface(hebBold);

		noBtn.setOnClickListener(btnNoListener);
		yesBtn.setOnClickListener(btnYesListener);		
		SharedPreferences sharedPreferences = getSharedPreferences(
				"PazWashPreferences", MODE_PRIVATE);
		int balance = sharedPreferences.getInt("Station_Typing_Limit_Balance",0);	
		StringBuilder sb = new StringBuilder();		
		sb.append(this.getString(R.string.ToManualLabel));
		sb.append("\n");
		sb.append(this.getString(R.string.TypingLimitBalance) + ": " + balance);
		tomanualTv.setText(sb.toString());
		
		// userName = getUserName();
	}


	private OnClickListener btnNoListener = new OnClickListener() {
		public void onClick(View v) {
			try {
				showProgress();
				yesBtn.setClickable(false);
				yesBtn.setEnabled(false);
				noBtn.setClickable(false);
				noBtn.setEnabled(false);

				openCamera();
				Handler handler = new Handler();
				handler.postDelayed(new Runnable() {
					public void run() {
						finish();
					}
				}, 3000);

			} catch (Exception e) {
				GlobalApp.WriteLogException(e);
			}

		}

	};

	private OnClickListener btnYesListener = new OnClickListener() {
		public void onClick(View v) {
			try {
				showProgress();
				yesBtn.setClickable(false);
				yesBtn.setEnabled(false);
				noBtn.setClickable(false);
				noBtn.setEnabled(false);
				openManual();
				Handler handler = new Handler();
				handler.postDelayed(new Runnable() {
					public void run() {
						finish();
					}
				}, 3000);
			} catch (Exception e) {
				GlobalApp.WriteLogException(e);
			}

		}

	};

	@Override
	protected void onDestroy() {
		super.onDestroy();
		System.gc();
	}

}