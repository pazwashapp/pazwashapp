package com.segment.pazwash;

import java.util.ArrayList;
import java.util.List;

import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Window;
import android.widget.ListView;
import android.widget.TextView;

import com.segment.pazwash.BL.GlobalApp;
import com.segment.pazwash.BL.PurchaseAdapter;
import com.segment.pazwash.DM.DataParameter;
import com.segment.pazwash.DM.Purchase;
import com.segment.pazwash.DM.PurchaseData;
import com.segment.pazwash.Enums.ClientExceptionCodeResult;
import com.segment.pazwash.Enums.RequestStatuz;



import com.segment.pazwash.WS.ReportWS;

public class ReportPurchases extends BaseActivity {

	List<Purchase> purchases = new ArrayList<Purchase>();
	PurchaseAdapter adapter;
	ListView lstView;
	TextView PurchaseNumberAllTv;
	TextView PurchaseValueAllTv;
	TextView MamAllTv;
	TextView PurchaseValueMamAllTv;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ActivityType = 1;
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		// setContentView(R.layout.datereport);
		setContentView(R.layout.reportpurchase);
		loadProgressDialog();
		showProgress();
		
		TextView title = (TextView)findViewById(R.id.ReportPurchases);
		Typeface hebBold = Typeface.createFromAsset(getAssets(), "fonts/droidsanshebrew-bold.ttf");	
		title.setTypeface(hebBold);

		PurchaseNumberAllTv = (TextView) findViewById(R.id.PurchaseNumberAll);
		PurchaseValueAllTv = (TextView) findViewById(R.id.PurchaseValueAll);
		MamAllTv = (TextView) findViewById(R.id.PurchaseMamAll);
		PurchaseValueMamAllTv = (TextView) findViewById(R.id.PurchaseValueMamAll);

		/*
		 * Bundle extras = getIntent().getExtras(); if (extras != null) {
		 * 
		 * selectedDate = extras.getString("selectedDate"); }
		 */

		getPurchases cl = new getPurchases(GlobalApp.getStationID());
		cl.execute(PAZWASH_WS_URL + "/getPurchasesByProduct");

	}

	private class getPurchases extends AsyncTask<String, Void, PurchaseData> {

		String stationId;

		protected PurchaseData doInBackground(String... urls) {
			ReportWS ws = new ReportWS();
			List<DataParameter> parameters = BuildParameters();
			return ws.getPurchaseData(parameters, urls[0]);
		}

		getPurchases(String id) {
			this.stationId = id;

		}

		private List<DataParameter> BuildParameters() {
			List<DataParameter> parameters = new ArrayList<DataParameter>();
			parameters.add(new DataParameter("stationId", this.stationId));
			return parameters;
		}

		protected void onPostExecute(PurchaseData result) {

			try {
				if (result.getRequestStatus().RequestStatusCode()
						.equals(RequestStatuz.WSGW_OK.toString())) {
					if (result.getClientClientExcCode().equals(
							ClientExceptionCodeResult.RESULT_OK.toString())) {
						if (result != null && result.getPurchases().size() > 0) {
							purchases = result.getPurchases();

							Typeface hebBold = Typeface.createFromAsset(getAssets(), "fonts/droidsanshebrew-bold.ttf");
							
							
							adapter = new PurchaseAdapter(ReportPurchases.this,
									purchases,hebBold );
							// adapter = new
							// ArrayAdapter<DeleteItem>(RejectActivity.this,
							// R.drawable.reject_item, BidsList);
							lstView = (ListView) findViewById(R.id.listPurchses);
							lstView.setBackgroundResource(R.drawable.listview);
							lstView.setChoiceMode(0);
							lstView.setAdapter(adapter);
							lstView.setCacheColorHint(0);
							lstView.setItemsCanFocus(false);

							PurchaseNumberAllTv.setText(result
									.getSumPurchaseNumber());
							PurchaseValueAllTv.setText(result
									.getSumPurchseValue());
							MamAllTv.setText(result.getSumMam());
							PurchaseValueMamAllTv.setText(result
									.getSumPurchaseValueMam());

						}

						hideProgress();
					} else {
						if (result.getClientClientExcCode().equals(
								ClientExceptionCodeResult.RESULT_ERROR
										.toString())) {
							hideProgress();
							if (!isFinishing()) {
								showAlertDialog(R.string.Exception,
										R.string.NoData, null);
							}

						}
						if (result.getClientClientExcCode().equals(
								ClientExceptionCodeResult.RESULT_TIMEOUT
										.toString())) {
							hideProgress();
							if (!isFinishing()) {
								showAlertDialog(R.string.Exception,
										R.string.TimeOutException, null);
							}
						}
					}
				} else {
					hideProgress();
					showAlertDialog(R.string.Exception, result
							.getRequestStatus().RequestStatusCode(), null);
				}

			} catch (Exception e) {
				GlobalApp.WriteLogException(e);
				hideProgress();
				showAlertDialog(R.string.Exception,
						R.string.Exception, null);
			}
		}

	}

}
