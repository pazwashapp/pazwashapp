package com.segment.pazwash;


import java.util.ArrayList;


import java.util.List;

import com.segment.pazwash.BL.CurrentDayAdapter;
import com.segment.pazwash.BL.GlobalApp;

import com.segment.pazwash.DM.DataParameter;
import com.segment.pazwash.DM.SelectedDayReport;

import com.segment.pazwash.DM.SelectedDayWashes;
import com.segment.pazwash.Enums.ClientExceptionCodeResult;
import com.segment.pazwash.Enums.RequestStatuz;

import com.segment.pazwash.WS.ReportWS;

import android.graphics.Typeface;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Window;

import android.widget.ListView;
import android.widget.TextView;

public class ReportSelectedDate extends BaseActivity {

	String selectedDate;
	List<SelectedDayWashes> washes = new ArrayList<SelectedDayWashes>();
	CurrentDayAdapter adapter;
	ListView lstView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ActivityType = 1;
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		// setContentView(R.layout.datereport);
		setContentView(R.layout.dayreport);
		loadProgressDialog();
		showProgress();

		TextView title = (TextView) findViewById(R.id.titleSelectedDate);
		Typeface hebBold = Typeface.createFromAsset(getAssets(),
				"fonts/droidsanshebrew-bold.ttf");
		title.setTypeface(hebBold);

		TextView titleDate = (TextView) findViewById(R.id.titleDate);

		Bundle extras = getIntent().getExtras();
		if (extras != null) {

			selectedDate = extras.getString("selectedDate");
			String titleDateTxt = extras.getString("selectedDate");

			String[] separated = titleDateTxt.split("-");

			StringBuilder sb = new StringBuilder();

			sb.append(separated[2].trim()).append("/")
					.append(separated[1].trim()).append("/")
					.append(separated[0].trim());
			titleDateTxt = sb.toString();

			titleDate.setText(titleDateTxt);
		}

		getSelectedDate cl = new getSelectedDate(GlobalApp.getUserName(),
				selectedDate);
		cl.execute(PAZWASH_WS_URL + "/getReportByDate");

	}

	private class getSelectedDate extends
			AsyncTask<String, Void, SelectedDayReport> {

		String UserName;
		String SelectedDate;

		protected SelectedDayReport doInBackground(String... urls) {
			ReportWS ws = new ReportWS();
			List<DataParameter> parameters = BuildParameters();
			return ws.getSelectedDayWashes(parameters, urls[0]);
		}

		getSelectedDate(String userName, String selectedDay) {
			this.UserName = userName;
			this.SelectedDate = selectedDay;
		}

		private List<DataParameter> BuildParameters() {
			List<DataParameter> parameters = new ArrayList<DataParameter>();
			parameters.add(new DataParameter("userName", this.UserName));
			parameters.add(new DataParameter("selectedDay", this.SelectedDate));
			parameters.add(new DataParameter("deviceId", GlobalApp
					.getDeviceId()));
			parameters.add(new DataParameter("mdCode", GlobalApp.getMDCode()));
			parameters.add(new DataParameter("stationId", GlobalApp
					.getStationID()));
			return parameters;
		}

		protected void onPostExecute(SelectedDayReport result) {

			try {

				if (result.getRequestStatus().RequestStatusCode()
						.equals(RequestStatuz.WSGW_OK.toString())) {
					if (result.getClientClientExcCode().equals(
							ClientExceptionCodeResult.RESULT_OK.toString())) {

						if (result != null && result.SelectedDay() != null
								&& result.SelectedDay().size() > 0) {
							washes = result.SelectedDay();

							Typeface hebBold = Typeface.createFromAsset(
									getAssets(),
									"fonts/droidsanshebrew-bold.ttf");

							adapter = new CurrentDayAdapter(
									ReportSelectedDate.this, washes, hebBold);

							lstView = (ListView) findViewById(R.id.gridDay);
							lstView.setBackgroundResource(R.drawable.listview);
							lstView.setChoiceMode(0);
							lstView.setAdapter(adapter);
							lstView.setCacheColorHint(0);
							lstView.setItemsCanFocus(false);

						}

						hideProgress();
					} else {
						if (result.getClientClientExcCode().equals(
								ClientExceptionCodeResult.RESULT_ERROR
										.toString())) {
							hideProgress();
							if (!isFinishing()) {
								showAlertDialog(R.string.Exception,
										R.string.NoData, null);
							}

						}
						if (result.getClientClientExcCode().equals(
								ClientExceptionCodeResult.RESULT_TIMEOUT
										.toString())) {
							hideProgress();
							if (!isFinishing()) {
								showAlertDialog(R.string.Exception,
										R.string.TimeOutException, null);
							}
						}
					}
				} else {
					hideProgress();
					showAlertDialog(R.string.Exception, result
							.getRequestStatus().RequestStatusCode(), null);
				}

			} catch (Exception e) {
				GlobalApp.WriteLogException(e);
				hideProgress();
				showAlertDialog(R.string.Exception, R.string.Exception, null);
			}
		}

	}

}
