package com.segment.pazwash;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.segment.pazwash.BL.BBStepWriter;
import com.segment.pazwash.BL.DBAdapter;
import com.segment.pazwash.BL.GPS;
import com.segment.pazwash.BL.GlobalApp;
import com.segment.pazwash.BL.NetworkChk;
import com.segment.pazwash.DM.ApproveResult;
import com.segment.pazwash.DM.DataParameter;
import com.segment.pazwash.DM.GPSData;
import com.segment.pazwash.DM.LPResult;
import com.segment.pazwash.Enums.BlackBoxStep;
import com.segment.pazwash.Enums.ClientExceptionCodeResult;
import com.segment.pazwash.Enums.LPScanType;
import com.segment.pazwash.Enums.RequestStatuz;
import com.segment.pazwash.Enums.WashResult;
import com.segment.pazwash.WS.WS;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;


public class DealNoApproveActivity extends BaseActivity {
	ApproveResult approveResult;

	LPResult lpResult;
	Button btnCancel;
	boolean isCancelClick = false;
	Button btnOffline;
	boolean isOfflineClick = false;
	Button btnTryAgain;
	boolean isTryAgainClick = false;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		ActivityType = 1;

		approveResult = GlobalApp.getApproveResult();
		lpResult = GlobalApp.getLPResult();

		setContentView(R.layout.deal_not_approve);

		loadProgressDialog();
		hideProgress();

		Typeface hebBold = Typeface.createFromAsset(getAssets(),
				"fonts/droidsanshebrew-bold.ttf");

		TextView dnaTitle = (TextView) findViewById(R.id.dealnoapproveTv);
		dnaTitle.setTypeface(hebBold);

		btnTryAgain = (Button) findViewById(R.id.tryagainBtn);
		btnTryAgain.setTypeface(hebBold);

		btnOffline = (Button) findViewById(R.id.offlineBtn);
		btnOffline.setTypeface(hebBold);
		int typeNotApproved = approveResult.TypeNotApproved();

		btnOffline.setOnClickListener(btnOfflineListener);
		btnTryAgain.setOnClickListener(btnTryAgainListener);
		btnTryAgain.setTypeface(hebBold);

		if (typeNotApproved == WashResult.NOTOK.ToInt()) {
			btnOffline.setVisibility(View.GONE);
			btnTryAgain.setVisibility(View.GONE);
		}

		btnCancel = (Button) findViewById(R.id.cancelBtn);
		btnCancel.setOnClickListener(btnCancelListener);
		btnCancel.setTypeface(hebBold);

		TextView reason = (TextView) findViewById(R.id.reasonTv);
		reason.setTypeface(hebBold);
		reason.setText("(" + approveResult.getBBResult().getBBResultCode() + ")" + 
		               "\n" + approveResult.getBBResult().getBBResultDesc()); 
		               //+  "\n" + this.getString(R.string.TypingLimitBalanceShort) + ": " + Integer.toString(approveResult.getStationTypingLimitBalance()));
	}

	private void SendBBS() {
		if (!GlobalApp.IsOffline()) {
			BBStepWriter bbs = new BBStepWriter(this);
			try {
				bbs.open();
				boolean result = bbs.deleteSendedBB();
				bbs.close();
				bbs.sendBB();

			} catch (Exception e) {
				GlobalApp.WriteLogException(e);
				// logger.DeleteSendedLogs();
			}
		}
	}

	private OnClickListener btnTryAgainListener = new OnClickListener() {
		public void onClick(View v) {
			try {
				showProgress();

				if (!isTryAgainClick) {
					isTryAgainClick = true;
					btnTryAgain.setClickable(false);
					btnTryAgain.setEnabled(false);
					isOfflineClick = true;
					btnOffline.setClickable(false);
					btnOffline.setEnabled(false);
					isCancelClick = true;
					btnCancel.setClickable(false);
					btnCancel.setEnabled(false);
					Handler handler = new Handler();
					handler.postDelayed(new Runnable() {
						public void run() {
							isTryAgainClick = false;
							btnTryAgain.setClickable(true);
							btnTryAgain.setEnabled(true);
							isOfflineClick = false;
							btnOffline.setClickable(true);
							btnOffline.setEnabled(true);
							isCancelClick = false;
							btnCancel.setClickable(true);
							btnCancel.setEnabled(true);

						}
					}, 3000);

					WriteStep(
							GlobalApp.getWashId(),
							BlackBoxStep.TRY_AGAIN_APPROVE_REQUEST,
							"Send request " + " CarPlate "
									+ lpResult.CarNumber() + " WashCode "
									+ GlobalApp.getWashCode() + " AddItemCode "
									+ GlobalApp.getAddItem());
					if (NetworkChk.checkInternet(DealNoApproveActivity.this,
							GlobalApp.getIsVPN())) {
						
						
						SharedPreferences sharedPreferences = getSharedPreferences(
								"PazWashPreferences", MODE_PRIVATE);
						boolean is_check_gps = sharedPreferences.getBoolean("is_check_gps",
								true);
						if (is_check_gps) {
							checkGPS();
							gps = GPS.getGPS(DealNoApproveActivity.this);
						} else {
							gps = new GPSData("0.0", "0.0");
						}
						
						
						new getApprove(lpResult.CarNumber(),
								approveResult.DbId(),
								Integer.toString(GlobalApp.getWashCode()),
								Integer.toString(GlobalApp.getAddItem()))
								.execute(PAZWASH_WS_URL + "/setBBWashApproveV2");
					} else {
						// Toast.makeText(DealNoApproveActivity.this,
						// "No Network", Toast.LENGTH_SHORT).show();

						if (GlobalApp.IsOffline()) {
							insertOffline();
							//openMain();
						} else {
							openOffline();
						}

					}
				}

			}

			catch (Exception e) {
				GlobalApp.WriteLogException(e);

			}
		}
	};

	private OnClickListener btnOfflineListener = new OnClickListener() {
		public void onClick(View v) {
			try {

				if (!isOfflineClick) {
					isTryAgainClick = true;
					btnTryAgain.setClickable(false);
					btnTryAgain.setEnabled(false);
					isOfflineClick = true;
					btnOffline.setClickable(false);
					btnOffline.setEnabled(false);
					isCancelClick = true;
					btnCancel.setClickable(false);
					btnCancel.setEnabled(false);
					Handler handler = new Handler();
					handler.postDelayed(new Runnable() {
						public void run() {
							isTryAgainClick = false;
							btnTryAgain.setClickable(true);
							btnTryAgain.setEnabled(true);
							isOfflineClick = false;
							btnOffline.setClickable(true);
							btnOffline.setEnabled(true);
							isCancelClick = false;
							btnCancel.setClickable(true);
							btnCancel.setEnabled(true);

						}
					}, 3000);

					insertOffline();
					openMain();
				}
			} catch (Exception e) {
				GlobalApp.WriteLogException(e);

			}
		}
	};

	private OnClickListener btnCancelListener = new OnClickListener() {
		public void onClick(View v) {
			try {

				showProgress();

				if (!isCancelClick) {
					isTryAgainClick = true;
					btnTryAgain.setClickable(false);
					btnTryAgain.setEnabled(false);
					isOfflineClick = true;
					btnOffline.setClickable(false);
					btnOffline.setEnabled(false);
					isCancelClick = true;
					btnCancel.setClickable(false);
					btnCancel.setEnabled(false);
					Handler handler = new Handler();
					handler.postDelayed(new Runnable() {
						public void run() {
							isTryAgainClick = false;
							btnTryAgain.setClickable(true);
							btnTryAgain.setEnabled(true);
							isOfflineClick = false;
							btnOffline.setClickable(true);
							btnOffline.setEnabled(true);
							isCancelClick = false;
							btnCancel.setClickable(true);
							btnCancel.setEnabled(true);

						}
					}, 3000);

					SendBBS();

					if (GlobalApp.getImagePath() != null) {
						File file = new File(GlobalApp.getImagePath());
						boolean deleted = file.delete();
						/*
						 * if (deleted) { WriteStep(GlobalApp.getWashId(),
						 * BlackBoxStep.IMAGE_DELETED_ON_MD, "Image " +
						 * GlobalApp.getImagePath() + " deleted"); }
						 */
					}
					openMain();
				}
			} catch (Exception e) {
				GlobalApp.WriteLogException(e);

			}
		}
	};

	private DialogInterface.OnClickListener sendOfflineListener = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			try {
				if (NetworkChk.checkInternet(DealNoApproveActivity.this,
						GlobalApp.getIsVPN())) {
					DBAdapter db = new DBAdapter(DealNoApproveActivity.this);
					db.checkOfflineData();
				}

			} catch (Exception e) {
				GlobalApp.WriteLogException(e);
			}
		}
	};

	@Override
	public void onBackPressed() {
		btnCancel.performClick();
	}

	public boolean insertOffline() {
		try {
			setGPS();
			DBAdapter db = new DBAdapter(DealNoApproveActivity.this);
			db.open();
			int offline_count = db.getCountOffline();
			db.close();

			SharedPreferences sharedPreferences = getSharedPreferences(
					"PazWashPreferences", MODE_PRIVATE);

			int offline_limit = sharedPreferences.getInt("offline_limit", 10);

			if (offline_count < offline_limit) {

				db.open();

				Date now = new Date();
				SimpleDateFormat format = new SimpleDateFormat(
						"yyyy-MM-dd HH:mm:ss");

				long id = db.insertOffline(GlobalApp.getWashId(),
						lpResult.CarNumber(), GlobalApp.getImagePath(),
						"false", format.format(now), gps.getLon(),
						gps.getLat(), GlobalApp.getUserName(),
						Integer.toString(GlobalApp.getWashCode()),
						Integer.toString(GlobalApp.getAddItem()),
						GlobalApp.getDeviceId(), GlobalApp.getMDCode(),
						GlobalApp.getStationNumber(), GlobalApp.getStationID(),
						GlobalApp.getQRCode(), GlobalApp.getLPScanType(),
						GlobalApp.getVersionApp());

				db.close();

				if (id != -1) {

					WriteStep(
							GlobalApp.getWashId(),
							BlackBoxStep.WASH_SAVED_OFFLINE_ON_MD,
							"Saved offline " + " CarPlate "
									+ lpResult.CarNumber() + " WashCode "
									+ GlobalApp.getWashCode() + " AddItemCode "
									+ GlobalApp.getAddItem());

					// showAlertDialog(R.string.OfflineMsgTitle,
					// R.string.OfflineMsgText, openMainListener);
					// Toast.makeText(DealNoApproveActivity.this,
					// R.string.OfflineMsgText, Toast.LENGTH_SHORT).show();

					showAlertDialog(R.string.OfflineMsgTitle,
							R.string.OfflineMsgText, SaveOkListener);

				} else {
					showAlertDialog(R.string.OfflineMsgTitle,
							R.string.SaveOfflineError, null);
					hideProgress();
				}

			} else {
				WriteStep(GlobalApp.getWashId(),
						BlackBoxStep.OFFLINE_LIMIT_EXCEEDED_ON_MD,
						"count offline: " + "" + " limit: " + offline_limit
								+ " CarPlate " + lpResult.CarNumber()
								+ " WashCode " + GlobalApp.getWashCode()
								+ " AddItemCode " + GlobalApp.getAddItem());
				
				
				WriteOfflineLimitDB(offline_limit);	

				showAlertDialog(R.string.OfflineMsgTitle,
						R.string.OfflineMsgLimitError, sendOfflineListener);
			}

			return true;
		} catch (Exception e) {

			GlobalApp.WriteLogException(e);
			// WriteLogException(e);
			return false;
		}
	}

	private DialogInterface.OnClickListener SaveOkListener = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			openMain();
		}
	};

	private class getApprove extends AsyncTask<String, Void, ApproveResult> {

		String carNumber;
		String DbId;
		String service_code;
		String addServiceCode;
		String user;

		protected ApproveResult doInBackground(String... urls) {

			WS ws = new WS();
			List<DataParameter> parameters = BuildParameters();
			return ws.getApproveAddHTTP(parameters, urls[0]);
		}

		getApprove(String carNumber, String DbId, String servicecode,
				String AddServiceCode) {
			this.carNumber = carNumber;
			this.DbId = DbId;
			this.service_code = servicecode;
			this.addServiceCode = AddServiceCode;
			this.user = GlobalApp.getUserName();

		}

		private List<DataParameter> BuildParameters() {

		
			List<DataParameter> parameters = new ArrayList<DataParameter>();
			Date now = new Date();
			SimpleDateFormat format = new SimpleDateFormat(
					"yyyy-MM-dd HH:mm:ss");
			parameters.add(new DataParameter("washId", GlobalApp.getWashId()));
			parameters.add(new DataParameter("userName", this.user));
			parameters.add(new DataParameter("lpReference", this.carNumber));
			parameters.add(new DataParameter("lpImageId", this.DbId));
			parameters.add(new DataParameter("serviceCode", this.service_code));
			parameters.add(new DataParameter("washdate", format.format(now)));
			parameters.add(new DataParameter("additionalItems",
					this.addServiceCode));
			parameters.add(new DataParameter("GPS_Lon", gps.getLon()));
			parameters.add(new DataParameter("GPS_Lat", gps.getLat()));
			parameters.add(new DataParameter("deviceId", GlobalApp
					.getDeviceId()));
			parameters.add(new DataParameter("mdCode", GlobalApp.getMDCode()));

			if(GlobalApp.getQRCode() != "")
			{							
				parameters.add(new DataParameter("scan_type_code", LPScanType.QR_CODE.toString()));				
			}
			else
			{			
				parameters.add(new DataParameter("scan_type_code", GlobalApp
					.getLPScanType()));
			}	
			
			
			//parameters.add(new DataParameter("scan_type_code", GlobalApp
				//	.getLPScanType()));
			parameters.add(new DataParameter("qrCodeValue", GlobalApp
					.getQRCode()));
			// parameters.add(new DataParameter("tender_scan_type_code",
			// GlobalApp.getTenderScantType()));
			parameters.add(new DataParameter("is_offline", "0"));
			parameters.add(new DataParameter("stationNumber", GlobalApp
					.getStationNumber()));
			parameters.add(new DataParameter("stationId", GlobalApp
					.getStationID()));
			parameters.add(new DataParameter("sysVer", GlobalApp
					.getSysVersion()));
			parameters.add(new DataParameter("mdType", GlobalApp.getMDType()));
			parameters.add(new DataParameter("pazWashAppVer", GlobalApp
					.getVersionApp()));
			
			if(GlobalApp.getIsDWASH())
				parameters.add(new DataParameter("Type", "DWASH"));
			else
				parameters.add(new DataParameter("Type", "PZM"));

			return parameters;
		}

		protected void onPostExecute(ApproveResult result) {

			try {
				hideProgress();

				if (result.getRequestStatus().RequestStatusCode()
						.equals(RequestStatuz.WSGW_OK.toString())) {
					if (result.getClientClientExcCode().equals(
							ClientExceptionCodeResult.RESULT_OK.toString())) {
						GlobalApp.setApproveResult(result);

						if (result.IsApprove()) {
							/*
							 * WriteStep(GlobalApp.getWashId(),
							 * BlackBoxStep.DEAL_APPROVE, " Deal " +
							 * result.DealNumber() + " Approved " + " CarPlate "
							 * + lpResult.CarNumber());
							 */

							if (GlobalApp.getImagePath() != null) {
								File file = new File(GlobalApp.getImagePath());
								boolean deleted = file.delete();
								/*
								 * if (deleted) {
								 * WriteStep(GlobalApp.getWashId(),
								 * BlackBoxStep.IMAGE_DELETED_ON_MD, "Image " +
								 * GlobalApp.getImagePath() + " deleted"); }
								 */
							}

							Intent dealApprove = new Intent(
									DealNoApproveActivity.this,
									DealApproveActivity.class);
							dealApprove
									.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							startActivity(dealApprove);
							finish();
						} else {

							if (result.TypeNotApproved() == WashResult.NOTOK
									.ToInt()) {
								/*
								 * WriteStep(GlobalApp.getWashId(),
								 * BlackBoxStep.WASH_NO_APPROVE_DENY, " Deal " +
								 * " NOT Approved " + " CarPlate " +
								 * lpResult.CarNumber());
								 */
							} else if (result.TypeNotApproved() == WashResult.EXCEPTION
									.ToInt()) {

								/*
								 * WriteStep(GlobalApp.getWashId(),
								 * BlackBoxStep.WASH_NO_APPROVE_SERVER_ERROR,
								 * " Deal " + " NOT Approved " + " CarPlate " +
								 * lpResult.CarNumber());
								 */
							}

							Intent dealnoApprove = new Intent(
									DealNoApproveActivity.this,
									DealNoApproveActivity.class);
							dealnoApprove
									.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							startActivity(dealnoApprove);
							finish();
						}

					} else {
						if (result.getClientClientExcCode().equals(
								ClientExceptionCodeResult.RESULT_ERROR
										.toString())) {
							hideProgress();
							showAlertDialog(R.string.Exception,
									R.string.ServerException, null);

						}
						if (result.getClientClientExcCode().equals(
								ClientExceptionCodeResult.RESULT_TIMEOUT
										.toString())) {
							hideProgress();
							showAlertDialog(R.string.Exception,
									R.string.TimeOutException, null);
						}
					}
				}

				else {
					hideProgress();
					showAlertDialog(R.string.Exception, result
							.getRequestStatus().RequestStatusCode(), null);
				}

			}

			catch (Exception e) {
				GlobalApp.WriteLogException(e);
				hideProgress();
			}
		}

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		System.gc();
	}

}