package com.segment.pazwash;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.segment.pazwash.BL.DBAdapter;
import com.segment.pazwash.BL.GPS;
import com.segment.pazwash.BL.GlobalApp;
import com.segment.pazwash.BL.NetworkChk;
import com.segment.pazwash.DM.ApproveResult;
import com.segment.pazwash.DM.DataParameter;
import com.segment.pazwash.DM.GPSData;
import com.segment.pazwash.DM.LPResult;
import com.segment.pazwash.Enums.BBResult;
import com.segment.pazwash.Enums.BlackBoxStep;
import com.segment.pazwash.Enums.ClientExceptionCodeResult;
import com.segment.pazwash.Enums.LPScanType;
import com.segment.pazwash.Enums.RequestStatuz;
import com.segment.pazwash.Enums.TimeOutTry;
import com.segment.pazwash.WS.WS;

public class DwashApproveActivity extends BaseActivity {
	Button Send;
	Button btnDeny;

	LPResult lpResult = new LPResult();
	TextView errorText;
	boolean isSendClick = false;
	


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);

		lpResult = GlobalApp.getLPResult();

		setContentView(R.layout.dwash_approve);

		loadProgressDialog();

		Typeface hebBold = Typeface.createFromAsset(getAssets(),
				"fonts/droidsanshebrew-bold.ttf");

		TextView title = (TextView) findViewById(R.id.titleTv);
		title.setTypeface(hebBold);

		errorText = (TextView) findViewById(R.id.ErrorTv);
		errorText.setTypeface(hebBold);
		// selectedAddItem = (TextView) findViewById(R.id.addItemTv);
		

		Send = (Button) findViewById(R.id.SendBt);
		Send.setOnClickListener(btnSendListener);
		Send.setTypeface(hebBold);
		
		btnDeny =(Button) findViewById(R.id.denyBtn);
		btnDeny.setOnClickListener(btnDenyListener);
		btnDeny.setTypeface(hebBold);
	}

	

	private OnClickListener btnSendListener = new OnClickListener() {
		public void onClick(View v) {

			showProgress();

			if (!isSendClick) {
				isSendClick = true;
				Send.setClickable(false);
				Send.setEnabled(false);
				btnDeny.setClickable(false);
				btnDeny.setEnabled(false);
				Handler handler = new Handler();
				handler.postDelayed(new Runnable() {
					public void run() {
						isSendClick = false;
						Send.setClickable(true);
						Send.setEnabled(true);
						btnDeny.setClickable(true);
						btnDeny.setEnabled(true);

					}
				}, 3000);
				try {
					if (NetworkChk.checkInternet(DwashApproveActivity.this, GlobalApp.getIsVPN()) && !GlobalApp.IsOffline()) {
						SharedPreferences sharedPreferences = getSharedPreferences("PazWashPreferences", MODE_PRIVATE);
						boolean is_check_gps = sharedPreferences.getBoolean("is_check_gps", true);
						if (is_check_gps) {
							checkGPS();
							gps = GPS.getGPS(DwashApproveActivity.this);
						} else {
							gps = new GPSData("0.0", "0.0");
						}
						new getApprove(lpResult.CarNumber(), lpResult.DbId(),
								Integer.toString(GlobalApp.getWashCode()),
								Integer.toString(GlobalApp.getAddItem()))
								.execute(PAZWASH_WS_URL + "/setBBWashApproveV2");
					} else {
				
							openOffline();
						
					}
				} catch (Exception e) {
					GlobalApp.WriteLogException(e);
					finish();
				}
			}
		}
	};
	
	//Yaron
		private OnClickListener btnDenyListener = new OnClickListener() {
			public void onClick(View v) {
				
				openMain();

			}

		};
		
	

	private class getApprove extends AsyncTask<String, Void, ApproveResult> {

		String carNumber;
		String DbId;
		String service_code;
		String addServiceCode;
		String user;

		protected ApproveResult doInBackground(String... urls) {

			WS ws = new WS();
			List<DataParameter> parameters = BuildParameters();
			return ws.getApproveAddHTTP(parameters, urls[0]);
		}

		getApprove(String carNumber, String DbId, String servicecode,
				String AddServiceCode) {
			this.carNumber = carNumber;
			this.DbId = DbId;
			this.service_code = servicecode;
			this.addServiceCode = AddServiceCode;
			this.user = GlobalApp.getUserName();

		}

		private List<DataParameter> BuildParameters() {
			List<DataParameter> parameters = new ArrayList<DataParameter>();

		

			Date now = new Date();
			SimpleDateFormat format = new SimpleDateFormat(
					"yyyy-MM-dd HH:mm:ss");

			parameters.add(new DataParameter("washId", GlobalApp.getWashId()));
			parameters.add(new DataParameter("userName", this.user));
			parameters.add(new DataParameter("lpReference", this.carNumber));
			parameters.add(new DataParameter("lpImageId", this.DbId));
			parameters.add(new DataParameter("serviceCode", this.service_code));
			parameters.add(new DataParameter("washdate", format.format(now)));
			parameters.add(new DataParameter("additionalItems",
					this.addServiceCode));
			parameters.add(new DataParameter("GPS_Lon", gps.getLon()));
			parameters.add(new DataParameter("GPS_Lat", gps.getLat()));
			parameters.add(new DataParameter("deviceId", GlobalApp
					.getDeviceId()));
			parameters.add(new DataParameter("mdCode", GlobalApp.getMDCode()));

			
			
			if(GlobalApp.getQRCode() != "")
			{							
				parameters.add(new DataParameter("scan_type_code", LPScanType.QR_CODE.toString()));				
			}
			else
			{			
				parameters.add(new DataParameter("scan_type_code", GlobalApp
					.getLPScanType()));
			}			
			
			
			
			parameters.add(new DataParameter("qrCodeValue", GlobalApp
					.getQRCode()));
			
			
			parameters.add(new DataParameter("is_offline", "0"));
			parameters.add(new DataParameter("stationNumber", GlobalApp
					.getStationNumber()));
			parameters.add(new DataParameter("stationId", GlobalApp
					.getStationID()));
			parameters.add(new DataParameter("sysVer", GlobalApp
					.getSysVersion()));
			parameters.add(new DataParameter("mdType", GlobalApp.getMDType()));
			parameters.add(new DataParameter("pazWashAppVer", GlobalApp
					.getVersionApp()));
			parameters.add(new DataParameter("Type", "DWASH"));

			return parameters;
		}

		protected void onPostExecute(ApproveResult result) {
			try {
				hideProgress();

				if (result.getRequestStatus().RequestStatusCode()
						.equals(RequestStatuz.WSGW_OK.toString())) {
					if (result.getClientClientExcCode().equals(
							ClientExceptionCodeResult.RESULT_OK.toString())) {
						GlobalApp.setApproveResult(result);

						if (result.IsApprove()) {							

							if (GlobalApp.getImagePath() != null) {
								File file = new File(GlobalApp.getImagePath());
								boolean deleted = file.delete();
								
							}

							// Open DealApproveActivity
							Intent dealApprove = new Intent(
									getApplicationContext(),
									DealApproveActivity.class);
							dealApprove
									.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							startActivity(dealApprove);
							finish();
						} else {

								if(result.getBBResult().getBBResultCode() == BBResult.DWASH_WAIT_APPROVE.ToInt()){
									errorText.setVisibility(View.VISIBLE);
									btnDeny.setVisibility(View.VISIBLE);
								}else{
	
								Intent dealnoApprove = new Intent(
										getApplicationContext(),
										DealNoApproveActivity.class);
								dealnoApprove
										.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
								startActivity(dealnoApprove);
								finish();
							}
						}

					}

					else {
						if (result.getClientClientExcCode().equals(
								ClientExceptionCodeResult.RESULT_ERROR
										.toString())) {
							hideProgress();
							showAlertDialog(R.string.Exception,
									R.string.ServerException, null);

						}
						if (result.getClientClientExcCode().equals(
								ClientExceptionCodeResult.RESULT_TIMEOUT
										.toString())) {

							hideProgress();
							
							showAlertDialog(R.string.Exception,
									R.string.TimeOutException, null);							
						}
					}
				}

				else {
					hideProgress();
					showAlertDialog(R.string.Exception, result
							.getRequestStatus().RequestStatusCode(), null);
				}

			}

			catch (Exception e) {
				GlobalApp.WriteLogException(e);
				hideProgress();
			}

		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		System.gc();
	}

}
