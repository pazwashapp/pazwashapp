package com.segment.pazwash;

import com.segment.pazwash.BL.GlobalApp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class SettingsActivity extends BaseActivity {
	Button Save;
	RadioGroup rbgrp;
	final String URL = "http://172.30.4.75:8090/PazWashService.asmx";
	final String LINK = "http://172.30.4.75:8091/PazWashApp.apk";
	final String ZXINGLINK = "http://172.30.4.75:8091/Barcode.apk";

	// final String LINK = "http://172.30.13.60/pazwashws";

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);

		ActivityType = 0;

		setContentView(R.layout.settings);

		loadProgressDialog();
		hideProgress();

		Typeface hebBold = Typeface.createFromAsset(getAssets(),
				"fonts/droidsanshebrew-bold.ttf");

		TextView SettingsTv = (TextView) findViewById(R.id.SettingsTv);
		SettingsTv.setTypeface(hebBold);

		/*
		 * Sets the font on all TextViews in the ViewGroup. Searches recursively
		 * for all inner ViewGroups as well. Just add a check for any other
		 * views you want to set as well (EditText, etc.)
		 */

		rbgrp = (RadioGroup) findViewById(R.id.chooseServers);

		Save = (Button) findViewById(R.id.SaveSettingsBtn);
		Save.setTypeface(hebBold);

		Save.setOnClickListener(btnSaveListener);

		SharedPreferences sharedPreferences = getSharedPreferences(
				"PazWashPreferences", MODE_PRIVATE);

		String PAZWASH_WS_URL = sharedPreferences.getString("BaseUrl",
				"http://172.30.4.75:8090/PazWashService.asmx");

		String QA = "http://172.30.4.175:8090/PazWashService.asmx";
		String Segment = "http://wt1.segment.co.il/PazWashWS/PazWashService.asmx";

		if (PAZWASH_WS_URL.equals(URL)) {
			rbgrp.check(R.id.ServerProductionPaz);
		}
		if (PAZWASH_WS_URL.equals(QA)) {
			rbgrp.check(R.id.ServerPaz);
		}
		if (PAZWASH_WS_URL.equals(Segment)) {
			rbgrp.check(R.id.ServerSegment);
		}

	}
	public String getPixels()
	{
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		int hp = metrics.heightPixels;
		int wp = metrics.widthPixels;
		return 	"h:" + Integer.toString(hp) + "/w:" + Integer.toString(wp) + " px" + "\r\n" +
		        "h:" + Integer.toString(pxToDp(hp)) + "/w:" + Integer.toString(pxToDp(wp)) + " dp";
	}
	public static int pxToDp(int px)
	{
	    return (int) (px / Resources.getSystem().getDisplayMetrics().density);
	}
	public void btnSettingsOnClick(View v){
		Toast t = Toast.makeText(getApplicationContext(), getPixels(), Toast.LENGTH_LONG);
		t.show();
	}
	private OnClickListener btnSaveListener = new OnClickListener() {
		public void onClick(View v) {
			// showProgress();
			try {
				int rbid = rbgrp.getCheckedRadioButtonId();
				View rb = rbgrp.findViewById(rbid);
				int idx = rbgrp.indexOfChild(rb);

				SharedPreferences sharedPreferences = getSharedPreferences(
						"PazWashPreferences", MODE_PRIVATE);

				SharedPreferences.Editor prefEditor = sharedPreferences.edit();

				if (idx == 0) {
					// Paz
					prefEditor.putString("BaseUrl", URL);
					prefEditor.putString("BaseLink", LINK);
					prefEditor.putString("BaseZxingLink", ZXINGLINK);
				} else if (idx == 1) {

					prefEditor.putString("BaseUrl",
							"http://172.30.4.175:8090/PazWashService.asmx");
					prefEditor.putString("BaseLink",
							"http://172.30.4.175:8091/PazWashApp.apk");
					prefEditor.putString("BaseZxingLink",
							"http://172.30.4.175:8091/Barcode.apk");

				} else if (idx == 2) {

					// Segment
					prefEditor
							.putString("BaseUrl",
									"http://wt1.segment.co.il/PazWashWS/PazWashService.asmx");
					prefEditor
							.putString("BaseLink",
									"http://wt1.segment.co.il/pazwashws/apk/PazWashApp.apk");
					prefEditor
							.putString("BaseZxingLink",
									"http://wt1.segment.co.il/pazwashws/apk/Barcode.apk");

				}		

				prefEditor.putLong("LoginTime", 999999999);

				prefEditor.commit();

				Intent initActivity = new Intent(SettingsActivity.this,
						InitActivity.class);
				initActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(initActivity);
				finish();
			} catch (Exception e) {
				GlobalApp.WriteLogException(e);
			}

		}
	};

	@Override
	protected void onDestroy() {
		super.onDestroy();
		System.gc();
	}

}
