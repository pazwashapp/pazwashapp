package com.segment.pazwash;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONObject;


import com.segment.pazwash.BL.BBStepWriter;
import com.segment.pazwash.BL.DBAdapter;
import com.segment.pazwash.BL.GlobalApp;
import com.segment.pazwash.BL.JSONSharedPreferences;
import com.segment.pazwash.BL.Logger;

import com.segment.pazwash.BL.NetworkChk;

import com.segment.pazwash.DM.AddItems;
import com.segment.pazwash.DM.ConnectionResult;
import com.segment.pazwash.DM.DataParameter;
import com.segment.pazwash.DM.VPNState;

import com.segment.pazwash.DM.ServerData;

import com.segment.pazwash.Enums.ClientExceptionCodeResult;
import com.segment.pazwash.Enums.RequestStatuz;
import com.segment.pazwash.Enums.ServerPreferences;
import com.segment.pazwash.Enums.VPNStatus;

import com.segment.pazwash.WS.WS;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;

import android.media.AudioManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class InitActivity extends BaseActivity {
	protected String version;
	protected String versionResult;
	ProgressDialog mProgressDialog;
	ArrayList<AddItems> addItems;
	// boolean isUpdateZxing = false;
	Button refreshBtn;
	TextView versionT;

	
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.init);
		ActivityType = 0;

		loadProgressDialog();
		showProgress();

		AudioManager am = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
		am.loadSoundEffects();
		Settings.System.putInt(getContentResolver(), Settings.System.SOUND_EFFECTS_ENABLED,1);		
		
		versionT = (TextView) findViewById(R.id.versionTv);
		setVersion();

		refreshBtn = (Button) findViewById(R.id.btn_refresh);
		refreshBtn.setOnClickListener(btnRefreshListener);

		SharedPreferences sharedPreferences = getSharedPreferences("PazWashPreferences", MODE_PRIVATE);
		PAZWASH_WS_URL = sharedPreferences.getString("BaseUrl", "http://172.30.4.75:8090/PazWashService.asmx");
		PAZWASH_WS_LINK = sharedPreferences.getString("BaseLink", "http://172.30.4.75:8091/PazWashApp.apk");
		PAZWASH_WS_ZXINGLINK = sharedPreferences.getString("ZXINGLINK", "http://172.30.4.75:8091/Barcode.apk");
		GlobalApp.setBaseContext(InitActivity.this); //Peter 
		getSetDeviceId();//Peter
		new checkConnectionWithServer().execute(PAZWASH_WS_URL + "/CheckConnection");
	}


	private void setUserName(){
		SharedPreferences sharedPreferences = getSharedPreferences("PazWashPreferences", MODE_PRIVATE);
		String userName = sharedPreferences.getString("UserName", "0");
		if (!userName.equals("0"))
			GlobalApp.setDeviceId(userName);
	}
	
	private void getSetDeviceId(){
		SharedPreferences sharedPreferences = getSharedPreferences("PazWashPreferences", MODE_PRIVATE);
		String deviceId = sharedPreferences.getString("deviceId", "0");
		if (deviceId == "0") {
			TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
			GlobalApp.setDeviceId(telephonyManager.getDeviceId());
			SharedPreferences.Editor prefEditor = sharedPreferences.edit();
			prefEditor.putString("deviceId", telephonyManager.getDeviceId());
			prefEditor.commit();
		} else {
			GlobalApp.setDeviceId(deviceId);
		}
		
	}

	
	private void setVersion() {
		try {
			version = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
			GlobalApp.setVersionApp(version);
			versionT.setText(version);
		} catch (Exception ex) {
			GlobalApp.WriteLogException(ex);
			// WriteLogException(ex);
		}
	}

	private OnClickListener btnRefreshListener = new OnClickListener() {
		public void onClick(View v) {
			showProgress();
			refreshBtn.setClickable(false);
			refreshBtn.setEnabled(false);
			new checkConnectionWithServer().execute(PAZWASH_WS_URL + "/CheckConnection");
			Handler handler = new Handler();
			handler.postDelayed(new Runnable() {
				public void run() {
					refreshBtn.setClickable(true);
					refreshBtn.setEnabled(true);
				}
			}, 3000);
		}
	};

	private void Initializing() {
		try {
			Intent intentScan = new Intent("com.google.zxing.client.android.SCAN");
			intentScan.addCategory(Intent.CATEGORY_DEFAULT);
			if (!isCallable(intentScan)) {
				hideProgress();
				DownloadFile downloadFile = new DownloadFile("Barcode.apk");
				downloadFile.execute(PAZWASH_WS_ZXINGLINK);
			} else {
				CheckSQL();
				CleanImages();
				//Peter GlobalApp.setBaseContext(InitActivity.this);
				/*Peter
				SharedPreferences sharedPreferences = getSharedPreferences("PazWashPreferences", MODE_PRIVATE);
				String deviceId = sharedPreferences.getString("deviceId", "0");
				if (deviceId == "0") {
					TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
					GlobalApp.setDeviceId(telephonyManager.getDeviceId());
					SharedPreferences.Editor prefEditor = sharedPreferences.edit();
					prefEditor.putString("deviceId", telephonyManager.getDeviceId());
					prefEditor.commit();
				} else {
					GlobalApp.setDeviceId(deviceId);
				}*/
				getSetDeviceId(); //Peter
				String sysVersion = Build.VERSION.RELEASE;
				GlobalApp.setSysVersion(sysVersion);

				String md_type = android.os.Build.MODEL;
				GlobalApp.setMDType(md_type);

				if (NetworkChk.checkInternet(InitActivity.this, GlobalApp.getIsVPN())) {
					new getServerData(GlobalApp.getDeviceId()).execute(PAZWASH_WS_URL + "/getServerData");
				} else {
					showAlertDialog(R.string.Connection, R.string.NoConnection, noConnection);
				}
			}
		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
			hideProgress();
			showAlertDialog(R.string.Exception, R.string.Exception, null);
		}

	}

	@Override
	public void onBackPressed() {

		android.os.Process.killProcess(android.os.Process.myPid());

	}

	private void CleanImages() {

		try {

			int countOffline = 0;

			DBAdapter db = new DBAdapter(this);
			try {
				db.open();
				countOffline = db.getCountOffline();
				db.close();

				// db.DeleteSendedOfflineData();
			} catch (Exception ex) {
				GlobalApp.WriteLogException(ex);
				// db.DeleteSendedOfflineData();
			}

			if (countOffline == 0) {
				String sdcard = Environment.getExternalStorageDirectory()
						+ "/images/";

				File fileList = new File(sdcard);
				if (fileList.isDirectory()) {
					if (fileList != null) { // check if dir is not null
						File[] filenames = fileList.listFiles();

						for (File tmpf : filenames) {
							tmpf.delete();
						}
					}
				}
			}
		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
		}
	}

	private boolean ifUpdate(String v1, String v2) {

		String s1 = normalisedVersion(v1);
		String s2 = normalisedVersion(v2);
		int cmp = s1.compareTo(s2);

		if (cmp < 0) {
			return true;
		}

		else {
			return false;
		}
		// String cmpStr = cmp < 0 ? "<" : cmp > 0 ? ">" : "==";
		// System.out.printf("'%s' %s '%s'%n", v1, cmpStr, v2);
	}

	public String normalisedVersion(String version) {
		return normalisedVersion(version, ".", 4);
	}

	public String normalisedVersion(String version, String sep, int maxWidth) {
		String[] split = Pattern.compile(sep, Pattern.LITERAL).split(version);
		StringBuilder sb = new StringBuilder();
		for (String s : split) {
			sb.append(String.format("%" + maxWidth + 's', s));
		}
		return sb.toString();
	}

	private void chekForNewVersion() {
		try {
			version = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			GlobalApp.WriteLogException(e);
			// WriteLogException(e1);
			// e1.printStackTrace();
		}

		if (version != null && version != "") {
			try {

				DBAdapter db = new DBAdapter(this);
				db.open();
				if (!isOfflineData(db)) {
					SharedPreferences sharedPreferences = getSharedPreferences("PazWashPreferences", MODE_PRIVATE);
					SharedPreferences.Editor prefEditor = sharedPreferences.edit();
					String serverVersion = sharedPreferences.getString("server_app_version", "0.0");
					try {
						if (ifUpdate(version, serverVersion)) {

							InitActivity.this.deleteDatabase("paz_offline.db");

							prefEditor.clear();
							prefEditor.commit();

							// New Version - Go Download
							/*
							 * Toast.makeText(InitActivity.this,
							 * "Starting update ...", Toast.LENGTH_SHORT)
							 * .show();
							 */
							hideProgress();
							DownloadFile downloadFile = new DownloadFile(
									"PazWashApp.apk");
							downloadFile.execute(PAZWASH_WS_LINK);

						} else {

							chkPreferences();
						}

					} catch (NumberFormatException nfe) {
						GlobalApp.WriteLogException(nfe);					
					}

				} else {
					Toast.makeText(InitActivity.this, "present unsended data",
							Toast.LENGTH_LONG);
					chkPreferences();
				}
				db.close();

			}

			catch (Exception e) {
				// TODO Auto-generated catch block
				GlobalApp.WriteLogException(e);
				// WriteLogException(e1);
				// e1.printStackTrace();
			}

		}
	}

	private void chkPreferences() {
		try {
			SharedPreferences sharedPreferences = getSharedPreferences( "PazWashPreferences", MODE_PRIVATE);
			boolean RememberMe = sharedPreferences.getBoolean("RememberMe", false);
			if (!RememberMe) {
				openLogin();
				return;
			}
			if (getUserName().isEmpty()) {
				openLogin();
				return;
			}
			openLogin();
			return;
		}
		catch (Exception e) {
			GlobalApp.WriteLogException(e);
		}
	}

	private boolean isOfflineData(DBAdapter db) {

		try {

			Cursor c = db.getNotSendedData();

			if (c.getCount() > 0) {

				return true;
			} else {

				return false;
			}
		} catch (Exception ex) {

			GlobalApp.WriteLogException(ex);
			return false;// ?true?
		}

	}

	private void CheckSQL() {
		DBAdapter db = new DBAdapter(InitActivity.this);

		try {

			String destPath = "/data/data/" + getPackageName() + "/databases";
			File f = new File(destPath);
			if (!f.exists()) {
				f.mkdirs();
				f.createNewFile();
				// ---copy the db from the assets folder into
				// the databases folder---
				db.CopyDB(getBaseContext().getAssets().open("paz_offline.db"),
						new FileOutputStream(destPath + "/paz_offline.db"));
			} else {

				// check directly sqlLite interface (offline_db)

				String destPath2 = destPath + "/paz_offline.db";
				int countOffline = 0;
				try {
					db.open();
					countOffline = db.getCountOffline();
					db.close();
					
					
					Logger lg = new Logger(this);
					lg.open();
					int countExc = lg.getCountOffline();
					lg.close();
					
					BBStepWriter bbSw = new BBStepWriter(this);
					bbSw.open();
					int _countbbsteps = bbSw.getCountBB();
					bbSw.close();
				} catch (Exception ex) {
					// file not exist
					f.mkdirs();
					f.createNewFile();
					db.CopyDB(
							getBaseContext().getAssets().open("paz_offline.db"),
							new FileOutputStream(destPath2));

				}

			}
		} catch (FileNotFoundException e) {
			GlobalApp.WriteLogException(e);
			// WriteLogException(e);
		} catch (IOException e) {
			GlobalApp.WriteLogException(e);
			// WriteLogException(e);
		}

	}

	public class getServerData extends AsyncTask<String, Void, ServerData> {
		String mdIMEI;
		public getServerData(String deviceId) {
			this.mdIMEI = deviceId;
		}
		protected ServerData doInBackground(String... urls) {
			WS ws = new WS();
			return ws.getServerData(BuildParameters(), urls[0]);
		}

		private List<DataParameter> BuildParameters() {
			List<DataParameter> parameters = new ArrayList<DataParameter>();
			parameters.add(new DataParameter("deviceId", this.mdIMEI));
			return parameters;
		}

		protected void onPostExecute(ServerData result) {
			try {
				if (result.getRequestStatus().RequestStatusCode().equals(RequestStatuz.WSGW_OK.toString())) {
					if (result.getClientClientExcCode().equals(ClientExceptionCodeResult.RESULT_OK.toString())) {
						JSONArray generalItems = new JSONArray();
						JSONArray addItems = new JSONArray();
						if (result != null && result.getItems().size() > 0) {
							for (int temp = 0; temp < result.getItems().size(); temp++) {
								JSONObject jO = new JSONObject();
								if (result.getItems().get(temp).getIsMain() == 1) {
									jO.put("Code", result.getItems().get(temp).ItemCode());
									jO.put("Desc", result.getItems().get(temp).ItemDesc());
									jO.put("Sort", result.getItems().get(temp).ItemSort());
									jO.put("Main", result.getItems().get(temp).getIsMain());
									generalItems.put(jO);
								} else {
									jO.put("Code", result.getItems().get(temp).ItemCode());
									jO.put("Desc", result.getItems().get(temp).ItemDesc());
									jO.put("Sort", result.getItems().get(temp).ItemSort());
									jO.put("Main", result.getItems().get(temp).getIsMain());
									addItems.put(jO);
								}
							}
						}

						JSONSharedPreferences.saveJSONArray(getBaseContext(), "generalProducts", "", generalItems);
						JSONSharedPreferences.saveJSONArray(getBaseContext(), "additionalProducts", "", addItems);
						SharedPreferences sharedPreferences = getSharedPreferences("PazWashPreferences", MODE_PRIVATE);
						SharedPreferences.Editor prefEditor = sharedPreferences.edit();
						if (result != null && result.getProperties().size() > 0) {
							for (int temp = 0; temp < result.getProperties().size(); temp++) {
								int propertyCode = result.getProperties().get(temp).getPropertyCode();
								if (propertyCode == ServerPreferences.COMPRESS_IMAGE_QUALITY.ToInt()) {
									prefEditor.putInt("image_quality", Integer.parseInt(result.getProperties().get(temp).getPropertyValue()));
									prefEditor.commit();
								}
								if (propertyCode == ServerPreferences.WASH_APPROVE_TIMEOUT_SECONDS.ToInt()) {
									GlobalApp.setWashApproveTimeoutSeconds(Integer.parseInt(result.getProperties().get(temp).getPropertyValue()));
								}
								if (propertyCode == ServerPreferences.TIMEOUT_SECONDS.ToInt()) {
									GlobalApp.setTimeoutSeconds(Integer.parseInt(result.getProperties().get(temp).getPropertyValue()));
								}
								if (propertyCode == ServerPreferences.LOGIN_SESSION_MIN.ToInt()) {
									prefEditor.putInt("login_session_min", Integer.parseInt(result.getProperties().get(temp).getPropertyValue()));
									prefEditor.commit();
								}
								if (propertyCode == ServerPreferences.MAX_NO_LPR.ToInt()) {
									prefEditor.putInt("max_no_lpr", Integer.parseInt(result.getProperties().get(temp).getPropertyValue()));
									prefEditor.commit();
								}
								if (propertyCode == ServerPreferences.WASH_DEFAULT.ToInt()) {
									prefEditor.putInt("selectedWashType", Integer.parseInt(result.getProperties().get(temp).getPropertyValue()));
									prefEditor.commit();
								}
								if (propertyCode == ServerPreferences.PASSWORD_CANCEL_WASH_OFFLINE.ToInt()) {
									prefEditor.putString("password_cancel_offline", result.getProperties().get(temp).getPropertyValue());
									prefEditor.commit();
								}
								if (propertyCode == ServerPreferences.APP_VERSION.ToInt()) {
									prefEditor.putString("server_app_version",result.getProperties().get(temp).getPropertyValue());
									prefEditor.commit();
								}
								if (propertyCode == ServerPreferences.NUMBER_OF_LOG_ROWS.ToInt()) {
									prefEditor.putInt("number_of_log_rows", Integer.parseInt(result.getProperties().get(temp).getPropertyValue()));
									prefEditor.commit();
								}
								if (propertyCode == ServerPreferences.WRITE_GENERAL_LOG.ToInt()) {
									prefEditor.putString("write_General_Log", result.getProperties().get(temp).getPropertyValue());
									prefEditor.commit();
								}
								if (propertyCode == ServerPreferences.mrkt_msg.ToInt()) {
									prefEditor.putString("mrkt_msg", result.getProperties().get(temp).getPropertyValue());
									prefEditor.commit();
								}
								if (propertyCode == ServerPreferences.mrkt_msg2.ToInt()) {
									prefEditor.putString("mrkt_msg2", result.getProperties().get(temp).getPropertyValue());
									prefEditor.commit();
								}
								if (propertyCode == ServerPreferences.mrkt_msg_show.ToInt()) {
									prefEditor.putInt("mrkt_msg_show", Integer.parseInt(result.getProperties().get(temp).getPropertyValue()));
									prefEditor.commit();
								}
								if (propertyCode == ServerPreferences.short_process.ToInt()) {
									prefEditor.putInt("short_process", Integer.parseInt(result.getProperties().get(temp).getPropertyValue()));
									prefEditor.commit();
								}
							}
						}

						prefEditor.commit();
						GlobalApp.setIpAddress();
						chekForNewVersion();

					} else {
						if (result.getClientClientExcCode().equals(ClientExceptionCodeResult.RESULT_ERROR.toString())) {
							hideProgress();
							showAlertDialog(R.string.Exception, R.string.ServerException, noConnection);
						}
						if (result.getClientClientExcCode().equals(
								ClientExceptionCodeResult.RESULT_TIMEOUT
										.toString())) {
							hideProgress();
							showAlertDialog(R.string.Exception,
									R.string.TimeOutException, noConnection);
						}
					}
				}

				else {
					hideProgress();
					showAlertDialog(R.string.Exception, result.getRequestStatus().RequestStatusCode(),noConnection);
				}
			}
			catch (Exception e) {
				hideProgress();
				GlobalApp.WriteLogException(e);
				showAlertDialog(R.string.Exception, R.string.Exception, null);
			}
		}
	}

	public class GetLatestVersion extends AsyncTask<String, Void, String> {
		protected String doInBackground(String... urls) {
			WS ws = new WS();
			versionResult = ws.getLastVersion(urls[0]);
			return versionResult;
		}

		protected void onPostExecute(String result) {

			try {
				if (Double.parseDouble(versionResult) > Double
						.parseDouble(version)) {

					InitActivity.this.deleteDatabase("paz_offline.db");
					SharedPreferences sharedPreferences = getSharedPreferences(
							"PazWashPreferences", MODE_PRIVATE);
					SharedPreferences.Editor prefEditor = sharedPreferences
							.edit();
					prefEditor.clear();
					prefEditor.commit();

					DownloadFile downloadFile = new DownloadFile(
							"PazWashApp.apk");
					downloadFile.execute(PAZWASH_WS_LINK
							+ "/apk/PazWashApp.apk");
				} else {

					chkPreferences();
				}

			} catch (NumberFormatException nfe) {
				GlobalApp.WriteLogException(nfe);
			}
		}
	}

	public class DownloadFile extends AsyncTask<String, Integer, String> {

		String appName;

		DownloadFile(String _appName) {

			this.appName = _appName;

		}

		@Override
		protected String doInBackground(String... sUrl) {
			try {
				// hideProgress();
				URL url = new URL(sUrl[0]);
				URLConnection connection = url.openConnection();
				HttpURLConnection httpConn = (HttpURLConnection) connection;
				httpConn.setAllowUserInteraction(false);
				httpConn.setInstanceFollowRedirects(true);
				httpConn.setDoOutput(true);
				httpConn.connect();
				// this will be useful so that you can show a typical 0-100%
				// progress bar
				int fileLength = connection.getContentLength();

				// download the file
				InputStream input = new BufferedInputStream(url.openStream());
				OutputStream output = new FileOutputStream(
				// "/sdcard/PazWashApp.apk");
						"/sdcard/" + this.appName);
				byte data[] = new byte[1024];
				long total = 0;
				int count;
				while ((count = input.read(data)) != -1) {
					total += count;
					// publishing the progress....
					publishProgress((int) (total * 100 / fileLength));
					output.write(data, 0, count);
				}

				output.flush();
				output.close();
				input.close();

				// progress.setVisibility(View.GONE);

				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setDataAndType(
						// Uri.fromFile(new File("/sdcard/PazWashApp.apk")),
						Uri.fromFile(new File("/sdcard/" + this.appName)),
						"application/vnd.android.package-archive");
				startActivity(intent);

			} catch (Exception e) {

				GlobalApp.WriteLogException(e);
				hideProgress();
				showAlertDialog(R.string.Exception, R.string.Exception, null);
				// WriteLogException(e);
				return "-1";
			}
			return null;
		}

		@Override
		protected void onPreExecute() {
			try {
				super.onPreExecute();
				Resources res = getResources();
				mProgressDialog = new ProgressDialog(InitActivity.this);
				mProgressDialog.setMessage(res.getString(R.string.DownloadLatestVersion));
				mProgressDialog.setIndeterminate(false);
				mProgressDialog.setMax(100);
				mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
				mProgressDialog.show();
			} catch (Exception e) {
				hideProgress();
				GlobalApp.WriteLogException(e);
				showAlertDialog(R.string.Exception, R.string.Exception, null);
			}
		}

		@Override
		protected void onProgressUpdate(Integer... progress) {
			super.onProgressUpdate(progress);
			mProgressDialog.setProgress(progress[0]);
		}

		protected void onPostExecute(String s) {
			//android.os.Process.killProcess(android.os.Process.myPid());
			finish();
		}

	}

	private DialogInterface.OnClickListener noConnection = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {

			hideProgress();
			// finish();
			// System.exit(0);
		}
	};

	private class checkConnectionWithServer extends AsyncTask<String, Void, ConnectionResult> {
		protected ConnectionResult doInBackground(String... urls) {
			WS ws = new WS();
			return ws.CheckConnection(urls[0]);
		}

		protected void onPostExecute(ConnectionResult result) {
			try {
				if (result.getRequestStatus().RequestStatusCode().equals(RequestStatuz.WSGW_OK.toString())) {
					if (result.getClientClientExcCode().equals(ClientExceptionCodeResult.RESULT_OK.toString())) {
						if (result.IsConnected()) {
							if (GlobalApp.getIsVPN() == true) {
								VPNStatus vps = VPNStatus.CONNECTED;
								VPNState v = new VPNState();
								v.setVPNStatus(vps);
								GlobalApp.setVPNState(v);
							}
							Toast.makeText(InitActivity.this, "Initializing...", Toast.LENGTH_LONG).show();
							Initializing();
						} else {
							if (!isFinishing()) {
								showAlertDialog(R.string.Connection, R.string.NoConnection, noConnection);
							}
						}
					} else {
						if (result.getClientClientExcCode().equals(ClientExceptionCodeResult.RESULT_ERROR.toString())) {
							hideProgress();
							if (!isFinishing()) {
								//GlobalApp.WriteSimpleLog("InitActivity", "checkConnectionWithServer", "ConnectionError");
								showAlertDialog(R.string.Exception, R.string.NoConnection, noConnection);
							}
						}
						if (result.getClientClientExcCode().equals(ClientExceptionCodeResult.RESULT_TIMEOUT.toString())) {
							hideProgress();
							if (!isFinishing()) {
								//GlobalApp.WriteSimpleLog("InitActivity", "checkConnectionWithServer", "ConnectionTimeout");
								showAlertDialog(R.string.Exception, R.string.NoConnection, noConnection);
							}
						}
					}
				} else {
					hideProgress();
					showAlertDialog(R.string.Exception, result.getRequestStatus().RequestStatusCode(), null);
				}
			} catch (Exception e) {
				//GlobalApp.WriteLogException(e);
				hideProgress();
				if (!isFinishing()) {
					showAlertDialog(R.string.NoConnection,R.string.NoConnection, noConnection);
				}

			}

		}

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		System.gc();
	}

}