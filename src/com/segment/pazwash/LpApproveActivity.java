package com.segment.pazwash;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.json.JSONArray;
import org.json.JSONObject;

import com.segment.pazwash.BL.CameraHelper;
import com.segment.pazwash.BL.GlobalApp;
import com.segment.pazwash.BL.JSONSharedPreferences;
import com.segment.pazwash.BL.NetworkChk;
import com.segment.pazwash.DM.AddItems;
import com.segment.pazwash.DM.ApproveLPRv2Result;
import com.segment.pazwash.DM.ApproveResult;
import com.segment.pazwash.DM.DataParameter;
import com.segment.pazwash.DM.LPResult;
import com.segment.pazwash.DM.RequestStatus;
import com.segment.pazwash.Enums.BBResult;
import com.segment.pazwash.Enums.BlackBoxStep;
import com.segment.pazwash.Enums.ClientExceptionCodeResult;
import com.segment.pazwash.Enums.LPScanType;
import com.segment.pazwash.Enums.MdFlowType;
import com.segment.pazwash.Enums.RequestStatuz;
import com.segment.pazwash.WS.WS;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class LpApproveActivity extends BaseActivity {
	/** Called when the activity is first created. */

	ImageView _image;
	TextView plate;
	Button btnOk;
	Button btnDeny;
	Button btnShortProccess;
	TextView tvApprove;

	// int denyClickCount = 0;
	boolean isBtnOkClick = false;
	boolean isBtnDenyClick = false;
	boolean isTimeout = false;
	boolean isError = false;
	boolean isWsLogicalError = false;

	// byte[] data;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		byte[] data = null;
		int mOrientation = 0;
		ActivityType = 1;
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			mOrientation = extras.getInt("MOrientation");
			data = extras.getByteArray("Data");
		}
		GlobalApp.setImagePath(setImagePath());
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.lp_approve);
		loadProgressDialog();
		showProgress();
		Typeface hebBold = Typeface.createFromAsset(getAssets(),
				"fonts/droidsanshebrew-bold.ttf");
		TextView approveTv = (TextView) findViewById(R.id.approveTv);
		approveTv.setTypeface(hebBold);

		TextView plateLTv = (TextView) findViewById(R.id.plateLTv);
		plateLTv.setTypeface(hebBold);
		SharedPreferences sharedPreferences = getSharedPreferences(
				"PazWashPreferences", MODE_PRIVATE);
		int image_quality = sharedPreferences.getInt("image_quality", 100);
		CameraHelper.SaveImage(data, GlobalApp.getImagePath(), mOrientation,
				image_quality);
		GlobalApp.setIsDWASH(false);// Yaron
		GlobalApp.setisDWASHLast15MinDeal(false);

		getCarPlate();

		plate = (TextView) findViewById(R.id.plateTv);
		_image = (ImageView) findViewById(R.id.lpimage);
		btnDeny = (Button) findViewById(R.id.denyBtn);
		btnDeny.setOnClickListener(btnDenyListener);
		btnDeny.setTypeface(hebBold);
		btnOk = (Button) findViewById(R.id.approveBtn);
		btnOk.setOnClickListener(btnOkListener);
		btnOk.setTypeface(hebBold);
		btnShortProccess = (Button) findViewById(R.id.approveShortBtn);
		tvApprove = (TextView) findViewById(R.id.approveTv);
		if (GlobalApp.short_process == 1)
			btnShortProccess.setVisibility(View.VISIBLE);
		btnShortProccess.setOnClickListener(btnShortProccessListener);

	}

	private void getCarPlate() {
		try {
			if (NetworkChk.checkInternet(LpApproveActivity.this,
					GlobalApp.getIsVPN())) {
				Runnable r = new Runnable() {
					@Override
					public void run() {
						String image_to_string = "";
						try {
							image_to_string = CameraHelper
									.getLastImage(GlobalApp.getImagePath());
						} catch (IOException e) {
							// TODO Auto-generated catch block
							GlobalApp.WriteLogException(e);
							isError = true;
						}
						getPazTicket lprWS = new getPazTicket(image_to_string,
								GlobalApp.getUserName());
						try {
							lprWS.execute(PAZWASH_WS_URL + "/getLPRv2"); // Yaron
							lprWS.get(GlobalApp.getTimeoutSeconds() + 5,
									TimeUnit.SECONDS);
						} catch (InterruptedException e) {
							GlobalApp.WriteLogException(e);
							lprWS.cancel(true);
							isError = true;
						} catch (ExecutionException e) {
							GlobalApp.WriteLogException(e);
							lprWS.cancel(true);
							isError = true;

						} catch (TimeoutException e) {
							GlobalApp.WriteLogException(e);
							lprWS.cancel(true);
							isTimeout = true;

						} catch (CancellationException e) {
							GlobalApp.WriteLogException(e);
							lprWS.cancel(true);
							isError = true;
						} finally {

						}

					}
				};

				Executors.newSingleThreadExecutor().execute(r);

				Handler handler = new Handler();
				handler.postDelayed(new Runnable() {
					public void run() {

						hideProgress();

						if (isTimeout == true) {

							if (!isFinishing()) {
								showAlertDialog(R.string.Exception,
										R.string.TimeOutException, error);
							}

						}

						if (isError == true) {

							if (!isFinishing()) {
								showAlertDialog(R.string.Exception,
										R.string.TimeOutException, error);
							}

						}

					}
				}, 11000);

			} else {

				openOffline();

			}
		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
			hideProgress();
			finish();
		}
	}

	/*
	 * @Override protected void onActivityResult(int requestCode, int
	 * resultCode, Intent data) {
	 * 
	 * switch (resultCode) { case 0: break;
	 * 
	 * case -1: getCarPlate(); break; } }
	 */

	private OnClickListener btnDenyListener = new OnClickListener() {
		public void onClick(View v) {

			if (!isBtnDenyClick) {
				isBtnOkClick = true;
				isBtnDenyClick = true;
				btnOk.setClickable(false);
				btnOk.setEnabled(false);
				btnDeny.setClickable(false);
				btnDeny.setEnabled(false);
				Handler handler = new Handler();
				handler.postDelayed(new Runnable() {
					public void run() {
						isBtnOkClick = false;
						isBtnDenyClick = false;
						btnOk.setClickable(true);
						btnOk.setEnabled(true);
						btnDeny.setClickable(true);
						btnDeny.setEnabled(true);

					}
				}, 2000);

				denyClick();
			}

			/*
			 * if (denyClickCount == 0) { denyClickCount++; denyClick(); }
			 */
		}

		private void denyClick() {
			File file = new File(GlobalApp.getImagePath());
			boolean deleted = file.delete();
			/*
			 * if (deleted) { WriteStep(GlobalApp.getWashId(),
			 * BlackBoxStep.IMAGE_DELETED_ON_MD, "Image " +
			 * GlobalApp.getImagePath() + " deleted"); }
			 */

			SharedPreferences sharedPreferences = getSharedPreferences(
					"PazWashPreferences", MODE_PRIVATE);

			int max_no_lpr = sharedPreferences.getInt("max_no_lpr", 3);

			if (GlobalApp.getCountDhe() < max_no_lpr || isWsLogicalError) {
				// Open Camera again
				if (!isWsLogicalError) // If server returend a logical error -
										// no need to count
					GlobalApp.setCountDhe();

				openCamera();
				Handler handler = new Handler();
				handler.postDelayed(new Runnable() {
					public void run() {
						finish();
					}
				}, 3000);

			} else {
				// open ToManual
				GlobalApp.setCountDhe(0);
				Intent LpToManualIntent = new Intent(LpApproveActivity.this,
						LpToManualActivity.class);
				LpToManualIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(LpToManualIntent);
				finish();
			}
		}
	};

	private OnClickListener btnShortProccessListener = new OnClickListener() {
		public void onClick(View v) {
			try {
				ArrayList<AddItems> generalItems = new ArrayList<AddItems>();
				JSONArray jItems = JSONSharedPreferences.loadJSONArray(
						getBaseContext(), "generalProducts", "");
				for (int i = 0; i < jItems.length(); i++) {
					JSONObject jO = jItems.getJSONObject(i);
					AddItems ad = new AddItems();
					ad.setItemCode(jO.getInt("Code"));
					ad.setItemDesc(jO.getString("Desc"));
					ad.setItemSort(jO.getInt("Sort"));
					ad.setItemIsMain(jO.getInt("Main"));
					generalItems.add(ad);
					GlobalApp.setAdditem(-1);
					GlobalApp.setAddItemDesc("");
				}
				GlobalApp.setWashCode(generalItems.get(0).ItemCode());
				GlobalApp.setGeneralItemDesc(generalItems.get(0).ItemDesc());
				Intent addItems = new Intent(LpApproveActivity.this,
						ChooseSummaryActivity.class);
				startActivity(addItems);
			} catch (Exception e) {
				GlobalApp.WriteLogException(e);
			}

		}
	};

	private OnClickListener btnOkListener = new OnClickListener() {
		public void onClick(View v) {
			try {

				if (!isBtnOkClick) {
					isBtnOkClick = true;
					isBtnDenyClick = true;
					btnOk.setClickable(false);
					btnOk.setEnabled(false);
					btnDeny.setClickable(false);
					btnDeny.setEnabled(false);
					Handler handler = new Handler();
					handler.postDelayed(new Runnable() {
						public void run() {
							isBtnOkClick = false;
							isBtnDenyClick = false;
							btnOk.setClickable(true);
							btnOk.setEnabled(true);
							btnDeny.setClickable(true);
							btnDeny.setEnabled(true);

						}
					}, 2000);

					if (isWsLogicalError) {
						// open ToManual
						GlobalApp.setCountDhe(0);
						Intent LpToManualIntent = new Intent(
								LpApproveActivity.this,
								LpToManualActivity.class);
						LpToManualIntent
								.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(LpToManualIntent);
						finish();
					} else {
						if (GlobalApp.getIsDWASH()) {
							showProgress();
							// Call Ws to Create approve popup at client app
							// (Yellow APP)
							new getApprove().execute(PAZWASH_WS_URL
									+ "/ApproveLPRv2");
						} else {
							WriteStep(GlobalApp.getWashId(),
									BlackBoxStep.LP_APPROVE_BY_MD, "CarPlate "
											+ GlobalApp.getLPResult()
													.CarNumber() + " approved");

							Intent serviceSelect = new Intent(
									LpApproveActivity.this,
									ChooseWashActivity.class);
							serviceSelect
									.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							startActivity(serviceSelect);
							finish();
						}

					}
				}
			} catch (Exception e) {
				GlobalApp.WriteLogException(e);
			}

		}
	};

	public class getPazTicket extends AsyncTask<String, Void, LPResult> {

		String userName;
		String image;

		getPazTicket(String img, String user) {
			this.userName = user;
			this.image = img;
		}

		protected LPResult doInBackground(String... urls) {
			try {
				WS ws = new WS();
				List<DataParameter> parameters = BuildParameters();
				return ws.getLPR(parameters, urls[0]);
			} catch (Exception e) {
				GlobalApp.WriteLogException(e);
				LPResult lpr = new LPResult();
				lpr.setCarNumber("-1");
				lpr.setConfidence("0");
				return lpr;
			}
		}

		private List<DataParameter> BuildParameters() {
			List<DataParameter> parameters = new ArrayList<DataParameter>();
			parameters.add(new DataParameter("img", this.image));
			parameters.add(new DataParameter("userName", this.userName));
			parameters.add(new DataParameter("deviceId", GlobalApp
					.getDeviceId()));
			parameters.add(new DataParameter("mdCode", GlobalApp.getMDCode()));

			parameters.add(new DataParameter("isLpr", "1"));
			parameters.add(new DataParameter("washId", GlobalApp.getWashId())); // Yaron
			parameters.add(new DataParameter("MdLP", "")); // Yaron
			parameters.add(new DataParameter("stationId", GlobalApp
					.getStationNumber())); // Yaron

			return parameters;
		}

		protected void onPostExecute(LPResult result) {
			try {
				if (result.getRequestStatus().RequestStatusCode()
						.equals(RequestStatuz.WSGW_OK.toString())) {
					if (result.getClientClientExcCode().equals(
							ClientExceptionCodeResult.RESULT_OK.toString())) {
						File file = new File(GlobalApp.getImagePath());
						_image.setImageURI(Uri.fromFile(file));
						plate.setText(result.CarNumber());
						if (result.CarNumber().equals("-1")) {
							showProgress();
							setContentView(R.layout.empty);
							boolean deleted = file.delete();
							if (!isFinishing()) {
								showAlertDialog(R.string.ConfidenceTitle,
										result.getBBResult().getBBResultDesc(),
										backToCamera);
							}
						} else {
							switch (result.getBBResult().getMdFlowType()) {
							case 1: // success
								isWsLogicalError = false;
								btnDeny.setText(R.string.Deny);
								btnOk.setText(R.string.Approve);
								tvApprove.setText(R.string.ApproveTitle);
								tvApprove.setTextColor(getResources().getColor(
										R.color.TitleColor));
								tvApprove.setBackgroundColor(Color.TRANSPARENT);

								if (result.getBBResult().getBBResultCode() == BBResult.DWASH_OK
										.ToInt()
										|| result.getBBResult()
												.getBBResultCode() == BBResult.DWASH_IN_PROCESS
												.ToInt()
										|| result.getBBResult()
												.getBBResultCode() == BBResult.DEAL_LAST_15_MIN_DWASH
												.ToInt()) {
									btnShortProccess
											.setVisibility(View.INVISIBLE);

									GlobalApp.setIsDWASH(true);
									if (result.getBBResult().getBBResultCode() == BBResult.DEAL_LAST_15_MIN_DWASH
											.ToInt()) {
										GlobalApp.setisDWASHLast15MinDeal(true);
									}
								}
								break;

							case 2:// Error - try again
								isWsLogicalError = true;
								btnShortProccess.setVisibility(View.INVISIBLE);
								btnDeny.setText(R.string.TakeAnotherPicture);
								btnOk.setText(R.string.CarNumberEmptyTitle);
								tvApprove.setText(result.getBBResult()
										.getBBResultDesc());
								tvApprove.setTextColor(Color.RED);
								tvApprove.setBackgroundColor(Color.WHITE);
								break;

							case 3: // Error - can't continue
							default:
								setContentView(R.layout.empty);
								if (!isFinishing()) {
									showAlertDialog(R.string.ConfidenceTitle,
											result.getBBResult()
													.getBBResultDesc(),
											backToCamera);
								}
								break;
							}

						}
						GlobalApp.setLPResult(result);
						hideProgress();
					} else {
						if (result.getClientClientExcCode().equals(
								ClientExceptionCodeResult.RESULT_ERROR
										.toString())) {
							File file = new File(GlobalApp.getImagePath());
							boolean deleted = file.delete();
							hideProgress();
							if (!isFinishing()) {
								showAlertDialog(R.string.Exception,
										R.string.ServerException, error);
							}
						}
						if (result.getClientClientExcCode().equals(
								ClientExceptionCodeResult.RESULT_TIMEOUT
										.toString())) {
							File file = new File(GlobalApp.getImagePath());
							boolean deleted = file.delete();
							hideProgress();
							if (!isFinishing()) {
								showAlertDialog(R.string.Exception,
										R.string.TimeOutException, error);
							}
						}
					}
					hideProgress();
				} else {
					hideProgress();
					showAlertDialog(R.string.Exception, result
							.getRequestStatus().RequestStatusCode(), null);
				}
			} catch (Exception e) {
				GlobalApp.WriteLogException(e);
				hideProgress();
			}
		}
	}

	@Override
	public void onBackPressed() {
		File file = new File(GlobalApp.getImagePath());
		boolean deleted = file.delete();
		/*
		 * if (deleted) { WriteStep(GlobalApp.getWashId(),
		 * BlackBoxStep.IMAGE_DELETED_ON_MD, "Image " + GlobalApp.getImagePath()
		 * + " deleted"); }
		 */

		finish();
	}

	private DialogInterface.OnClickListener error = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			try {
				hideProgress();
				finish();
			} catch (Exception e) {
				GlobalApp.WriteLogException(e);
				hideProgress();
				finish();
			}

		}
	};

	private DialogInterface.OnClickListener backToCamera = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			try {
				btnDeny.performClick();
			} catch (Exception e) {
				GlobalApp.WriteLogException(e);
				hideProgress();
			}

		}
	};

	private class getApprove extends
			AsyncTask<String, Void, ApproveLPRv2Result> {

		LPResult lpResult;

		protected ApproveLPRv2Result doInBackground(String... urls) {

			WS ws = new WS();
			List<DataParameter> parameters = BuildParameters();
			return ws.getApproveLPRv2(parameters, urls[0]);
		}

		getApprove() {

			this.lpResult = GlobalApp.getLPResult();

		}

		private List<DataParameter> BuildParameters() {
			List<DataParameter> parameters = new ArrayList<DataParameter>();

			Date now = new Date();
			SimpleDateFormat format = new SimpleDateFormat(
					"yyyy-MM-dd HH:mm:ss");

			parameters.add(new DataParameter("washId", GlobalApp.getWashId()));
			parameters
					.add(new DataParameter("lpImageId", this.lpResult.DbId()));
			parameters.add(new DataParameter("userName", GlobalApp
					.getUserName()));
			parameters.add(new DataParameter("lpReference", this.lpResult
					.CarNumber()));
			parameters
					.add(new DataParameter("approvedate", format.format(now)));
			parameters.add(new DataParameter("deviceId", GlobalApp
					.getDeviceId()));
			parameters.add(new DataParameter("mdCode", GlobalApp.getMDCode()));
			parameters.add(new DataParameter("stationNumber", GlobalApp
					.getStationNumber()));
			parameters.add(new DataParameter("stationId", GlobalApp
					.getStationID()));
			parameters.add(new DataParameter("sysVer", GlobalApp
					.getSysVersion()));
			parameters.add(new DataParameter("mdType", GlobalApp.getMDType()));
			parameters.add(new DataParameter("pazWashAppVer", GlobalApp
					.getVersionApp()));
			parameters.add(new DataParameter("Type", "DWASH"));

			return parameters;
		}

		protected void onPostExecute(ApproveLPRv2Result result) {
			try {

				if (result.getRequestStatus().RequestStatusCode()
						.equals(RequestStatuz.WSGW_OK.toString())) {
					if (result.getClientClientExcCode().equals(
							ClientExceptionCodeResult.RESULT_OK.toString())) {

						if (result.IsApprove()) {
							Intent dWashApprove;
							if (GlobalApp.getisDWASHLast15MinDeal()) {
								if (GlobalApp.getImagePath() != null) {
									File file = new File(GlobalApp.getImagePath());
									boolean deleted = file.delete();
									
								}
								//Creating ApproveResult object for DealApproveActivity
								RequestStatus requestStatus = new RequestStatus(RequestStatuz.WSGW_OK.toString(),"");
								ApproveResult approveResult = new ApproveResult();
								approveResult.setClientClientExcCode(ClientExceptionCodeResult.RESULT_OK.toString());
								approveResult.setRequestStatus(requestStatus);
								approveResult.setBBResult(result.getBBResult());				
								approveResult.setIsApprove(true);
								approveResult.setCarType(result.getCarType());
								approveResult.setServiceCode(result.getServiceCode());
								approveResult.setWashType(result.getWashType());
								
								GlobalApp.setApproveResult(approveResult);
								
								// Open DealApproveActivity
								dWashApprove = new Intent(
										LpApproveActivity.this,
										DealApproveActivity.class);
							} else {
								// Open DwashApproveActivity
								dWashApprove = new Intent(
										LpApproveActivity.this,
										DwashApproveActivity.class);

							}
							dWashApprove
									.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

							startActivity(dWashApprove);

							hideProgress();
							finish();
						} else {
							hideProgress();
							if (!isFinishing()) {
								showAlertDialog(R.string.Exception, result
										.getBBResult().getBBResultDesc(), null);
							}
						}

					}

					else {
						if (result.getClientClientExcCode().equals(
								ClientExceptionCodeResult.RESULT_ERROR
										.toString())) {
							hideProgress();
							showAlertDialog(R.string.Exception,
									R.string.ServerException, null);

						}
						if (result.getClientClientExcCode().equals(
								ClientExceptionCodeResult.RESULT_TIMEOUT
										.toString())) {

							hideProgress();

							showAlertDialog(R.string.Exception,
									R.string.TimeOutException, null);
						}
					}
				}

				else {
					hideProgress();
					showAlertDialog(R.string.Exception, result
							.getRequestStatus().RequestStatusCode(), null);
				}

			}

			catch (Exception e) {
				GlobalApp.WriteLogException(e);
				hideProgress();
			}

		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		System.gc();
	}
}