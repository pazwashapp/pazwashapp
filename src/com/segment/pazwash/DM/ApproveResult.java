package com.segment.pazwash.DM;



public class ApproveResult {

	private int stationTypingLimitBalance;
	
	private String dealNumber ;
	private String lastWashDate;
	private String monthWashCount;
	private String dBId;
	private boolean isApprove;
	private String washType;
	
    private int countSelecteditems;	
    private int typeNotApproved;
    private String carType;
    private String serviceCode;
    
    
    private BBResult bbResult;	
    
    
	private String clientClientExcCode;
	
	
	private RequestStatus requestStatus;

	public void setRequestStatus(RequestStatus exc) {
		this.requestStatus = exc;
	}

	public RequestStatus getRequestStatus() {
		return requestStatus;
	}

	public void setClientClientExcCode(String exc) {
		this.clientClientExcCode = exc;
	}

	public String getClientClientExcCode() {
		return clientClientExcCode;
	}
	
   	public void setBBResult(BBResult bbr)
   	{
   		this.bbResult = bbr;
   	}	
   	public BBResult getBBResult()
   	{
   		return bbResult;
   	}
    
   	public void setStationTypingLimitBalance(int balance)
	{
		this.stationTypingLimitBalance = balance;
	}
	
	public int getStationTypingLimitBalance()
	{
		return stationTypingLimitBalance;
	}
    
   public void setWashType(String ct)
    {
    	this.washType = ct;
    }
    
    public String getWashType()
    {
    	return washType;
    }

    
    
    
   public void setCarType(String ct)
    {
    	this.carType = ct;
    }
    
    public String getCarType()
    {
    	return carType;
    }
    
 
    public int TypeNotApproved()
    {
    	return typeNotApproved;
    }
    
    public void setTypeNotApproved(int typeCode){
    	this.typeNotApproved = typeCode;
    }
    
 
    
    public int CountSelecteditems() {
		return countSelecteditems;
	}

	public String MonthWashCount() {
		return monthWashCount;
	}

	public String LastWashDate() {
		return lastWashDate;
	}

	public boolean IsApprove() {
		return isApprove;
	}


	public String DealNumber() {
		return dealNumber;
	}

	public String DbId() {
		return dBId;
	}


	public void setLastWashDate(String ld) {
		this.lastWashDate = ld;
	}

	public void setMonthWashCount(String mwc) {
		this.monthWashCount = mwc;
	}
	
	public void setDealNumber(String lp) {
		this.dealNumber = lp;
	}

	public void setDbId(String id) {
		this.dBId = id;
	}

	public void setCountSelecteditems(int count) {
		this.countSelecteditems = count;
	}
	
	public void setIsApprove(boolean isa) {
		this.isApprove = isa;
	}
	
	public void setServiceCode(String sc)
    {
    	this.serviceCode = sc;
    }
    
    public String getServiceCode()
    {
    	return serviceCode;
    }



}
