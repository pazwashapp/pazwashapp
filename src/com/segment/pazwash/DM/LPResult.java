package com.segment.pazwash.DM;

public class LPResult {

	private String carNumber;
	private String dbId;
	private String ticket;
	private String confidence;
	
	
    private BBResult bbResult;	
    
	private String clientClientExcCode;
	
	private RequestStatus requestStatus;

	public void setRequestStatus(RequestStatus exc) {
		this.requestStatus = exc;
	}

	public RequestStatus getRequestStatus() {
		return requestStatus;
	}

	public void setClientClientExcCode(String exc) {
		this.clientClientExcCode = exc;
	}

	public String getClientClientExcCode() {
		return clientClientExcCode;
	}
	
	public void setBBResult(BBResult bbr)
	{
		this.bbResult = bbr;
	}	
	public BBResult getBBResult()
	{
		return bbResult;
	}

	public String CarNumber() {
		return carNumber;
	}

	public String DbId() {
		return dbId;
	}

	public String Ticket() {
		return ticket;
	}

	public String Confidence() {
		return confidence;
	}

	public void setCarNumber(String lp) {
		carNumber = lp;
	}

	public void setDbId(String id) {
		dbId = id;
	}

	public void setTicket(String t) {
		ticket = t;
	}

	public void setConfidence(String c) {
		confidence = c;
	}
}
