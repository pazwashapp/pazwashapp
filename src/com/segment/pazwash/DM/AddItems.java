package com.segment.pazwash.DM;

public class AddItems {
	
	
	int itemCode;
	String itemDesc;
	int itemSort;
	int isMain;
	
	public void setItemIsMain(int main)
	{
		isMain = main; 
	}
	
	public int getIsMain()
	{
		return isMain; 
	}
	
	public int ItemSort() {
		return itemSort;
	}
	public void setItemSort(int sort) {
		this.itemSort = sort;
	}
	
	public int ItemCode() {
		return itemCode;
	}
    
	public String ItemDesc() {
		return itemDesc;
	}
	
	public void setItemDesc(String desc) {
		this.itemDesc = desc;
	}
	public void setItemCode(int code) {
		this.itemCode = code;
	}
	
	
	 @Override
	    
	    public String toString() {         
	   
		 return ItemDesc();
	
	  };    
}
