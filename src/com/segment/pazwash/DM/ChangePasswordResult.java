package com.segment.pazwash.DM;

public class ChangePasswordResult {

	private String userName;
	private String changePasswordStatus;
	private String clientClientExcCode;
	
	private RequestStatus requestStatus;

	public void setRequestStatus(RequestStatus exc) {
		this.requestStatus = exc;
	}

	public RequestStatus getRequestStatus() {
		return requestStatus;
	}
	
	public void setClientClientExcCode(String exc) {
		this.clientClientExcCode = exc;
	}

	public String getClientClientExcCode() {
		return clientClientExcCode;
	}

	public String UserName() {
		return userName;
	}
	
	public void setUserName(String _userName) {
		userName = _userName;
	}
	

	public String ChangePasswordStatus() {
		return changePasswordStatus;
	}

	public void setChangePasswordStatus(String _changePasswordStatus) {
		changePasswordStatus = _changePasswordStatus;
	}	
}
