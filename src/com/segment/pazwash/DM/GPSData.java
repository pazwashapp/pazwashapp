package com.segment.pazwash.DM;

public class GPSData {
	
	private String latitude;
	private String longtitude;
	private String address;
	private String gpsData;
	private Long gpsMiliSec;
	
	
	public GPSData(String lat,String lon, String a,String data,Long miliSec)
	{
		this.latitude = lat;
		this.longtitude = lon;
		this.address = a;
		this.gpsData = data;
		this.gpsMiliSec = miliSec;
		
	}
	public GPSData(String lat,String lon,Long miliSec)
	{
		this.latitude = lat;
		this.longtitude = lon;	
		this.gpsMiliSec = miliSec;
		
	}
	
	public Long getGPSTime()
	{
		return gpsMiliSec;
	}
	
	public GPSData(String lat,String lon)
	{
		this.latitude = lat;
		this.longtitude = lon;
		this.address = "";
	
	}
	public GPSData()
	{		
	
	}
	public String getGPSData()
	{
		return gpsData;
	}
	
	public String getLat()
	{
		return latitude;
	}
	public String getLon()
	{
		return longtitude;
	}
	public String getAddress()
	{
		return address;
	}


}
