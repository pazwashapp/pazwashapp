package com.segment.pazwash.DM;

public class ServerProp {
	
	private int propertyCode;
	
	private String propertyValue;
	
	
	
	public void setPropertyCode(int code)
	{
		propertyCode = code;
	}
	
	public void setPropertyValue(String value)
	{
		propertyValue = value;
	}
	
	public int getPropertyCode()
	{
		return propertyCode;
	}
	
	public String getPropertyValue()
	{
		return propertyValue;
	}
	
	
	
	

}
