package com.segment.pazwash.DM;

public class RequestWash {
	
	
	private int stationTypingLimitBalance;
	
	private int requestWashResult;
	
	private BBResult bbResult;	
	
	private String clientClientExcCode;
	
	private RequestStatus requestStatus;

	public void setRequestStatus(RequestStatus exc) {
		this.requestStatus = exc;
	}

	public RequestStatus getRequestStatus() {
		return requestStatus;
	}

	public void setClientClientExcCode(String exc) {
		this.clientClientExcCode = exc;
	}

	public String getClientClientExcCode() {
		return clientClientExcCode;
	}
	
	
	
	public void setBBResult(BBResult bbr)
	{
		this.bbResult = bbr;
	}
	
	public BBResult getBBResult()
	{
		return bbResult;
	}	

	
	public void setStationTypingLimitBalance(int balance)
	{
		this.stationTypingLimitBalance = balance;
	}
	
	public int getStationTypingLimitBalance()
	{
		return stationTypingLimitBalance;
	}
	
	public void setRequestWashResult(int result)
	{
		this.requestWashResult = result;
	}
	
	public int getRequestWashResult()
	{
		return requestWashResult;
	}

}
