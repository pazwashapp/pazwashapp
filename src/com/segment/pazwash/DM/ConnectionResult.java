package com.segment.pazwash.DM;

public class ConnectionResult {

	private String clientClientExcCode;

	private RequestStatus requestStatus;

	private String connectionStatusWSResult;
	
	private boolean isConnected;

	public void setRequestStatus(RequestStatus exc) {
		this.requestStatus = exc;
	}

	public RequestStatus getRequestStatus() {
		return requestStatus;
	}

	public void setClientClientExcCode(String exc) {
		this.clientClientExcCode = exc;
	}

	public String getClientClientExcCode() {
		return clientClientExcCode;
	}

	public void setConnectionStatusWSResult(String cr) {
		this.connectionStatusWSResult = cr;
	}

	public String getConnectionStatusWSResult() {
		return connectionStatusWSResult;
	}
	
	public void setIsConnected(boolean ic) {
		this.isConnected = ic;
	}

	public boolean IsConnected() {
		return isConnected;
	}

}
