package com.segment.pazwash.DM;



public class ApproveLPRv2Result {

	private String washId;
	private boolean isApprove;
    
    
    private BBResult bbResult;	
    
    
	private String clientClientExcCode;
	
	
	private RequestStatus requestStatus;
	
	private String carType;
    private String serviceCode;
    private String washType;

	public void setRequestStatus(RequestStatus exc) {
		this.requestStatus = exc;
	}

	public RequestStatus getRequestStatus() {
		return requestStatus;
	}

	public void setClientClientExcCode(String exc) {
		this.clientClientExcCode = exc;
	}

	public String getClientClientExcCode() {
		return clientClientExcCode;
	}
	
   	public void setBBResult(BBResult bbr)
   	{
   		this.bbResult = bbr;
   	}	
   	public BBResult getBBResult()
   	{
   		return bbResult;
   	}
    
   
	public boolean IsApprove() {
		return isApprove;
	}

	
	public void setIsApprove(boolean isa) {
		this.isApprove = isa;
	}
	
	public String getWashId() {
		return washId;
	}

	
	public void setWashId(String wId) {
		this.washId = wId;
	}

	
	public void setCarType(String ct)
    {
    	this.carType = ct;
    }
    
    public String getCarType()
    {
    	return carType;
    }
    
    public void setServiceCode(String sc)
    {
    	this.serviceCode = sc;
    }
    
    public String getServiceCode()
    {
    	return serviceCode;
    }
    
    public void setWashType(String ct)
    {
    	this.washType = ct;
    }
    
    public String getWashType()
    {
    	return washType;
    }


}
