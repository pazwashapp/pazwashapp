package com.segment.pazwash.DM;

import java.util.ArrayList;

public class ServerData {

	private ArrayList<AddItems> items;
	private ArrayList<ServerProp> properties;
	private String clientClientExcCode;
	private RequestStatus requestStatus;

	public void setRequestStatus(RequestStatus exc) {
		this.requestStatus = exc;
	}

	public RequestStatus getRequestStatus() {
		return requestStatus;
	}

	public void setClientClientExcCode(String exc) {
		this.clientClientExcCode = exc;
	}

	public String getClientClientExcCode() {
		return clientClientExcCode;
	}
	public ArrayList<ServerProp> getProperties() {
		return properties;
	}

	public void setProperties(ArrayList<ServerProp> p) {
		this.properties = p;
	}

	public ArrayList<AddItems> getItems() {
		return items;
	}

	public void setItems(ArrayList<AddItems> i) {
		this.items = i;
	}

}
