package com.segment.pazwash.DM;

import java.util.ArrayList;

public class SummaryReportResult {
	
	private ArrayList<ReportSummaryItem> summaryItems;
	
	private String clientClientExcCode;

	private RequestStatus requestStatus;

	public void setRequestStatus(RequestStatus exc) {
		this.requestStatus = exc;
	}

	public RequestStatus getRequestStatus() {
		return requestStatus;
	}

	public void setClientClientExcCode(String exc) {
		this.clientClientExcCode = exc;
	}

	public String getClientClientExcCode() {
		return clientClientExcCode;
	}
	
	public ArrayList<ReportSummaryItem> ReportSummary() {
		return summaryItems;
	}

	public void setSummaryItems(ArrayList<ReportSummaryItem> si) {
		this.summaryItems = si;
	}

}
