package com.segment.pazwash.DM;

import com.segment.pazwash.Enums.VPNStatus;

public class VPNState {

	private VPNStatus vpnStatus;
	private int codeError;

	public VPNState() {

	}

	public VPNState(VPNStatus value) {
		this.vpnStatus = value;
	}

	public VPNState(VPNStatus value, int error) {
		this.vpnStatus = value;
		this.codeError = error;
	}

	public void setVPNStatus(VPNStatus value) {
		this.vpnStatus = value;
	}

	public VPNStatus getVPNStatus() {
		return vpnStatus;
	}

	public void setCodeError(int error) {
		this.codeError = error;
	}

	public int getCodeError() {
		return codeError;
	}

}
