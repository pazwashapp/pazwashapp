package com.segment.pazwash.DM;

import java.util.ArrayList;

public class PurchaseData {

	private ArrayList<Purchase> purchases;

	private String sumPurchaseNumber;
	private String sumPurchseValue;
	private String sumMam;
	private String sumPurchaseValueMam;

	private String clientClientExcCode;

	private RequestStatus requestStatus;

	public void setRequestStatus(RequestStatus exc) {
		this.requestStatus = exc;
	}

	public RequestStatus getRequestStatus() {
		return requestStatus;
	}

	public void setClientClientExcCode(String exc) {
		this.clientClientExcCode = exc;
	}

	public String getClientClientExcCode() {
		return clientClientExcCode;
	}

	public String getSumPurchaseValueMam() {
		return sumPurchaseValueMam;
	}

	public void setSumPurchaseValueMam(String s) {
		this.sumPurchaseValueMam = s;
	}

	public String getSumPurchseValue() {
		return sumPurchseValue;
	}

	public void setSumPurchseValue(String s) {
		this.sumPurchseValue = s;
	}

	public String getSumMam() {
		return sumMam;
	}

	public void setSumMam(String s) {
		this.sumMam = s;
	}

	public String getSumPurchaseNumber() {
		return sumPurchaseNumber;
	}

	public void setSumPurchaseNumber(String s) {
		this.sumPurchaseNumber = s;
	}

	public ArrayList<Purchase> getPurchases() {
		return purchases;
	}

	public void setPurchases(ArrayList<Purchase> p) {
		this.purchases = p;
	}

}
