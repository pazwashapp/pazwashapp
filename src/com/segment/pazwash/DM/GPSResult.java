package com.segment.pazwash.DM;

public class GPSResult {

	private boolean inRadius;

	private BBResult bbResult;
	
	
	public void setBBResult(BBResult bbr) {
		this.bbResult = bbr;
	}

	public BBResult getBBResult() {
		return bbResult;
	}
	
	public boolean InRadius()
	{
		return inRadius;
	}	
	public void setInRadius(boolean r)
	{
		this.inRadius = r;
	}

}
