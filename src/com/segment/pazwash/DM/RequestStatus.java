package com.segment.pazwash.DM;

import com.segment.pazwash.Enums.RequestStatuz;

public class RequestStatus {

	private String requestStatusCode;
	private String requestStatusDesc;

	public RequestStatus() {
		this.requestStatusCode = RequestStatuz.WSGW_OK.toString();
		this.requestStatusDesc = "";
	}

	public RequestStatus(String sc, String sd) {
		this.requestStatusCode = sc;
		this.requestStatusDesc = sd;
	}

	public String RequestStatusCode() {
		return requestStatusCode;
	}
	
	public void setRequestStatusCode(String sc) {
		this.requestStatusCode = sc;
	}

	public void setRequestStatusDesc(String sd) {
		this.requestStatusDesc = sd;
	}
	
	public String RequestStatusDesc() {
		return requestStatusDesc;
	}

	

}
