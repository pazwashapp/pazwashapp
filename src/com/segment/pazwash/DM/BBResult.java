package com.segment.pazwash.DM;

public class BBResult {
	
	
	private int bbResultCode;
	private String bbResultDesc;
	private int MdFlowType;
	
	public void setBBResultCode(int rsc)
	{
		this.bbResultCode = rsc;
	}
	public int getBBResultCode()
	{
		return bbResultCode;
	}
	
	public String getBBResultDesc()
	{
		return bbResultDesc;
	}
	public void setBBResultDesc(String d)
	{
		this.bbResultDesc = d;
	}
	public void setMdFlowType(int rsc)
	{
		this.MdFlowType = rsc;
	}
	public int getMdFlowType()
	{
		return MdFlowType;
	}

}
