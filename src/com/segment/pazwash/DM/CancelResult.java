package com.segment.pazwash.DM;

public class CancelResult {

	private String washId;
	private String carNumber;
	private boolean isCanceled;

	private String clientClientExcCode;
	
	private RequestStatus requestStatus;

	public void setRequestStatus(RequestStatus exc) {
		this.requestStatus = exc;
	}

	public RequestStatus getRequestStatus() {
		return requestStatus;
	}

	public void setClientClientExcCode(String exc) {
		this.clientClientExcCode = exc;
	}

	public String getClientClientExcCode() {
		return clientClientExcCode;
	}

	private BBResult bbResult;

	public void setBBResult(BBResult bbr) {
		this.bbResult = bbr;
	}

	public BBResult getBBResult() {
		return bbResult;
	}

	public String WashId() {
		return washId;
	}

	public String CarNumber() {
		return carNumber;
	}

	public boolean IsCanceled() {
		return isCanceled;
	}

	public void setWashId(String id) {
		this.washId = id;
	}

	public void setCarNumber(String number) {
		this.carNumber = number;
	}

	public void setIsCanceled(boolean bul) {
		this.isCanceled = bul;
	}
}
