package com.segment.pazwash.DM;

public class ReportSummaryItem {
	
	String title;
	String value;
	
	
	
	public String getTitle()
	{
		return title;
	}
	
	public String getValue()
	{
		return value;
	}
	
	public void setTitle(String t)
	{
		this.title = t;
	}
	
	public void setValue(String v)
	{
		this.value = v;
	}

}
