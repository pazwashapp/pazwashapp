package com.segment.pazwash.DM;

import java.util.ArrayList;

public class SelectedDayReport {
	
	private ArrayList<SelectedDayWashes> selectedDayReport;
	
	private String clientClientExcCode;

	private RequestStatus requestStatus;

	public void setRequestStatus(RequestStatus exc) {
		this.requestStatus = exc;
	}

	public RequestStatus getRequestStatus() {
		return requestStatus;
	}

	public void setClientClientExcCode(String exc) {
		this.clientClientExcCode = exc;
	}

	public String getClientClientExcCode() {
		return clientClientExcCode;
	}
	
	public ArrayList<SelectedDayWashes> SelectedDay() {
		return selectedDayReport;
	}

	public void setSelectedDayReport(ArrayList<SelectedDayWashes> si) {
		this.selectedDayReport = si;
	}

}
