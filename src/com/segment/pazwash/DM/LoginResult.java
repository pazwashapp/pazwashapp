package com.segment.pazwash.DM;

public class LoginResult {

	private String userName;
	private String userId;
	private String loginStatus;
	private String mdCode;
	private String stationId;
	private String stationNumber;
	private String offlineLimit;
	private String clientClientExcCode;	
    private String isCheckGPS;
    private String userStatusDesc;
    
	private RequestStatus requestStatus;

	public void setRequestStatus(RequestStatus exc) {
		this.requestStatus = exc;
	}

	public RequestStatus getRequestStatus() {
		return requestStatus;
	}
    
    
    
    
	public void setUserStatusDesc(String ud) {
		this.userStatusDesc = ud;
	}

	public String getUserStatusDesc() {
		return userStatusDesc;
	}
	
    
    
	public void setIsCheckGPS(String gps) {
		this.isCheckGPS = gps;
	}

	public String getIsCheckGPS() {
		return isCheckGPS;
	}
	
	public void setClientClientExcCode(String exc) {
		this.clientClientExcCode = exc;
	}

	public String getClientClientExcCode() {
		return clientClientExcCode;
	}

	public void setOfflineLimit(String olimit) {
		this.offlineLimit = olimit;
	}

	public String getOfflineLimit() {
		return offlineLimit;
	}

	public String MDCode() {
		return mdCode;
	}

	public String StationId() {
		return stationId;
	}

	public String stationNumber() {
		return stationNumber;
	}

	public void setMDCode(String md) {
		this.mdCode = md;
	}

	public void setStationId(String id) {
		this.stationId = id;
	}

	public void setStationNumber(String number) {
		this.stationNumber = number;
	}

	public String UserName() {
		return userName;
	}

	public void setUserName(String _userName) {
		this.userName = _userName;
	}

	public String UserId() {
		return userId;
	}

	public void setUserId(String _userId) {
		this.userId = _userId;
	}

	public String LoginStatus() {
		return loginStatus;
	}

	public void setLoginStatus(String _loginStatus) {
		this.loginStatus = _loginStatus;
	}
}
