package com.segment.pazwash.DM;

public class IsActiveResult {

	private boolean isActive;

	private BBResult bbResult;
	
	
	private String clientClientExcCode;
	
	
	private RequestStatus requestStatus;

	public void setRequestStatus(RequestStatus exc) {
		this.requestStatus = exc;
	}

	public RequestStatus getRequestStatus() {
		return requestStatus;
	}

	public void setClientClientExcCode(String exc) {
		this.clientClientExcCode = exc;
	}

	public String getClientClientExcCode() {
		return clientClientExcCode;
	}

	public void setBBResult(BBResult bbr) {
		this.bbResult = bbr;
	}

	public BBResult getBBResult() {
		return bbResult;
	}

	public boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(boolean a) {
		this.isActive = a;
	}

}
