package com.segment.pazwash.DM;

public class SQLiteWSResult {

	private String result;
	String clientClientExcCode;
	private RequestStatus requestStatus;

	public void setRequestStatus(RequestStatus exc) {
		this.requestStatus = exc;
	}

	public RequestStatus getRequestStatus() {
		return requestStatus;
	}

	public void setClientClientExcCode(String exc) {
		this.clientClientExcCode = exc;
	}

	public String getClientClientExcCode() {
		return clientClientExcCode;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String id) {
		this.result = id;
	}

}
