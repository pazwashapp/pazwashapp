package com.segment.pazwash.DM;

public class DataParameter {
	
	public DataParameter()
	{
		
	}	
	
	public DataParameter(String parameterName, String parameterValue)
	{
		this.parameterName = parameterName;
		this.value = parameterValue;		
	}
	
	
	private String parameterName;
	private String value;
	
	
	public void setParameterName(String name)
	{
		this.parameterName = name;		
	}
	
	public void setParameterValue(String pvalue)
	{
		this.value = pvalue;
	}
	
	public String getParameterName()
	{
		return parameterName;
	}
	
	public String getParameterValue()
	{
		return value;
	}
	
	
	
	

}
