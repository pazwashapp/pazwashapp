package com.segment.pazwash.DM;



public class DeleteItem {
	
	
	String washId;
	String carNumber;
	String serviceCode;
	String washDate;
	String washIdTitle;
	String carTitle;
	String dateTitle;
	
	public void setDealTitle(String title)
	{
		this.washIdTitle = title;
	}
	
	public void setCarTitle(String cartitle)
	{
		this.carTitle = cartitle;
	}
	public void setDateTitle(String datetitle)
	{
		this.dateTitle = datetitle;
	}
	
	public String DealTitle()
	{
		return washIdTitle;
	}
	public String CarTitle()
	{
		return carTitle;
	}
	public String DateTitle()
	{
		return dateTitle;
	}
	
	public String WashId() {
		return washId;
	}
    
	public String CarNumber() {
		return carNumber;
	}
	
	public String ServiceCode() {
		return serviceCode;
	}
    
	public String WashDate() {
		return washDate;
	}
	
	public void setWashId(String id) {
		this.washId = id;
	}
	public void setCarNumber(String number) {
		this.carNumber = number;
	}
	
	
	public void setServiceCode(String code) {
		this.serviceCode = code;
	}
	public void setWashDate(String date) {
		this.washDate = date;
	}
	
	 @Override
	    
	    public String toString() {         
	   
		 
		 StringBuilder s = new StringBuilder(3);
		 
		 s.append(" " + DealTitle() + " ");
		 s.append(WashId());
		
		 
		 s.append("\n");
		 s.append(" " + CarTitle() + " ");
		 s.append(CarNumber());
		
		 s.append("\n");
		 s.append(" " + DateTitle() + " ");
		 s.append(WashDate());
		
		 
		 return s.toString();
		 
	     // return CarNumber() + " " + WashDate() ;   
	  };    
	      

}
