package com.segment.pazwash.DM;

public class ForgotPasswordResult {

	private String userName;
	private String forgotPasswordStatus;
	private String clientClientExcCode;
	
	
	private RequestStatus requestStatus;

	public void setRequestStatus(RequestStatus exc) {
		this.requestStatus = exc;
	}

	public RequestStatus getRequestStatus() {
		return requestStatus;
	}
	
	public void setClientClientExcCode(String exc) {
		this.clientClientExcCode = exc;
	}

	public String getClientClientExcCode() {
		return clientClientExcCode;
	}

	public String UserName() {
		return userName;
	}
	
	public void setUserName(String _userName) {
		userName = _userName;
	}
	

	public String ForgotPasswordStatus() {
		return forgotPasswordStatus;
	}

	public void setForgotPasswordStatus(String _forgotPasswordStatus) {
		forgotPasswordStatus = _forgotPasswordStatus;
	}	
}
