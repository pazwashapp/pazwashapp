package com.segment.pazwash.DM;

public class Purchase {
	
	   private String Product_Code; 
	   private String Product_Desc;
	   private String Purchase_Number;
	   private String Purchase_Value;
	   private String Mam;
	   private String Purchase_Value_Mam;
	   
	   
	   public String getProductCode()
	   {
		   return Product_Code;
	   }
	   
	   public void setProductCode(String code)
	   {
		   this.Product_Code = code;
	   }
	   
	   public String getProductDesc()
	   {
		   return Product_Desc;
	   }
	   
	   public void setProductDesc(String desc)
	   {
		   this.Product_Desc = desc;
	   }
	   
	   public String getPurchaseNumber()
	   {
		   return Purchase_Number;
	   }
	   
	   public void setPurchaseNumber(String number)
	   {
		   this.Purchase_Number = number;
	   }
	   
	   public String getPurchaseValue()
	   {
		   return Purchase_Value;
	   }
	   
	   public void setPurchaseValue(String value)
	   {
		   this.Purchase_Value = value;
	   }
	   
	   public String getMam()
	   {
		   return Mam;
	   }
	   
	   public void setMam(String m)
	   {
		   this.Mam = m;
	   }
	   
	   public String getPurchaseValueMam()
	   {
		   return Purchase_Value_Mam;
	   }
	   
	   public void setPurchaseValueMam(String v)
	   {
		   this.Purchase_Value_Mam = v;
	   }
	   
	   

}
