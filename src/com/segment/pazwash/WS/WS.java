package com.segment.pazwash.WS;

import com.segment.pazwash.BL.GlobalApp;
import com.segment.pazwash.DM.AddItems;
import com.segment.pazwash.DM.ApproveLPRv2Result;
import com.segment.pazwash.DM.ApproveResult;
import com.segment.pazwash.DM.BBResult;
import com.segment.pazwash.DM.CancelResult;
import com.segment.pazwash.DM.ConnectionResult;
import com.segment.pazwash.DM.DataParameter;
import com.segment.pazwash.DM.DeleteItem;
import com.segment.pazwash.DM.GPSResult;
import com.segment.pazwash.DM.IsActiveResult;
import com.segment.pazwash.DM.LPResult;
import com.segment.pazwash.DM.RequestWash;
import com.segment.pazwash.DM.ServerData;
import com.segment.pazwash.DM.ServerProp;
import com.segment.pazwash.Enums.WSRequestFlags;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.util.ByteArrayBuffer;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

//rest of ws...

public class WS extends BaseWS {
	String version;

	public WS() {

	}

	public ApproveResult getApproveAddHTTP(List<DataParameter> parameters, String url) {
		ApproveResult apResult = new ApproveResult();
		try {

			String in = null;
			Document doc = null;

			String data = BuildDataParameters(parameters);
			in = WashApproveRequest(data, url);

			if (requestResult == WSRequestFlags.RESULT_OK) {
				try {
					doc = CreateDocument(in);
				} catch (Exception e) {
					GlobalApp.WriteLogException(e);
					requestResult = WSRequestFlags.RESULT_ERROR;
				}
			}

			apResult.setClientClientExcCode(requestResult.toString());
			if (requestResult == WSRequestFlags.RESULT_OK) {

				NodeList nList2 = doc.getElementsByTagName("RequestStatus");
				for (int temp = 0; temp < nList2.getLength(); temp++) {

					Node nNode = nList2.item(temp);
					if (nNode.getNodeType() == Node.ELEMENT_NODE) {

						Element eElement = (Element) nNode;

						rqSt.setRequestStatusCode((getTagValue(
								"RequestStatusCode", eElement)));

					}
				}
				apResult.setRequestStatus(rqSt);

				NodeList nList = doc.getElementsByTagName("ApproveResult");
				Node nNode = nList.item(0);
				Element eElement = (Element) nNode;

				apResult.setIsApprove(Boolean.parseBoolean(getTagValue(
						"IsApprove", eElement)));
				
				apResult.setStationTypingLimitBalance(Integer
						.parseInt(getTagValue("StationTypingLimitBalance", eElement)));
				

				if (apResult.IsApprove() == true) {
					apResult.setDealNumber(getTagValue("DealNumber", eElement));
					apResult.setDbId(getTagValue("DBId", eElement));
					apResult.setLastWashDate(getTagValue("LastWashDate",
							eElement));
					apResult.setMonthWashCount(getTagValue("MonthWashCount",
							eElement));
					apResult.setCarType(getTagValue("CarType", eElement));
					apResult.setWashType(getTagValue("WashType", eElement));
					if(GlobalApp.getIsDWASH()){
						apResult.setServiceCode(getTagValue("serviceCode", eElement));
					}else{
						apResult.setServiceCode("-1");
					}
					
				} else {

					NodeList nList1 = doc.getElementsByTagName("BBResult");
					Node nNode1 = nList1.item(0);
					Element eElement1 = (Element) nNode1;

					BBResult bbr = new BBResult();
					bbr.setBBResultCode(Integer.parseInt(getTagValue(
							"ResultCode", eElement1)));
					bbr.setBBResultDesc(getTagValue("ResultDesc", eElement1));
					apResult.setBBResult(bbr);

					apResult.setTypeNotApproved(Integer.parseInt(getTagValue(
							"TypeNotApproved", eElement)));

				}
			}

			if (requestResult == WSRequestFlags.RESULT_ERROR) {
				BBResult bbr = new BBResult();
				bbr.setBBResultCode(-100);
				bbr.setBBResultDesc("");
				apResult.setRequestStatus(rqSt);
				apResult.setBBResult(bbr);
				apResult.setDealNumber("-1");
				apResult.setIsApprove(false);
			}
			if (requestResult == WSRequestFlags.RESULT_TIMEOUT) {
				BBResult bbr = new BBResult();
				bbr.setBBResultCode(-99);
				bbr.setBBResultDesc("");
				apResult.setBBResult(bbr);
				apResult.setRequestStatus(rqSt);
				apResult.setDealNumber("-1");
				apResult.setIsApprove(false);
			}

			return apResult;
		}

		catch (Exception e) {
			GlobalApp.WriteLogException(e);
			apResult.setClientClientExcCode(WSRequestFlags.RESULT_ERROR
					.toString());
			BBResult bbr = new BBResult();
			bbr.setBBResultCode(-100);
			bbr.setBBResultDesc("");
			apResult.setRequestStatus(rqSt);
			apResult.setBBResult(bbr);
			apResult.setDealNumber("-1");
			apResult.setIsApprove(false);

			return apResult;
		}

		// ---return the definitions of the word---

	}

	public LPResult getLPR(List<DataParameter> parameters, String url) {
		LPResult lpResult = new LPResult();
		try {
			Document doc = tryRequest(parameters, url);
			lpResult.setClientClientExcCode(requestResult.toString());
			if (requestResult == WSRequestFlags.RESULT_OK) {
				NodeList nList2 = doc.getElementsByTagName("RequestStatus");
				for (int temp = 0; temp < nList2.getLength(); temp++) {
					Node nNode = nList2.item(temp);
					if (nNode.getNodeType() == Node.ELEMENT_NODE) {
						Element eElement = (Element) nNode;
						rqSt.setRequestStatusCode((getTagValue("RequestStatusCode", eElement)));
						rqSt.setRequestStatusDesc((getTagValue("RequestStatusDesc", eElement)));
					}
				}
				lpResult.setRequestStatus(rqSt);
				NodeList nList = doc.getElementsByTagName("LPR");
				Node nNode = nList.item(0);
				Element eElement = (Element) nNode;
				lpResult.setCarNumber(getTagValue("CarNumber", eElement));
				try{
					lpResult.setDbId(getTagValue("DBId", eElement));
				}catch(Exception ex){
				//Do nothing
				}
				lpResult.setConfidence(getTagValue("Confidence", eElement));

				//if (lpResult.CarNumber().equals("-1")) {
					NodeList nList1 = doc.getElementsByTagName("BBResult");
					Node nNode1 = nList1.item(0);
					Element eElement1 = (Element) nNode1;
					BBResult bbr = new BBResult();
					bbr.setBBResultCode(Integer.parseInt(getTagValue("ResultCode", eElement1)));
					bbr.setBBResultDesc(getTagValue("ResultDesc", eElement1));
					bbr.setMdFlowType(Integer.parseInt(getTagValue("MdFlowType", eElement1)));
					lpResult.setBBResult(bbr);
				//} else {
//					lpResult.setDbId(getTagValue("DBId", eElement));
//					lpResult.setConfidence(getTagValue("Confidence", eElement));
					//Peter 2014-06-24
//					BBResult bbr = new BBResult();
//					bbr.setBBResultCode(1018); 
//					bbr.setBBResultDesc("LpManualActivity");
//					lpResult.setBBResult(bbr);
					//Peter 2014-06-24 end (in order to prevent NullPointerExceptionError in LOG)
				//}
			} else {
				if (requestResult == WSRequestFlags.RESULT_ERROR) {
					BBResult bbr = new BBResult();
					bbr.setBBResultCode(-100);
					bbr.setBBResultDesc("");
					lpResult.setRequestStatus(rqSt);
					lpResult.setBBResult(bbr);
					lpResult.setCarNumber("-1");
					lpResult.setConfidence("0");
					lpResult.setDbId("-1");
				}
				if (requestResult == WSRequestFlags.RESULT_TIMEOUT) {
					BBResult bbr = new BBResult();
					bbr.setBBResultCode(-99);
					bbr.setBBResultDesc("");
					lpResult.setRequestStatus(rqSt);
					lpResult.setBBResult(bbr);
					lpResult.setCarNumber("-1");
					lpResult.setConfidence("0");
					lpResult.setDbId("-1");
				}
				if (requestResult != WSRequestFlags.RESULT_TIMEOUT && requestResult != WSRequestFlags.RESULT_ERROR) {
					lpResult.setClientClientExcCode(WSRequestFlags.RESULT_ERROR.toString());
					BBResult bbr = new BBResult();
					bbr.setBBResultCode(-100);
					bbr.setBBResultDesc("");
					lpResult.setRequestStatus(rqSt);
					lpResult.setBBResult(bbr);
					lpResult.setCarNumber("-1");
					lpResult.setConfidence("0");
					lpResult.setDbId("-1");
					return lpResult;
				}

			}
			return lpResult;
		}

		catch (Exception e) {
			GlobalApp.WriteLogException(e);
			lpResult.setClientClientExcCode(WSRequestFlags.RESULT_ERROR.toString());
			BBResult bbr = new BBResult();
			bbr.setBBResultCode(-100);
			bbr.setBBResultDesc("");
			lpResult.setRequestStatus(rqSt);
			lpResult.setBBResult(bbr);
			lpResult.setCarNumber("-1");
			lpResult.setConfidence("0");
			lpResult.setDbId("-1");
			return lpResult;
		}
	}

	public ConnectionResult CheckConnection(String url) {
		ConnectionResult cr = new ConnectionResult();
		try {
			String in = null;
			in = getHTTP(url);
			Document doc = CreateDocument(in);
			cr.setClientClientExcCode(requestResult.toString());
			if (requestResult == WSRequestFlags.RESULT_OK) {
				NodeList nList2 = doc.getElementsByTagName("RequestStatus");
				for (int temp = 0; temp < nList2.getLength(); temp++) {
					Node nNode = nList2.item(temp);
					if (nNode.getNodeType() == Node.ELEMENT_NODE) {
						Element eElement = (Element) nNode;
						rqSt.setRequestStatusCode((getTagValue("RequestStatusCode", eElement)));
					}
				}
				cr.setRequestStatus(rqSt);
				NodeList nList = doc.getElementsByTagName("ConnectionStatus");
				Node nNode = nList.item(0);
				Element eElement = (Element) nNode;
				cr.setConnectionStatusWSResult(getTagValue("ConnectionStatusResult", eElement));
				cr.setIsConnected(true);
				return cr;
			} else {
				cr.setRequestStatus(rqSt);
				cr.setIsConnected(false);
				return cr;
			}

		} catch (Exception ex) {
			//GlobalApp.WriteLogException(ex);
			cr.setClientClientExcCode(WSRequestFlags.RESULT_ERROR.toString());
			cr.setRequestStatus(rqSt);
			cr.setIsConnected(false);
			return cr;
		}
	}

	public GPSResult checkGPS(List<DataParameter> parameters, String url) {
		GPSResult gpsResult = new GPSResult();

		try {

			String in = null;

			String data = BuildDataParameters(parameters);
			in = BaseRequest(data, url);
			Document doc = CreateDocument(in);

			NodeList nList = doc.getElementsByTagName("GPSResult");
			Node nNode = nList.item(0);
			Element eElement = (Element) nNode;

			gpsResult.setInRadius(Boolean.parseBoolean(getTagValue("InRadius",
					eElement)));

			if (!gpsResult.InRadius()) {
				NodeList nList1 = doc.getElementsByTagName("BBResult");
				Node nNode1 = nList1.item(0);
				Element eElement1 = (Element) nNode1;

				BBResult bbr = new BBResult();
				bbr.setBBResultCode(Integer.parseInt(getTagValue("ResultCode",
						eElement1)));
				bbr.setBBResultDesc(getTagValue("ResultDesc", eElement1));

				gpsResult.setBBResult(bbr);
			}

			return gpsResult;

		} catch (Exception ex) {
			GlobalApp.WriteLogException(ex);
			gpsResult.setInRadius(false);
			BBResult bbr = new BBResult();
			bbr.setBBResultCode(-100);
			bbr.setBBResultDesc("");
			gpsResult.setBBResult(bbr);
			return gpsResult;
		}
	}

	public RequestWash getRequestWashResult(List<DataParameter> parameters, String url) {
		RequestWash requestWashResult = new RequestWash();
		try {
			Document doc = tryRequest(parameters, url);
			requestWashResult.setClientClientExcCode(requestResult.toString());
			if (requestResult == WSRequestFlags.RESULT_OK) {
				NodeList nList2 = doc.getElementsByTagName("RequestStatus");
				for (int temp = 0; temp < nList2.getLength(); temp++) {
					Node nNode = nList2.item(temp);
					if (nNode.getNodeType() == Node.ELEMENT_NODE) {
						Element eElement = (Element) nNode;
						rqSt.setRequestStatusCode((getTagValue("RequestStatusCode", eElement)));
					}
				}
				requestWashResult.setRequestStatus(rqSt);
				NodeList nList = doc.getElementsByTagName("WashRequest");
				Node nNode = nList.item(0);
				Element eElement = (Element) nNode;
				requestWashResult.setRequestWashResult(Integer.parseInt(getTagValue("WashRequestResult", eElement)));
				requestWashResult.setStationTypingLimitBalance(Integer.parseInt(getTagValue("StationTypingLimitBalance", eElement)));
				if (requestWashResult.getRequestWashResult() == -1) {
					NodeList nList1 = doc.getElementsByTagName("BBResult");
					Node nNode1 = nList1.item(0);
					Element eElement1 = (Element) nNode1;
					BBResult bbr = new BBResult();
					bbr.setBBResultCode(Integer.parseInt(getTagValue("ResultCode", eElement1)));
					bbr.setBBResultDesc(getTagValue("ResultDesc", eElement1));
					requestWashResult.setBBResult(bbr);
				}
			}
			if (requestResult == WSRequestFlags.RESULT_ERROR) {
				requestWashResult.setRequestWashResult(-1);
				BBResult bbr = new BBResult();
				requestWashResult.setRequestStatus(rqSt);
				bbr.setBBResultCode(-100);
				bbr.setBBResultDesc("");
				requestWashResult.setBBResult(bbr);
			}
			if (requestResult == WSRequestFlags.RESULT_TIMEOUT) {
				requestWashResult.setRequestWashResult(-1);
				BBResult bbr = new BBResult();
				requestWashResult.setRequestStatus(rqSt);
				bbr.setBBResultCode(-99);
				bbr.setBBResultDesc("");
				requestWashResult.setBBResult(bbr);
			}
			requestWashResult.setRequestStatus(rqSt);
			return requestWashResult;
		} catch (Exception ex) {
			GlobalApp.WriteLogException(ex);
			requestWashResult.setClientClientExcCode(WSRequestFlags.RESULT_ERROR.toString());
			requestWashResult.setRequestWashResult(-1);
			BBResult bbr = new BBResult();
			requestWashResult.setRequestStatus(rqSt);
			bbr.setBBResultCode(-100);
			bbr.setBBResultDesc("");
			requestWashResult.setBBResult(bbr);
			return requestWashResult;
		}
	}

	public CancelResult cancelDealHTTP(List<DataParameter> parameters,
			String url) {
		CancelResult cancelRes = new CancelResult();

		try {

			Document doc = tryRequest(parameters, url);
			cancelRes.setClientClientExcCode(requestResult.toString());
			if (requestResult == WSRequestFlags.RESULT_OK) {

				NodeList nList = doc.getElementsByTagName("CancelWashResult");
				Node nNode = nList.item(0);
				Element eElement = (Element) nNode;

				cancelRes.setIsCanceled(Boolean.parseBoolean(getTagValue(
						"IsCanceled", eElement)));

				if (cancelRes.IsCanceled()) {
					cancelRes.setWashId(getTagValue("WashId", eElement));
					cancelRes.setCarNumber(getTagValue("CarNumber", eElement));
				}

				else {

					NodeList nList1 = doc.getElementsByTagName("BBResult");
					Node nNode1 = nList1.item(0);
					Element eElement1 = (Element) nNode1;

					BBResult bbr = new BBResult();
					bbr.setBBResultCode(Integer.parseInt(getTagValue(
							"ResultCode", eElement1)));
					bbr.setBBResultDesc(getTagValue("ResultDesc", eElement1));
					cancelRes.setBBResult(bbr);

				}
			}

			// //////////////////////////not exist in OFFLINE logic

			/*
			 * if (requestResult == WSRequestFlags.RESULT_ERROR) {
			 * cancelRes.setIsCanceled(false); BBResult bbr = new BBResult();
			 * bbr.setBBResultCode(-100); bbr.setBBResultDesc("");
			 * cancelRes.setBBResult(bbr); } if (requestResult ==
			 * WSRequestFlags.RESULT_TIMEOUT) { cancelRes.setIsCanceled(false);
			 * BBResult bbr = new BBResult(); bbr.setBBResultCode(-99);
			 * bbr.setBBResultDesc(""); cancelRes.setBBResult(bbr); }
			 */
			return cancelRes;

		} catch (Exception ex) {

			GlobalApp.WriteLogException(ex);
			cancelRes.setIsCanceled(false);
			BBResult bbr = new BBResult();
			bbr.setBBResultCode(-100);
			bbr.setBBResultDesc("");
			cancelRes.setBBResult(bbr);
			cancelRes.setClientClientExcCode(WSRequestFlags.RESULT_ERROR
					.toString());

			return cancelRes;
		}
	}

	public ArrayList<DeleteItem> getListOfDeleteItems(
			List<DataParameter> parameters, String url) {
		ArrayList<DeleteItem> deleteItems = new ArrayList<DeleteItem>();
		try {

			Document doc = tryRequest(parameters, url);

			if (requestResult == WSRequestFlags.RESULT_OK) {

				NodeList nList = doc.getElementsByTagName("DeleteItem");
				for (int temp = 0; temp < nList.getLength(); temp++) {

					DeleteItem item = new DeleteItem();
					Node nNode = nList.item(temp);
					if (nNode.getNodeType() == Node.ELEMENT_NODE) {

						Element eElement = (Element) nNode;

						item.setWashId(getTagValue("WashId", eElement));
						item.setCarNumber(getTagValue("CarNumber", eElement));
						item.setServiceCode(getTagValue("WashType", eElement));
						item.setWashDate(getTagValue("Wash_Date", eElement));
						deleteItems.add(item);

					}

				}
			}

			return deleteItems;
		}

		catch (Exception e) {

			GlobalApp.WriteLogException(e);
			return null;
		}

		// ---return the definitions of the word---

	}

	public ServerData getServerData(List<DataParameter> parameters, String url) {
		ServerData sd = new ServerData();
		ArrayList<AddItems> addItems = new ArrayList<AddItems>();
		ArrayList<ServerProp> properties = new ArrayList<ServerProp>();
		try {
			Document doc = tryRequest(parameters, url);
			sd.setClientClientExcCode(requestResult.toString());
			if (requestResult == WSRequestFlags.RESULT_OK) {
				NodeList nList1 = doc.getElementsByTagName("Property");
				for (int temp1 = 0; temp1 < nList1.getLength(); temp1++) {
					ServerProp p = new ServerProp();
					Node nNode1 = nList1.item(temp1);
					if (nNode1.getNodeType() == Node.ELEMENT_NODE) {
						Element eElement1 = (Element) nNode1;
						p.setPropertyCode(Integer.parseInt(getTagValue("PropertyCode", eElement1)));
						p.setPropertyValue(getTagValue("PropertyValue", eElement1));
						properties.add(p);
					}
				}
				NodeList nList = doc.getElementsByTagName("Product");
				for (int temp = 0; temp < nList.getLength(); temp++) {
					AddItems item = new AddItems();
					Node nNode = nList.item(temp);
					if (nNode.getNodeType() == Node.ELEMENT_NODE) {
						Element eElement = (Element) nNode;
						item.setItemCode(Integer.parseInt(getTagValue("ItemCode", eElement)));
						item.setItemDesc(getTagValue("ItemDesc", eElement));
						item.setItemSort(Integer.parseInt(getTagValue("ItemSort", eElement)));
						item.setItemIsMain(Integer.parseInt(getTagValue("IsMain", eElement)));
						addItems.add(item);
					}
				}

				NodeList nList2 = doc.getElementsByTagName("RequestStatus");
				for (int temp = 0; temp < nList2.getLength(); temp++) {

					Node nNode = nList2.item(temp);
					if (nNode.getNodeType() == Node.ELEMENT_NODE) {

						Element eElement = (Element) nNode;

						rqSt.setRequestStatusCode((getTagValue(
								"RequestStatusCode", eElement)));

					}
				}
				sd.setRequestStatus(rqSt);
				sd.setProperties(properties);
				sd.setItems(addItems);
			}

			sd.setRequestStatus(rqSt);
			return sd;

		}

		catch (Exception e) {
			GlobalApp.WriteLogException(e);
			sd.setClientClientExcCode(WSRequestFlags.RESULT_ERROR.toString());
			sd.setRequestStatus(rqSt);
			return sd;
		}

		// ---return the definitions of the word---

	}

	public String getLastVersion(String urlString) {

		try {
			URL updateURL = new URL(urlString);
			URLConnection conn = updateURL.openConnection();
			InputStream is = conn.getInputStream();
			BufferedInputStream bis = new BufferedInputStream(is);
			ByteArrayBuffer baf = new ByteArrayBuffer(50);

			int current = 0;
			while ((current = bis.read()) != -1) {
				baf.append((byte) current);
			}

			/* Convert the Bytes read to a String. */
			final String s = new String(baf.toByteArray());
			return s;

		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
			return "0";

		}
	}

	public IsActiveResult getIsActiveResult(List<DataParameter> parameters,
			String url) {
		IsActiveResult isar = new IsActiveResult();

		try {
			Document doc = tryRequest(parameters, url);
			isar.setClientClientExcCode(requestResult.toString());
			if (requestResult == WSRequestFlags.RESULT_OK) {

				NodeList nList2 = doc.getElementsByTagName("RequestStatus");
				for (int temp = 0; temp < nList2.getLength(); temp++) {

					Node nNode = nList2.item(temp);
					if (nNode.getNodeType() == Node.ELEMENT_NODE) {

						Element eElement = (Element) nNode;

						rqSt.setRequestStatusCode((getTagValue(
								"RequestStatusCode", eElement)));

					}
				}

				isar.setRequestStatus(rqSt);

				NodeList nList = doc.getElementsByTagName("IsActiveResult");
				Node nNode = nList.item(0);
				Element eElement = (Element) nNode;

				isar.setIsActive(Boolean.parseBoolean(getTagValue("IsActive",
						eElement)));
				if (isar.getIsActive() == false) {
					NodeList nList1 = doc.getElementsByTagName("BBResult");
					Node nNode1 = nList1.item(0);
					Element eElement1 = (Element) nNode1;

					BBResult bbr = new BBResult();
					bbr.setBBResultCode(Integer.parseInt(getTagValue(
							"ResultCode", eElement1)));
					bbr.setBBResultDesc(getTagValue("ResultDesc", eElement1));
					isar.setBBResult(bbr);

				}
			}
			isar.setRequestStatus(rqSt);
			return isar;

		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
			BBResult bbr = new BBResult();
			bbr.setBBResultCode(-100);
			bbr.setBBResultDesc("");
			isar.setBBResult(bbr);
			isar.setIsActive(false);
			isar.setRequestStatus(rqSt);
			isar.setClientClientExcCode(WSRequestFlags.RESULT_ERROR.toString());
			return isar;
		}

	}
	
	public ApproveLPRv2Result getApproveLPRv2(List<DataParameter> parameters, String url) {
		ApproveLPRv2Result lpResult = new ApproveLPRv2Result();
		try {
			Document doc = tryRequest(parameters, url);
			lpResult.setClientClientExcCode(requestResult.toString());
			if (requestResult == WSRequestFlags.RESULT_OK) {
				NodeList nList2 = doc.getElementsByTagName("RequestStatus");
				for (int temp = 0; temp < nList2.getLength(); temp++) {
					Node nNode = nList2.item(temp);
					if (nNode.getNodeType() == Node.ELEMENT_NODE) {
						Element eElement = (Element) nNode;
						rqSt.setRequestStatusCode((getTagValue("RequestStatusCode", eElement)));
						rqSt.setRequestStatusDesc((getTagValue("RequestStatusDesc", eElement)));
					}
				}
				lpResult.setRequestStatus(rqSt);
					
				
				NodeList nList = doc.getElementsByTagName("LprApproveResult");
				Node nNode = nList.item(0);
				Element eElement = (Element) nNode;
				lpResult.setWashId(getTagValue("washId", eElement));
				
				lpResult.setIsApprove(Boolean.parseBoolean(getTagValue("IsApprove", eElement)));
				try{
					lpResult.setCarType(getTagValue("carType", eElement));
					lpResult.setServiceCode(getTagValue("serviceCode", eElement));
					lpResult.setWashType(getTagValue("WashType", eElement));
				}catch(Exception ex){
				//Do nothing
				}

				NodeList nList1 = doc.getElementsByTagName("BBResult");
				Node nNode1 = nList1.item(0);
				Element eElement1 = (Element) nNode1;
				BBResult bbr = new BBResult();
				bbr.setBBResultCode(Integer.parseInt(getTagValue("ResultCode", eElement1)));
				bbr.setBBResultDesc(getTagValue("ResultDesc", eElement1));
				bbr.setMdFlowType(Integer.parseInt(getTagValue("MdFlowType", eElement1)));
				lpResult.setBBResult(bbr);
	
			} else {
				if (requestResult == WSRequestFlags.RESULT_ERROR) {
					BBResult bbr = new BBResult();
					bbr.setBBResultCode(-100);
					bbr.setBBResultDesc("");
					lpResult.setRequestStatus(rqSt);
					lpResult.setBBResult(bbr);
					lpResult.setWashId("-1");
					lpResult.setIsApprove(false);					
				}
				if (requestResult == WSRequestFlags.RESULT_TIMEOUT) {
					BBResult bbr = new BBResult();
					bbr.setBBResultCode(-99);
					bbr.setBBResultDesc("");
					lpResult.setRequestStatus(rqSt);
					lpResult.setBBResult(bbr);
					lpResult.setWashId("-1");
					lpResult.setIsApprove(false);	
				}
				if (requestResult != WSRequestFlags.RESULT_TIMEOUT && requestResult != WSRequestFlags.RESULT_ERROR) {
					lpResult.setClientClientExcCode(WSRequestFlags.RESULT_ERROR.toString());
					BBResult bbr = new BBResult();
					bbr.setBBResultCode(-100);
					bbr.setBBResultDesc("");
					lpResult.setRequestStatus(rqSt);
					lpResult.setBBResult(bbr);
					lpResult.setWashId("-1");
					lpResult.setIsApprove(false);	
					return lpResult;
				}

			}
			return lpResult;
		}

		catch (Exception e) {
			GlobalApp.WriteLogException(e);
			lpResult.setClientClientExcCode(WSRequestFlags.RESULT_ERROR.toString());
			BBResult bbr = new BBResult();
			bbr.setBBResultCode(-100);
			bbr.setBBResultDesc("");
			lpResult.setRequestStatus(rqSt);
			lpResult.setBBResult(bbr);
			lpResult.setWashId("-1");
			lpResult.setIsApprove(false);	
			return lpResult;
		}
	}

}
