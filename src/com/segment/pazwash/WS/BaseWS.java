package com.segment.pazwash.WS;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;

import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.segment.pazwash.BL.GlobalApp;
import com.segment.pazwash.DM.DataParameter;
import com.segment.pazwash.DM.RequestStatus;

import com.segment.pazwash.Enums.TimeOutTry;
import com.segment.pazwash.Enums.WSRequestFlags;

public abstract class BaseWS {

	WSRequestFlags requestResult = WSRequestFlags.RESULT_OK;
	RequestStatus rqSt = new RequestStatus();
	TimeOutTry currentTimeOutTry = GlobalApp.getTimeOutTry();

	protected Document tryRequest(List<DataParameter> parameters, String url) {

		String in = null;
		Document doc = null;

		String data = BuildDataParameters(parameters);
		in = BaseRequest(data, url);

		if (requestResult == WSRequestFlags.RESULT_OK) {
			try {
				doc = CreateDocument(in);
			} catch (Exception e) {
				GlobalApp.WriteLogException(e);
				requestResult = WSRequestFlags.RESULT_ERROR;
			}
		}

		return doc;
	}

	protected String BuildDataParameters(List<DataParameter> parameters) {

		try {
			String data = "";

			for (int i = 0; i < parameters.size(); i++) {

				DataParameter dp = parameters.get(i);

				if (dp.getParameterValue() == null) {
					dp.setParameterValue("");
				}

				if (i == 0) {
					data = URLEncoder.encode(dp.getParameterName(), "UTF-8")
							+ "="
							+ URLEncoder
									.encode(dp.getParameterValue(), "UTF-8");
				} else {
					data += "&"
							+ URLEncoder.encode(dp.getParameterName(), "UTF-8")
							+ "="
							+ URLEncoder
									.encode(dp.getParameterValue(), "UTF-8");
				}
			}

			return data;
		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
			return "";
		}
	}

	protected static String getTagValue(String sTag, Element eElement) {
		
		NodeList nlList = eElement.getElementsByTagName(sTag).item(0).getChildNodes();
		String  nodeValue = "";
		if (nlList.getLength() > 0)
		{
			if(nlList.item(0) != null)
			{
				Node nValue = (Node) nlList.item(0);
				nodeValue = nValue.getNodeValue();	
			}
		}
		
		return nodeValue;
	}

	protected String getHTTP(String urlString) throws IOException

	{

		StringBuilder stringBuilder = new StringBuilder();
		// HttpClient client = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(urlString);
		HttpParams httpParameters = new BasicHttpParams();
		int timeoutConnection = (GlobalApp.getTimeoutSeconds() + currentTimeOutTry
				.ToInt()) * 1000;
		HttpConnectionParams.setConnectionTimeout(httpParameters,
				timeoutConnection);
		int timeoutSocket = (GlobalApp.getTimeoutSeconds() + currentTimeOutTry
				.ToInt()) * 1000;
		HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
		DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
		try {
			HttpResponse response = httpClient.execute(httpGet);
			StatusLine statusLine = response.getStatusLine();
			int statusCode = statusLine.getStatusCode();
			if (statusCode == 200) {
				HttpEntity entity = response.getEntity();
				InputStream content = entity.getContent();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(content));
				String line;
				while ((line = reader.readLine()) != null) {
					stringBuilder.append(line);
				}
			} else {

			}
		} catch (ClientProtocolException e) {
			GlobalApp.WriteLogException(e);
			requestResult = WSRequestFlags.RESULT_ERROR;
		} catch (IOException e) {
			GlobalApp.WriteLogException(e); 
			requestResult = WSRequestFlags.RESULT_ERROR;
		}
		GlobalApp.setTimeOutTry(TimeOutTry.TIMEOUT_TRY_ZERO);
		return stringBuilder.toString();
	}

	protected Document CreateDocument(String in) {
		Document doc = null;
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db;
		try {
			db = dbf.newDocumentBuilder();
			if (in.length()>0)
				doc = db.parse(new ByteArrayInputStream(in.getBytes()));
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			GlobalApp.WriteLogException(e);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			GlobalApp.WriteLogException(e);
		}
		doc.getDocumentElement().normalize();
		return doc;
	}

	protected String WashApproveRequest(String data, String urlString) {

		try {

			String in = null;

			URL url = new URL(urlString);
			URLConnection conn = url.openConnection();
			conn.setConnectTimeout(GlobalApp.getWashApproveTimeoutSeconds() * 1000);
			conn.setReadTimeout(GlobalApp.getWashApproveTimeoutSeconds() * 1000);
			if (!(conn instanceof HttpURLConnection))
				throw new IOException("Not an HTTP connection");
			try {
				HttpURLConnection httpConn = (HttpURLConnection) conn;
				httpConn.setConnectTimeout(GlobalApp
						.getWashApproveTimeoutSeconds() * 1000);
				httpConn.setReadTimeout(GlobalApp
						.getWashApproveTimeoutSeconds() * 1000);
				httpConn.setAllowUserInteraction(false);
				httpConn.setInstanceFollowRedirects(true);
				httpConn.setDoOutput(true);

				try {
					OutputStreamWriter wr = new OutputStreamWriter(
							httpConn.getOutputStream());

					wr.write(data);
					wr.flush();

					BufferedReader rd = null;

					// Get the response
					rd = new BufferedReader(new InputStreamReader(
							conn.getInputStream(), "UTF-8"));

					String line = null;
					String response2 = "";
					while ((line = rd.readLine()) != null) {
						response2 += line;
					}

					in = response2;
					GlobalApp.setTimeOutTry(TimeOutTry.TIMEOUT_TRY_ZERO);
				}

				catch (SocketTimeoutException ste) {
					GlobalApp.WriteLogException(ste);
					requestResult = WSRequestFlags.RESULT_TIMEOUT;
					if (currentTimeOutTry == TimeOutTry.TIMEOUT_TRY_ZERO) {
						GlobalApp.setTimeOutTry(TimeOutTry.TIMEOUT_TRY_FIRST);
					}
					if (currentTimeOutTry == TimeOutTry.TIMEOUT_TRY_FIRST) {
						GlobalApp.setTimeOutTry(TimeOutTry.TIMEOUT_TRY_SECOND);
					}
					if (currentTimeOutTry == TimeOutTry.TIMEOUT_TRY_SECOND) {
						GlobalApp.setTimeOutTry(TimeOutTry.TIMEOUT_TRY_OFFLINE);
					}

					return "";
				}

			}

			catch (Exception ex) {
				GlobalApp.WriteLogException(ex);
				requestResult = WSRequestFlags.RESULT_ERROR;

				return "";
			}

			return in;
		}

		catch (Exception e) {
			// String s = e.getMessage();
			GlobalApp.WriteLogException(e);
			requestResult = WSRequestFlags.RESULT_ERROR;

			return "";
		}

	}

	protected String BaseRequest(String data, String urlString) {

		try {

			String in = null;

			URL url = new URL(urlString);
			URLConnection conn = url.openConnection();
			conn.setConnectTimeout((GlobalApp.getTimeoutSeconds() + currentTimeOutTry
					.ToInt()) * 1000);
			conn.setReadTimeout((GlobalApp.getTimeoutSeconds() + currentTimeOutTry
					.ToInt()) * 1000);
			if (!(conn instanceof HttpURLConnection))
				throw new IOException("Not an HTTP connection");
			try {
				HttpURLConnection httpConn = (HttpURLConnection) conn;
				httpConn.setConnectTimeout((GlobalApp.getTimeoutSeconds() + currentTimeOutTry
						.ToInt()) * 1000);
				httpConn.setReadTimeout((GlobalApp.getTimeoutSeconds() + currentTimeOutTry
						.ToInt()) * 1000);
				httpConn.setAllowUserInteraction(false);
				httpConn.setInstanceFollowRedirects(true);
				httpConn.setDoOutput(true);

				try {
					OutputStreamWriter wr = new OutputStreamWriter(
							httpConn.getOutputStream());

					wr.write(data);
					wr.flush();

					BufferedReader rd = null;

					// Get the response
					rd = new BufferedReader(new InputStreamReader(
							conn.getInputStream(), "UTF-8"));

					String line = null;
					String response2 = "";
					while ((line = rd.readLine()) != null) {
						response2 += line;
					}

					in = response2;
					GlobalApp.setTimeOutTry(TimeOutTry.TIMEOUT_TRY_ZERO);
				}

				catch (SocketTimeoutException ste) {
					GlobalApp.WriteLogException(ste);
					requestResult = WSRequestFlags.RESULT_TIMEOUT;
					if (currentTimeOutTry == TimeOutTry.TIMEOUT_TRY_ZERO) {
						GlobalApp.setTimeOutTry(TimeOutTry.TIMEOUT_TRY_FIRST);
					}
					if (currentTimeOutTry == TimeOutTry.TIMEOUT_TRY_FIRST) {
						GlobalApp.setTimeOutTry(TimeOutTry.TIMEOUT_TRY_SECOND);
					}
					if (currentTimeOutTry == TimeOutTry.TIMEOUT_TRY_SECOND) {
						GlobalApp.setTimeOutTry(TimeOutTry.TIMEOUT_TRY_OFFLINE);
					}
					/*
					if (currentTimeOutTry == TimeOutTry.TIMEOUT_TRY_THIRD) {
						GlobalApp.setTimeOutTry(TimeOutTry.TIMEOUT_TRY_OFFLINE);
					}
					*/

					return "";
				}

			}

			catch (Exception ex) {
				GlobalApp.WriteLogException(ex);
				requestResult = WSRequestFlags.RESULT_ERROR;

				return "";
			}

			return in;
		}

		catch (Exception e) {
			// String s = e.getMessage();
			GlobalApp.WriteLogException(e);
			requestResult = WSRequestFlags.RESULT_ERROR;

			return "";
		}

	}

	// if (counter == Count) {
	//
	// DeleteSendedOfflineData();
	//
	// }
	// }

}
