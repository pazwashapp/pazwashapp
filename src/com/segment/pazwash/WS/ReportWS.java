package com.segment.pazwash.WS;

import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import com.segment.pazwash.BL.GlobalApp;
import com.segment.pazwash.DM.DataParameter;
import com.segment.pazwash.DM.Purchase;
import com.segment.pazwash.DM.PurchaseData;
import com.segment.pazwash.DM.ReportSummaryItem;

import com.segment.pazwash.DM.SelectedDayReport;
import com.segment.pazwash.DM.SelectedDayWashes;
import com.segment.pazwash.DM.SummaryReportResult;
import com.segment.pazwash.Enums.WSRequestFlags;

public class ReportWS extends BaseWS {

	public PurchaseData getPurchaseData(List<DataParameter> parameters,
			String url) {
		PurchaseData pd = new PurchaseData();
		ArrayList<Purchase> purchases = new ArrayList<Purchase>();

		try {

			Document doc = tryRequest(parameters, url);

			pd.setClientClientExcCode(requestResult.toString());

			if (requestResult == WSRequestFlags.RESULT_OK) {

				NodeList nList4 = doc.getElementsByTagName("RequestStatus");
				for (int temp = 0; temp < nList4.getLength(); temp++) {

					Node nNode = nList4.item(temp);
					if (nNode.getNodeType() == Node.ELEMENT_NODE) {

						Element eElement = (Element) nNode;

						rqSt.setRequestStatusCode((getTagValue(
								"RequestStatusCode", eElement)));

					}
				}
				pd.setRequestStatus(rqSt);

				NodeList nList2 = doc.getElementsByTagName("PurchaseData");
				Node nNode2 = nList2.item(0);
				Element eElement2 = (Element) nNode2;

				pd.setSumPurchaseNumber(getTagValue("sumPurchaseNumber",
						eElement2).toString());
				pd.setSumPurchseValue(getTagValue("sumPurchseValue", eElement2)
						.toString());
				pd.setSumMam(getTagValue("sumMam", eElement2).toString());
				pd.setSumPurchaseValueMam(getTagValue("sumPurchaseValueMam",
						eElement2).toString());

				NodeList nList1 = doc.getElementsByTagName("Purchase");
				for (int temp1 = 0; temp1 < nList1.getLength(); temp1++) {

					Purchase p = new Purchase();
					Node nNode1 = nList1.item(temp1);
					if (nNode1.getNodeType() == Node.ELEMENT_NODE) {

						Element eElement1 = (Element) nNode1;

						p.setProductCode(getTagValue("Product_Code", eElement1));
						p.setProductDesc(getTagValue("Product_Desc", eElement1));
						p.setPurchaseNumber(getTagValue("Purchase_Number",
								eElement1));
						p.setPurchaseValue(getTagValue("Purchase_Value",
								eElement1));
						p.setMam(getTagValue("Mam", eElement1));
						p.setPurchaseValueMam(getTagValue("Purchase_Value_Mam",
								eElement1));

						purchases.add(p);

					}
				}

				pd.setPurchases(purchases);
			}

			pd.setRequestStatus(rqSt);
			return pd;

		}

		catch (Exception e) {
			GlobalApp.WriteLogException(e);
			pd.setRequestStatus(rqSt);
			pd.setClientClientExcCode(WSRequestFlags.RESULT_ERROR.toString());
			return pd;
		}

		// ---return the definitions of the word---

	}

	public SummaryReportResult getSummary(List<DataParameter> parameters,
			String url) {
		SummaryReportResult srr = new SummaryReportResult();
		ArrayList<ReportSummaryItem> summary = new ArrayList<ReportSummaryItem>();

		try {

			Document doc = tryRequest(parameters, url);

			srr.setClientClientExcCode(requestResult.toString());

			if (requestResult == WSRequestFlags.RESULT_OK) {
				NodeList nList4 = doc.getElementsByTagName("RequestStatus");
				for (int temp = 0; temp < nList4.getLength(); temp++) {

					Node nNode = nList4.item(temp);
					if (nNode.getNodeType() == Node.ELEMENT_NODE) {

						Element eElement = (Element) nNode;

						rqSt.setRequestStatusCode((getTagValue(
								"RequestStatusCode", eElement)));

					}
				}
				srr.setRequestStatus(rqSt);

				NodeList nList = doc.getElementsByTagName("SummaryReport");
				for (int temp = 0; temp < nList.getLength(); temp++) {

				//	if (temp != 0) {

						ReportSummaryItem item = new ReportSummaryItem();
						Node nNode = nList.item(temp);
						if (nNode.getNodeType() == Node.ELEMENT_NODE) {

							Element eElement = (Element) nNode;

							item.setTitle(getTagValue("Title", eElement));
							item.setValue(getTagValue("Value", eElement));

							summary.add(item);

						}
					//}

				}
			}

			srr.setRequestStatus(rqSt);

			srr.setSummaryItems(summary);

			return srr;
		}

		catch (Exception e) {

			GlobalApp.WriteLogException(e);
			srr.setRequestStatus(rqSt);
			srr.setClientClientExcCode(WSRequestFlags.RESULT_ERROR.toString());
			return srr;
		}

		// ---return the definitions of the word---

	}

	public SelectedDayReport getSelectedDayWashes(
			List<DataParameter> parameters, String url) {
		SelectedDayReport sdr = new SelectedDayReport();
		ArrayList<SelectedDayWashes> washes = new ArrayList<SelectedDayWashes>();

		try {

			Document doc = tryRequest(parameters, url);

			sdr.setClientClientExcCode(requestResult.toString());
			if (requestResult == WSRequestFlags.RESULT_OK) {

				NodeList nList4 = doc.getElementsByTagName("RequestStatus");
				for (int temp = 0; temp < nList4.getLength(); temp++) {

					Node nNode = nList4.item(temp);
					if (nNode.getNodeType() == Node.ELEMENT_NODE) {

						Element eElement = (Element) nNode;

						rqSt.setRequestStatusCode((getTagValue(
								"RequestStatusCode", eElement)));

					}
				}
				sdr.setRequestStatus(rqSt);

				NodeList nList = doc.getElementsByTagName("SelectedDayWashes");
				for (int temp = 0; temp < nList.getLength(); temp++) {

					//if (temp != 0) {

						SelectedDayWashes item = new SelectedDayWashes();
						Node nNode = nList.item(temp);
						if (nNode.getNodeType() == Node.ELEMENT_NODE) {

							Element eElement = (Element) nNode;

							item.setWashId(getTagValue("WashId", eElement));
							item.setCarNumber(getTagValue("CarNumber", eElement));
							item.setServiceCode(getTagValue("WashType",
									eElement));
							item.setWashDate(getTagValue("Wash_Date", eElement));
							// item.setAddItem(getTagValue("AddItem",
							// eElement));
							item.setUserName(getTagValue("UserName", eElement));
							washes.add(item);

						}

					//}
				}
			}
			sdr.setRequestStatus(rqSt);
			sdr.setSelectedDayReport(washes);
			return sdr;
		}

		catch (Exception e) {

			GlobalApp.WriteLogException(e);
			sdr.setRequestStatus(rqSt);
			sdr.setClientClientExcCode(WSRequestFlags.RESULT_ERROR.toString());
			return sdr;
		}

		// ---return the definitions of the word---

	}

}
