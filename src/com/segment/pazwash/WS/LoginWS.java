package com.segment.pazwash.WS;

import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.segment.pazwash.BL.GlobalApp;
import com.segment.pazwash.DM.ChangePasswordResult;
import com.segment.pazwash.DM.DataParameter;
import com.segment.pazwash.DM.ForgotPasswordResult;
import com.segment.pazwash.DM.LoginResult;

import com.segment.pazwash.Enums.LoginServerStatus;
import com.segment.pazwash.Enums.WSRequestFlags;

//Login Methods

public class LoginWS extends BaseWS {

	public LoginResult CheckLoginShortHTTP(List<DataParameter> parameters, String url) {
		LoginResult loginResult = new LoginResult();
		try {
			Document doc = tryRequest(parameters, url);
			loginResult.setClientClientExcCode(requestResult.toString());
			if (requestResult == WSRequestFlags.RESULT_OK) {
				
				NodeList nList2 = doc.getElementsByTagName("RequestStatus");
				for (int temp = 0; temp < nList2.getLength(); temp++) {
					Node nNode = nList2.item(temp);
					if (nNode.getNodeType() == Node.ELEMENT_NODE) {
						Element eElement = (Element) nNode;
						rqSt.setRequestStatusCode((getTagValue("RequestStatusCode", eElement)));
					}
				}
				loginResult.setRequestStatus(rqSt);
				NodeList nList = doc.getElementsByTagName("LoginResult");
				Node nNode = nList.item(0);
				Element eElement = (Element) nNode;

				loginResult.setUserName(getTagValue("UserName", eElement));
				loginResult.setUserId(getTagValue("UserId", eElement));
				loginResult.setLoginStatus(getTagValue("LoginStatus", eElement));
				loginResult.setUserStatusDesc(getTagValue("UserStatusDesc", eElement));

				if (loginResult.LoginStatus().equals(LoginServerStatus.LOGIN_OK.toString())) {

					loginResult.setOfflineLimit(getTagValue("OfflineLimit", eElement));
					loginResult.setMDCode(getTagValue("MDCode", eElement));
					loginResult.setStationId(getTagValue("SationID", eElement));
					loginResult.setStationNumber(getTagValue("StationNumber", eElement));
					loginResult.setIsCheckGPS(getTagValue("IsCheckGPS", eElement));
					
				}
			}

			loginResult.setRequestStatus(rqSt);
			return loginResult;
		}

		catch (Exception e) {
			GlobalApp.WriteLogException(e);
			loginResult.setClientClientExcCode(WSRequestFlags.RESULT_ERROR.toString());
			loginResult.setRequestStatus(rqSt);
			return loginResult;
		}
	}

	public ChangePasswordResult ChangePasswordShortHTTP(
			List<DataParameter> parameters, String url) {
		ChangePasswordResult changePasswordResult = new ChangePasswordResult();
		changePasswordResult.setChangePasswordStatus("-1");
	

		try {
			
			Document doc = tryRequest(parameters, url);
			changePasswordResult.setClientClientExcCode(requestResult
					.toString());
			if (requestResult == WSRequestFlags.RESULT_OK) {
				
				NodeList nList2 = doc.getElementsByTagName("RequestStatus");
				for (int temp = 0; temp < nList2.getLength(); temp++) {

					Node nNode = nList2.item(temp);
					if (nNode.getNodeType() == Node.ELEMENT_NODE) {

						Element eElement = (Element) nNode;

						rqSt.setRequestStatusCode((getTagValue(
								"RequestStatusCode", eElement)));

					}
				}
				
				changePasswordResult.setRequestStatus(rqSt);
				

				NodeList nList = doc
						.getElementsByTagName("ChangePasswordResult");
				Node nNode = nList.item(0);
				Element eElement = (Element) nNode;

				changePasswordResult.setUserName(getTagValue("UserName",
						eElement));
				changePasswordResult.setChangePasswordStatus(getTagValue(
						"ChangePasswordStatus", eElement));
			}
			changePasswordResult.setRequestStatus(rqSt);
			return changePasswordResult;
		}

		catch (Exception e) {

			GlobalApp.WriteLogException(e);
			changePasswordResult
					.setClientClientExcCode(WSRequestFlags.RESULT_ERROR
							.toString());
			changePasswordResult.setRequestStatus(rqSt);
			return changePasswordResult;
		}

		// ---return the definitions of the word---

	}

	public ForgotPasswordResult getForgotPasswordHTTP(
			List<DataParameter> parameters, String url) {
		ForgotPasswordResult forgotPasswordResult = new ForgotPasswordResult();
		forgotPasswordResult.setForgotPasswordStatus("0");

		try {

			Document doc = tryRequest(parameters, url);
			forgotPasswordResult.setClientClientExcCode(requestResult
					.toString());
			if (requestResult == WSRequestFlags.RESULT_OK) {

				NodeList nList = doc
						.getElementsByTagName("ForgotPasswordResult");
				Node nNode = nList.item(0);
				Element eElement = (Element) nNode;

				forgotPasswordResult.setUserName(getTagValue("UserName",
						eElement));
				forgotPasswordResult.setForgotPasswordStatus(getTagValue(
						"ForgotPasswordStatus", eElement));
			}
			return forgotPasswordResult;
		}

		catch (Exception e) {

			GlobalApp.WriteLogException(e);
			forgotPasswordResult.setForgotPasswordStatus("-1");
			forgotPasswordResult
					.setClientClientExcCode(WSRequestFlags.RESULT_ERROR
							.toString());
			return forgotPasswordResult;
		}

		// ---return the definitions of the word---

	}

	public String getQuestionHTTP(List<DataParameter> parameters, String url) {
		String getQuestionResult = "";

		try {
			String strDefinition = "";
			String in = null;

			String data = BuildDataParameters(parameters);
			in = BaseRequest(data, url);
			Document doc = CreateDocument(in);

			NodeList nList = doc.getElementsByTagName("QuestionResult");
			Node nNode = nList.item(0);
			Element eElement = (Element) nNode;

			strDefinition = getTagValue("UpdateQuestionResult", eElement);

			try {
				// MainActivity.updateWashResult = strDefinition.toString();
				getQuestionResult = strDefinition.toString();
			} catch (Exception e) {
				// MainActivity.updateWashResult = "-1";
				GlobalApp.WriteLogException(e);
				getQuestionResult = "0";
			}

			// return MainActivity.updateWashResult;
			return getQuestionResult;
		}

		catch (Exception e) {

			GlobalApp.WriteLogException(e);
			return "0";
		}

		// ---return the definitions of the word---

	}

}
