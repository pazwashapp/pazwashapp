package com.segment.pazwash.WS;

import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.segment.pazwash.BL.GlobalApp;
import com.segment.pazwash.DM.DataParameter;

import com.segment.pazwash.DM.SQLiteWSResult;
import com.segment.pazwash.Enums.WSRequestFlags;

//send data from db to server

public class SqlLiteWS extends BaseWS {

	public SQLiteWSResult SendBBHTTP(List<DataParameter> parameters, String url) {

		SQLiteWSResult sqlWsResult = new SQLiteWSResult();
	

		try {
			
			Document doc = tryRequest(parameters, url);
			sqlWsResult.setClientClientExcCode(requestResult.toString());
			if (requestResult == WSRequestFlags.RESULT_OK) {
				
				
				NodeList nList2 = doc.getElementsByTagName("RequestStatus");
				for (int temp = 0; temp < nList2.getLength(); temp++) {

					Node nNode = nList2.item(temp);
					if (nNode.getNodeType() == Node.ELEMENT_NODE) {

						Element eElement = (Element) nNode;

						rqSt.setRequestStatusCode((getTagValue(
								"RequestStatusCode", eElement)));

					}
				}
				
				
				sqlWsResult.setRequestStatus(rqSt);
				
				NodeList nList = doc.getElementsByTagName("BBStepResult");
				Node nNode = nList.item(0);
				Element eElement = (Element) nNode;

				sqlWsResult.setResult(getTagValue("BBStepRes", eElement));
			}
			else
			{
				sqlWsResult.setRequestStatus(rqSt);
				sqlWsResult.setResult("-1");
			}

		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
			sqlWsResult.setClientClientExcCode(WSRequestFlags.RESULT_ERROR
					.toString());
			sqlWsResult.setRequestStatus(rqSt);
			sqlWsResult.setResult("-1");

		}
		sqlWsResult.setRequestStatus(rqSt);
		return sqlWsResult;
		// ---return the definitions of the word---

	}

	public SQLiteWSResult SendLogsHTTP(List<DataParameter> parameters, String url) {
		SQLiteWSResult sqlWsResult = new SQLiteWSResult();
		try {
			Document doc = tryRequest(parameters, url);
			sqlWsResult.setClientClientExcCode(requestResult.toString());
			if (requestResult == WSRequestFlags.RESULT_OK) {
				NodeList nList2 = doc.getElementsByTagName("RequestStatus");
				for (int temp = 0; temp < nList2.getLength(); temp++) {
					Node nNode = nList2.item(temp);
					if (nNode.getNodeType() == Node.ELEMENT_NODE) {
						Element eElement = (Element) nNode;
						rqSt.setRequestStatusCode((getTagValue("RequestStatusCode", eElement)));
					}
				}
				sqlWsResult.setRequestStatus(rqSt);
				NodeList nList = doc.getElementsByTagName("LogResult");
				Node nNode = nList.item(0);
				Element eElement = (Element) nNode;
				sqlWsResult.setResult(getTagValue("AddLogResult", eElement));
			}
			else
			{
				sqlWsResult.setRequestStatus(rqSt);
				sqlWsResult.setResult("-1");
			}
		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
			sqlWsResult.setRequestStatus(rqSt);
			sqlWsResult.setClientClientExcCode(WSRequestFlags.RESULT_ERROR.toString());
			sqlWsResult.setResult("-1");
		}
		sqlWsResult.setRequestStatus(rqSt);
		return sqlWsResult;
	}

	public SQLiteWSResult SendOfflineHTTP(List<DataParameter> parameters,
			String url) {

		SQLiteWSResult sqlWsResult = new SQLiteWSResult();
	
		try {

			
			Document doc = tryRequest(parameters, url);
			sqlWsResult.setClientClientExcCode(requestResult.toString());
			if (requestResult == WSRequestFlags.RESULT_OK) {
				
				NodeList nList2 = doc.getElementsByTagName("RequestStatus");
				for (int temp = 0; temp < nList2.getLength(); temp++) {

					Node nNode = nList2.item(temp);
					if (nNode.getNodeType() == Node.ELEMENT_NODE) {

						Element eElement = (Element) nNode;

						rqSt.setRequestStatusCode((getTagValue(
								"RequestStatusCode", eElement)));

					}
				}
				
				sqlWsResult.setRequestStatus(rqSt);
				
				
				NodeList nList = doc.getElementsByTagName("OfflineData");
				Node nNode = nList.item(0);
				Element eElement = (Element) nNode;

				// strDefinition = getTagValue("GetOfflineData", eElement);
				sqlWsResult.setResult(getTagValue("GetOfflineData", eElement));
			}
			else
			{
				sqlWsResult.setRequestStatus(rqSt);
				sqlWsResult.setResult("-1");
			}
			// strDefinition = strDefinition.toString();

		} catch (Exception e) {
			GlobalApp.WriteLogException(e);
			requestResult = WSRequestFlags.RESULT_ERROR;
			sqlWsResult.setClientClientExcCode(WSRequestFlags.RESULT_ERROR
					.toString());
			sqlWsResult.setRequestStatus(rqSt);
			sqlWsResult.setResult("-1");

		}
		sqlWsResult.setRequestStatus(rqSt);
		return sqlWsResult;
		// ---return the definitions of the word---

	}

}
